#!/usr/bin/env bash

# Put jars not available in maven repo inside local repository
mvn install:install-file -Dfile=./lib/og-analytics-tst-2.1.0.jar -DgroupId=og-analytics-test -DartifactId=og-analytics-test -Dversion=2.1.0 -Dpackaging=jar -DgeneratePom=true