import java.util.Arrays;
import java.util.Map;

import org.threeten.bp.Period;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.sungard.api.ServiceApi;
import com.gammatrace.sungard.api.ServiceApiImpl;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeIR;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swaption.SwaptionPhysicalFixedIborDefinition;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Coupon;
import com.opengamma.analytics.financial.interestrate.swap.derivative.SwapFixedCoupon;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.analytics.financial.interestrate.swaption.provider.SwaptionPhysicalFixedIborBlackMethod;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteDiscountingCalculator;
import com.opengamma.analytics.financial.provider.description.interestrate.BlackSwaptionFlatProvider;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderInterface;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.analytics.math.interpolation.CombinedInterpolatorExtrapolatorFactory;
import com.opengamma.analytics.math.interpolation.GridInterpolator2D;
import com.opengamma.analytics.math.interpolation.Interpolator1D;
import com.opengamma.analytics.math.interpolation.Interpolator1DFactory;
import com.opengamma.analytics.math.surface.DoublesSurface;
import com.opengamma.analytics.math.surface.InterpolatedDoublesSurface;
import com.opengamma.analytics.math.surface.NodalDoublesSurface;
import com.opengamma.analytics.math.surface.Surface;
import com.opengamma.analytics.util.time.TimeCalculator;
import com.opengamma.gammatrace.exceptions.UnsupportedTradeException;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.marketconstruction.quote.SABRNodeQuote;
import com.opengamma.gammatrace.marketconstruction.sabrconfiguration.SABRConfiguration;
import com.opengamma.gammatrace.marketconstruction.sabrconfiguration.USDSABRConfiguration;
import com.opengamma.gammatrace.rates.calculator.BlackImpliedVolatilityCalculator;
import com.opengamma.gammatrace.rates.instrument.FixingBuilder;
import com.opengamma.util.ArgumentChecker;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.time.DateUtils;
import com.opengamma.analytics.financial.model.option.parameters.BlackFlatSwaptionParameters;
import com.opengamma.analytics.financial.model.option.pricing.analytic.formula.EuropeanVanillaOption;
import com.opengamma.analytics.financial.model.volatility.smile.fitting.SABRATMVolatilityCalculator;
import com.opengamma.analytics.financial.model.volatility.smile.fitting.SABRModelFitter;
import com.opengamma.analytics.financial.model.volatility.smile.fitting.SmileModelFitter;
import com.opengamma.analytics.financial.model.volatility.smile.function.SABRFormulaData;
import com.opengamma.analytics.financial.model.volatility.smile.function.SABRHaganVolatilityFunction;
import com.opengamma.analytics.financial.model.volatility.smile.function.VolatilityFunctionProvider;

public class SABRCalibrationTemp {
	private static ZonedDateTime executionTimestamp = DateUtils.getUTCDate(2015, 2, 11, 14, 0);
	private static ZonedDateTime tradeDate = executionTimestamp.toLocalDate().atStartOfDay(ZoneOffset.UTC);
	private static ZonedDateTime effectiveDate = DateUtils.getUTCDate(2015, 2, 13, 0, 0);
	final static ParSpreadMarketQuoteDiscountingCalculator PSMQC = ParSpreadMarketQuoteDiscountingCalculator.getInstance();
	private static ServiceApi serviceApi = ServiceApiImpl.getInstance();
	private static VolatilityFunctionProvider<SABRFormulaData> SABR = new SABRHaganVolatilityFunction();
	
	
	public static void main (String[] args) {
		CurveMaker cm = null;
		try {
			cm = new CurveMaker(Currency.USD, executionTimestamp);
		} catch (UnsupportedTradeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MulticurveProviderDiscount curve = cm.makeCurve().getFirst();
		SABRConfiguration config = new USDSABRConfiguration();
		GeneratorSwapFixedIbor generator = config.getUnderlyingGenerator();
		SABRNodeQuote[] quotes = config.getVolQuotes();
		
		SABRFormulaData assumption = new SABRFormulaData(0.1, 0.3, 0.0, 0.2);
		
		Period[] expiryPeriod = SABRNodeQuote.getExpiriesFromArray(quotes);
		Period[] underlyingTenor = SABRNodeQuote.getUnderlyingTenorFromArray(quotes);
		double[] expiryPeriodConverted = new double[expiryPeriod.length];
		double[] underlyingTenorConverted = new double[underlyingTenor.length];
		// convert to double[]
		for (int i = 0; i < expiryPeriod.length; i++) {
			expiryPeriodConverted[i] = TimeCalculator.getTimeBetween(tradeDate, ScheduleCalculator.getAdjustedDate(tradeDate, expiryPeriod[i], generator.getBusinessDayConvention(), generator.getCalendar()));
		}
		for (int i = 0; i < underlyingTenor.length; i++) {
			// this is not really how it is.. tenor is not calculated from settlementDate, but we just want a double interval, so doesn't really matter
			underlyingTenorConverted[i] = TimeCalculator.getTimeBetween(tradeDate, ScheduleCalculator.getAdjustedDate(tradeDate, underlyingTenor[i], generator.getBusinessDayConvention(), generator.getCalendar())); 
		}
		
		Map<String, Double> requestedValues = serviceApi.getQuotes(Arrays.asList(SABRNodeQuote.getStraddleATMPremiumTickersFromArray(quotes)), executionTimestamp.toEpochSecond());
		SwaptionPhysicalFixedIbor[][] swaptionStraddle = new SwaptionPhysicalFixedIbor[quotes.length][2];
		
		double[] impliedVols = new double[quotes.length];
		double[] forwardRate = new double[quotes.length];
		SABRFormulaData[] data = new SABRFormulaData[quotes.length]; 
		
		for (int i = 0; i < quotes.length; i++) {
			GeneratorAttributeIR tenor = new GeneratorAttributeIR(quotes[i].getExpiry(), quotes[i].getUnderlyingTenor());		// the spotLag between expiry and underlying effective date is put in the generation part below
			SwapFixedIborDefinition tempDef = generator.generateInstrument(effectiveDate, 0, 1, tenor);				// make the definition with zero rate
			SwapFixedCoupon<Coupon> swap = tempDef.toDerivative(tradeDate, FixingBuilder.makeFixingTimeSeries(tempDef, effectiveDate));
			forwardRate[i] = swap.accept(PSMQC, curve);
			
			SwapFixedIborDefinition swapDefinitionPayer = generateSwapInstrument(effectiveDate, forwardRate[i], 1, tenor, generator, true);					// payer swap with rate = forward
			SwapFixedIborDefinition swapDefinitionReceiver = generateSwapInstrument(effectiveDate, forwardRate[i], 1, tenor, generator, false);				// receiver swap with rate = forward
			
			ZonedDateTime expiryDate = ScheduleCalculator.getAdjustedDate(tradeDate, quotes[i].getExpiry(), generator.getBusinessDayConvention(), generator.getCalendar());
			
			SwaptionPhysicalFixedIborDefinition swaptionDefinitionPayer = SwaptionPhysicalFixedIborDefinition.from(expiryDate, swapDefinitionPayer, true);
			SwaptionPhysicalFixedIborDefinition swaptionDefinitionReceiver = SwaptionPhysicalFixedIborDefinition.from(expiryDate, swapDefinitionReceiver, true);
			swaptionStraddle[i] = new SwaptionPhysicalFixedIbor[] {swaptionDefinitionPayer.toDerivative(tradeDate), swaptionDefinitionReceiver.toDerivative(tradeDate)};
			impliedVols[i] = BlackImpliedVolatilityCalculator.getImpliedVolatility(swaptionStraddle[i], requestedValues.get(quotes[i].getStraddleATMPremiumTicker())/10000, curve);
		
			SABRATMVolatilityCalculator alphaCalculator = new SABRATMVolatilityCalculator(SABR);
			double alpha = alphaCalculator.calculate(assumption, swaptionStraddle[i][0], forwardRate[i], impliedVols[i]);
			data[i] = assumption.withAlpha(alpha);
			System.out.println(alpha);
		
		}

		final Interpolator1D linearFlat = CombinedInterpolatorExtrapolatorFactory.getInterpolator(Interpolator1DFactory.LINEAR, Interpolator1DFactory.FLAT_EXTRAPOLATOR, Interpolator1DFactory.FLAT_EXTRAPOLATOR);
		final GridInterpolator2D interpolator2D = new GridInterpolator2D(linearFlat, linearFlat);
		DoublesSurface info = new InterpolatedDoublesSurface(expiryPeriodConverted, underlyingTenorConverted, impliedVols, interpolator2D);
		BlackFlatSwaptionParameters blackParameters = new BlackFlatSwaptionParameters(info, generator);
		
		for (int i = 0; i < quotes.length; i++) {
			MultipleCurrencyAmount pv1 = SwaptionPhysicalFixedIborBlackMethod.getInstance().presentValue(swaptionStraddle[i][0], new BlackSwaptionFlatProvider(curve, blackParameters));
			MultipleCurrencyAmount pv2 = SwaptionPhysicalFixedIborBlackMethod.getInstance().presentValue(swaptionStraddle[i][1], new BlackSwaptionFlatProvider(curve, blackParameters));
		    double volCalculated = SABR.getVolatilityFunction(swaptionStraddle[i][0], forwardRate[i]).evaluate(data[i]);
			double pv = pv1.plus(pv2).getAmount(Currency.USD);
			double val = requestedValues.get(quotes[i].getStraddleATMPremiumTicker())/10000;
			double diff = pv-val;
			System.out.println(quotes[i].getExpiry().toString()+"  "+quotes[i].getUnderlyingTenor().toString()+"  "+pv+"  "+val+"  "+diff+"  "+impliedVols[i]+"  "+volCalculated);
		}
	}
	
	// in test check that black model reprices the straddle premia using the impliedVols. 
	
	
	public static SwapFixedIborDefinition generateSwapInstrument(ZonedDateTime date, double rate, double notional, GeneratorAttributeIR attribute, GeneratorSwapFixedIbor generator, boolean isFixedPayer) {
		final ZonedDateTime spot = ScheduleCalculator.getAdjustedDate(date, generator.getSpotLag(), generator.getCalendar());
		final ZonedDateTime startDate = ScheduleCalculator.getAdjustedDate(spot, attribute.getStartPeriod(), generator.getIborIndex(), generator.getCalendar());
		return SwapFixedIborDefinition.from(startDate, attribute.getEndPeriod(), generator, notional, rate, isFixedPayer);
	}
	
}
