import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.curve.interestrate.generator.GeneratorCurveYieldInterpolated;
import com.opengamma.analytics.financial.curve.interestrate.generator.GeneratorYDCurve;
import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.instrument.InstrumentDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponFixedDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponIborDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.cash.CashDefinition;
import com.opengamma.analytics.financial.instrument.fra.ForwardRateAgreementDefinition;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttribute;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeIR;
import com.opengamma.analytics.financial.instrument.index.GeneratorDepositIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorDepositON;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIborMaster;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedONMaster;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborON;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.analytics.financial.instrument.payment.CouponFixedDefinition;
import com.opengamma.analytics.financial.instrument.payment.CouponIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedONDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapIborIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapIborONDefinition;
import com.opengamma.analytics.financial.interestrate.InstrumentDerivative;
import com.opengamma.analytics.financial.interestrate.InstrumentDerivativeVisitor;
import com.opengamma.analytics.financial.interestrate.YieldCurveBundle;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Coupon;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.interestrate.swap.derivative.SwapFixedCoupon;
import com.opengamma.analytics.financial.model.interestrate.curve.YieldAndDiscountCurve;
import com.opengamma.analytics.financial.model.interestrate.curve.YieldCurve;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueMarketQuoteSensitivityCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueMarketQuoteSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.generic.LastTimeCalculator;
import com.opengamma.analytics.financial.provider.calculator.generic.MarketQuoteSensitivityBlockCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.curve.MultiCurveBundle;
import com.opengamma.analytics.financial.provider.curve.SingleCurveBundle;
import com.opengamma.analytics.financial.provider.curve.multicurve.MulticurveDiscountBuildingRepository;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderInterface;
import com.opengamma.analytics.financial.provider.sensitivity.multicurve.MulticurveSensitivity;
import com.opengamma.analytics.financial.provider.sensitivity.multicurve.MultipleCurrencyParameterSensitivity;
import com.opengamma.analytics.financial.provider.sensitivity.parameter.ParameterSensitivityParameterCalculator;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.analytics.math.interpolation.CombinedInterpolatorExtrapolatorFactory;
import com.opengamma.analytics.math.interpolation.Interpolator1D;
import com.opengamma.analytics.math.interpolation.Interpolator1DFactory;
import com.opengamma.analytics.math.matrix.DoubleMatrix1D;
import com.opengamma.analytics.util.time.TimeCalculator;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.timeseries.precise.zdt.ImmutableZonedDateTimeDoubleTimeSeries;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;

public class USDSingleCCY {

	private static final Interpolator1D INTERPOLATOR_LINEAR = CombinedInterpolatorExtrapolatorFactory.getInterpolator(Interpolator1DFactory.LINEAR, Interpolator1DFactory.FLAT_EXTRAPOLATOR,
	    Interpolator1DFactory.FLAT_EXTRAPOLATOR);

	private static final LastTimeCalculator MATURITY_CALCULATOR = LastTimeCalculator.getInstance();
	private static final double TOLERANCE_ROOT = 1.0E-10;
	private static final int STEP_MAX = 100;

	private static final Calendar NYC = new MondayToFridayCalendar("NYC");
	private static final Currency USD = Currency.USD;
	private static final FXMatrix FX_MATRIX = new FXMatrix(USD);

	private static final double NOTIONAL = 1.0;

	// Generators for 3m Libor instruments
	private static final GeneratorSwapFixedIborMaster GENERATOR_SWAP_MASTER = GeneratorSwapFixedIborMaster.getInstance();
	private static final GeneratorSwapFixedIbor USD6MLIBOR3M = GENERATOR_SWAP_MASTER.getGenerator("USD6MLIBOR3M", NYC);
	private static final IborIndex USDLIBOR3M = USD6MLIBOR3M.getIborIndex();
	private static final GeneratorDepositIbor GENERATOR_USDLIBOR3M = new GeneratorDepositIbor("GENERATOR_USDLIBOR3M", USDLIBOR3M, NYC);
	
	// Generators for OIS instruments
	private static final GeneratorSwapFixedON GENERATOR_OIS_USD = GeneratorSwapFixedONMaster.getInstance().getGenerator("USD1YFEDFUND", NYC);
	private static final IndexON INDEX_ON_USD = GENERATOR_OIS_USD.getIndex();
	private static final GeneratorDepositON GENERATOR_DEPOSIT_ON_USD = new GeneratorDepositON("USD Deposit ON", USD, NYC, INDEX_ON_USD.getDayCount());
	private static final GeneratorSwapIborON GENERATOR_3MON_USD = new GeneratorSwapIborON("USD3MON", USDLIBOR3M, INDEX_ON_USD, USDLIBOR3M.getBusinessDayConvention(), USDLIBOR3M.isEndOfMonth(), USDLIBOR3M.getSpotLag(), NYC, NYC);
	
	// Generators for 3m / 6m Libor basis instruments
	private static final GeneratorSwapFixedIbor USD6MLIBOR6M = GENERATOR_SWAP_MASTER.getGenerator("USD6MLIBOR6M", NYC);
	private static final IborIndex USDLIBOR6M = USD6MLIBOR6M.getIborIndex();
	private static final GeneratorSwapIborIbor GENERATOR_3M6M_USD = new GeneratorSwapIborIbor("USD3M6M", USDLIBOR3M, USDLIBOR6M,  NYC, NYC);
	// Generators for 3m / 1m Libor basis instruments
	private static final IborIndex USDLIBOR1M = new IborIndex(Currency.USD, Period.ofMonths(1), 2, DayCountFactory.INSTANCE.getDayCount("Actual/360"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "USDLIBOR1M");
	private static final GeneratorSwapIborIbor GENERATOR_3M1M_USD = new GeneratorSwapIborIbor("USD3M1M", USDLIBOR3M, USDLIBOR1M,  NYC, NYC);
	
	private static final ZonedDateTime NOW = DateUtils.getUTCDate(2014, 8, 15);

	private static final String CURVE_NAME_DSC_USD = "USD Dsc";
	private static final String CURVE_NAME_FWD3_USD = "USD Fwd 3M";
	private static final String CURVE_NAME_FWD6_USD = "Basis 3M6M";
	private static final String CURVE_NAME_FWD1_USD = "Basis 3M1M";
	
	/* Market values for the dsc USD curve */
	private static final double[] DSC_USD_MARKET_QUOTES = new double[] {0.0400, -0.0099070, -0.0089573, -0.0092961, -0.0096535, -0.0095962, -0.0096197, -0.0096779, -0.0097301, -0.0096257 };
	/* Generators for the dsc USD curve */
	private static final GeneratorInstrument<? extends GeneratorAttribute>[] DSC_USD_GENERATORS = new GeneratorInstrument<?>[] {GENERATOR_DEPOSIT_ON_USD, GENERATOR_3MON_USD, GENERATOR_3MON_USD,
	  GENERATOR_3MON_USD, GENERATOR_3MON_USD, GENERATOR_3MON_USD, GENERATOR_3MON_USD, GENERATOR_3MON_USD, GENERATOR_3MON_USD, GENERATOR_3MON_USD, GENERATOR_3MON_USD, GENERATOR_3MON_USD };
	/* Tenors for the dsc USD curve */
	private static final Period[] DSC_USD_TENOR = new Period[] {Period.ofDays(0), Period.ofMonths(3),
		Period.ofMonths(6), Period.ofMonths(9), Period.ofYears(1),
		Period.ofYears(2), Period.ofYears(3), Period.ofYears(4), Period.ofYears(5), Period.ofYears(10) };
	private static final GeneratorAttributeIR[] DSC_USD_ATTR = new GeneratorAttributeIR[DSC_USD_TENOR.length];
	static {
		for (int loopins = 0; loopins < DSC_USD_TENOR.length; loopins++) {
			DSC_USD_ATTR[loopins] = new GeneratorAttributeIR(DSC_USD_TENOR[loopins]);
	    }
	}	

	
	/* Market values for the Fwd 3M USD curve */
	private static final double[] FWD3_USD_MARKET_QUOTES = new double[] {0.0500, 0.0500, 0.0500, 0.0500, 0.0500, 0.0501, 0.0500, 0.0500 };
	/* Generators for the Fwd 3M USD curve */
	private static final GeneratorInstrument<? extends GeneratorAttribute>[] FWD3_USD_GENERATORS = new GeneratorInstrument<?>[] {GENERATOR_USDLIBOR3M, USD6MLIBOR3M, USD6MLIBOR3M, USD6MLIBOR3M,
    USD6MLIBOR3M, USD6MLIBOR3M, USD6MLIBOR3M, USD6MLIBOR3M };
	/* Tenors for the Fwd 3M USD curve */
	private static final Period[] FWD3_USD_TENOR = new Period[] {Period.ofMonths(0), Period.ofMonths(6), Period.ofYears(1), Period.ofYears(2),
		Period.ofYears(3), Period.ofYears(5), Period.ofYears(7), Period.ofYears(10) };
	private static final GeneratorAttributeIR[] FWD3_USD_ATTR = new GeneratorAttributeIR[FWD3_USD_TENOR.length];
	static {
		for (int loopins = 0; loopins < FWD3_USD_TENOR.length; loopins++) {
			FWD3_USD_ATTR[loopins] = new GeneratorAttributeIR(FWD3_USD_TENOR[loopins]);
		}
	}
	
	/* Market values for the Fwd 6M USD curve*/
	private static final double[] FWD3M6M_USD_MARKET_QUOTES = new double[] {0.0020, 0.0020, 0.0020, 0.00200, 0.00190, 0.00200, 0.00200 };
	/* Generators for the Fwd 3M USD curve */
	private static final GeneratorInstrument<? extends GeneratorAttribute>[] FWD3M6M_USD_GENERATORS = new GeneratorInstrument<?>[] {GENERATOR_3M6M_USD, GENERATOR_3M6M_USD, GENERATOR_3M6M_USD,
		GENERATOR_3M6M_USD, GENERATOR_3M6M_USD, GENERATOR_3M6M_USD, GENERATOR_3M6M_USD };
	/* Tenors for the Fwd 3M USD curve */
	private static final Period[] FWD3M6M_USD_TENOR = new Period[] {Period.ofMonths(6), Period.ofYears(1), Period.ofYears(2),
		Period.ofYears(3), Period.ofYears(5), Period.ofYears(7), Period.ofYears(10) };
	private static final GeneratorAttributeIR[] FWD3M6M_USD_ATTR = new GeneratorAttributeIR[FWD3M6M_USD_TENOR.length];
	static {
		for (int loopins = 0; loopins < FWD3M6M_USD_TENOR.length; loopins++) {
			FWD3M6M_USD_ATTR[loopins] = new GeneratorAttributeIR(FWD3M6M_USD_TENOR[loopins]);
		}
	}
	
	/* Market values for the Fwd 1M USD curve*/
	private static final double[] FWD3M1M_USD_MARKET_QUOTES = new double[] {-0.0030, -0.0030, -0.0030, -0.00300, -0.0030, -0.0030, -0.0030 };
	/* Generators for the Fwd 3M USD curve */
	private static final GeneratorInstrument<? extends GeneratorAttribute>[] FWD3M1M_USD_GENERATORS = new GeneratorInstrument<?>[] {GENERATOR_3M1M_USD, GENERATOR_3M1M_USD, GENERATOR_3M1M_USD,
		GENERATOR_3M1M_USD, GENERATOR_3M1M_USD, GENERATOR_3M1M_USD, GENERATOR_3M1M_USD };
	/* Tenors for the Fwd 3M USD curve */
	private static final Period[] FWD3M1M_USD_TENOR = new Period[] {Period.ofMonths(6), Period.ofYears(1), Period.ofYears(2),
		Period.ofYears(3), Period.ofYears(5), Period.ofYears(7), Period.ofYears(10) };
	private static final GeneratorAttributeIR[] FWD3M1M_USD_ATTR = new GeneratorAttributeIR[FWD3M1M_USD_TENOR.length];
	static {
		for (int loopins = 0; loopins < FWD3M1M_USD_TENOR.length; loopins++) {
			FWD3M1M_USD_ATTR[loopins] = new GeneratorAttributeIR(FWD3M1M_USD_TENOR[loopins]);
		}
	}
	
	/* Standard USD discounting curve instrument definitions */
	private static final InstrumentDefinition<?>[] DEFINITIONS_DSC_USD;
	/* Standard USD Forward 3M curve instrument definitions */
	private static final InstrumentDefinition<?>[] DEFINITIONS_FWD3_USD;
	/* Standard USD Forward 6M curve instrument definitions */
	private static final InstrumentDefinition<?>[] DEFINITIONS_FWD6_USD;
	/* Standard USD Forward 1M curve instrument definitions */
	private static final InstrumentDefinition<?>[] DEFINITIONS_FWD1_USD;

	/** Units of curves */
    private static final int[] NB_UNITS = new int[] {1 };
    private static final int NB_BLOCKS = NB_UNITS.length;
    private static final InstrumentDefinition<?>[][][][] DEFINITIONS_UNITS = new InstrumentDefinition<?>[NB_BLOCKS][][][];
    private static final GeneratorYDCurve[][][] GENERATORS_UNITS = new GeneratorYDCurve[NB_BLOCKS][][];
    private static final String[][][] NAMES_UNITS = new String[NB_BLOCKS][][];
    
    private static final MulticurveProviderDiscount KNOWN_DATA = new MulticurveProviderDiscount(FX_MATRIX);
    
    private static final LinkedHashMap<String, Currency> DSC_MAP = new LinkedHashMap<>();
    private static final LinkedHashMap<String, IndexON[]> FWD_ON_MAP = new LinkedHashMap<>();
    private static final LinkedHashMap<String, IborIndex[]> FWD_IBOR_MAP = new LinkedHashMap<>();

	static {
	DEFINITIONS_DSC_USD = getDefinitions(DSC_USD_MARKET_QUOTES, DSC_USD_GENERATORS, DSC_USD_ATTR);
	DEFINITIONS_FWD3_USD = getDefinitions(FWD3_USD_MARKET_QUOTES, FWD3_USD_GENERATORS, FWD3_USD_ATTR);
	DEFINITIONS_FWD6_USD = getDefinitions(FWD3M6M_USD_MARKET_QUOTES, FWD3M6M_USD_GENERATORS, FWD3M6M_USD_ATTR);
	DEFINITIONS_FWD1_USD = getDefinitions(FWD3M1M_USD_MARKET_QUOTES, FWD3M1M_USD_GENERATORS, FWD3M1M_USD_ATTR);
	
	for (int loopblock = 0; loopblock < NB_BLOCKS; loopblock++) {
		DEFINITIONS_UNITS[loopblock] = new InstrumentDefinition<?>[NB_UNITS[loopblock]][][];
	    GENERATORS_UNITS[loopblock] = new GeneratorYDCurve[NB_UNITS[loopblock]][];
	    NAMES_UNITS[loopblock] = new String[NB_UNITS[loopblock]][];
	}
	
	DEFINITIONS_UNITS[0][0] = new InstrumentDefinition<?>[][] {DEFINITIONS_FWD3_USD , DEFINITIONS_DSC_USD, DEFINITIONS_FWD6_USD ,DEFINITIONS_FWD1_USD};
	
	final GeneratorYDCurve genIntLin = new GeneratorCurveYieldInterpolated(MATURITY_CALCULATOR, INTERPOLATOR_LINEAR);
	
	GENERATORS_UNITS[0][0] = new GeneratorYDCurve[] {genIntLin , genIntLin , genIntLin, genIntLin};
	

	NAMES_UNITS[0][0] = new String[] {CURVE_NAME_FWD3_USD ,CURVE_NAME_DSC_USD , CURVE_NAME_FWD6_USD ,CURVE_NAME_FWD1_USD};
	
	DSC_MAP.put(CURVE_NAME_DSC_USD, USD);
	FWD_ON_MAP.put(CURVE_NAME_DSC_USD, new IndexON[] {INDEX_ON_USD });
	FWD_IBOR_MAP.put(CURVE_NAME_FWD3_USD, new IborIndex[] {USDLIBOR3M });
	FWD_IBOR_MAP.put(CURVE_NAME_FWD6_USD, new IborIndex[] {USDLIBOR6M });
	FWD_IBOR_MAP.put(CURVE_NAME_FWD1_USD, new IborIndex[] {USDLIBOR1M });
	}

	@SuppressWarnings({"rawtypes", "unchecked" })
	public static InstrumentDefinition<?>[] getDefinitions(final double[] marketQuotes, final GeneratorInstrument[] generators, final GeneratorAttribute[] attribute) {
		final InstrumentDefinition<?>[] definitions = new InstrumentDefinition<?>[marketQuotes.length];
		for (int loopmv = 0; loopmv < marketQuotes.length; loopmv++) {
			definitions[loopmv] = generators[loopmv].generateInstrument(NOW, marketQuotes[loopmv], NOTIONAL, attribute[loopmv]);
		}
		return definitions;
	}

	// Calculator
	private static final PresentValueDiscountingCalculator PVC = PresentValueDiscountingCalculator.getInstance();
	private static final ParSpreadMarketQuoteDiscountingCalculator PSMQC = ParSpreadMarketQuoteDiscountingCalculator.getInstance();
	private static final ParSpreadMarketQuoteCurveSensitivityDiscountingCalculator PSMQCSC = ParSpreadMarketQuoteCurveSensitivityDiscountingCalculator.getInstance();

	private static final MulticurveDiscountBuildingRepository CURVE_BUILDING_REPOSITORY = new MulticurveDiscountBuildingRepository(TOLERANCE_ROOT, TOLERANCE_ROOT, STEP_MAX);

	private static final double TOLERANCE_CAL = 1.0E-9;	

	@SuppressWarnings("unchecked")
	private static Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> makeCurvesFromDefinitions(final InstrumentDefinition<?>[][][] definitions, final GeneratorYDCurve[][] curveGenerators,
	    final String[][] curveNames, final MulticurveProviderDiscount knownData, final InstrumentDerivativeVisitor<MulticurveProviderInterface, Double> calculator,
	    final InstrumentDerivativeVisitor<MulticurveProviderInterface, MulticurveSensitivity> sensitivityCalculator, final boolean withToday) {
	    final int nUnits = definitions.length;
	    final MultiCurveBundle<GeneratorYDCurve>[] curveBundles = new MultiCurveBundle[nUnits];
	    for (int i = 0; i < nUnits; i++) {
	    	final int nCurves = definitions[i].length;
	    	final SingleCurveBundle<GeneratorYDCurve>[] singleCurves = new SingleCurveBundle[nCurves];
	    	for (int j = 0; j < nCurves; j++) {
	    		final int nInstruments = definitions[i][j].length;
	    		final InstrumentDerivative[] derivatives = new InstrumentDerivative[nInstruments];
	    		final double[] initialGuess = new double[nInstruments];
	    			for (int k = 0; k < nInstruments; k++) {
	    				derivatives[k] = definitions[i][j][k].toDerivative(NOW); 
	    				initialGuess[k] = initialGuess(definitions[i][j][k]);
	    			}
	    		final GeneratorYDCurve generator = curveGenerators[i][j].finalGenerator(derivatives);
	    		singleCurves[j] = new SingleCurveBundle<>(curveNames[i][j], derivatives, initialGuess, generator);
	    	}
	    	curveBundles[i] = new MultiCurveBundle<>(singleCurves);
	    }
	    return CURVE_BUILDING_REPOSITORY.makeCurvesFromDerivatives(curveBundles, knownData, DSC_MAP, FWD_IBOR_MAP, FWD_ON_MAP, calculator, sensitivityCalculator);
	}
	
	private static double initialGuess(final InstrumentDefinition<?> instrument) {
	    if (instrument instanceof SwapFixedONDefinition) {
	    	return ((SwapFixedONDefinition) instrument).getFixedLeg().getNthPayment(0).getRate();
	    }
	    if (instrument instanceof SwapFixedIborDefinition) {
	    	return ((SwapFixedIborDefinition) instrument).getFixedLeg().getNthPayment(0).getRate();
	    }
	    if (instrument instanceof ForwardRateAgreementDefinition) {
	    	return ((ForwardRateAgreementDefinition) instrument).getRate();
	    }
	    if (instrument instanceof SwapIborIborDefinition) {
	    	return 0.002;
	    }
	    if (instrument instanceof CashDefinition) {
	    	return ((CashDefinition) instrument).getRate();
	    }
	    	return 0.01;
	  }
	
	public static void main(String[] args) {
		/******************
		 * Test to print out discount factors and forward 3m Libor rates
		 * Note this is not generating dates properly due to bumps!
		 ******************/
		DecimalFormat dfFormat = new DecimalFormat("0.00000");
		// initialise startDate to t+2, and declare endDate
		ZonedDateTime startDate = ScheduleCalculator.getAdjustedDate(NOW, USDLIBOR3M.getSpotLag(), NYC);
		ZonedDateTime endDate3M, endDate6M, endDate1M;
		// getForwardRate takes time periods as double, so need to declare variables that will be time period now and date 
		double startTime;
		double endTime3M, endTime6M, endTime1M;

		final int numDates = 40;		
		int loopblock = 0;
		
		Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> pair = makeCurvesFromDefinitions(DEFINITIONS_UNITS[loopblock], GENERATORS_UNITS[loopblock], NAMES_UNITS[loopblock], KNOWN_DATA, PSMQC, PSMQCSC, false);
		MulticurveProviderDiscount curve = pair.getFirst();
		
		for (int i = 0; i < numDates; i++) {
			startTime = TimeCalculator.getTimeBetween(NOW, startDate);
			// increment endDate by 3 months (corresponding to tenor of ibor index)... this is problematic due to bumps!
			endDate3M = ScheduleCalculator.getAdjustedDate(startDate, USDLIBOR3M, NYC);
			endDate6M = ScheduleCalculator.getAdjustedDate(startDate, USDLIBOR6M, NYC);
			endDate1M = ScheduleCalculator.getAdjustedDate(startDate, USDLIBOR1M, NYC);
			endTime3M = TimeCalculator.getTimeBetween(NOW, endDate3M);
			endTime6M = TimeCalculator.getTimeBetween(NOW, endDate6M);
			endTime1M = TimeCalculator.getTimeBetween(NOW, endDate1M);
			
			final double accrualFactor3M = USDLIBOR3M.getDayCount().getDayCountFraction(startDate, endDate3M);
			final double accrualFactor6M = USDLIBOR6M.getDayCount().getDayCountFraction(startDate, endDate6M);
			final double accrualFactor1M = USDLIBOR1M.getDayCount().getDayCountFraction(startDate, endDate1M);
			final double discountFactor = curve.getDiscountFactor(USD, startTime);
			double fwd3M = curve.getForwardRate(USDLIBOR3M, startTime, endTime3M, accrualFactor3M);
			double fwd6M = curve.getForwardRate(USDLIBOR6M, startTime, endTime6M, accrualFactor6M);
			double fwd1M = curve.getForwardRate(USDLIBOR1M, startTime, endTime1M, accrualFactor1M);			
			
			System.out.println(startDate+"DF: "+dfFormat.format(discountFactor)+" 3mL: "+dfFormat.format(fwd3M)+" 6mL: "+dfFormat.format(fwd6M)
					+" 1mL: "+dfFormat.format(fwd1M));
			// increment startDate of next period to end of this period
			startDate = endDate3M;
		}
		System.out.println("====================================================");
		
		// create a Ibor-Ibor swap definition
		final Period SWAP_TENOR = Period.ofYears(5);
		final ZonedDateTime START = DateUtils.getUTCDate(2014, 8, 15);
		final ZonedDateTime SETTLEMENT_DATE = ScheduleCalculator.getAdjustedDate(START, GENERATOR_3M6M_USD.getSpotLag(), NYC);
		double NOTIONAL = 100000000; //100m
		final double SPREAD = 0.002; //NOTE: spread goes on first leg by default using the from method
		// true means is payer of the first leg
		final SwapIborIborDefinition SWAP_IBOR_IBOR_DEFINITION = SwapIborIborDefinition.from(SETTLEMENT_DATE, SWAP_TENOR, GENERATOR_3M6M_USD, NOTIONAL, SPREAD, true);
		
		final double SPREAD_1 = -0.003;
		final SwapIborIborDefinition SWAP_IBOR_IBOR_DEFINITION_1 = SwapIborIborDefinition.from(SETTLEMENT_DATE, SWAP_TENOR, GENERATOR_3M1M_USD, NOTIONAL, SPREAD_1, true);
		
		// 3mL and 6mL swaps 
		final double RATE_FIXED = 0.03;
		final SwapFixedIborDefinition SWAP3M_FIXED_IBOR_DEFINITION = SwapFixedIborDefinition.from(SETTLEMENT_DATE, SWAP_TENOR, USD6MLIBOR3M, NOTIONAL, RATE_FIXED, true);
		final  SwapFixedIborDefinition SWAP6M_FIXED_IBOR_DEFINITION = SwapFixedIborDefinition.from(SETTLEMENT_DATE, SWAP_TENOR, USD6MLIBOR6M, NOTIONAL, RATE_FIXED, true); 
		
		//creating today's swap instrument from a swap definition
		final Swap<Coupon, Coupon> swap = SWAP_IBOR_IBOR_DEFINITION.toDerivative(NOW);
		final Swap<Coupon, Coupon> swap1 = SWAP_IBOR_IBOR_DEFINITION_1.toDerivative(NOW);
		final SwapFixedCoupon<Coupon> swap3M = SWAP3M_FIXED_IBOR_DEFINITION.toDerivative(NOW);
		final SwapFixedCoupon<Coupon> swap6M = SWAP6M_FIXED_IBOR_DEFINITION.toDerivative(NOW);
		
		final MultipleCurrencyAmount pv = swap.accept(PVC, curve);
		final MultipleCurrencyAmount pv1 = swap1.accept(PVC, curve);
		final MultipleCurrencyAmount pv2 = swap3M.accept(PVC, curve);
		final MultipleCurrencyAmount pv3 = swap6M.accept(PVC, curve);
		final double parSpread = swap.accept(PSMQC, curve);
		final double parSpread1 = swap1.accept(PSMQC, curve);
		System.out.println("PV = "+pv+" Fair rate = "+parSpread);
		System.out.println("PV = "+pv1+" Fair rate = "+parSpread1);
		System.out.println("PV = "+pv2);
		System.out.println("PV = "+pv3);
		System.out.println("====================================================");
		
		for (int i = 1; i < DSC_USD_TENOR.length; i++) {
			SwapFixedONDefinition SWAP_FIXED_OIS_DEFINITION = SwapFixedONDefinition.from(SETTLEMENT_DATE, DSC_USD_TENOR[i], NOTIONAL, GENERATOR_OIS_USD, 0.04, true);	
			SwapFixedCoupon<Coupon> oisSwap = SWAP_FIXED_OIS_DEFINITION.toDerivative(NOW);
			double oisRate = oisSwap.accept(PSMQC, curve);
			
			System.out.println(DSC_USD_TENOR[i]+"  "+ (0.04 + oisRate)*100+"%");
		}
		System.out.println("====================================================");
		final PresentValueCurveSensitivityDiscountingCalculator PVCSC = PresentValueCurveSensitivityDiscountingCalculator.getInstance();
		final ParameterSensitivityParameterCalculator<MulticurveProviderInterface> PSC = new ParameterSensitivityParameterCalculator<>(PVCSC);
		
		final MarketQuoteSensitivityBlockCalculator<MulticurveProviderInterface> MQSC = new MarketQuoteSensitivityBlockCalculator<>(PSC);

		//run it and save as a vector per curve, currency pair
		MultipleCurrencyParameterSensitivity[] delta = new MultipleCurrencyParameterSensitivity[1];
		
		//delta[0] = MQSC.fromInstrument(swap, curve, pair.getSecond());
		//delta[1] = MQSC.fromInstrument(swap1, curve, pair.getSecond());		
		delta[0] = MQSC.fromInstrument(swap3M, curve, pair.getSecond());
		//delta[3] = MQSC.fromInstrument(swap6M, curve, pair.getSecond());
		
		DecimalFormat deltaFormat = new DecimalFormat("0");
	    
	    //check the pairs
		for (int i = 0; i < delta.length; i++)
		{	
			Iterator<Pair<String, Currency>> itr = delta[i].getAllNamesCurrency().iterator();
			while(itr.hasNext())
			{
				Pair<String, Currency> riskPair = itr.next();
				System.out.println(riskPair.getFirst());      //+"  "+riskPair.getSecond().toString()
				DoubleMatrix1D vectorRisk = delta[i].getSensitivity(riskPair);	
				for (int j = 0; j < vectorRisk.getNumberOfElements(); j++)
				{
					if (riskPair.getFirst() == "USD Dsc") // this method is rubbish..must be a better way to get the curve nodes
					{
						System.out.println(DSC_USD_TENOR[j]+"   "+deltaFormat.format(vectorRisk.getEntry(j)/-1E4)); //this scaling is a guess!!!
					}
					else if(riskPair.getFirst() == "USD Fwd 3M")
					{
						System.out.println(FWD3_USD_TENOR[j]+"   "+deltaFormat.format(vectorRisk.getEntry(j)/-1E4));
					}
					else if(riskPair.getFirst() == "Basis 3M6M")
					{
						System.out.println(FWD3M6M_USD_TENOR[j]+"  "+deltaFormat.format(vectorRisk.getEntry(j)/-1E4));
					}
					else if(riskPair.getFirst() == "Basis 3M1M")
					{
						System.out.println(FWD3M1M_USD_TENOR[j]+"  "+deltaFormat.format(vectorRisk.getEntry(j)/-1E4));
					}
				}
				
			}
			System.out.println("====================================================");
			
		}
		
	}
}
