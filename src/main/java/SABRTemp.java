

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

import java.util.Arrays;
import java.util.BitSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import cern.jet.random.engine.MersenneTwister;

import com.opengamma.analytics.financial.model.option.pricing.analytic.formula.BlackFunctionData;
import com.opengamma.analytics.financial.model.option.pricing.analytic.formula.EuropeanVanillaOption;
import com.opengamma.analytics.financial.model.volatility.smile.fitting.EuropeanOptionMarketData;
import com.opengamma.analytics.financial.model.volatility.smile.fitting.SABRModelFitter;
import com.opengamma.analytics.financial.model.volatility.smile.fitting.SABRNonLinearLeastSquareFitter;
import com.opengamma.analytics.financial.model.volatility.smile.fitting.SmileModelFitter;
import com.opengamma.analytics.financial.model.volatility.smile.fitting.sabr.PiecewiseSABRFitterRootFinder;
import com.opengamma.analytics.financial.model.volatility.smile.function.SABRFormulaData;
import com.opengamma.analytics.financial.model.volatility.smile.function.SABRHaganVolatilityFunction;
import com.opengamma.analytics.financial.model.volatility.smile.function.VolatilityFunctionProvider;
import com.opengamma.analytics.math.matrix.DoubleMatrix1D;
import com.opengamma.analytics.math.statistics.distribution.NormalDistribution;
import com.opengamma.analytics.math.statistics.distribution.ProbabilityDistribution;
import com.opengamma.analytics.math.statistics.leastsquare.LeastSquareResultsWithTransform;
import com.opengamma.util.monitor.OperationTimer;

public class SABRTemp {
  
	private static double forward = 0.03;
	private static double expiry = 1.0;
	private static double[] strikes = {0.02, 0.025, 0.03, 0.035, 0.04};
	private static double alpha = 0.05;
	private static double beta = 0.5;
	private static double rho = -0.3;
	private static double nu = 0.2;
	private static final SABRFormulaData sabrData = new SABRFormulaData(alpha, beta, rho, nu);
	private static VolatilityFunctionProvider<SABRFormulaData> SABR = new SABRHaganVolatilityFunction();
	
	public static void main(String[] args) {
		double[] vols = new double[strikes.length];
		double[] errors = new double[strikes.length];
		Arrays.fill(errors, 0.0001);
		EuropeanOptionMarketData[] market = new EuropeanOptionMarketData[strikes.length];
		for (int i = 0; i < strikes.length; i++) {
			EuropeanVanillaOption option = new EuropeanVanillaOption(strikes[i], expiry, true);
		    vols[i] = SABR.getVolatilityFunction(option, forward).evaluate(sabrData);
		    System.out.println("vol["+i+"] = "+vols[i]);
		}
		SmileModelFitter<SABRFormulaData> FITTER = new SABRModelFitter(forward, strikes, expiry, vols, errors, SABR);
		
		final double[] start = new double[] {0.1, 0.7, 0.0, 0.3};
		final double[] startBeta = new double[] {0.1, 0.6, 0.0, 0.3};
		
	    final LeastSquareResultsWithTransform results = FITTER.solve(new DoubleMatrix1D(start));
	    final double[] res = results.getModelParameters().getData();
	    for (double d : res) {
	    	System.out.println(d);
	    }
	    
	    SABRFormulaData sabrDataFitted = new SABRFormulaData(res[0], res[1], res[2], res[3]);
	    for (int i = 0; i < strikes.length; i++) {
			EuropeanVanillaOption option = new EuropeanVanillaOption(strikes[i], expiry, true);
		    vols[i] = SABR.getVolatilityFunction(option, forward).evaluate(sabrData);
		    System.out.println("vol["+i+"] = "+vols[i]);
		    
		}
	    
	    final BitSet fixed = new BitSet();
	    fixed.set(1);
	    final LeastSquareResultsWithTransform resultsBeta = FITTER.solve(new DoubleMatrix1D(startBeta), fixed);
	    final double[] resBeta = resultsBeta.getModelParameters().getData();
	    for (double d : resBeta) {
	    	System.out.println(d);
	    }
	    
	    sabrDataFitted = new SABRFormulaData(resBeta[0], resBeta[1], resBeta[2], resBeta[3]);
	    for (int i = 0; i < strikes.length; i++) {
			EuropeanVanillaOption option = new EuropeanVanillaOption(strikes[i], expiry, true);
		    vols[i] = SABR.getVolatilityFunction(option, forward).evaluate(sabrData);
		    System.out.println("vol["+i+"] = "+vols[i]);
		    
		}
	    
		
	}
	
	
	/*

	protected int _hotspotWarmupCycles = 0;
  protected int _benchmarkCycles = 1;
  private static final double F = 0.03;
  private static final double T = 7.0;
  private static final double ALPHA = 0.05;
  private static final double BETA = 0.5;
  private static double RHO = -0.3;
  private static double NU = 0.2;

  private static final EuropeanOptionMarketData[] MARKETDATA;
  private static final EuropeanOptionMarketData[] NOISY_MARKETDATA;
  private static double[] STRIKES;
  private static double[] CLEAN_VOLS;
  private static double[] NOISY_VOLS;
  private static double[] ERRORS;
  private static final SABRFormulaData SABR_DATA = new SABRFormulaData(ALPHA, BETA, RHO, NU);
  
  private static ProbabilityDistribution<Double> RANDOM = new NormalDistribution(0, 1, new MersenneTwister(12));
  private static SmileModelFitter<SABRFormulaData> FITTER;
  private static SmileModelFitter<SABRFormulaData> NOISY_FITTER;

  static {
    STRIKES = new double[] {0.005, 0.01, 0.02, 0.03, 0.04, 0.05, 0.07};
    final int n = STRIKES.length;
    MARKETDATA = new EuropeanOptionMarketData[n];
    NOISY_MARKETDATA = new EuropeanOptionMarketData[n];
    CLEAN_VOLS = new double[n];
    NOISY_VOLS = new double[n];
    ERRORS = new double[n];
    Arrays.fill(ERRORS, 0.0001); //1bps error

    for (int i = 0; i < n; i++) {
      final EuropeanVanillaOption option = new EuropeanVanillaOption(STRIKES[i], T, true);
      CLEAN_VOLS[i] = SABR.getVolatilityFunction(option, F).evaluate(SABR_DATA);
      NOISY_VOLS[i] = CLEAN_VOLS[i] + RANDOM.nextRandom() * ERRORS[i];
    }

    FITTER = new SABRModelFitter(F, STRIKES, T, CLEAN_VOLS, ERRORS, SABR);
    NOISY_FITTER = new SABRModelFitter(F, STRIKES, T, NOISY_VOLS, ERRORS, SABR);
  }
  
  public static void maina(String[] args) {

    final double[] start = new double[] {0.1, 0.7, 0.0, 0.3};
    final LeastSquareResultsWithTransform results = FITTER.solve(new DoubleMatrix1D(start));
    final double[] res = results.getModelParameters().getData();
    final double eps = 1e-6;
    assertEquals(ALPHA, res[0], eps);
    assertEquals(BETA, res[1], eps);
    assertEquals(RHO, res[2], eps);
    assertEquals(NU, res[3], eps);
    assertEquals(0.0, results.getChiSq(), eps);
  }*/

}