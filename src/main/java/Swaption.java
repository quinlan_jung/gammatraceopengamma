import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIborMaster;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swaption.SwaptionCashFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swaption.SwaptionPhysicalFixedIborDefinition;
import com.opengamma.analytics.financial.interestrate.PresentValueSABRSensitivityDataBundle;
import com.opengamma.analytics.financial.interestrate.SABRSensitivityNodeCalculator;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionCashFixedIbor;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.analytics.financial.model.option.definition.SABRInterestRateParameters;
import com.opengamma.analytics.financial.model.volatility.smile.function.SABRFormulaData;
import com.opengamma.analytics.financial.model.volatility.smile.function.SABRHaganAlternativeVolatilityFunction;
import com.opengamma.analytics.financial.model.volatility.smile.function.SABRHaganVolatilityFunction;
import com.opengamma.analytics.financial.model.volatility.smile.function.VolatilityFunctionProvider;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParRateDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.generic.MarketQuoteSensitivityBlockCalculator;
import com.opengamma.analytics.financial.provider.calculator.sabrswaption.PresentValueCurveSensitivitySABRSwaptionCalculator;
import com.opengamma.analytics.financial.provider.calculator.sabrswaption.PresentValueCurveSensitivitySABRSwaptionRightExtrapolationCalculator;
import com.opengamma.analytics.financial.provider.calculator.sabrswaption.PresentValueSABRSensitivitySABRSwaptionCalculator;
import com.opengamma.analytics.financial.provider.calculator.sabrswaption.PresentValueSABRSwaptionCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderInterface;
import com.opengamma.analytics.financial.provider.description.interestrate.SABRSwaptionProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.SABRSwaptionProviderInterface;
import com.opengamma.analytics.financial.provider.sensitivity.multicurve.MultipleCurrencyMulticurveSensitivity;
import com.opengamma.analytics.financial.provider.sensitivity.multicurve.MultipleCurrencyParameterSensitivity;
import com.opengamma.analytics.financial.provider.sensitivity.parameter.ParameterSensitivityParameterCalculator;
import com.opengamma.analytics.financial.provider.sensitivity.sabrswaption.ParameterSensitivitySABRSwaptionDiscountInterpolatedFDCalculator;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.analytics.math.interpolation.CombinedInterpolatorExtrapolatorFactory;
import com.opengamma.analytics.math.interpolation.GridInterpolator2D;
import com.opengamma.analytics.math.interpolation.Interpolator1D;
import com.opengamma.analytics.math.interpolation.Interpolator1DFactory;
import com.opengamma.analytics.math.matrix.DoubleMatrix1D;
import com.opengamma.analytics.math.surface.InterpolatedDoublesSurface;
import com.opengamma.analytics.util.amount.SurfaceValue;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.gammatrace.exceptions.UnsupportedTradeException;
import com.opengamma.gammatrace.instrument.SwaptionInstrument;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.gammatrace.marketconstruction.sabr.SABRSwaptionCurveMaker;
import com.opengamma.gammatrace.prod.VegaMatrix;
import com.opengamma.gammatrace.rates.calculator.BlackImpliedVolatilityCalculator;
import com.opengamma.gammatrace.rates.calculator.DeltaRiskCalculatorOld;
import com.opengamma.gammatrace.rates.calculator.SABRSwaptionVegaCalculator;
import com.opengamma.gammatrace.rates.calculator.SABRSwaptionVegaStructureTest;
import com.opengamma.gammatrace.rates.calculator.SABRSwaptionVegaCalculator.Alpha;
import com.opengamma.gammatrace.rates.calculator.SABRSwaptionVegaCalculator.Expiry;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;


public class Swaption {
	private static final Calendar CALENDAR = new MondayToFridayCalendar("A");
	private static final BusinessDayConvention BUSINESS_DAY = BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following");
	private static final ZonedDateTime REFERENCE_DATE = DateUtils.getUTCDate(2014, 12, 15);
	private static final Currency CURRENCY = Currency.of("USD");
	
	private static final DayCount DAY_COUNT = DayCountFactory.INSTANCE.getDayCount("30/360");
	
	//interpolators
	private static final Interpolator1D LINEAR_FLAT = CombinedInterpolatorExtrapolatorFactory.getInterpolator(Interpolator1DFactory.LINEAR, Interpolator1DFactory.FLAT_EXTRAPOLATOR,
			Interpolator1DFactory.FLAT_EXTRAPOLATOR);
	private static final GridInterpolator2D INTERPOLATOR_2D = new GridInterpolator2D(LINEAR_FLAT, LINEAR_FLAT);
	
	// calculators
	private static final PresentValueDiscountingCalculator PVDC = PresentValueDiscountingCalculator.getInstance();
	private static final ParRateDiscountingCalculator PRDC = ParRateDiscountingCalculator.getInstance();
	private static final PresentValueSABRSwaptionCalculator PVSSC = PresentValueSABRSwaptionCalculator.getInstance();
	private static final PresentValueCurveSensitivitySABRSwaptionCalculator PVCSSSC = PresentValueCurveSensitivitySABRSwaptionCalculator.getInstance();
	private static final PresentValueSABRSensitivitySABRSwaptionCalculator PVSSSSC = PresentValueSABRSensitivitySABRSwaptionCalculator.getInstance();

	private static final ParameterSensitivityParameterCalculator<SABRSwaptionProviderInterface> PS_SS_C = new ParameterSensitivityParameterCalculator<>(PVCSSSC);
	
	// swaption
	private static final ZonedDateTime EXPIRY_DATE = DateUtils.getUTCDate(2016, 12, 15);
	private static final boolean IS_LONG = true;
	private static final ZonedDateTime SETTLEMENT_DATE = ScheduleCalculator.getAdjustedDate(EXPIRY_DATE, 2, CALENDAR);
	private static final int ANNUITY_TENOR_YEAR = 5;
	private static final Period ANNUITY_TENOR = Period.ofYears(ANNUITY_TENOR_YEAR);
	private static final double NOTIONAL = 100000000; //100m
	private static final double RATE = 0.024;
	
	
	public static void main(String[] args) {
		// make the curve
		//CurveMaker cm = new CurveMaker(CURRENCY, REFERENCE_DATE);
		//Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> pair = cm.makeCurve();
		//MulticurveProviderDiscount curve = pair.getFirst();
		// make the sabr parameters (expiry, tenor, value)
		SABRHaganVolatilityFunction sabrFunction = new SABRHaganVolatilityFunction();
		final InterpolatedDoublesSurface alphaSurface = InterpolatedDoublesSurface.from(new double[] {0.0, 0.5, 1, 2, 5, 10, 0.0, 0.5, 1, 2, 5, 10, 0.0, 0.5, 1, 2, 5, 10, 0.0, 0.5, 1, 2, 5, 10},
		        new double[] {0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 10, 10, 10, 10, 10, 10, 100, 100, 100, 100, 100, 100}, new double[] {0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05,
		            0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06, 0.06}, INTERPOLATOR_2D);
		    final InterpolatedDoublesSurface betaSurface = InterpolatedDoublesSurface.from(new double[] {0.0, 0.5, 1, 2, 5, 10, 100, 0.0, 0.5, 1, 2, 5, 10, 100, 0.0, 0.5, 1, 2, 5, 10, 100, 0.0, 0.5, 1, 2, 5,
		        10, 100}, new double[] {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 10, 10, 10, 10, 10, 10, 10, 100, 100, 100, 100, 100, 100, 100}, new double[] {0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5,
		        0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5}, INTERPOLATOR_2D);
		    final InterpolatedDoublesSurface rhoSurface = InterpolatedDoublesSurface.from(new double[] {0.0, 0.5, 1, 2, 5, 10, 100, 0.0, 0.5, 1, 2, 5, 10, 100, 0.0, 0.5, 1, 2, 5, 10, 100, 0.0, 0.5, 1, 2, 5,
		        10, 100}, new double[] {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 10, 10, 10, 10, 10, 10, 10, 100, 100, 100, 100, 100, 100, 100}, new double[] {-0.25, -0.25, -0.25, -0.25, -0.25, -0.25,
		        -0.25, -0.25, -0.25, -0.25, -0.25, -0.25, -0.25, -0.25, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}, INTERPOLATOR_2D);
		    final InterpolatedDoublesSurface nuSurface = InterpolatedDoublesSurface.from(new double[] {0.0, 0.5, 1, 2, 5, 10, 100, 0.0, 0.5, 1, 2, 5, 10, 100, 0.0, 0.5, 1, 2, 5, 10, 100, 0.0, 0.5, 1, 2, 5,
		        10, 100}, new double[] {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 10, 10, 10, 10, 10, 10, 10, 100, 100, 100, 100, 100, 100, 100}, new double[] {0.50, 0.50, 0.50, 0.50, 0.50, 0.50, 0.50, 0.50,
		        0.50, 0.50, 0.50, 0.50, 0.50, 0.50, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30}, INTERPOLATOR_2D);

		SABRInterestRateParameters sabrParameters = new SABRInterestRateParameters(alphaSurface, betaSurface, rhoSurface, nuSurface, DAY_COUNT, sabrFunction);
		GeneratorSwapFixedIbor EUR1YEURIBOR6M = GeneratorSwapFixedIborTemplate.getInstance().getGenerator(Generators.USD6MLIBOR3M, CALENDAR);
		// make the sabr multicurves
		//SABRSwaptionProviderDiscount sabrMulticurves = new SABRSwaptionProviderDiscount(curve, sabrParameters, EUR1YEURIBOR6M);
		
		SABRSwaptionCurveMaker test123 = null;
		try {
			test123 = new SABRSwaptionCurveMaker(CURRENCY, REFERENCE_DATE);
		} catch (UnsupportedTradeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SABRSwaptionProviderDiscount sabrMulticurves = test123.makeSABRSwaptionCurve();
		
		SwapFixedIborDefinition swapDefinition = SwapFixedIborDefinition.from(SETTLEMENT_DATE, ANNUITY_TENOR, EUR1YEURIBOR6M, NOTIONAL, RATE, true);
		SwaptionCashFixedIborDefinition swaptionDefinition = SwaptionCashFixedIborDefinition.from(EXPIRY_DATE, swapDefinition, IS_LONG);	
		SwaptionCashFixedIbor swaption = swaptionDefinition.toDerivative(REFERENCE_DATE);
		MultipleCurrencyAmount pv = swaption.accept(PVSSC, sabrMulticurves);
		System.out.println(pv);
		
		double implied = BlackImpliedVolatilityCalculator.getImpliedVolatility(new SwaptionCashFixedIbor[] {swaption}, pv.getAmount(CURRENCY), sabrMulticurves.getMulticurveProvider());
		System.out.println(implied);
		
		final MultipleCurrencyMulticurveSensitivity delta =  swaption.accept(PVCSSSC, sabrMulticurves);//PVCSSSC.visit(swaption, sabrMulticurves);
		
		System.out.println(delta);
		
		PresentValueSABRSensitivityDataBundle vega = SABRSensitivityNodeCalculator.calculateNodeSensitivities(PVSSSSC.visit(swaption, sabrMulticurves), sabrMulticurves.getSABRParameter());
		
		
		//+++++++ VEGA
		SABRSwaptionVegaCalculator vegaCalc = new SABRSwaptionVegaCalculator(swaption, sabrMulticurves);
		VegaMatrix testtest = vegaCalc.getVegaMatrix();
		Double[] expiries = testtest.getBeta().getExpiries();
		Double[] tenors = testtest.getBeta().getTenors();
		SurfaceValue sV = testtest.getAlpha().getVegaPoints();
		
		System.out.println(new DoubleMatrix1D(expiries).toString()+"   HIHIHIHIHIHIHIHIHIH");
		System.out.println(new DoubleMatrix1D(tenors).toString()+"   HIHIHIHIHIHIHIHIHIH");
		System.out.println(sV.toString()+"   HIHIHIHIHIHIHIHIHIH " +sV.toSingleValue());
		System.out.println(vega.getAlpha().toSingleValue());
		
		/*
		Double[] extractXData = sabrMulticurves.getSABRParameter().getAlphaSurface().getXData();
		ArrayList<Double> xList= new ArrayList<>();
		int xArrayCounter = 0;
		while (!xList.contains(extractXData[xArrayCounter])) {
			xList.add(extractXData[xArrayCounter]);
			xArrayCounter++;
		}
		Double[] xArray = xList.toArray(new Double[xList.size()]);
		
		for (Double e : xArray) {
			System.out.println(e);
		}
		
		System.out.println("alpha: "+vega.getAlpha().getMap());
		System.out.println("beta: "+vega.getBeta().getMap());
		System.out.println("rho: "+vega.getRho().getMap());
		System.out.println("nu: "+vega.getNu().getMap());
		 */
		
		
		
		ParameterSensitivityParameterCalculator<SABRSwaptionProviderInterface> PS_SS_C = new ParameterSensitivityParameterCalculator<>(PVCSSSC);
		MarketQuoteSensitivityBlockCalculator<SABRSwaptionProviderInterface> MQSC = new MarketQuoteSensitivityBlockCalculator<>(PS_SS_C);
		MultipleCurrencyParameterSensitivity sensi = MQSC.fromInstrument(swaption, sabrMulticurves, test123.getCurveBuildingBlockBundle());
		/*
		for (String s : curve.getAllNames()) {
			System.out.print(s+":  ");
			System.out.println(sensi.getSensitivity(s, CURRENCY));
		}
		
		DeltaRiskCalculator deltaSwap = new DeltaRiskCalculator(swaption, sabrMulticurves, test123.getCurveBuildingBlockBundle(), test123.getDeltaDisplayBundle());
		System.out.println(deltaSwap.extractFixedDelta());
		System.out.println(deltaSwap.extractSpreadDelta());*/
		
	}	
}
