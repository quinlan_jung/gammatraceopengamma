
import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.forex.conversion.ForexQuoteConversion;
import com.opengamma.analytics.financial.forex.definition.ForexDefinition;
import com.opengamma.analytics.financial.forex.definition.ForexOptionVanillaDefinition;
import com.opengamma.analytics.financial.forex.derivative.ForexOptionVanilla;
import com.opengamma.analytics.financial.forex.method.PresentValueForexBlackVolatilityNodeSensitivityDataBundle;
import com.opengamma.analytics.financial.forex.method.PresentValueForexBlackVolatilityQuoteSensitivityDataBundle;
import com.opengamma.analytics.financial.forex.method.PresentValueForexBlackVolatilitySensitivity;
import com.opengamma.analytics.financial.forex.provider.ForexOptionVanillaBlackSmileMethod;
import com.opengamma.analytics.financial.model.volatility.BlackFormulaRepository;
import com.opengamma.analytics.financial.model.volatility.surface.SmileDeltaTermStructureParametersStrikeInterpolation;
import com.opengamma.analytics.financial.provider.calculator.blackforex.BucketedVegaForexBlackSmileCalculator;
import com.opengamma.analytics.financial.provider.calculator.blackforex.CurrencyExposureForexBlackSmileCalculator;
import com.opengamma.analytics.financial.provider.calculator.blackforex.ImpliedVolatilityForexBlackSmileCalculator;
import com.opengamma.analytics.financial.provider.calculator.blackforex.PresentValueCurveSensitivityForexBlackSmileCalculator;
import com.opengamma.analytics.financial.provider.calculator.blackforex.PresentValueForexBlackSmileCalculator;
import com.opengamma.analytics.financial.provider.calculator.blackforex.PresentValueForexVolatilitySensitivityForexBlackSmileCalculator;
import com.opengamma.analytics.financial.provider.calculator.blackforex.QuoteBucketedVegaForexBlackSmileCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.forex.BlackForexSmileProviderDiscount;
import com.opengamma.analytics.financial.provider.description.forex.BlackForexSmileProviderInterface;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.sensitivity.blackforex.ParameterSensitivityForexBlackSmileDiscountInterpolatedFDCalculator;
import com.opengamma.analytics.financial.provider.sensitivity.parameter.ParameterSensitivityParameterCalculator;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.analytics.math.matrix.DoubleMatrix1D;
import com.opengamma.analytics.math.matrix.DoubleMatrix2D;
import com.opengamma.analytics.math.statistics.distribution.NormalDistribution;
import com.opengamma.analytics.math.statistics.distribution.ProbabilityDistribution;
import com.opengamma.analytics.util.time.TimeCalculator;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.forex.calculator.CurrencyExposureCalculator;
import com.opengamma.gammatrace.forex.calculator.ForexBlackImpliedVolatilityCalculator;
import com.opengamma.gammatrace.forex.calculator.ForexVegaCalculator;
import com.opengamma.gammatrace.forex.instrument.ForexPair;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.marketconstruction.forexvol.ForexSmileCurveMaker;
import com.opengamma.gammatrace.rates.calculator.BlackImpliedVolatilityCalculator;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;

public class FXOption {
	
	// General
	private static final Calendar CALENDAR = new MondayToFridayCalendar("A");
	private static final BusinessDayConvention BUSINESS_DAY = BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following");
	private static final int SETTLEMENT_DAYS = 2;
	
	private static final Currency EUR = Currency.EUR;
	private static final Currency USD = Currency.USD;
	private static final ZonedDateTime TIMESTAMP = DateUtils.getUTCDate(2014, 12, 15);
	
	// Methods and curves
	private static final ProbabilityDistribution<Double> NORMAL = new NormalDistribution(0, 1);
	private static final PresentValueDiscountingCalculator PVDC = PresentValueDiscountingCalculator.getInstance();
	private static final PresentValueForexBlackSmileCalculator PVFBC = PresentValueForexBlackSmileCalculator.getInstance();
	private static final CurrencyExposureForexBlackSmileCalculator CEFBC = CurrencyExposureForexBlackSmileCalculator.getInstance();
	private static final PresentValueCurveSensitivityForexBlackSmileCalculator PVCSFBC = PresentValueCurveSensitivityForexBlackSmileCalculator.getInstance();
	private static final PresentValueForexVolatilitySensitivityForexBlackSmileCalculator PVVSFBSC = PresentValueForexVolatilitySensitivityForexBlackSmileCalculator.getInstance();
	
	private static final BucketedVegaForexBlackSmileCalculator BVFBSC = BucketedVegaForexBlackSmileCalculator.getInstance();
	private static final QuoteBucketedVegaForexBlackSmileCalculator QBVFBSC = QuoteBucketedVegaForexBlackSmileCalculator.getInstance();
	
	private static final ForexOptionVanillaBlackSmileMethod METHOD_FX_VAN = ForexOptionVanillaBlackSmileMethod.getInstance();
	
	private static final ImpliedVolatilityForexBlackSmileCalculator IVFBSC = ImpliedVolatilityForexBlackSmileCalculator.getInstance();

	
	private static final double SHIFT_FD = 1.0E-6;
	private static final ParameterSensitivityParameterCalculator<BlackForexSmileProviderInterface> PS_FBS_C = new ParameterSensitivityParameterCalculator<>(
	      PVCSFBC);
	private static final ParameterSensitivityForexBlackSmileDiscountInterpolatedFDCalculator PS_FBS_FDC = new ParameterSensitivityForexBlackSmileDiscountInterpolatedFDCalculator(
	      PVFBC, SHIFT_FD);
	
	public static void main(String[] args) {
		CurveMaker cm = new CurveMaker(EUR, USD, TIMESTAMP);
		Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> pair = cm.makeCurve();
		MulticurveProviderDiscount curve = pair.getFirst();
		/*
		Period[] expiryPeriod = new Period[] {Period.ofMonths(3), Period.ofMonths(6), Period.ofYears(1), Period.ofYears(2), 
				Period.ofYears(3), Period.ofYears(5), Period.ofYears(10)};
		int numExpiry = expiryPeriod.length;
		ZonedDateTime spotReferenceDate = ScheduleCalculator.getAdjustedDate(TIMESTAMP, SETTLEMENT_DAYS, CALENDAR);
		ZonedDateTime[] payDate = new ZonedDateTime[numExpiry];
		ZonedDateTime[] expiries = new ZonedDateTime[numExpiry];
		double[] TIME_TO_EXPIRY = new double[numExpiry + 1];
		TIME_TO_EXPIRY[0] = 0.0;
		for (int loopexp = 0; loopexp < numExpiry; loopexp++) {
		    payDate[loopexp] = ScheduleCalculator.getAdjustedDate(spotReferenceDate, expiryPeriod[loopexp], BUSINESS_DAY, CALENDAR);
		    expiries[loopexp] = ScheduleCalculator.getAdjustedDate(payDate[loopexp], -SETTLEMENT_DAYS, CALENDAR);
		    TIME_TO_EXPIRY[loopexp + 1] = TimeCalculator.getTimeBetween(TIMESTAMP, expiries[loopexp]);
	    }
		double[] ATM = {0.175, 0.175, 0.18, 0.18, 0.175, 0.16, 0.16, 0.15 };
		double[] DELTA = new double[] {0.10, 0.25 };
		double[][] RISK_REVERSAL = new double[][] { {-0.010, -0.0050 }, {-0.010, -0.0050 }, {-0.011, -0.0060 }, {-0.012, -0.0070 }, {-0.013, -0.0080 }, {-0.014, -0.0090 }, {-0.014, -0.0090 } , {-0.014, -0.0090 }};
		double[][] STRANGLE = new double[][] { {0.0300, 0.0100 }, {0.0300, 0.0100 }, {0.0310, 0.0110 }, {0.0320, 0.0120 }, {0.0330, 0.0130 }, {0.0340, 0.0140 }, {0.0340, 0.0140 }, {0.0340, 0.0140 } };
		int NB_STRIKE = 2 * DELTA.length + 1;
		SmileDeltaTermStructureParametersStrikeInterpolation SMILE_TERM = new SmileDeltaTermStructureParametersStrikeInterpolation(TIME_TO_EXPIRY, DELTA, ATM, RISK_REVERSAL, STRANGLE);
		
		BlackForexSmileProviderDiscount SMILE_MULTICURVES = new BlackForexSmileProviderDiscount(curve, SMILE_TERM, Pair.of(EUR, USD));
		*/
		ForexSmileCurveMaker blah = new ForexSmileCurveMaker(EUR, USD, TIMESTAMP);
		BlackForexSmileProviderDiscount SMILE_MULTICURVES = blah.makeForexSmileCurve();
		
		double strike = 1.5118574837;//1.5118574839;
		boolean isCall = true;
		boolean isLong = true;
		double notional = 1000000;
		ZonedDateTime optionPayDate = ScheduleCalculator.getAdjustedDate(TIMESTAMP, Period.ofMonths(6), BUSINESS_DAY, CALENDAR);
		ZonedDateTime optionExpiryDate = ScheduleCalculator.getAdjustedDate(optionPayDate, -SETTLEMENT_DAYS, CALENDAR);
		ForexDefinition forexDefinition = new ForexDefinition(EUR, USD, optionPayDate, notional, strike);
		ForexOptionVanillaDefinition forexOptionCallDefinition = new ForexOptionVanillaDefinition(forexDefinition, optionExpiryDate, isCall, isLong);
		ForexOptionVanilla forexCallOption = forexOptionCallDefinition.toDerivative(TIMESTAMP);
	
		MultipleCurrencyAmount pvCalculator = forexCallOption.accept(PVFBC, SMILE_MULTICURVES);
		final MultipleCurrencyAmount ceCalculator = forexCallOption.accept(CEFBC, SMILE_MULTICURVES);
		final CurrencyAmount newCalc = new CurrencyExposureCalculator(forexCallOption, SMILE_MULTICURVES).getForeignDelta();
		double ivCalculator = forexCallOption.accept(IVFBSC, SMILE_MULTICURVES);
		
		double spot = curve.getFxRate(EUR, USD);
		final double forward = spot * curve.getDiscountFactor(EUR, TimeCalculator.getTimeBetween(TIMESTAMP, optionPayDate)) / curve.getDiscountFactor(USD, TimeCalculator.getTimeBetween(TIMESTAMP, optionPayDate));

		double impliedVol = ForexBlackImpliedVolatilityCalculator.getImpliedVolatility(forexCallOption, pvCalculator.getAmount(USD), curve);
		
		System.out.println(forexCallOption.getUnderlyingForex().getPaymentCurrency1());
		System.out.println(forward);
		
		System.out.println(forexCallOption.getCurrency1());
		
		System.out.println(pvCalculator);
		System.out.println(ceCalculator);
		System.out.println(newCalc);
		System.out.println(ivCalculator);
		System.out.println(impliedVol);

		
		final PresentValueForexBlackVolatilityNodeSensitivityDataBundle sensiStrike = METHOD_FX_VAN.presentValueBlackVolatilityNodeSensitivity(forexCallOption,SMILE_MULTICURVES);
		
		PresentValueForexBlackVolatilityNodeSensitivityDataBundle testVegaNode = forexCallOption.accept(BVFBSC, SMILE_MULTICURVES);
		
		System.out.println(testVegaNode.getVega().toString());
		
		//System.out.println(sensiStrike.getVega().toString());
		// vega in domestic currency, like pv
		PresentValueForexBlackVolatilityQuoteSensitivityDataBundle testVega = forexCallOption.accept(QBVFBSC, SMILE_MULTICURVES);
		ForexVegaCalculator fvc = new ForexVegaCalculator(forexCallOption, SMILE_MULTICURVES);
		
		PresentValueForexBlackVolatilityQuoteSensitivityDataBundle testVegaNew = fvc.getVega();
		
		
		System.out.println((new DoubleMatrix2D(testVega.getVega())).toString());
		System.out.println((new DoubleMatrix2D(testVegaNew.getVega())).toString());
		System.out.println((new DoubleMatrix1D(testVega.getExpiries())).toString());
		System.out.println((new DoubleMatrix1D(testVega.getDelta())).toString());
		for (String s : fvc.getVegaLabels()) {
			System.out.println(s);
		}
		
		
		// why does this strike produce non-zero RR and strangle risk?
		double impliedStrike = BlackFormulaRepository.impliedStrike(0.50, true, METHOD_FX_VAN.forwardForexRate(forexCallOption, curve), 0.5, impliedVol);
		
		System.out.println("impliedStrike ="+impliedStrike);
		System.out.println("Fwd = "+METHOD_FX_VAN.forwardForexRate(forexCallOption, curve));
		
		final PresentValueForexBlackVolatilitySensitivity pvvsCalculator = forexCallOption.accept(PVVSFBSC, SMILE_MULTICURVES);
		System.out.println(pvvsCalculator.getVega());
	}	
}

