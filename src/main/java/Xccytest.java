import java.util.List;
import java.util.Map;
import java.util.Set;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;






























import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityDefinition;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.payment.CouponIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.payment.PaymentDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapXCcyIborIborDefinition;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Coupon;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Payment;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.interestrate.swap.derivative.SwapFixedCoupon;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueMarketQuoteSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.generic.MarketQuoteSensitivityBlockCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderInterface;
import com.opengamma.analytics.financial.provider.sensitivity.multicurve.MultipleCurrencyParameterSensitivity;
import com.opengamma.analytics.financial.provider.sensitivity.parameter.ParameterSensitivityParameterCalculator;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.analytics.math.matrix.DoubleMatrix1D;
import com.opengamma.analytics.util.time.TimeCalculator;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.gammatrace.rates.calculator.DeltaDisplayBundle;
import com.opengamma.gammatrace.rates.calculator.DeltaRiskCalculatorOld;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapXCcyIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplate;
import com.opengamma.gammatrace.rates.instrument.FixingBuilder;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;


public class Xccytest {
	private static final PresentValueDiscountingCalculator PVDC = PresentValueDiscountingCalculator.getInstance();
	private static final PresentValueCurveSensitivityDiscountingCalculator PVCSDC = PresentValueCurveSensitivityDiscountingCalculator.getInstance();
	private static final ParameterSensitivityParameterCalculator<MulticurveProviderInterface> PSC = new ParameterSensitivityParameterCalculator<>(PVCSDC);
	private static final MarketQuoteSensitivityBlockCalculator<MulticurveProviderInterface> MQSC = new MarketQuoteSensitivityBlockCalculator<>(PSC);
	
	static final PresentValueMarketQuoteSensitivityDiscountingCalculator PVMQSC = PresentValueMarketQuoteSensitivityDiscountingCalculator.getInstance();
	static final ParSpreadMarketQuoteDiscountingCalculator PSMQC = ParSpreadMarketQuoteDiscountingCalculator.getInstance();
	
    
	private static final Calendar NYC = new MondayToFridayCalendar("NYC");
	private static final Calendar TARGET = new MondayToFridayCalendar("TARGET");
	
	private static final ZonedDateTime settlementDate = DateUtils.getUTCDate(2014, 8, 12);
	private static final ZonedDateTime maturityDate = DateUtils.getUTCDate(2018, 8, 12);
	private static final ZonedDateTime timestamp = DateUtils.getUTCDate(2014, 8, 8);
	
	private static final GeneratorSwapXCcyIborIborTemplate SWAP_MASTER = GeneratorSwapXCcyIborIborTemplate.getInstance();
	private static final GeneratorSwapXCcyIborIbor generator = SWAP_MASTER.getGenerator(Generators.EUR3MUSD3M, NYC, TARGET);
	
	private static final GeneratorSwapXCcyIborIbor testGenerator = new GeneratorSwapXCcyIborIbor("testtest", generator.getIborIndex1(), generator.getIborIndex1(), NYC, NYC);
	
	private static double notionalEUR = 8e7;
	private static double notionalUSD = 5e7;
	
	private static final SwapXCcyIborIborDefinition swapDefinition =  SwapXCcyIborIborDefinition.from(settlementDate, maturityDate, generator, notionalEUR, notionalUSD, -0.0038375, 0, true);
	
	private static final ZonedDateTimeDoubleTimeSeries[] TS_IBOR = FixingBuilder.makeFixingTimeSeries(swapDefinition, timestamp);
	private static final Swap<?, ?> swap = swapDefinition.toDerivative(timestamp, TS_IBOR);
	
	public static void main(String[] args) {
		CurveMaker cm = new CurveMaker(Currency.USD, Currency.EUR, timestamp);
		Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> pair = cm.makeCurve();
		MulticurveProviderDiscount curve = pair.getFirst(); 
		MultipleCurrencyAmount pv = swap.accept(PVDC, curve);
		double parSpread = swap.accept(PSMQC, curve); 
		System.out.println(curve.getFxRates().convert(pv, Currency.EUR));
		System.out.println(parSpread);
		
		CurveBuildingBlockBundle block = pair.getSecond();
	    MultipleCurrencyParameterSensitivity mqs = MQSC.fromInstrument(swap, curve, block).converted(curve.getFxRates(), Currency.USD);

	    
	    for (Payment p : swap.getFirstLeg().getPayments()) {
	    	System.out.println(curve.getFxRates().convert(p.accept(PVDC, curve),Currency.USD).getAmount());
	    }
	    for (Payment p : swap.getSecondLeg().getPayments()) {
	    	System.out.println(curve.getFxRates().convert(p.accept(PVDC, curve),Currency.USD).getAmount());
	    }
	    
	    for (Payment p : swap.getFirstLeg().getPayments()) {
	    	System.out.println(p.toString());
	    }
	    for (Payment p : swap.getSecondLeg().getPayments()) {
	    	System.out.println(p.toString());
	    }
	    
	    Set<String> curveNames = curve.getAllNames();
		int i = 0;
		for (Object name : curveNames.toArray()) {
			//System.out.println(curveNames.toArray()[i]);
			i++;
		}
		/*
		IborIndex JPYLIBOR3M = IborTemplate.getInstance().getIndex(Currency.JPY, Period.ofMonths(3));
		IborIndex EURIBOR3M = IborTemplate.getInstance().getIndex(Currency.JPY, Period.ofMonths(3));
		
		ZonedDateTime startDate = ScheduleCalculator.getAdjustedDate(settlementDate, 2, NYC);
		ZonedDateTime endDate;
		double startTime;
		double endTime;
        
		final int numDates = 20;
		*/
		
		/*
		for (int j = 0; j < 20; j++) {
			startTime = TimeCalculator.getTimeBetween(settlementDate, startDate);
			// increment endDate by 3 months (corresponding to tenor of ibor index)... this is problematic due to bumps!
			endDate = ScheduleCalculator.getAdjustedDate(startDate, JPYLIBOR3M, NYC);
			endTime = TimeCalculator.getTimeBetween(settlementDate, endDate);
			
			final double accrualFactor = JPYLIBOR3M.getDayCount().getDayCountFraction(startDate, endDate);
			final double discountFactor = curve.getDiscountFactor(Currency.JPY, startTime);
			final double discountFactor2 = curve.getDiscountFactor(Currency.EUR, startTime);
			double forwardRate = curve.getForwardRate(JPYLIBOR3M, startTime, endTime, accrualFactor);
			double forwardRate2 = curve.getForwardRate(EURIBOR3M, startTime, endTime, accrualFactor);
			
			System.out.println(startDate+" "+endDate+" "+"DF: "+discountFactor+" "+"DF EUR: "+discountFactor2+" 3mY: "+forwardRate+" 3mE: "+forwardRate2);
			startDate = endDate;
		}
		*/
		/*
		PaymentDefinition[] test = swapDefinition.getFirstLeg().getPayments();
		
		CouponIborSpreadDefinition[] coupons = new CouponIborSpreadDefinition[test.length - 2];
		for (int k = 1; k < test.length - 1; k++) {
			coupons[k-1] = (CouponIborSpreadDefinition) test[k];
		}
		
		 
		AnnuityCouponIborSpreadDefinition legNoNotional = new AnnuityCouponIborSpreadDefinition(coupons, NYC);
		for (int l = 0; l < legNoNotional.getNumberOfPayments(); l++) {
			System.out.println(legNoNotional.getNthPayment(l).equals(test[l+1]));
		}
		
		for (PaymentDefinition p : test) {
			System.out.println(p+"  "+ p.getClass());
		}
		
		System.out.println(swap.getFirstLeg().getNthPayment(0).getReferenceAmount());
		//swap.getFirstLeg().getNthPayment(n)
		System.out.println(swap.getFirstLeg().toString());
		
		*/
		/*
	    for (Map.Entry<Pair<String, Currency>, DoubleMatrix1D> e : mqs.getSensitivities().entrySet()) {
	    	System.out.println(e.getKey());
	    	System.out.println(e.getValue().toString());
	    }
		*/
	    DeltaRiskCalculatorOld delta = new DeltaRiskCalculatorOld(swap, pair, cm.getDeltaDisplayBundle());

		System.out.println(delta.extractSpreadDelta().getCurveDeltas().get(0).getName());
		System.out.println(delta.extractSpreadDelta().getCurveDeltas().get(0).getQuoteInstrumentTypes().toString());
		System.out.println(delta.extractSpreadDelta().getCurveDeltas().get(0).getDeltaValues().toString());
		
		System.out.println(delta.extractFixedDelta().getCurveDeltas().get(1).getName());
		System.out.println(delta.extractFixedDelta().getCurveDeltas().get(1).getQuoteInstrumentTypes().toString());
		System.out.println(delta.extractFixedDelta().getCurveDeltas().get(1).getDeltaValues().toString());
		System.out.println(delta.extractFixedDelta().getCurveDeltas().get(0).getName());
		System.out.println(delta.extractFixedDelta().getCurveDeltas().get(0).getQuoteInstrumentTypes().toString());
		System.out.println(delta.extractFixedDelta().getCurveDeltas().get(0).getDeltaValues().toString());

		System.out.println(delta.extractXCcySpreadDeltaOld().toString());
		System.out.println(delta.extractXCcySpreadDelta().getCurveDeltas().get(0).getName());
		System.out.println(delta.extractXCcySpreadDelta().getCurveDeltas().get(0).getQuoteInstrumentTypes().toString());
		System.out.println(delta.extractXCcySpreadDelta().getCurveDeltas().get(0).getDeltaValues().toString());
		
	    /*
	    for (Map.Entry<String, List<Pair<QuoteInstrumentType,Double>>> e : delta.getFirst().entrySet()) {
			int printCounter = 0;
			System.out.println(e.getKey());
			
			for (Pair<QuoteInstrumentType,Double> d : e.getValue()) {
				String displayValue = d.toString();
				
				//String displayValue = (d.getFirst() == null)? "-" : d.toString();
				System.out.println(delta.getSecond().get(printCounter)+"     "+displayValue);
				printCounter++;
			}
		}*/
	    
	}
}
