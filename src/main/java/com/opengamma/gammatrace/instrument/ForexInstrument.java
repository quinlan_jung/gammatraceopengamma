package com.opengamma.gammatrace.instrument;

import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.forex.derivative.ForexOptionVanilla;
import com.opengamma.util.money.CurrencyAmount;

public class ForexInstrument {
	
	private ForexOptionVanilla derivative;
	private ZonedDateTime executionTimestamp;
	private CurrencyAmount optionPremium;
	
	public ForexInstrument(ForexOptionVanilla derivative, ZonedDateTime executionTimestamp, CurrencyAmount optionPremium) {
		this.derivative = derivative;
		this.executionTimestamp = executionTimestamp;
		this.optionPremium = optionPremium;
	}
	
	public ForexOptionVanilla getDerivative() {
		return derivative;
	}
	
	public ZonedDateTime getExecutionTimestamp() {
		return executionTimestamp;
	}
	
	public CurrencyAmount getOptionPremium() {
		return optionPremium;
	}
	
}
