package com.opengamma.gammatrace.instrument;

import java.util.Map;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.commonutils.Utils;
import com.gammatrace.datamodel.Trade;
import com.gammatrace.enums.Taxonomy;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborIbor;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexDeposit;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.analytics.financial.instrument.swap.SwapIborIborDefinition;
import com.opengamma.analytics.financial.interestrate.InstrumentDerivative;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.gammatrace.exceptions.UnsupportedOperation;
import com.opengamma.gammatrace.rates.instrument.AssetMapper;
import com.opengamma.gammatrace.rates.instrument.SwapFixedIborBuilder;
import com.opengamma.gammatrace.rates.instrument.SwapFixedONBuilder;
import com.opengamma.gammatrace.rates.instrument.SwapIborIborBuilder;
import com.opengamma.gammatrace.rates.instrument.SwapIborONBuilder;
import com.opengamma.gammatrace.rates.instrument.SwapXCcyFixedFixedBuilder;
import com.opengamma.gammatrace.rates.instrument.SwapXCcyFixedIborBuilder;
import com.opengamma.gammatrace.rates.instrument.SwapXCcyIborIborBuilder;
import com.opengamma.gammatrace.rates.instrument.SwaptionCashFixedIborBuilder;
import com.opengamma.gammatrace.rates.instrument.SwaptionPhysicalFixedIborBuilder;
import com.opengamma.util.ArgumentChecker;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.tuple.Pair;

public class InstrumentBuildManager {
	
	// the full trade
	private final Trade trade;
	
	private final String taxonomy; // see TODO below
	private final String firstAssetName;
	private final String secondAssetName;
	private final Period firstLegResetPeriod;
	private final Period secondLegResetPeriod;
	private final Currency firstNotionalCcy;
	private final Currency secondNotionalCcy;	 
		
	public InstrumentBuildManager(Trade trade) {
		this.trade = trade;
		
		taxonomy = trade.getTaxonomy();
		firstAssetName = trade.getUnderlying_asset_1();
		secondAssetName = trade.getUnderlying_asset_2();
		firstLegResetPeriod = trade.getReset_frequency_1_asPeriod();
    	secondLegResetPeriod = trade.getReset_frequency_2_asPeriod();
    	firstNotionalCcy = Currency.parse(trade.getNotional_currency_1());
		secondNotionalCcy = Currency.parse(trade.getNotional_currency_2());
	}
	
	public Instrument makeInstrument() throws UnsupportedOperation {
		Instrument instrument;
		// TODO: once InterestRate:IRSwap:Basis is added to Taxonomy enum in the parser, use this switch statement and change taxonomy to type Taxonomy. 
		/*
		switch (taxonomy) {
		case INTERESTRATE_IRSWAP_FIXEDFLOAT:
			return new SwapFixedIborBuilder(trade).getInstrument();
		case INTERESTRATE_IRSWAP_OIS:
			return new SwapFixedONBuilder(trade).getInstrument();
		case INTERESTRATE_IRSWAP_BASIS:
			IndexDeposit firstIndex = AssetMapper.getInstance().getIndex(firstAssetName, settlementCurrency, firstLegPeriod);
			IndexDeposit secondIndex = AssetMapper.getInstance().getIndex(secondAssetName, settlementCurrency, secondLegPeriod);
			if (firstIndex instanceof IborIndex && secondIndex instanceof IborIndex) {
				return new SwapIborIborBuilder(trade).getInstrument();
			} else if (firstIndex instanceof IndexON && secondIndex instanceof IborIndex || firstIndex instanceof IborIndex && secondIndex instanceof IndexON) {
				throw new UnsupportedOperation("Ibor ON basis swaps are not yet supported");
			} else {
				throw new UnsupportedOperation("Only Ibor and ON basis swaps are supported");
			}
		case INTERESTRATE_CROSSCURRENCY_BASIS:
			return new SwapXCcyIborIborBuilder(trade).getInstrument();
		case INTERESTRATE_CROSSCURRENCY_FIXEDFLOAT:
			throw new UnsupportedOperation("Fixed float xccy are not yet supported");
		case INTERESTRATE_CROSSCURRENCY_FIXEDFIXED:
			throw new UnsupportedOperation("Fixed fixed xccy are not yet supported");
		case INTERESTRATE_OPTION_SWAPTION:
			retrun new SwaptionPhysicalFixedIborBuilder(trade).getInstrument();
		case INTERESTRATE_CAPFLOOR:
			throw new UnsupportedOperation("Cap/floor options are not yet supported");
		default:
			throw new UnsupportedOperation(taxonomy+" trades are not yet supported");
		}*/
		
		Taxonomy t = Taxonomy.INTERESTRATE_IRSWAP_FIXEDFLOAT;
		if (taxonomy.equalsIgnoreCase("InterestRate:IRSwap:FixedFloat")) {
			instrument = new SwapFixedIborBuilder(trade).getInstrument();
		} else if (taxonomy.equalsIgnoreCase("InterestRate:IRSwap:OIS")) {
			instrument = new SwapFixedONBuilder(trade).getInstrument();
		} else if (taxonomy.equalsIgnoreCase("InterestRate:IRSwap:Basis")) {
			IndexDeposit firstIndex = AssetMapper.getInstance().getIndex(firstAssetName, firstNotionalCcy, firstLegResetPeriod);
			IndexDeposit secondIndex = AssetMapper.getInstance().getIndex(secondAssetName, secondNotionalCcy, secondLegResetPeriod);
			if (firstIndex instanceof IborIndex && secondIndex instanceof IborIndex) {
				instrument = new SwapIborIborBuilder(trade).getInstrument();
			} else if (firstIndex instanceof IndexON && secondIndex instanceof IborIndex || firstIndex instanceof IborIndex && secondIndex instanceof IndexON) {			
				instrument = new SwapIborONBuilder(trade).getInstrument();
			} else {
				throw new UnsupportedOperation("Only Ibor and ON basis swaps are supported");
			}
		} else if (taxonomy.equalsIgnoreCase("InterestRate:CrossCurrency:Basis")) {
			instrument = new SwapXCcyIborIborBuilder(trade).getInstrument();
		} else if (taxonomy.equalsIgnoreCase("InterestRate:CrossCurrency:FixedFloat")) {
			instrument = new SwapXCcyFixedIborBuilder(trade).getInstrument();
		} else if (taxonomy.equalsIgnoreCase("InterestRate:CrossCurrency:FixedFixed")){
			instrument = new SwapXCcyFixedFixedBuilder(trade).getInstrument();
		} else if (taxonomy.equalsIgnoreCase("InterestRate:Option:Swaption")) {
			if (firstNotionalCcy.equals(Currency.EUR) || firstNotionalCcy.equals(Currency.GBP) || firstNotionalCcy.equals(Currency.CHF) || 
					firstNotionalCcy.equals(Currency.DKK) || firstNotionalCcy.equals(Currency.NOK) || firstNotionalCcy.equals(Currency.SEK)){	// List the currencies that are cash annuity settled here, see OG conventions guide.
				instrument = new SwaptionCashFixedIborBuilder(trade).getInstrument();
			} else {
				instrument = new SwaptionPhysicalFixedIborBuilder(trade).getInstrument();
			}
		} else {
			throw new UnsupportedOperation("Unsupported trade type:" +taxonomy);
		}
		return instrument;
	}
}
