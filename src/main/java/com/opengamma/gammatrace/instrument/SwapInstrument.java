package com.opengamma.gammatrace.instrument;

import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.interestrate.InstrumentDerivative;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.tuple.Pair;
 
/**
 * Generic wrapper class for holding the derivative and other information not easily accessed from the derivative related to the trade details. 
 * @author sakeebzaman
 *
 */
public class SwapInstrument implements Instrument {

	private Swap<?, ?> derivative;
	private ZonedDateTime executionTimestamp;
	private ZonedDateTime effectiveDate;
	private ZonedDateTime endDate;
	private CurrencyAmount firstNotional;
	private CurrencyAmount secondNotional;
	private ZonedDateTime tradeDate;
	private MultipleCurrencyAmount upfront;
	private InstrumentDescription tradeType;
	private Pair<String, String> assetPair;
	
	public SwapInstrument(Swap<?, ?> derivative, ZonedDateTime executionTimestamp, ZonedDateTime effectiveDate, ZonedDateTime endDate, CurrencyAmount firstNotional, CurrencyAmount secondNotional, 
			ZonedDateTime tradeDate, MultipleCurrencyAmount upfront, InstrumentDescription tradeType, Pair<String, String> assetPair) {
		this.derivative = derivative;
		this.executionTimestamp = executionTimestamp;
		this.effectiveDate = effectiveDate;
		this.endDate = endDate;
		this.firstNotional = firstNotional;
		this.secondNotional = secondNotional;
		this.tradeDate = tradeDate;
		this.upfront = upfront;
		this.tradeType = tradeType;
		this.assetPair = assetPair;
	}
	
	public Swap<?, ?> getDerivative() {
		return derivative;
	}
	public ZonedDateTime getExecutionTimestamp() {
		return executionTimestamp;
	}
	public ZonedDateTime getEffectiveDate() {
		return effectiveDate;
	}
	public ZonedDateTime getEndDate() {
		return endDate;
	}
	public CurrencyAmount getFirstNotional() {
		return firstNotional;
	}
	public CurrencyAmount getSecondNotional() {
		return secondNotional;
	}
	public ZonedDateTime getTradeDate() {
		return tradeDate;
	}
	public MultipleCurrencyAmount getUpfront() {
		return upfront;
	}
	public InstrumentDescription getInstrumentDescription() {
		return tradeType;
	}
	public Pair<String, String> getAssetPair() {
		return assetPair;
	}
} 
