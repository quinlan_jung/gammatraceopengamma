package com.opengamma.gammatrace.instrument;

public enum InstrumentDescription {
	FIXED_IBOR_SWAP("FixedVsIborSwap"),
	FIXED_ON_SWAP("FixedVsONSwap"),
	IBOR_IBOR_SWAP("IborVsIborSwap"),
	IBOR_ON_SWAP("IborVsONSwap"),
	XCCY_IBOR_IBOR_SWAP("XCcyIborVsIborSwap"),
	XCCY_FIXED_IBOR_SWAP("XCcyFixedVsIborSwap"),
	XCCY_FIXED_FIXED_SWAP("XCcyFixedVsFixedSwap"),
	FIXED_IBOR_SWAPTION("FixedVsIborSwaption"),
	FIXED_IBOR_STRADDLE_SWAPTION("FixedVsIborStraddleSwaption"),
	FIXED_IBOR_CASH_SWAPTION("FixedVsIborCashSettledSwaption"),
	FIXED_IBOR_STRADDLE_CASH_SWAPTION("FixedVsIborStraddleCashSettledSwaption"),
	FX_OPTION("FxOption");
	
	private String description;
	private InstrumentDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return description;
	}
}
