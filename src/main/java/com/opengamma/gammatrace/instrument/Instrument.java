package com.opengamma.gammatrace.instrument;

import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.interestrate.InstrumentDerivative;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.tuple.Pair;

public interface Instrument {
	/* 	left out getDerivative() because in SwaptionInstrument getDerivative returns an array (to handle straddle trades comprised of 2 swaptions), whereas in SwapInstrument getDerivative() returns
	 *	a single InstrumentDerivative. We could make InstrumentDerivative[]'s in SwapInstrument, but then would just have to extract the first element in the main code (a bit messy?) or would have to
	 *	rewrite the calculator classes to take arrays (unclear economic meaning... e.g. what is the fair rate of a package of swaps?)  
	 */ 
	public ZonedDateTime getExecutionTimestamp();
	
	public ZonedDateTime getEffectiveDate();
	
	public ZonedDateTime getEndDate();
	
	public CurrencyAmount getFirstNotional();
	
	public CurrencyAmount getSecondNotional();
	
	public ZonedDateTime getTradeDate();
	
	public MultipleCurrencyAmount getUpfront();
	
	public InstrumentDescription getInstrumentDescription();
	
	public Pair<String, String> getAssetPair(); 
}
