package com.opengamma.gammatrace.instrument;

import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.interestrate.InstrumentDerivative;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionCashFixedIbor;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.analytics.financial.model.option.pricing.analytic.formula.EuropeanVanillaOption;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.tuple.Pair;

public class SwaptionInstrument implements Instrument {

	private InstrumentDerivative[] derivatives;
	private ZonedDateTime executionTimestamp;
	private double spotOptionPremium;
	private ZonedDateTime expirationDate;
	private SwapInstrument[] underlyingSwaps;
	
	
	public SwaptionInstrument(SwaptionPhysicalFixedIbor[] derivatives, ZonedDateTime executionTimestamp, double spotOptionPremium, ZonedDateTime expirationDate, ZonedDateTime effectiveDate, ZonedDateTime endDate, 
			CurrencyAmount firstNotional, CurrencyAmount secondNotional, ZonedDateTime tradeDate, MultipleCurrencyAmount upfront, InstrumentDescription tradeType, Pair<String, String> assetPair) {
		this.derivatives = derivatives;
		this.executionTimestamp = executionTimestamp;
		this.spotOptionPremium = spotOptionPremium;
		this.expirationDate = expirationDate;
		underlyingSwaps = new SwapInstrument[derivatives.length];
		for (int i = 0; i < derivatives.length; i++) {
			underlyingSwaps[i] = new SwapInstrument(derivatives[i].getUnderlyingSwap(), executionTimestamp, effectiveDate, endDate, firstNotional, secondNotional, tradeDate, upfront, tradeType, assetPair);
		}	
	}
	
	public SwaptionInstrument(SwaptionCashFixedIbor[] derivatives, ZonedDateTime executionTimestamp, double spotOptionPremium, ZonedDateTime expirationDate, ZonedDateTime effectiveDate, ZonedDateTime endDate, 
			CurrencyAmount firstNotional, CurrencyAmount secondNotional, ZonedDateTime tradeDate, MultipleCurrencyAmount upfront, InstrumentDescription tradeType, Pair<String, String> assetPair) {
		this.derivatives = derivatives;
		this.executionTimestamp = executionTimestamp;
		this.spotOptionPremium = spotOptionPremium;
		this.expirationDate = expirationDate;
		underlyingSwaps = new SwapInstrument[derivatives.length];
		for (int i = 0; i < derivatives.length; i++) {
			underlyingSwaps[i] = new SwapInstrument(derivatives[i].getUnderlyingSwap(), executionTimestamp, effectiveDate, endDate, firstNotional, secondNotional, tradeDate, upfront, tradeType, assetPair);
		}	
	}
	
	public InstrumentDerivative[] getDerivative() {
		return derivatives;
	}
	
	public ZonedDateTime getExpirationDate() {
		return expirationDate;
	}
	
	public SwapInstrument[] getUnderlyingSwaps() {
		return underlyingSwaps;
	}
	
	public double getSpotOptionPremium() {
		return spotOptionPremium;
	}

	@Override
	public ZonedDateTime getExecutionTimestamp() {
		return executionTimestamp;
	}
	
	@Override
	public ZonedDateTime getEffectiveDate() {
		return underlyingSwaps[0].getEffectiveDate();
	}

	@Override
	public ZonedDateTime getEndDate() {
		return underlyingSwaps[0].getEndDate();
	}

	@Override
	public CurrencyAmount getFirstNotional() {
		return underlyingSwaps[0].getFirstNotional();
	}

	@Override
	public CurrencyAmount getSecondNotional() {
		return underlyingSwaps[0].getSecondNotional();
	}

	@Override
	public ZonedDateTime getTradeDate() {
		return underlyingSwaps[0].getTradeDate();
	}

	@Override
	public MultipleCurrencyAmount getUpfront() {
		return underlyingSwaps[0].getUpfront();
	}

	@Override
	public InstrumentDescription getInstrumentDescription() {
		return underlyingSwaps[0].getInstrumentDescription();
	}

	@Override
	public Pair<String, String> getAssetPair() {
		return underlyingSwaps[0].getAssetPair();
	}
	
}
