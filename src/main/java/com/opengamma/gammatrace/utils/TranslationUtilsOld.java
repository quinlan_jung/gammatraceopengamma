package com.opengamma.gammatrace.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@Deprecated
public class TranslationUtilsOld {
	public static void printResults(Map <String, String[]> tickerNames, Map <String, Double> quotes){
		for (Entry<String, String[]> entry : tickerNames.entrySet()){
			String curveName = entry.getKey();
			String [] tickers = entry.getValue();
			
			System.out.println("Curve Name: " + curveName);
			for (int i = 0; i < tickers.length; i++){
				System.out.println(String.format("%s : %f", tickers[i], convertQuote(quotes.get(tickers[i]), curveName)));
			}
		}	
	}
	
	/**
	 * Takes a Map<CurveName, Symbols[]> and returns all the symbols as a List
	 * @param tickerNames, Map<CurveName, Symbol[]>
	 * @return List of symbols
	 */
	public static List<String> extractSymbols (Map <String, String[]> tickerNames){
		List<String> tickerList = new ArrayList<String>();
		for (String[] tickers : tickerNames.values()){
			for (String ticker : tickers){
				tickerList.add(ticker);
			}
		}
		return tickerList;
	}
	
	/**
	 * Takes a Map<CurveName, Symbols[]> and Map<Symbol, Quote> and returns Map<CurveName, Quote[]>
	 * @return The accepted format to store quotes, in order, under a curve name
	 */
	public static Map<String, Double[]> translateQuotes(Map <String, String[]> tickerNames, Map <String, Double> quotes){
		Map<String, Double[]> valuesMap = new LinkedHashMap<String, Double[]> ();
		for (Entry<String, String[]> entry : tickerNames.entrySet()){
			String curveName = entry.getKey();
			String [] tickers = entry.getValue();
			Double[] curveQuotes = new Double[tickers.length];
			for (int i = 0; i < tickers.length; i++){
				curveQuotes[i] = convertQuote(quotes.get(tickers[i]), curveName);
			}
			valuesMap.put(curveName, curveQuotes);
		}
		return valuesMap;
	}
	
	private static Double convertQuote(Double quote, String curveName){
		// Hacks for USD Configuration and EURMultiCcyConfiguration
		if (curveName.equals("USD DSC") || curveName.equals("USD fwd 3m") || curveName.equals("EUR fwd 6m")){
			return convertFromPercent(quote);
		}
		else if (curveName.equals("USD fwd 1m") || curveName.equals("USD fwd 6m") || curveName.equals("EUR DSC") || curveName.equals("EUR fwd 3m") || curveName.equals("EUR fwd 1m")){
			return convertFromBP(quote);
		}
		else{
			return quote;
		}
	}
	
	private static Double convertFromPercent (Double quote){
		return quote/100;
	}
	
	private static Double convertFromBP(Double quote){
		return quote/10000;
	}
}
