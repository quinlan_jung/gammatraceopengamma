package com.opengamma.gammatrace.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.gammatrace.logging.CustomLogger;
import com.gammatrace.sungard.api.ServiceApi;
import com.opengamma.gammatrace.marketconstruction.quote.SwapQuote;

// Consider making the CurveMaker interface an abstract class and putting these methods there
public class CurveMakerUtils {
	static Logger logger = CustomLogger.getLogger(CurveMakerUtils.class);

	public static void printResults(Map <String, SwapQuote[]> quoteMap){
		for (Entry<String, SwapQuote[]> e : quoteMap.entrySet()) {
			logger.info("Curve Name: "+e.getKey());
			for (SwapQuote q : e.getValue()) {
				logger.info(String.format("%s : %f", q.getTicker(), q.getValue()));
			}
		}
	}
	
	/**
	 * Update quote map with requestedValues
	 */
	public static void updateQuoteMap(Map<String, Double> requestedValues, Map<String, SwapQuote[]> quoteMap){
		for (Entry<String, SwapQuote[]> entry : quoteMap.entrySet()) {
			for (SwapQuote quote : entry.getValue()) {
				quote.setSungardValue(requestedValues.get(quote.getTicker()));
			}
		}
	}
	
	/**
	 * Request all the quotes in the quoteMap
	 */
	public static Map<String, Double> requestAllQuotes(Map<String, SwapQuote[]> quoteMap, ServiceApi serviceApi, long timestamp){
		return requestAllQuotes(new ArrayList<String>(), quoteMap, serviceApi, timestamp);
	}
	
	/**
	 * Request all the quotes in tickers list and the quoteMap
	 */
	public static Map<String, Double> requestAllQuotes(List<String> tickers, Map<String, SwapQuote[]> quoteMap, ServiceApi serviceApi, long timestamp){
		for (Entry<String, SwapQuote[]> entry : quoteMap.entrySet()) {
			for (SwapQuote quote : entry.getValue()) {
				tickers.add(quote.getTicker());
			}
		}
		return serviceApi.getQuotes(tickers, timestamp);
	}
}
