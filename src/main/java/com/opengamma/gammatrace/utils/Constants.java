package com.opengamma.gammatrace.utils;
@Deprecated
public class Constants {
	// The different types of curves
	public static final String DSC_USD = "USD Dsc";
	public static final String FWD3_USD = "USD Fwd 3M";
	public static final String FWD3M6M_USD = "Basis 3M6M";
	public static final String FWD3M1M_USD = "Basis 3M1M";
}
