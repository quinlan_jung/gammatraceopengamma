package com.opengamma.gammatrace.rates.calculator;

import java.util.HashMap;
import java.util.Map;

import org.threeten.bp.Period;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttribute;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeIR;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexDeposit;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.CalendarNoHoliday;
import com.opengamma.gammatrace.exceptions.UnsupportedOperation;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapXCcyIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.util.tuple.Pair;

public class MarketStandardGeneratorMapper {
	/**
	 * The method unique instance.
	 */
	private static final MarketStandardGeneratorMapper INSTANCE = new MarketStandardGeneratorMapper();

	/**
	 * Return the unique instance of the class.
	 * @return The instance.
	 */
	public static MarketStandardGeneratorMapper getInstance() {
	  return INSTANCE;
	}

	/**
	 * The map with the list of asset names and their indices.
	 */
	private final Map<Pair<String, String>, GeneratorInstrument<? extends GeneratorAttribute>> _generators;
	
	/**
	 * The generator templates.
	 */
	private final GeneratorSwapFixedIborTemplate _fixedIborMaster;
	private final GeneratorSwapFixedONTemplate _fixedONMaster;
	private final GeneratorSwapIborIborTemplate _iborIborMaster;
	private final GeneratorSwapIborONTemplate _iborONMaster;
	private final GeneratorSwapXCcyIborIborTemplate _xCcyIborIborMaster;
	
	/**
	 * Private constructor.
	 */
	private MarketStandardGeneratorMapper() {
		_fixedIborMaster = GeneratorSwapFixedIborTemplate.getInstance();
		_fixedONMaster = GeneratorSwapFixedONTemplate.getInstance();
		_iborIborMaster = GeneratorSwapIborIborTemplate.getInstance();
		_iborONMaster = GeneratorSwapIborONTemplate.getInstance();
		_xCcyIborIborMaster = GeneratorSwapXCcyIborIborTemplate.getInstance();
		final Calendar baseCalendar = new CalendarNoHoliday("No Holidays");
		_generators = new HashMap<Pair<String, String>, GeneratorInstrument<? extends GeneratorAttribute>>();
		//fixedIbor
		_generators.put(Pair.of("FIXED","USDLIBOR1M"), _fixedIborMaster.getGenerator(Generators.USD6MLIBOR1M, baseCalendar));
		_generators.put(Pair.of("FIXED","USDLIBOR3M"), _fixedIborMaster.getGenerator(Generators.USD6MLIBOR3M, baseCalendar));
		_generators.put(Pair.of("FIXED", "USDLIBOR6M"), _fixedIborMaster.getGenerator(Generators.USD6MLIBOR6M, baseCalendar));
		
		_generators.put(Pair.of("FIXED", "EURIBOR1M"), _fixedIborMaster.getGenerator(Generators.EUR1YEURIBOR1M, baseCalendar));
		_generators.put(Pair.of("FIXED", "EURIBOR3M"), _fixedIborMaster.getGenerator(Generators.EUR1YEURIBOR3M, baseCalendar));
		_generators.put(Pair.of("FIXED", "EURIBOR6M"), _fixedIborMaster.getGenerator(Generators.EUR1YEURIBOR6M, baseCalendar));
		
		_generators.put(Pair.of("FIXED", "GBPLIBOR6M"), _fixedIborMaster.getGenerator(Generators.GBP6MLIBOR6M, baseCalendar));
		_generators.put(Pair.of("FIXED", "GBPLIBOR3M"), _fixedIborMaster.getGenerator(Generators.GBP1YLIBOR3M, baseCalendar));
		
		_generators.put(Pair.of("FIXED", "JPYLIBOR6M"), _fixedIborMaster.getGenerator(Generators.JPY6MLIBOR6M, baseCalendar));
		
		_generators.put(Pair.of("FIXED", "AUDBB6M"), _fixedIborMaster.getGenerator(Generators.AUD6MBBSW6M, baseCalendar));
		//fixedON
		_generators.put(Pair.of("FIXED","FED FUND"), _fixedONMaster.getGenerator(Generators.USD1YFEDFUND, baseCalendar));
		_generators.put(Pair.of("FIXED","EONIA"), _fixedONMaster.getGenerator(Generators.EUR1YEONIA, baseCalendar));
		_generators.put(Pair.of("FIXED","SONIA"), _fixedONMaster.getGenerator(Generators.GBP1YSONIA, baseCalendar));
		_generators.put(Pair.of("FIXED","TONAR"), _fixedONMaster.getGenerator(Generators.JPY1YTONAR, baseCalendar));
		_generators.put(Pair.of("FIXED","RBA ON"), _fixedONMaster.getGenerator(Generators.AUD1YRBAON, baseCalendar));
		//iborIbor
		_generators.put(Pair.of("USDLIBOR3M","USDLIBOR1M"), _iborIborMaster.getGenerator(Generators.USD1M3M, baseCalendar));
		_generators.put(Pair.of("USDLIBOR3M","USDLIBOR6M"), _iborIborMaster.getGenerator(Generators.USD3M6M, baseCalendar));
		
		_generators.put(Pair.of("EURIBOR3M","EURIBOR1M"), _iborIborMaster.getGenerator(Generators.EUR1M3M, baseCalendar));
		_generators.put(Pair.of("EURIBOR3M","EURIBOR6M"), _iborIborMaster.getGenerator(Generators.EUR3M6M, baseCalendar));
		
		_generators.put(Pair.of("GBPLIBOR1M","GBPLIBOR3M"), _iborIborMaster.getGenerator(Generators.GBP1M3M, baseCalendar));
		_generators.put(Pair.of("GBPLIBOR3M","GBPLIBOR6M"), _iborIborMaster.getGenerator(Generators.GBP3M6M, baseCalendar));
		
		_generators.put(Pair.of("JPYLIBOR1M", "JPYLIBOR3M"), _iborIborMaster.getGenerator(Generators.JPY1M3M, baseCalendar));
		_generators.put(Pair.of("JPYLIBOR3M", "JPYLIBOR6M"), _iborIborMaster.getGenerator(Generators.JPY3M6M, baseCalendar));
		//iborON
		_generators.put(Pair.of("USDLIBOR3M", "FED FUND"), _iborONMaster.getGenerator(Generators.USD3MON, baseCalendar));
		//XCcyIborIbor
		_generators.put(Pair.of("USDLIBOR3M", "EURIBOR3M"), _xCcyIborIborMaster.getGenerator(Generators.EUR3MUSD3M, baseCalendar, baseCalendar));
		_generators.put(Pair.of("USDLIBOR3M", "GBPLIBOR3M"), _xCcyIborIborMaster.getGenerator(Generators.GBP3MUSD3M, baseCalendar, baseCalendar));
		_generators.put(Pair.of("USDLIBOR3M", "JPYLIBOR3M"), _xCcyIborIborMaster.getGenerator(Generators.JPY3MUSD3M, baseCalendar, baseCalendar));
	}
	
	public GeneratorInstrument<? extends GeneratorAttribute> getGenerator(final Pair<String, String> assetPair, final InstrumentDescription tradeType, final Calendar cal) throws UnsupportedOperation {
		GeneratorInstrument<? extends GeneratorAttribute> generatorNoCalendar = _generators.get(assetPair);
		if (generatorNoCalendar == null) {
	    	generatorNoCalendar = _generators.get(invertPair(assetPair));
	    }
	    if (generatorNoCalendar == null) {
	    	throw new OpenGammaRuntimeException("Could not get market standard generator for asset pair:" + assetPair);
	    }
	    
	    switch (tradeType) {
	    	case FIXED_IBOR_SWAP: {
	    		GeneratorSwapFixedIbor generatorNoCalendarCast = (GeneratorSwapFixedIbor) generatorNoCalendar;
	    		return new GeneratorSwapFixedIbor(generatorNoCalendarCast.getName(), generatorNoCalendarCast.getFixedLegPeriod(), generatorNoCalendarCast.getFixedLegDayCount(),
	    				generatorNoCalendarCast.getIborIndex(), cal);
	    	}
	    	case FIXED_ON_SWAP: { 
	    		GeneratorSwapFixedON generatorNoCalendarCast = (GeneratorSwapFixedON) generatorNoCalendar;
		    	return new GeneratorSwapFixedON(generatorNoCalendarCast.getName(), generatorNoCalendarCast.getIndex(), generatorNoCalendarCast.getLegsPeriod(), generatorNoCalendarCast.getFixedLegDayCount(),
		    			generatorNoCalendarCast.getBusinessDayConvention(), generatorNoCalendarCast.isEndOfMonth(), generatorNoCalendarCast.getSpotLag(), generatorNoCalendarCast.getPaymentLag(), cal);
	    	} 
	    	case IBOR_IBOR_SWAP: {
	    		GeneratorSwapIborIbor generatorNoCalendarCast = (GeneratorSwapIborIbor) generatorNoCalendar;
	    		return new GeneratorSwapIborIbor(generatorNoCalendarCast.getName(), generatorNoCalendarCast.getIborIndex1(), generatorNoCalendarCast.getIborIndex2(), generatorNoCalendarCast.getBusinessDayConvention(),
	    				generatorNoCalendarCast.isEndOfMonth(), generatorNoCalendarCast.getSpotLag(), cal, cal);
	    	}
	    	case IBOR_ON_SWAP: {
	    		GeneratorSwapIborON generatorNoCalendarCast = (GeneratorSwapIborON) generatorNoCalendar;
		    	return new GeneratorSwapIborON(generatorNoCalendar.getName(), generatorNoCalendarCast.getIndexIbor(), generatorNoCalendarCast.getIndexON(), generatorNoCalendarCast.getIndexIbor().getBusinessDayConvention(), 
		    			generatorNoCalendarCast.getIndexIbor().isEndOfMonth(), generatorNoCalendarCast.getIndexIbor().getSpotLag(), cal,cal);
	    	}
	    	case XCCY_IBOR_IBOR_SWAP: {
	    		GeneratorSwapXCcyIborIbor generatorNoCalendarCast = (GeneratorSwapXCcyIborIbor) generatorNoCalendar;
		    	return new GeneratorSwapXCcyIborIbor(generatorNoCalendar.getName(), generatorNoCalendarCast.getIborIndex1(), generatorNoCalendarCast.getIborIndex2(), generatorNoCalendarCast.getIborIndex2().getBusinessDayConvention(),
		    			generatorNoCalendarCast.getIborIndex2().isEndOfMonth(), generatorNoCalendarCast.getIborIndex2().getSpotLag(), cal, cal);
	    	}
	    	case XCCY_FIXED_IBOR_SWAP: {
	    		throw new UnsupportedOperation("Trade type "+tradeType+" not yet implemented in market standard generators");
	    	}
	    	default:
	    		throw new UnsupportedOperation("Trade type "+tradeType+" not yet implemented in market standard generators");
	    }
	    
	    /*
	    if (tradeType.equals(InstrumentDescription.FIXED_IBOR_SWAP)) {
	    	GeneratorSwapFixedIbor generatorNoCalendarCast = (GeneratorSwapFixedIbor) generatorNoCalendar;
	    	return new GeneratorSwapFixedIbor(generatorNoCalendarCast.getName(), generatorNoCalendarCast.getFixedLegPeriod(), generatorNoCalendarCast.getFixedLegDayCount(),
	    			generatorNoCalendarCast.getIborIndex(), cal);
	    } else if (tradeType.equals("FIXED_ON")) {
	    	GeneratorSwapFixedON generatorNoCalendarCast = (GeneratorSwapFixedON) generatorNoCalendar;
	    	return new GeneratorSwapFixedON(generatorNoCalendarCast.getName(), generatorNoCalendarCast.getIndex(), generatorNoCalendarCast.getLegsPeriod(), generatorNoCalendarCast.getFixedLegDayCount(),
	    			generatorNoCalendarCast.getBusinessDayConvention(), generatorNoCalendarCast.isEndOfMonth(), generatorNoCalendarCast.getSpotLag(), generatorNoCalendarCast.getPaymentLag(), cal);
	    } else if (tradeType.equals("IBOR_IBOR")) {
	    	GeneratorSwapIborIbor generatorNoCalendarCast = (GeneratorSwapIborIbor) generatorNoCalendar;
	    	return new GeneratorSwapIborIbor(generatorNoCalendarCast.getName(), generatorNoCalendarCast.getIborIndex1(), generatorNoCalendarCast.getIborIndex2(), cal, cal);
	    } else if (tradeType.equals("IBOR_ON")) {
	    	GeneratorSwapIborON generatorNoCalendarCast = (GeneratorSwapIborON) generatorNoCalendar;
	    	return new GeneratorSwapIborON(generatorNoCalendar.getName(), generatorNoCalendarCast.getIndexIbor(), generatorNoCalendarCast.getIndexON(), generatorNoCalendarCast.getIndexIbor().getBusinessDayConvention(), 
	    			generatorNoCalendarCast.getIndexIbor().isEndOfMonth(), generatorNoCalendarCast.getIndexIbor().getSpotLag(), cal,cal); 
	    } else if (tradeType.equals("XCCY_IBOR_IBOR")) {
	    	GeneratorSwapXCcyIborIbor generatorNoCalendarCast = (GeneratorSwapXCcyIborIbor) generatorNoCalendar;
	    	return new GeneratorSwapXCcyIborIbor(generatorNoCalendar.getName(), generatorNoCalendarCast.getIborIndex1(), generatorNoCalendarCast.getIborIndex2(), generatorNoCalendarCast.getIborIndex1().getBusinessDayConvention(),
	    			generatorNoCalendarCast.getIborIndex1().isEndOfMonth(), generatorNoCalendarCast.getIborIndex1().getSpotLag(), cal, cal);
	    } else if (tradeType.equals("XCCY_FIXED_IBOR")) {
	    	//TODO
	    	return null;
	    } else {
	    	throw new IllegalArgumentException("Unknown trade type: "+tradeType);
	    }*/
	}	
	
	private Pair<String, String> invertPair(Pair<String, String> assetPair) {
		return Pair.of(assetPair.getSecond(), assetPair.getFirst());
	}
	
}
