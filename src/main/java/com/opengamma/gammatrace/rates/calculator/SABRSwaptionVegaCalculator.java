package com.opengamma.gammatrace.rates.calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.opengamma.analytics.financial.interestrate.InstrumentDerivative;
import com.opengamma.analytics.financial.interestrate.PresentValueSABRSensitivityDataBundle;
import com.opengamma.analytics.financial.interestrate.SABRSensitivityNodeCalculator;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.analytics.financial.provider.calculator.sabrswaption.PresentValueSABRSensitivitySABRSwaptionCalculator;
import com.opengamma.analytics.financial.provider.description.interestrate.SABRSwaptionProviderDiscount;
import com.opengamma.analytics.math.surface.InterpolatedDoublesSurface;
import com.opengamma.analytics.util.amount.SurfaceValue;
import com.opengamma.gammatrace.instrument.SwaptionInstrument;
import com.opengamma.gammatrace.prod.VegaMatrix;
import com.opengamma.util.tuple.DoublesPair;

public class SABRSwaptionVegaCalculator {
	private SABRSwaptionProviderDiscount sabrMulticurves;
	private PresentValueSABRSensitivityDataBundle vega = new PresentValueSABRSensitivityDataBundle();
	
	private static final PresentValueSABRSensitivitySABRSwaptionCalculator PVSSSSC = PresentValueSABRSensitivitySABRSwaptionCalculator.getInstance();
	
	public SABRSwaptionVegaCalculator(InstrumentDerivative derivative, SABRSwaptionProviderDiscount sabrMulticurves) {
		this.sabrMulticurves = sabrMulticurves;
		vega = SABRSensitivityNodeCalculator.calculateNodeSensitivities(PVSSSSC.visit(derivative, sabrMulticurves), sabrMulticurves.getSABRParameter());	// the full matrix
	}
	
	public SABRSwaptionVegaCalculator(SwaptionInstrument swaptionInstrument, SABRSwaptionProviderDiscount sabrMulticurves) {
		this.sabrMulticurves = sabrMulticurves;
		for (InstrumentDerivative eachSwaption : swaptionInstrument.getDerivative()) {
			vega = vega.plus(SABRSensitivityNodeCalculator.calculateNodeSensitivities(PVSSSSC.visit(eachSwaption, sabrMulticurves), sabrMulticurves.getSABRParameter())); // the full matrix
		}
	}
	
	public VegaMatrix getVegaMatrix() {
		VegaMatrix vegaMatrix = new VegaMatrix();
		vegaMatrix.setAlpha(new SABRSwaptionVegaStructure(extractDataColumn(new Alpha(), new Expiry()), extractDataColumn(new Alpha(), new Tenor()), flushBlanks(vega.getAlpha())));
		vegaMatrix.setBeta(new SABRSwaptionVegaStructure(extractDataColumn(new Beta(), new Expiry()), extractDataColumn(new Beta(), new Tenor()), flushBlanks(vega.getBeta())));
		vegaMatrix.setRho(new SABRSwaptionVegaStructure(extractDataColumn(new Rho(), new Expiry()), extractDataColumn(new Rho(), new Tenor()), flushBlanks(vega.getRho())));
		vegaMatrix.setNu(new SABRSwaptionVegaStructure(extractDataColumn(new Nu(), new Expiry()), extractDataColumn(new Nu(), new Tenor()), flushBlanks(vega.getNu())));
		return vegaMatrix;
	}
	
	private SurfaceValue flushBlanks(SurfaceValue input) {
		SurfaceValue flushed = new SurfaceValue(); 
		for (Map.Entry<DoublesPair, Double> entry : input.getMap().entrySet()) {
			if (entry.getValue() != 0) {
				flushed.add(entry.getKey(), entry.getValue());
			}
		}
		return flushed;
	}
	
	public interface SurfaceCommand {
		public InterpolatedDoublesSurface execute();
	}
	
	public class Alpha implements SurfaceCommand {
		public InterpolatedDoublesSurface execute() {
			return sabrMulticurves.getSABRParameter().getAlphaSurface();
		}
	}
	public class Beta implements SurfaceCommand {
		public InterpolatedDoublesSurface execute() {
			return sabrMulticurves.getSABRParameter().getBetaSurface();
		}
	}
	public class Rho implements SurfaceCommand {
		public InterpolatedDoublesSurface execute() {
			return sabrMulticurves.getSABRParameter().getBetaSurface();
		}
	}
	public class Nu implements SurfaceCommand {
		public InterpolatedDoublesSurface execute() {
			return sabrMulticurves.getSABRParameter().getBetaSurface();
		}
	}
	public interface ColumnCommand {
		public Double[] execute(InterpolatedDoublesSurface surface);
	}
	public class Expiry implements ColumnCommand {
		public Double[] execute(InterpolatedDoublesSurface surface) {
			return surface.getXData();
		}
	}
	public class Tenor implements ColumnCommand {
		public Double[] execute(InterpolatedDoublesSurface surface) {
			return surface.getYData();
		}
	}
	
	public Double[] extractExpiry(SurfaceCommand surface) {		// what if e.g. the tenor points are different for different expiries?
		Double[] data = surface.execute().getXData();
		ArrayList<Double> list= new ArrayList<>();
		int arrayCounter = 0;
		while (!list.contains(data[arrayCounter])) {
			list.add(data[arrayCounter]);
			arrayCounter++;
		}
		return list.toArray(new Double[list.size()]);
	}
	
	public Double[] extractDataColumn(SurfaceCommand surface, ColumnCommand column) {
		Double[] data = column.execute(surface.execute());
		Set<Double> set = new LinkedHashSet<>(Arrays.asList(data));
		return set.toArray(new Double[set.size()]);
	}
	
	
	
	public PresentValueSABRSensitivityDataBundle getVega() {
		return vega;
	}
}
