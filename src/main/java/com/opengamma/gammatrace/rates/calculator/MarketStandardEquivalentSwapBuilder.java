package com.opengamma.gammatrace.rates.calculator;

import java.util.ArrayList;
import java.util.List;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttribute;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeFX;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeIR;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.payment.CouponONSpreadDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedONDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapIborIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapXCcyIborIborDefinition;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.exceptions.UnsupportedOperation;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.ogextension.GeneratorSwapXCcyFixedIbor;
import com.opengamma.gammatrace.ogextension.SwapONSpreadIborSpreadDefinition;
import com.opengamma.gammatrace.ogextension.SwapXCcyFixedIborDefinition;
import com.opengamma.gammatrace.rates.instrument.FixingBuilder;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;

public class MarketStandardEquivalentSwapBuilder {

	private ZonedDateTime effectiveDate;
	private ZonedDateTime endDate;
	private CurrencyAmount firstNotional;
	private CurrencyAmount secondNotional;
	private ZonedDateTime tradeDate;
	private InstrumentDescription tradeType;
	private Pair<String, String> assetPair;
	private MulticurveProviderDiscount curve;
	
	public MarketStandardEquivalentSwapBuilder(SwapInstrument instrument, MulticurveProviderDiscount curve) {
		effectiveDate = instrument.getEffectiveDate();
		endDate = instrument.getEndDate();
		firstNotional = instrument.getFirstNotional();
		secondNotional = instrument.getSecondNotional();
		tradeDate = instrument.getTradeDate();
		tradeType = instrument.getInstrumentDescription();
		assetPair = instrument.getAssetPair();
		this.curve = curve;
	}
	
	public List<Swap<?, ?>> makeMarketStandardEquivalent() throws UnsupportedOperation {
		MarketStandardGeneratorMapper generatorMap = MarketStandardGeneratorMapper.getInstance();
		Calendar UTC = new MondayToFridayCalendar("UTC");
		
		GeneratorInstrument<? extends GeneratorAttribute> generator = generatorMap.getGenerator(assetPair, tradeType, UTC);
		
		List<Swap<?, ?>> standardisedSwaps = new ArrayList<>();
		if (effectiveDate.isAfter(tradeDate.plus(Period.ofDays(10)))){ // ... is there a less arbitrary way of doing this?
			standardisedSwaps.add(makeStandardInstrumentFromGenerator(generator, effectiveDate, endDate, true));		// the forward starting trade (for timeseries charts)
			standardisedSwaps.add(makeStandardInstrumentFromGenerator(generator, tradeDate, endDate, true)); 			// the longer trade, same direction (for termstructure charts)
			standardisedSwaps.add(makeStandardInstrumentFromGenerator(generator, tradeDate, effectiveDate, false));		// the shorter trade, opposite direction (for termstructure charts)
		} 	else {
			standardisedSwaps.add(makeStandardInstrumentFromGenerator(generator, effectiveDate, endDate, true));		
		}
		
		return standardisedSwaps;
	}

	private Swap<?, ?> makeStandardInstrumentFromGenerator(GeneratorInstrument<? extends GeneratorAttribute> generator, ZonedDateTime effectiveDateGuess, ZonedDateTime endDate, boolean payer) throws UnsupportedOperation {
		Swap<?, ?> swap;
		/* using generator.generateInstrument(..) applies the spot lag to the effectiveDate, hence bumping the date forward incorrectly when we give this method the forward starting trade. Therefore we use definition.from(..),
		 * and manually adjust the spotLag days when building the 2 part swaps.
		 */
		if (generator instanceof GeneratorSwapFixedIbor) {
			GeneratorSwapFixedIbor generatorCast = (GeneratorSwapFixedIbor) generator;
			ZonedDateTime settlementDate = (effectiveDateGuess.equals(this.effectiveDate))? effectiveDateGuess : ScheduleCalculator.getAdjustedDate(effectiveDateGuess, generatorCast.getSpotLag(), generatorCast.getCalendar());
			SwapFixedIborDefinition swapDefinition = SwapFixedIborDefinition.from(settlementDate, endDate, generatorCast, firstNotional.getAmount(), 0.0, payer);
			ZonedDateTimeDoubleTimeSeries[] fixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate);
			swap = swapDefinition.toDerivative(tradeDate, fixing);
			
		} else if (generator instanceof GeneratorSwapFixedON) {
			GeneratorSwapFixedON generatorCast = (GeneratorSwapFixedON) generator;
			ZonedDateTime settlementDate = (effectiveDateGuess.equals(this.effectiveDate))? effectiveDateGuess : ScheduleCalculator.getAdjustedDate(effectiveDateGuess, generatorCast.getSpotLag(), generatorCast.getOvernightCalendar());
			SwapFixedONDefinition swapDefinition = SwapFixedONDefinition.from(settlementDate, endDate, firstNotional.getAmount(), generatorCast, 0.0, payer);
			ZonedDateTimeDoubleTimeSeries[] fixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate);
			swap = swapDefinition.toDerivative(tradeDate, fixing);
		
		} else if (generator instanceof GeneratorSwapIborIbor) {
			// SwapIborIborDefinition doesn't have a from method that takes endDate and generator, so we manually construct from the annuities here.
			GeneratorSwapIborIbor generatorCast = (GeneratorSwapIborIbor) generator;
			ZonedDateTime settlementDate = (effectiveDateGuess.equals(this.effectiveDate))? effectiveDateGuess : ScheduleCalculator.getAdjustedDate(effectiveDateGuess, generatorCast.getSpotLag(), generatorCast.getCalendar1());
			AnnuityCouponIborSpreadDefinition firstLeg = AnnuityCouponIborSpreadDefinition.from(settlementDate, endDate, firstNotional.getAmount(), generatorCast.getIborIndex1(), 0.0,
				      payer, generatorCast.getCalendar1());
			AnnuityCouponIborSpreadDefinition secondLeg = AnnuityCouponIborSpreadDefinition.from(settlementDate, endDate, secondNotional.getAmount(), generatorCast.getIborIndex2(), 0.0,
				      !payer, generatorCast.getCalendar2());
			SwapIborIborDefinition swapDefinition = new SwapIborIborDefinition(firstLeg, secondLeg);
			ZonedDateTimeDoubleTimeSeries[] fixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate);
			swap = swapDefinition.toDerivative(tradeDate, fixing);
		
		} else if (generator instanceof GeneratorSwapIborON) {
			// have to use this class to make the first leg the OIS leg, so that when using PresentValueMarketQuoteSensitivityDiscountingCalculator, we get the pv01 of the OIS leg (which is where the spread is normally quoted)
			GeneratorSwapIborON generatorCast = (GeneratorSwapIborON) generator;
			ZonedDateTime settlementDate = (effectiveDateGuess.equals(this.effectiveDate))? effectiveDateGuess : ScheduleCalculator.getAdjustedDate(effectiveDateGuess, generatorCast.getSpotLag(), generatorCast.getIborCalendar());
			
			
			SwapONSpreadIborSpreadDefinition swapDefinition = SwapONSpreadIborSpreadDefinition.from(settlementDate, endDate, firstNotional.getAmount(), 0.0, 0.0, generatorCast, payer);
			ZonedDateTimeDoubleTimeSeries[] fixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate);
			swap = swapDefinition.toDerivative(tradeDate, fixing);
		
		} else if (generator instanceof GeneratorSwapXCcyIborIbor) {
			/* If the legs in the generator are in the other order to the trade, then the first/second notionals need to be switched for any trade where the first and second notionals are not necessarily the same (like xccy swaps). 
			 * TODO: change this comment..
			 * We leave the FX rate in the market equivalent trade the same as the actual trade in order to isolate the PV explanation to just 1 parameter (xccy basis)
			 * Consider a trade that has PV +10.05% and spread 0.10%, but PV from FX = +10% and PV from basis = +0.05%. Assume pv01 = 5. Our model has a market spread of 0.09%, but the trade is implying 0.10%. 
			 * Calculation will be: - ( PV of equiv trade with 0 spread - PV of trade ) / pv01 = -(9.55% - 10.05% ) / 5 = 0.10% giving the desired result.
			 * Note this whole calculation in the end is really just adjusting for daycount differences between trade and standard quote (and upfronts of course).
			 */
			
			GeneratorSwapXCcyIborIbor generatorCast = (GeneratorSwapXCcyIborIbor) generator;
			ZonedDateTime settlementDate = (effectiveDateGuess.equals(this.effectiveDate))? effectiveDateGuess : ScheduleCalculator.getAdjustedDate(effectiveDateGuess, generatorCast.getSpotLag(), generatorCast.getCalendar2());
			double notional1, notional2 = 0;
			
			
			
			if (firstNotional.getCurrency().equals(generatorCast.getIborIndex1().getCurrency())) {
				notional1 = firstNotional.getAmount();
				notional2 = firstNotional.getAmount() * curve.getFxRate(generatorCast.getIborIndex1().getCurrency(), generatorCast.getIborIndex2().getCurrency());
			} else {
				notional1 = secondNotional.getAmount();
				notional2 = secondNotional.getAmount() * curve.getFxRate(generatorCast.getIborIndex1().getCurrency(), generatorCast.getIborIndex2().getCurrency());
				//notional2 = secondNotional.getAmount() * curve.getFxRate(((GeneratorSwapXCcyIborIbor) generator).getIborIndex1().getCurrency(), ((GeneratorSwapXCcyIborIbor) generator).getIborIndex2().getCurrency());
			}
			SwapXCcyIborIborDefinition swapDefinition = SwapXCcyIborIborDefinition.from(settlementDate, endDate, generatorCast, notional1, notional2, 0.0, 0.0, payer);
			ZonedDateTimeDoubleTimeSeries[] fixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate);
			swap = swapDefinition.toDerivative(tradeDate, fixing);
		
		} else {
			throw new UnsupportedOperation("Generator type: "+generator+" should not be used to make a market standard equivalent swap");
		}
		return swap;
	}
}
