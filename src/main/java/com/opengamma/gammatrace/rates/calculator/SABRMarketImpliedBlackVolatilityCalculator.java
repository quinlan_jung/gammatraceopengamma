package com.opengamma.gammatrace.rates.calculator;

import com.opengamma.analytics.financial.model.option.pricing.analytic.formula.EuropeanVanillaOption;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParRateDiscountingCalculator;
import com.opengamma.analytics.financial.provider.description.interestrate.SABRSwaptionProviderDiscount;
import com.opengamma.analytics.util.time.TimeCalculator;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.instrument.SwaptionInstrument;

public class SABRMarketImpliedBlackVolatilityCalculator {
	private static final ParRateDiscountingCalculator PRDC = ParRateDiscountingCalculator.getInstance();
	
	public static double getSABRMarketImpliedBlackVolatility(SwaptionInstrument instrument, SABRSwaptionProviderDiscount sabrMulticurves) {
		double expiry = TimeCalculator.getTimeBetween(instrument.getExecutionTimestamp(), instrument.getEffectiveDate());
		double tenor = TimeCalculator.getTimeBetween(instrument.getExecutionTimestamp(), instrument.getEndDate());	// SABRSwaptionProviderDiscount seems to be built from expiry vs. tenor, so a 1y2y swaption would have x = 1.0 and y = 2.0 NOT the times from now i.e. x = 1.0 y = 3.0
		
		EuropeanVanillaOption[] derivative = (EuropeanVanillaOption[]) instrument.getDerivative();
		double strike = derivative[0].getStrike();
		SwapInstrument[] underlyingSwapInstruments = instrument.getUnderlyingSwaps();
		double forward = underlyingSwapInstruments[0].getDerivative().accept(PRDC, sabrMulticurves.getMulticurveProvider());
		return sabrMulticurves.getSABRParameter().getVolatility(expiry, tenor, strike, forward);
	}	
}
