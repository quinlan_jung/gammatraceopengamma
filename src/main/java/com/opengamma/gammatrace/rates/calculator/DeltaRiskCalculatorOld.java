package com.opengamma.gammatrace.rates.calculator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.interestrate.InstrumentDerivative;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionCashFixedIbor;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.generic.MarketQuoteSensitivityBlockCalculator;
import com.opengamma.analytics.financial.provider.calculator.sabrswaption.PresentValueCurveSensitivitySABRSwaptionCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderInterface;
import com.opengamma.analytics.financial.provider.description.interestrate.SABRSwaptionProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.SABRSwaptionProviderInterface;
import com.opengamma.analytics.financial.provider.sensitivity.multicurve.MultipleCurrencyParameterSensitivity;
import com.opengamma.analytics.financial.provider.sensitivity.parameter.ParameterSensitivityParameterCalculator;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.analytics.math.matrix.DoubleMatrix1D;
import com.opengamma.analytics.math.matrix.MatrixAlgebra;
import com.opengamma.analytics.math.matrix.MatrixAlgebraFactory;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.gammatrace.prod.CurveDelta;
import com.opengamma.gammatrace.prod.DeltaResults;
import com.opengamma.util.ArgumentChecker;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;
import com.opengamma.util.tuple.Triple;

public class DeltaRiskCalculatorOld {

	private HashMap<String, Pair<Currency, String>> curveRelations;
	private String[] curveNames;
	private LinkedHashMap<String, List<Triple<Period, QuoteInstrumentType, Double>>> fullDelta;
	private static final double BASIS_POINT = 1e-4; 
	
	public DeltaRiskCalculatorOld(Swap<?, ?> derivative, Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> market, DeltaDisplayBundle deltaBundle) {
		curveRelations = deltaBundle.getCurveRelations();
		curveNames = deltaBundle.getCurveNames();
		fullDelta = calculate(derivative, market, deltaBundle);
	}
	@Deprecated
	public DeltaRiskCalculatorOld(SwaptionPhysicalFixedIbor derivative, SABRSwaptionProviderDiscount sabrMulticurves, CurveBuildingBlockBundle curveBundle, DeltaDisplayBundle deltaBundle) {
		curveRelations = deltaBundle.getCurveRelations();
		curveNames = deltaBundle.getCurveNames();
		fullDelta = calculate(derivative, sabrMulticurves, curveBundle, deltaBundle);
	}
	
	public DeltaRiskCalculatorOld(SwaptionPhysicalFixedIbor[] derivative, SABRSwaptionProviderDiscount sabrMulticurves, CurveBuildingBlockBundle curveBundle, DeltaDisplayBundle deltaBundle) {
		curveRelations = deltaBundle.getCurveRelations();
		curveNames = deltaBundle.getCurveNames();
		fullDelta = calculate(derivative, sabrMulticurves, curveBundle, deltaBundle);
	}
	
	public DeltaRiskCalculatorOld(SwaptionCashFixedIbor[] derivative, SABRSwaptionProviderDiscount sabrMulticurves, CurveBuildingBlockBundle curveBundle, DeltaDisplayBundle deltaBundle) {
		curveRelations = deltaBundle.getCurveRelations();
		curveNames = deltaBundle.getCurveNames();
		fullDelta = calculate(derivative, sabrMulticurves, curveBundle, deltaBundle);
	}
	
	
	/**
	 * Extracts the delta for curves against a fixed rate.
	 * @return The DeltaResults.
	 */
	public DeltaResults extractFixedDelta() {
		LinkedHashMap<String, List<Triple<Period, QuoteInstrumentType, Double>>> fixedDelta = new LinkedHashMap<>();
		
		for (int i = 0; i < curveNames.length; i++) {
			if (curveRelations.get(curveNames[i]).getSecond().equalsIgnoreCase("Fixed")) {
				fixedDelta.put(curveNames[i], fullDelta.get(curveNames[i]));
			}
		}
		return makeDeltaResults(fixedDelta);
	}
	/**
	 * Extracts the delta for curves against another floating rate with the same currency.
	 * @return The DeltaResults.
	 */
	public DeltaResults extractSpreadDelta() {
		LinkedHashMap<String, List<Triple<Period, QuoteInstrumentType, Double>>> spreadDelta = new LinkedHashMap<>();
		
		for (int i = 0; i < curveNames.length; i++) {
			if (!curveRelations.get(curveNames[i]).getSecond().equalsIgnoreCase("Fixed") && !curveRelations.get(curveNames[i]).getSecond().equalsIgnoreCase("XCcy")) {
				String name = curveNames[i]+" - "+curveRelations.get(curveNames[i]).getSecond();
				spreadDelta.put(name, fullDelta.get(curveNames[i]));
			}
		}
		return makeDeltaResults(spreadDelta);
	}
	/**
	 * Extracts the delta for curves against another currency floating rate.
	 * @return The DeltaResults.
	 */
	public DeltaResults extractXCcySpreadDelta() {
		LinkedHashMap<String, List<Triple<Period, QuoteInstrumentType, Double>>> xCcySpreadDelta = new LinkedHashMap<>();
		
		for (int i = 0; i < curveNames.length; i++) {
			if (curveRelations.get(curveNames[i]).getSecond().equalsIgnoreCase("XCcy")) {
				xCcySpreadDelta.put(curveNames[i], fullDelta.get(curveNames[i]));
			}
		}
		
		return makeDeltaResults(xCcySpreadDelta);
	}
	/**
	 * Extracts the delta for curves against another currency floating rate.
	 * @return Pair containing the delta values mapped by the curve name and the periods corresponding to the sensitivity nodes
	 */
	@Deprecated
	public Pair<Map<String, List<Pair<QuoteInstrumentType, Double>>>, List<Period>> extractXCcySpreadDeltaOld() {
		LinkedHashMap<String, List<Triple<Period, QuoteInstrumentType, Double>>> xCcySpreadDelta = new LinkedHashMap<>();
		for (int i = 0; i < curveNames.length; i++) {
			if (curveRelations.get(curveNames[i]).getSecond().equalsIgnoreCase("XCcy")) {
				xCcySpreadDelta.put(curveNames[i], fullDelta.get(curveNames[i]));
			}
		}
		return unionizeNodes(xCcySpreadDelta);
	}
	
	private MultipleCurrencyParameterSensitivity testCalculate(Swap<?, ?> derivative, Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> market) {
		
		final PresentValueCurveSensitivityDiscountingCalculator PVCSC = PresentValueCurveSensitivityDiscountingCalculator.getInstance();
		final ParameterSensitivityParameterCalculator<MulticurveProviderInterface> PSC = new ParameterSensitivityParameterCalculator<>(PVCSC);
	
		return PSC.calculateSensitivity(derivative, market.getFirst(), market.getFirst().getAllNames());
	}

	
	
	/**
	 * Calculates the market quote sensitivity and attaches period nodes to the values.
	 * @param swap The instrument
	 * @param market The MulticurveProviderDiscount, CurveBuildingBlockBundle from makeCurve().
	 * @param deltaBundle The DeltaDisplayBundle containing extra info need to nicely display delta, made in the CurveMaker.
	 * @return A map from curve names to a list of node, value pairs.
	 */
	private LinkedHashMap<String, List<Triple<Period, QuoteInstrumentType, Double>>> calculate(Swap<?, ?> derivative, Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> market, 
			DeltaDisplayBundle deltaBundle) {
		
		final PresentValueCurveSensitivityDiscountingCalculator PVCSC = PresentValueCurveSensitivityDiscountingCalculator.getInstance();
		final ParameterSensitivityParameterCalculator<MulticurveProviderInterface> PSC = new ParameterSensitivityParameterCalculator<>(PVCSC);
		final MarketQuoteSensitivityBlockCalculator<MulticurveProviderInterface> MQSC = new MarketQuoteSensitivityBlockCalculator<>(PSC);
		
		MultipleCurrencyParameterSensitivity delta = MQSC.fromInstrument(derivative, market.getFirst(), market.getSecond());//.converted(market.getFirst().getFxRates(), displayCcy);
		MultipleCurrencyParameterSensitivity deltaConverted = convertToCurveCurrency(delta, market.getFirst().getFxRates());
		return makeFullDelta(deltaConverted, deltaBundle);
	}

	private LinkedHashMap<String, List<Triple<Period, QuoteInstrumentType, Double>>> calculate(InstrumentDerivative derivative, SABRSwaptionProviderDiscount sabrMulticurves, 
			CurveBuildingBlockBundle curveBundle, DeltaDisplayBundle deltaBundle) {
		
		final PresentValueCurveSensitivitySABRSwaptionCalculator PVCSSSC = PresentValueCurveSensitivitySABRSwaptionCalculator.getInstance();
		final ParameterSensitivityParameterCalculator<SABRSwaptionProviderInterface> PS_SS_C = new ParameterSensitivityParameterCalculator<>(PVCSSSC);
		final MarketQuoteSensitivityBlockCalculator<SABRSwaptionProviderInterface> MQSC = new MarketQuoteSensitivityBlockCalculator<>(PS_SS_C);
		
		MultipleCurrencyParameterSensitivity delta = MQSC.fromInstrument(derivative, sabrMulticurves, curveBundle);//.converted(sabrMulticurves.getMulticurveProvider().getFxRates(), displayCcy);
		MultipleCurrencyParameterSensitivity deltaConverted = convertToCurveCurrency(delta, sabrMulticurves.getMulticurveProvider().getFxRates());
		return makeFullDelta(deltaConverted, deltaBundle);
	}
	
	private LinkedHashMap<String, List<Triple<Period, QuoteInstrumentType, Double>>> calculate(InstrumentDerivative[] derivative, SABRSwaptionProviderDiscount sabrMulticurves, 
			CurveBuildingBlockBundle curveBundle, DeltaDisplayBundle deltaBundle) {
		
		final PresentValueCurveSensitivitySABRSwaptionCalculator PVCSSSC = PresentValueCurveSensitivitySABRSwaptionCalculator.getInstance();
		final ParameterSensitivityParameterCalculator<SABRSwaptionProviderInterface> PS_SS_C = new ParameterSensitivityParameterCalculator<>(PVCSSSC);
		final MarketQuoteSensitivityBlockCalculator<SABRSwaptionProviderInterface> MQSC = new MarketQuoteSensitivityBlockCalculator<>(PS_SS_C);
		
		MultipleCurrencyParameterSensitivity delta = new MultipleCurrencyParameterSensitivity();
		for (InstrumentDerivative eachSwaption : derivative) {
			delta = delta.plus(MQSC.fromInstrument(eachSwaption, sabrMulticurves, curveBundle));
		}
		MultipleCurrencyParameterSensitivity deltaConverted = convertToCurveCurrency(delta, sabrMulticurves.getMulticurveProvider().getFxRates());
		return makeFullDelta(deltaConverted, deltaBundle);
	}
	
	private LinkedHashMap<String, List<Triple<Period, QuoteInstrumentType, Double>>> makeFullDelta(MultipleCurrencyParameterSensitivity delta, DeltaDisplayBundle deltaBundle) {
		LinkedHashMap<String, List<Triple<Period, QuoteInstrumentType, Double>>> result = new LinkedHashMap<>();
		// loop over curves
		for (final Pair<String, Currency> nameCcy : delta.getAllNamesCurrency()) {
		  	final double[] array = delta.getSensitivity(nameCcy).getData();
		  	Period[] nodes = deltaBundle.getInstrumentTenors().get(nameCcy.getFirst());
		  	QuoteInstrumentType[] descriptions = deltaBundle.getQuoteInstrumentType().get(nameCcy.getFirst());
		  	List<Triple<Period, QuoteInstrumentType, Double>> list = new ArrayList<>();
		  	// loop over nodes
		  	for (int i = 0; i < array.length; i++) {
		  		list.add(Triple.of(nodes[i], descriptions[i], array[i] * BASIS_POINT));
		  	}
		  	result.put(nameCcy.getFirst(), list);		// put overwrites any entries in the map that are already there.. this becomes a problem if we have [xyz, EUR] and [xyz, USD] (because we are using just the name as key)
		}
		return result;
	}
	
	private MultipleCurrencyParameterSensitivity convertToCurveCurrency(final MultipleCurrencyParameterSensitivity rawDelta, final FXMatrix fxMatrix) {
		ArgumentChecker.notNull(rawDelta, "Delta input");
		ArgumentChecker.notNull(fxMatrix, "FX Matrix");
		MultipleCurrencyParameterSensitivity result = new MultipleCurrencyParameterSensitivity();
	    final MatrixAlgebra algebra = MatrixAlgebraFactory.COMMONS_ALGEBRA;
	    for (final Map.Entry<Pair<String, Currency>, DoubleMatrix1D> entry : rawDelta.getSensitivities().entrySet()) {
	    	final Pair<String, Currency> nameCcy = entry.getKey();
	    	Currency curveCurrency = curveRelations.get(nameCcy.getFirst()).getFirst();
	    	final double fxRate = fxMatrix.getFxRate(nameCcy.getSecond(), curveCurrency);
	    	final Pair<String, Currency> nameCcyNew = Pair.of(nameCcy.getFirst(), curveCurrency);
	    	final DoubleMatrix1D sensitivityNew = (DoubleMatrix1D) algebra.scale(entry.getValue(), fxRate);
	    	result = result.plus(nameCcyNew, sensitivityNew);
	    }
	    return result;
	}

	/**
	 * Creates the delta pair structure from the calculation output map, with the periods a union of all the periods of each curve.
	 * @param map A map from curve names to a list of node, value pairs.
	 * @return Pair containing the delta values mapped by the curve name and the periods corresponding to the sensitivity nodes.
	 */
	private Pair<Map<String, List<Pair<QuoteInstrumentType, Double>>>, List<Period>> unionizeNodes(LinkedHashMap<String, List<Triple<Period, QuoteInstrumentType, Double>>> map) {
		Set<Period> periodSet = new HashSet<>();
		List<Map<Period, Pair<QuoteInstrumentType, Double>>> values = new ArrayList<>();
		
		for (Map.Entry<String, List<Triple<Period, QuoteInstrumentType, Double>>> eachCurve : map.entrySet()) {
			Map<Period, Pair<QuoteInstrumentType, Double>> tempValues = new HashMap<>();
			for (Triple<Period, QuoteInstrumentType, Double> valuesPair : eachCurve.getValue()) {
				periodSet.add(valuesPair.getFirst());
				tempValues.put(valuesPair.getFirst(), Pair.of(valuesPair.getSecond(), valuesPair.getThird()));
			}
			values.add(tempValues);
		}
		
		List<Period> periodList = new ArrayList<>(periodSet);
		
		Collections.sort(periodList, new Comparator<Period>() {
			public int compare(Period p1, Period p2) {
				ZonedDateTime someDate = ZonedDateTime.now();
				ZonedDateTime d1 = ScheduleCalculator.getAdjustedDate(someDate, p1, BusinessDayConventionFactory.INSTANCE
			            .getBusinessDayConvention("Modified Following"), new MondayToFridayCalendar("NYC"));
				ZonedDateTime d2 = ScheduleCalculator.getAdjustedDate(someDate, p2, BusinessDayConventionFactory.INSTANCE
			            .getBusinessDayConvention("Modified Following"), new MondayToFridayCalendar("NYC"));
				return d1.compareTo(d2);
			}
		} ); 
		
		Map<String, List<Pair<QuoteInstrumentType, Double>>> resultMap = new HashMap<>();
		int curveCounter = 0;
		for (Map.Entry<String, List<Triple<Period, QuoteInstrumentType, Double>>> eachCurve : map.entrySet()) {
			List<Pair<QuoteInstrumentType, Double>> tempValues = new ArrayList<>();
			Map<Period, Pair<QuoteInstrumentType, Double>> tempMap = values.get(curveCounter);
			for (Period p : periodList) {
				tempValues.add(tempMap.get(p));
			}
			resultMap.put(eachCurve.getKey(), tempValues);
			curveCounter++;
		}
		Pair<Map<String, List<Pair<QuoteInstrumentType, Double>>>, List<Period>> result = Pair.of(resultMap, periodList);
		return result;
	}
	
	
	private DeltaResults makeDeltaResults(LinkedHashMap<String, List<Triple<Period, QuoteInstrumentType, Double>>> map) {
		Set<Period> periodSet = new HashSet<>();
		List<Map<Period, Pair<QuoteInstrumentType, Double>>> values = new ArrayList<>();
		
		for (Map.Entry<String, List<Triple<Period, QuoteInstrumentType, Double>>> eachCurve : map.entrySet()) {
			Map<Period, Pair<QuoteInstrumentType, Double>> tempValues = new HashMap<>();
			for (Triple<Period, QuoteInstrumentType, Double> valuesTriple : eachCurve.getValue()) {
				periodSet.add(valuesTriple.getFirst());
				tempValues.put(valuesTriple.getFirst(), Pair.of(valuesTriple.getSecond(), valuesTriple.getThird()));		// we make values here to save having to do this loop again over the Triple again.
			}
			values.add(tempValues);
		}
		
		List<Period> periodList = new ArrayList<>(periodSet);
		Collections.sort(periodList, new Comparator<Period>() {
			public int compare(Period p1, Period p2) {
				ZonedDateTime someDate = ZonedDateTime.now();
				ZonedDateTime d1 = ScheduleCalculator.getAdjustedDate(someDate, p1, BusinessDayConventionFactory.INSTANCE
			            .getBusinessDayConvention("Modified Following"), new MondayToFridayCalendar("NYC"));
				ZonedDateTime d2 = ScheduleCalculator.getAdjustedDate(someDate, p2, BusinessDayConventionFactory.INSTANCE
			            .getBusinessDayConvention("Modified Following"), new MondayToFridayCalendar("NYC"));
				return d1.compareTo(d2);
			}
		} ); 
		
		DeltaResults results = new DeltaResults(periodList);	
		int curveCounter = 0;
		for (Map.Entry<String, List<Triple<Period, QuoteInstrumentType, Double>>> eachCurve : map.entrySet()) {
			String name = eachCurve.getKey();
			List<String> quoteInstrumentTypes = new ArrayList<>();
			List<Double> deltaValues = new ArrayList<>();
			Map<Period, Pair<QuoteInstrumentType, Double>> tempMap = values.get(curveCounter);
			for (Period p : periodList) {
				if (tempMap.get(p) == null) {
					quoteInstrumentTypes.add(null);
					deltaValues.add(null);
				} else {
					quoteInstrumentTypes.add(tempMap.get(p).getFirst().toString());
					deltaValues.add(tempMap.get(p).getSecond());	
				}
			}
			results.addCurveDelta(new CurveDelta(name, quoteInstrumentTypes, deltaValues));
			curveCounter++;
		}
		return results;		
	}
}
