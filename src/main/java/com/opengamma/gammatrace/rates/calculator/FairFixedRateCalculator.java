package com.opengamma.gammatrace.rates.calculator;

import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Coupon;
import com.opengamma.analytics.financial.interestrate.payments.derivative.CouponFixed;
import com.opengamma.analytics.financial.interestrate.payments.derivative.CouponFixedAccruedCompounding;
import com.opengamma.analytics.financial.interestrate.payments.derivative.CouponIborSpread;
import com.opengamma.analytics.financial.interestrate.payments.derivative.CouponONCompounded;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.interestrate.swap.derivative.SwapFixedCoupon;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueMarketQuoteSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.gammatrace.marketconstruction.dataprovider.RandomGenerator;
import com.opengamma.util.ArgumentChecker;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;
//TODO: handle other types of swap e.g. IborIbor.
//TODO: handle Brazilian style swaps... see ParSpreadMarketQuoteDiscountingCalculator
@Deprecated
public class FairFixedRateCalculator {

	public static double calculate(Swap<?, ?> swap, MultipleCurrencyAmount upfront, MulticurveProviderDiscount multicurves, ZonedDateTime executionTimestamp) {
		ArgumentChecker.notNull(upfront, "upfront");
		
		final PresentValueDiscountingCalculator PVMC = PresentValueDiscountingCalculator.getInstance();
		final PresentValueMarketQuoteSensitivityDiscountingCalculator PVMQSC = PresentValueMarketQuoteSensitivityDiscountingCalculator.getInstance();
		CurrencyAmount[] upfrontArray = upfront.getCurrencyAmounts();
		RandomGenerator rg = new RandomGenerator(executionTimestamp);
		//TODO: is pv01FloatLeg accounting for coupons that have fixed but not paid? I think so... 
		//TODO: when we have CouponONSpread trades being built, need to update this.. 
		MultipleCurrencyAmount pvFloatSpread = MultipleCurrencyAmount.of(swap.getSecondLeg().getCurrency(), 0);
		if(swap.getSecondLeg().getNthPayment(swap.getSecondLeg().getNumberOfPayments()-1) instanceof CouponIborSpread) {
			double floatSpread = ((CouponIborSpread) swap.getSecondLeg().getNthPayment(swap.getSecondLeg().getNumberOfPayments()-1)).getSpread();
			double pv01FloatLeg = swap.getSecondLeg().accept(PVMQSC, multicurves);
			//PVMQSC returns a Double, so we must handle ccy conversion. Note also pv01FloatLeg is signed according to pay/rec
			pvFloatSpread = MultipleCurrencyAmount.of(swap.getSecondLeg().getCurrency(), floatSpread * pv01FloatLeg);
		}
		
		// TODO: random number right now, replace with market data
		FXMatrix fx = new FXMatrix(Currency.USD);
		if (!swap.getFirstLeg().getCurrency().equals(Currency.USD)){
			fx.addCurrency(swap.getFirstLeg().getCurrency(), Currency.USD, rg.randDouble(1.0, 1.5));
		}
		if (!swap.getSecondLeg().getCurrency().equals(Currency.USD) && !swap.getSecondLeg().getCurrency().equals(swap.getFirstLeg().getCurrency())) {
			fx.addCurrency(swap.getSecondLeg().getCurrency(), Currency.USD, rg.randDouble(1.0, 1.5));
		}
		for (CurrencyAmount c: upfrontArray){
			if (!c.getCurrency().equals(Currency.USD)) { // if its not USD (which is already there)
				if(!fx.containsPair(c.getCurrency(), Currency.USD)) { // if its not already there (from the swap first leg)
					fx.addCurrency(c.getCurrency(), Currency.USD, rg.randDouble(1.0, 1.5)); // all fx against USD.. is this always ok?
				}
			}
		}
		double pv = multicurves.getFxRates().convert(swap.accept(PVMC, multicurves), swap.getFirstLeg().getCurrency()).getAmount(); //using the MC fx, bc not sure that fx has ccy of second leg
		double upfrontConverted = fx.convert(upfront, swap.getFirstLeg().getCurrency()).getAmount();
		double signedUpfrontConverted = (pv > 0)? -upfrontConverted : upfrontConverted;

		double pvFloatSpreadConverted = fx.convert(pvFloatSpread, swap.getFirstLeg().getCurrency()).getAmount();
		
		double pv01FixedLeg = -swap.getFirstLeg().accept(PVMQSC, multicurves);
		
		double spread = (pvFloatSpreadConverted + signedUpfrontConverted) / pv01FixedLeg;
		double fixedRate = ((CouponFixed) swap.getFirstLeg().getNthPayment(0)).getFixedRate();
		//System.out.println("pv "+pv+", pv01FloatLeg "+pv01FloatLeg+", floatSpread "+floatSpread+", pv01FixedLeg "+pv01FixedLeg);
		
		return (fixedRate - spread);
	}
	
	// TODO: what trade in market convention terms would give the same pv as our trade.
	//use parSpread to calculate what rate gives PV = 0 for the standardised trade. Then return the (standardisedRate - parRate) + marketConventionRate.
	
	
	
}
