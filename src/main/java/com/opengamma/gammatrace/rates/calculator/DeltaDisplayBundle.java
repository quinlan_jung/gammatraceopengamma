package com.opengamma.gammatrace.rates.calculator;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.threeten.bp.Period;

import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class DeltaDisplayBundle {
	private HashMap<String, Pair<Currency, String>> curveRelations;
	private String[] curveNames;
	private LinkedHashMap<String, Period[]> instrumentTenors;
	private LinkedHashMap<String, QuoteInstrumentType[]> quoteInstrumentType;
	
	public DeltaDisplayBundle(String[] curveNames, HashMap<String, Pair<Currency, String>> curveRelations, LinkedHashMap<String, Period[]> instrumentTenors, LinkedHashMap<String, QuoteInstrumentType[]> quoteInstrumentType) {
		this.curveNames = curveNames;
		this.curveRelations = curveRelations;
		this.instrumentTenors = instrumentTenors;
		this.quoteInstrumentType = quoteInstrumentType;
	}

	public HashMap<String, Pair<Currency, String>> getCurveRelations() {
		return curveRelations;
	}

	public String[] getCurveNames() {
		return curveNames;
	}

	public LinkedHashMap<String, Period[]> getInstrumentTenors() {
		return instrumentTenors;
	}

	public LinkedHashMap<String, QuoteInstrumentType[]> getQuoteInstrumentType() {
		return quoteInstrumentType;
	}
}
