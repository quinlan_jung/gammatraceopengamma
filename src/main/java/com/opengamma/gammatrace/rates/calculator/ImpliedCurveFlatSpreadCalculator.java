package com.opengamma.gammatrace.rates.calculator;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Coupon;
import com.opengamma.analytics.financial.interestrate.swap.derivative.SwapFixedCoupon;
import com.opengamma.analytics.financial.provider.calculator.discounting.PV01CurveParametersCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderInterface;
import com.opengamma.analytics.util.amount.ReferenceAmount;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.marketconstruction.dataprovider.RandomGenerator;
import com.opengamma.lambdava.tuple.ObjectsPair;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.tuple.Pair;

// TODO: what about 6m swaps when curve base is 3m?
// TODO: what if delta = 0?
// TODO: write a test that compares the time taken to use this method vs. par spread calculation
@Deprecated
public class ImpliedCurveFlatSpreadCalculator {
	
	private static final double TOLERANCE = 1e-6; // tolerance on the next rate bump
	private static final int MAX_STEPS = 5;

	
	
	public static double calculate(SwapFixedCoupon<Coupon> swap, MultipleCurrencyAmount upfront, CurveMaker SCCM) {
		PresentValueDiscountingCalculator PVC = PresentValueDiscountingCalculator.getInstance();
		PV01CurveParametersCalculator<MulticurveProviderInterface> PV01CPC = new PV01CurveParametersCalculator<>(PresentValueCurveSensitivityDiscountingCalculator.getInstance());
		
		// find the curve for which the risk is greatest, to be used as our variable 
		LinkedHashMap<String, Double> bumpMap = new LinkedHashMap<>();
		Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> pair = SCCM.makeCurve(bumpMap); // have we already made this once in the calling class?
		MulticurveProviderDiscount curve = pair.getFirst();
		ReferenceAmount<Pair<String, Currency>> pv01Computed = swap.accept(PV01CPC, curve);
		Entry<Pair<String, Currency>, Double> primaryRisk = null;
		
		for (Entry<Pair<String, Currency>, Double> e: pv01Computed.getMap().entrySet()) {
			if (primaryRisk == null){
				primaryRisk = e;
			} else if (Math.abs(e.getValue()) > Math.abs(primaryRisk.getValue())) {
				primaryRisk = e;
			}
		}	
		
		String primaryRiskCurveName = primaryRisk.getKey().getFirst();
		Currency primaryRiskCurrency = primaryRisk.getKey().getSecond();
		
		CurrencyAmount[] upfrontArray = upfront.getCurrencyAmounts();
		RandomGenerator rg = new RandomGenerator(SCCM.getCurveTime());
		
		// making the FX matrix to convert upfront amounts into the ccy of the primary risk
		// TODO: random number right now, replace with market data
		FXMatrix fx = new FXMatrix(Currency.USD);
		if (!primaryRiskCurrency.equals(Currency.USD)){
			fx.addCurrency(primaryRiskCurrency, Currency.USD, rg.randDouble(1.0, 1.5));
		}
		for (CurrencyAmount c: upfrontArray){
			if (!c.getCurrency().equals(Currency.USD)){
				fx.addCurrency(c.getCurrency(), Currency.USD, rg.randDouble(1.0, 1.5)); // all fx against USD.. is this always ok?
			}
		}
		
		double upfrontConvertedToRiskCurrency = fx.convert(upfront, primaryRiskCurrency).getAmount();
		
		int steps = 0;
		double primaryRiskNextBump = 0.0;
		double impliedFlatSpread = 0.0;
		// iterative solving for netValue (pv adjusted by upfront) = 0 
		do {
			if (steps != 0 ){
				pair = SCCM.makeCurve(bumpMap);
			}
			curve = pair.getFirst();
			pv01Computed = swap.accept(PV01CPC, curve);
			double delta = -pv01Computed.getMap().get(Pair.of(primaryRiskCurveName, primaryRiskCurrency)); // normal convention of negative implies paying fixed receiving floating
			double PV = swap.accept(PVC, curve).getAmount(primaryRiskCurrency);
			double netValue = (PV > 0)? (PV - upfrontConvertedToRiskCurrency) : (PV + upfrontConvertedToRiskCurrency);
			primaryRiskNextBump = (netValue / delta) * 0.0001;
			
			// debug
			//System.out.println("step "+steps+" "+primaryRiskCurveName+" "+delta+" "+PV+" "+primaryRiskNextBump+" "+impliedFlatSpread);
			
			// update for next iteration
			impliedFlatSpread = impliedFlatSpread + primaryRiskNextBump;
			bumpMap.put(primaryRiskCurveName, impliedFlatSpread);	
			steps++;
		} while (Math.abs(primaryRiskNextBump) > TOLERANCE && steps < MAX_STEPS);

		return impliedFlatSpread;
	}
	
}
