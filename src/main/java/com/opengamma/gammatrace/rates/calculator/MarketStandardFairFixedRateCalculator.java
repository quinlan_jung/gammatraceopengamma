package com.opengamma.gammatrace.rates.calculator;

import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.payment.CouponONSpreadDefinition;
import com.opengamma.analytics.financial.interestrate.InstrumentDerivative;
import com.opengamma.analytics.financial.interestrate.payments.derivative.CouponFixed;
import com.opengamma.analytics.financial.interestrate.payments.derivative.CouponIborSpread;
import com.opengamma.analytics.financial.interestrate.payments.derivative.CouponONSpread;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Payment;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteDiscountingCalculator;
import com.opengamma.gammatrace.ogextension.CouponONSpreadDiscountingMethod;
import com.opengamma.gammatrace.ogextension.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueMarketQuoteSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.gammatrace.exceptions.UnsupportedOperation;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.marketconstruction.dataprovider.RandomGenerator;
import com.opengamma.gammatrace.ogextension.PresentValueMarketQuoteSensitivityDiscountingCalculatorExtended;
import com.opengamma.util.ArgumentChecker;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.tuple.Pair;

import java.lang.Math;
import java.util.List;

public class MarketStandardFairFixedRateCalculator {
	
	private static final PresentValueDiscountingCalculator PVDC = PresentValueDiscountingCalculator.getInstance();
	private static final PresentValueMarketQuoteSensitivityDiscountingCalculatorExtended PVMQSC = PresentValueMarketQuoteSensitivityDiscountingCalculatorExtended.getInstance();
	
	private final Swap<?, ?> swap;
	private final MultipleCurrencyAmount upfront;
	private final MulticurveProviderDiscount multicurves;
	private final ZonedDateTime executionTimestamp;
	private final List<Swap<?, ?>> marketStandardEquivalent;
	
	private double adjustedPv;
	private double marketStandardEquivalentPV;
	private double marketStandardEquivalentPV01;
	private double fairFixedRate;
	private double marketImpliedFixedRate;
	
	private Pair<Double, Double> twoPartFairFixedRate;
	
	
	public MarketStandardFairFixedRateCalculator(SwapInstrument swapInstrument, MulticurveProviderDiscount multicurves) throws UnsupportedOperation {
		swap = swapInstrument.getDerivative();
		upfront = swapInstrument.getUpfront();
		this.multicurves = multicurves;
		executionTimestamp = swapInstrument.getExecutionTimestamp();
		marketStandardEquivalent = new MarketStandardEquivalentSwapBuilder(swapInstrument, multicurves).makeMarketStandardEquivalent();
		InstrumentDescription description = swapInstrument.getInstrumentDescription(); 
		adjustedPv = adjustedPresentValue();
		marketStandardEquivalentPV = calculateStandardEquivalentPV();
		marketStandardEquivalentPV01 = calculateStandardEquivalentPV01();
		fairFixedRate = (adjustedPv - marketStandardEquivalentPV) / marketStandardEquivalentPV01;
		twoPartFairFixedRate = calculateTwoPartFairFixedRate();
		marketImpliedFixedRate = -marketStandardEquivalentPV / marketStandardEquivalentPV01;	
	}
	
	
	/**
	 * Constructor to store the information needed to make this calculation.
	 * @param swap the swap derivative for which we want to calculate fair rate
	 * @param upfront the initial payment(s) as a multi currency amount
	 * @param multicurves the market
	 * @param executionTimestamp the trade time (to build fx matrix for the conversion of any upfront amounts in different currencies)
	 * @param marketStandardEquivalent the list of market standard equivalent swaps 
	 */
	@Deprecated
	public MarketStandardFairFixedRateCalculator(InstrumentDerivative swap, MultipleCurrencyAmount upfront, MulticurveProviderDiscount multicurves, ZonedDateTime executionTimestamp,
			List<Swap<?, ?>> marketStandardEquivalent) {
		this.swap = (Swap<?, ?>) swap;
		this.upfront = upfront;
		this.multicurves = multicurves;
		this.executionTimestamp = executionTimestamp;
		this.marketStandardEquivalent = marketStandardEquivalent;
		adjustedPv = adjustedPresentValue();
		
		fairFixedRate = (adjustedPv - calculateStandardEquivalentPV()) / calculateStandardEquivalentPV01();
		twoPartFairFixedRate = calculateTwoPartFairFixedRate();
	}
	
	/**
	 * Calculate the pv of the instrument adjusted by the upfront amount. Direction of adjustment is such as the minimise the absolute value of the adjusted pv.
	 * @return the adjusted present value.
	 */
	private double adjustedPresentValue() {
		FXMatrix fx = multicurves.getFxRates();
		for (CurrencyAmount eachCcy : upfront.getCurrencyAmounts()) {
			if (!fx.containsPair(Currency.USD, eachCcy.getCurrency())) {
				fx.addCurrency(Currency.USD, eachCcy.getCurrency(), 1.0); 		// TODO: fix this!!!!
			}
		}
		double pv = multicurves.getFxRates().convert(swap.accept(PVDC, multicurves), marketStandardEquivalent.get(0).getFirstLeg().getCurrency()).getAmount(); //using the MC fx, bc not sure that fx has ccy of second leg
		double upfrontConverted = multicurves.getFxRates().convert(upfront, marketStandardEquivalent.get(0).getFirstLeg().getCurrency()).getAmount();
		adjustedPv = (Math.abs(pv + upfrontConverted) > Math.abs(pv - upfrontConverted))? (pv - upfrontConverted) : (pv + upfrontConverted);
		/*
		MultipleCurrencyAmount pv = swap.accept(PVDC, multicurves);
		if (fx.convert(pv.plus(upfront), swap.getFirstLeg().getCurrency()).getAmount() > fx.convert(pv.plus(upfront.multipliedBy(-1)), swap.getFirstLeg().getCurrency()).getAmount()) {
			adjustedPv = pv.plus(upfront.multipliedBy(-1));
		} else {
			adjustedPv = pv.plus(upfront);
		}*/
		
		/* If the legs in the generator are in the other order to the trade, then the sign of the adjustedPv needed to be reversed.
		 * E.g. PV of payer @ fair + 5bps = - PV of receiver @ fair + 5bps   
		 */
		int direction = (swap.getFirstLeg().getCurrency().equals(marketStandardEquivalent.get(0).getFirstLeg().getCurrency())? 1 : -1);
		return adjustedPv * direction;
	}
	
	/**
	 * Calculates the fixed rate for a market standard convention comparable trade, such that the pv of such trade matches the adjustedPv. 
	 * @return The fixed rate. 
	 */
	@Deprecated
	private double calculateFairFixedRate() {
		return (adjustedPv - multicurves.getFxRates().convert(marketStandardEquivalent.get(0).accept(PVDC, multicurves), marketStandardEquivalent.get(0).getFirstLeg().getCurrency()).getAmount())
				/ marketStandardEquivalent.get(0).getFirstLeg().accept(PVMQSC, multicurves);
	}
	
	private double calculateStandardEquivalentPV() {
		return multicurves.getFxRates().convert(marketStandardEquivalent.get(0).accept(PVDC, multicurves), marketStandardEquivalent.get(0).getFirstLeg().getCurrency()).getAmount();
	}

	private double calculateStandardEquivalentPV01() {
		return marketStandardEquivalent.get(0).getFirstLeg().accept(PVMQSC, multicurves);
	}
	
	@Deprecated
	private double calculateFairFixedRateOld() {
		final ParSpreadMarketQuoteDiscountingCalculator PSMQC = ParSpreadMarketQuoteDiscountingCalculator.getInstance();
		double parRate = marketStandardEquivalent.get(0).accept(PSMQC, multicurves);
		double pv01 = marketStandardEquivalent.get(0).getFirstLeg().accept(PVMQSC, multicurves);
		return parRate + adjustedPv / pv01;
	}

	private Pair<Double, Double> calculateTwoPartFairFixedRate() {
		if (marketStandardEquivalent.size() > 1) {
			//final ParSpreadMarketQuoteDiscountingCalculator PSMQC = ParSpreadMarketQuoteDiscountingCalculator.getInstance();
			double parRateLongTrade = -(multicurves.getFxRates().convert(marketStandardEquivalent.get(1).accept(PVDC, multicurves), marketStandardEquivalent.get(1).getFirstLeg().getCurrency()).getAmount())
					/marketStandardEquivalent.get(1).getFirstLeg().accept(PVMQSC, multicurves);
			double parRateShortTrade = -(multicurves.getFxRates().convert(marketStandardEquivalent.get(2).accept(PVDC, multicurves), marketStandardEquivalent.get(2).getFirstLeg().getCurrency()).getAmount())
					/marketStandardEquivalent.get(2).getFirstLeg().accept(PVMQSC, multicurves);
			
			//double parRateLongTrade = marketStandardEquivalent.get(1).accept(PSMQC, multicurves);
			//double parRateShortTrade = marketStandardEquivalent.get(2).accept(PSMQC, multicurves);
			
			double pv01Combined = marketStandardEquivalent.get(1).getFirstLeg().accept(PVMQSC, multicurves) - marketStandardEquivalent.get(2).getFirstLeg().accept(PVMQSC, multicurves);
			
			//System.out.println(parRateLongTrade+"  "+parRateShortTrade);
			//System.out.println(marketStandardEquivalent.get(1).getFirstLeg().accept(PVMQSC, multicurves)+"   "+marketStandardEquivalent.get(2).getFirstLeg().accept(PVMQSC, multicurves));
			//System.out.println(Pair.of((parRateLongTrade + adjustedPv / pv01Combined), (parRateShortTrade - adjustedPv / pv01Combined)));
			
			return Pair.of((parRateLongTrade + adjustedPv / pv01Combined), (parRateShortTrade - adjustedPv / pv01Combined));
		} else {
			return null;
		}
	}
	public double getAdjustedPv() {
		return adjustedPv;
	}
	public double getFairFixedRate() {
		return fairFixedRate;
	}
	public Pair<Double, Double> getTwoPartFairFixedRate() {
		return twoPartFairFixedRate;
	}
	public double getMarketImpliedFixedRate() {
		return marketImpliedFixedRate; 
	}
}
