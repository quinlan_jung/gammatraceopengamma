package com.opengamma.gammatrace.rates.calculator;

import java.util.Map;

import com.opengamma.analytics.util.amount.SurfaceValue;
import com.opengamma.util.tuple.DoublesPair;

public class SABRSwaptionVegaStructure {
	private Double[] expiries;
	private Double[] tenors;
	private SurfaceValue vegaPoints;

	public SABRSwaptionVegaStructure(Double[] expiries, Double[] tenors, SurfaceValue vegaPoints) {
		this.expiries = expiries;
		this.tenors = tenors;
		this.vegaPoints = vegaPoints;
	}

	public SABRSwaptionVegaStructure(SABRSwaptionVegaStructure structure) {
		this.expiries = structure.getExpiries();
		this.tenors = structure.getTenors();
		this.vegaPoints = structure.getVegaPoints();
	}
	
	
	public Double[] getExpiries() {
		return expiries;
	}
	public Double[] getTenors() {
		return tenors;
	}
	public SurfaceValue getVegaPoints() {
		return vegaPoints;
	}
	
	public static SABRSwaptionVegaStructure reverseDirection(SABRSwaptionVegaStructure original) {
		if (original == null) return null;
		SurfaceValue reversedSurface = new SurfaceValue();
		for (Map.Entry<DoublesPair, Double> entry : original.getVegaPoints().getMap().entrySet()) {
			reversedSurface.add(entry.getKey(), - entry.getValue()); 
		}		
		return new SABRSwaptionVegaStructure(original.getExpiries(), original.getTenors(), reversedSurface);
	}
	
	/**
	 * Intended for debugging purposes only.
	 *
	 */
	/*
	@Override
	public String toString() {
		for (int row = 0; row < expiries.length; row++) {
			System.out.print("   ");
			System.out.println()
		}
			
	}*/
	
}
