package com.opengamma.gammatrace.rates.calculator;

import com.opengamma.analytics.financial.interestrate.payments.derivative.Payment;
import com.opengamma.analytics.financial.interestrate.swap.derivative.SwapFixedCoupon;
import com.opengamma.analytics.financial.interestrate.swap.provider.SwapFixedCouponDiscountingMethod;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionCashFixedIbor;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.analytics.financial.model.option.pricing.analytic.formula.EuropeanVanillaOption;
import com.opengamma.analytics.financial.model.volatility.BlackFormulaRepository;
import com.opengamma.analytics.financial.model.volatility.SimpleOptionData;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParRateDiscountingCalculator;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.gammatrace.instrument.SwaptionInstrument;

public class BlackImpliedVolatilityCalculator {
	private static final ParRateDiscountingCalculator PRDC = ParRateDiscountingCalculator.getInstance();
	private static final SwapFixedCouponDiscountingMethod METHOD_SWAP = SwapFixedCouponDiscountingMethod.getInstance();
	
	public static double getImpliedVolatility (SwaptionPhysicalFixedIbor[] swaptions, double optionPremium, MulticurveProviderDiscount curve) {		
		SimpleOptionData[] swaptionData = new SimpleOptionData[swaptions.length]; 
		for (int i = 0; i < swaptions.length; i++) {
			SwapFixedCoupon<? extends Payment> underlyingSwap = swaptions[i].getUnderlyingSwap();
			double numeraire = METHOD_SWAP.presentValueBasisPoint(swaptions[i].getUnderlyingSwap(), curve);
		    boolean isCall = swaptions[i].isCall();
		    double f = underlyingSwap.accept(PRDC, curve);
		    double k = swaptions[i].getStrike();
		    double t = swaptions[i].getTimeToExpiry();
			swaptionData[i] = new SimpleOptionData(f, k, t, numeraire, isCall);
			//System.out.println("optionPremium = "+optionPremium+"numeraire = "+numeraire+"fwd = "+f+" Strike = "+k+" isCall? = "+isCall);
		}
		
		// it looks like this method just takes the price and not price / discountFactor (as above).. tested using 1 swaption and comparing.
		return BlackFormulaRepository.impliedVolatility(swaptionData, optionPremium);
	}
	
	public static double getImpliedVolatility (SwaptionCashFixedIbor[] swaptions, double optionPremium, MulticurveProviderDiscount curve) {
		SimpleOptionData[] swaptionData = new SimpleOptionData[swaptions.length];
		for (int i = 0; i < swaptions.length; i++) {
			SwapFixedCoupon<? extends Payment> underlyingSwap = swaptions[i].getUnderlyingSwap();
			double f = underlyingSwap.accept(PRDC, curve);
			double discountFactorSettle = curve.getDiscountFactor(swaptions[i].getCurrency(), swaptions[i].getSettlementTime());
			double numeraire = METHOD_SWAP.getAnnuityCash(swaptions[i].getUnderlyingSwap(), f) *discountFactorSettle;
			boolean isCall = swaptions[i].isCall();
		    double k = swaptions[i].getStrike();
		    double t = swaptions[i].getTimeToExpiry();
			swaptionData[i] = new SimpleOptionData(f, k, t, numeraire, isCall);
		}
		return BlackFormulaRepository.impliedVolatility(swaptionData, optionPremium);
	}

	
	
	/*
	@Deprecated
	public static double getImpliedVolatility(SwaptionPhysicalFixedIbor swaption, double optionPremium, MulticurveProviderDiscount curve ) {
		SwapFixedCoupon<? extends Payment> underlyingSwap = swaption.getUnderlyingSwap();
		final double numeraire = METHOD_SWAP.presentValueBasisPoint(swaption.getUnderlyingSwap(), curve);
	    final boolean isCall = swaption.isCall();	// isCall = true means payer swaption, isCall = false mean receiver swaption.
	    final double f = underlyingSwap.accept(PRDC, curve);
	    final double k = swaption.getStrike();
	    final double t = swaption.getTimeToExpiry();
	    final double fwdPrice = optionPremium / numeraire;
	    //System.out.println("optionPremium = "+optionPremium+"numeraire = "+numeraire+"fwdprice = "+fwdPrice+" fwd = "+f+" Strike = "+k+" isCall? = "+isCall);
	    return BlackFormulaRepository.impliedVolatility(fwdPrice, f, k, t, isCall);
	}*/
	
	
}
