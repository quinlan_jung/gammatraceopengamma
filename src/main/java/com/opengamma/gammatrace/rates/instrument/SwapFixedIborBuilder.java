package com.opengamma.gammatrace.rates.instrument;

import java.util.Map;

import org.threeten.bp.Period;

import com.gammatrace.commonutils.TradeUtils;
import com.gammatrace.datamodel.Trade;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.swap.SwapDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborSpreadDefinition;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplate;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;

/**
 * Class to extract the trade details from the map representing the database entry and to build the Instrument.
 * @author sakeebzaman
 */
public class SwapFixedIborBuilder extends SwapBuilder {
	private Period fixedPaymentPeriod;
	private Period floatPaymentPeriod;
	private Period floatResetPeriod;
	private Boolean isFixedPayer;
	private SwapFixedIborSpreadDefinition swapDefinition;
	private SwapInstrument instrument;
	
	/**
	 * Constructor from all the details contained in the trade map.
	 * @param trade
	 */
	public SwapFixedIborBuilder(Trade trade) {	// the isFixedPayer argument is used only for swaption underlying swap direction
		this(trade, true);
	}
	
	public SwapFixedIborBuilder(Trade trade, Boolean isFixedPayer) {
		super(trade);
		this.isFixedPayer = isFixedPayer;
		fixedPaymentPeriod = TradeUtils.getFixedPaymentPeriod(trade);
		floatPaymentPeriod = TradeUtils.getFloatPaymentPeriod(trade);
		floatResetPeriod = TradeUtils.getFloatResetPeriod(trade);
		instrument = makeInstrument(isFixedPayer);
	}	
	
	private SwapInstrument makeInstrument(Boolean isFixedPayer) {
		Swap<?, ?> derivative = makeSwap(isFixedPayer);
		return new SwapInstrument(derivative, executionTimestamp, effectiveDate, endDate, firstNotional, secondNotional, tradeDate, multiCcyUpfront, InstrumentDescription.FIXED_IBOR_SWAP, makeAssetPair());
	}
	
	private Swap<?, ?> makeSwap(Boolean isFixedPayer) {
		Calendar UTC = new MondayToFridayCalendar("UTC");

		IborIndex iborIndex = IborTemplate.getInstance().getIndex(currency, floatResetPeriod);
    	BusinessDayConvention businessDaysConvention = iborIndex.getBusinessDayConvention();
    	Boolean floatLegEOM = iborIndex.isEndOfMonth();
    	Boolean fixedLegEOM = true; // check this!!!
    	
    	DayCount floatDayCountConvention = iborIndex.getDayCount(); 
    	
		swapDefinition = SwapFixedIborSpreadDefinition.from(effectiveDate, endDate,
				fixedPaymentPeriod, fixedDayCountConvention, businessDaysConvention, fixedLegEOM, firstNotional.getAmount(), priceNotation1,
				floatPaymentPeriod, floatDayCountConvention, businessDaysConvention, floatLegEOM, firstNotional.getAmount(), iborIndex, priceNotation2,
				isFixedPayer, UTC);
    	
    	ZonedDateTimeDoubleTimeSeries[] iborFixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate); 
    	
    	return swapDefinition.toDerivative(tradeDate, iborFixing);
	}
	
	
	/**
	 * Gets the Instrument. 
	 * @return The Instrument.
	 */
	public SwapInstrument getInstrument(){
		return instrument;
	}
	
	
	protected SwapFixedIborSpreadDefinition getSwapDefinitionForPhysicalSwaption() {
		return swapDefinition;
	}
	
	// only used in swaption building
	protected SwapFixedIborDefinition getSwapDefinitionForCashSwaption() {
		Calendar UTC = new MondayToFridayCalendar("UTC");
		IborIndex iborIndex = IborTemplate.getInstance().getIndex(currency, floatResetPeriod);
    	BusinessDayConvention businessDaysConvention = iborIndex.getBusinessDayConvention();
    	Boolean floatLegEOM = iborIndex.isEndOfMonth();
    	Boolean fixedLegEOM = true;
    	DayCount floatDayCountConvention = iborIndex.getDayCount(); 
		return SwapFixedIborDefinition.from(effectiveDate, endDate, fixedPaymentPeriod, fixedDayCountConvention, businessDaysConvention, fixedLegEOM, firstNotional.getAmount(), priceNotation1, floatPaymentPeriod, 
				floatDayCountConvention, businessDaysConvention, floatLegEOM, firstNotional.getAmount(), iborIndex, isFixedPayer, UTC);
	}
	
}
	
