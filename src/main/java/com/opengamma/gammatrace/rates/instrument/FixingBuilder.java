package com.opengamma.gammatrace.rates.instrument;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.threeten.bp.LocalDate;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.temporal.ChronoUnit;

import com.gammatrace.logging.CustomLogger;
import com.gammatrace.sungard.api.ServiceApi;
import com.gammatrace.sungard.api.ServiceApiImpl;
import com.gammatrace.sungard.fedfunds.FedFundSymbol;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponIborDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponONDefinition;
import com.opengamma.gammatrace.ogextension.AnnuityCouponONSpreadDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityDefinition;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexDeposit;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.analytics.financial.instrument.payment.CouponDefinition;
import com.opengamma.analytics.financial.instrument.payment.CouponFloatingDefinition;
import com.opengamma.analytics.financial.instrument.payment.CouponIborCompoundingFlatSpreadDefinition;
import com.opengamma.analytics.financial.instrument.payment.CouponIborDefinition;
import com.opengamma.analytics.financial.instrument.payment.CouponIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.payment.CouponONDefinition;
import com.opengamma.gammatrace.ogextension.CouponONSpreadDefinition;
import com.opengamma.analytics.financial.instrument.payment.PaymentDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedONDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapIborIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapIborONDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapXCcyIborIborDefinition;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.dataprovider.RandomGenerator;
import com.opengamma.gammatrace.ogextension.SwapONSpreadIborSpreadDefinition;
import com.opengamma.gammatrace.ogextension.SwapXCcyFixedIborDefinition;
import com.opengamma.timeseries.precise.zdt.ImmutableZonedDateTimeDoubleTimeSeries;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.time.DateUtils;

public class FixingBuilder {
	static Logger logger = CustomLogger.getLogger(FixingBuilder.class);
	private static ServiceApi serviceApi = ServiceApiImpl.getInstance();
	
	private static ZonedDateTimeDoubleTimeSeries makeTimeSeries(ZonedDateTime[] requiredFixing, IndexDeposit index) {
		if (requiredFixing != null) {
			List<Double> values = new ArrayList<>();
			for (ZonedDateTime date : requiredFixing) {
				logger.info(String.format("Requesting fedfund index %s", index.getName()));
				String convertedSymbol = FedFundSymbol.convertIborSymbolToFedFund(index.getName());
				Double rate = serviceApi.getQuote(convertedSymbol, date.toEpochSecond());
				logger.info(String.format("Got fedfund name %s, symbol: %s, rate: %f", index.getName(), convertedSymbol, rate));
				values.add(rate);
			}
			ZonedDateTimeDoubleTimeSeries fixingTimeSeries = ImmutableZonedDateTimeDoubleTimeSeries.ofUTC(requiredFixing, values.toArray(new Double[values.size()]));
			return fixingTimeSeries;
		} else {
			ZonedDateTimeDoubleTimeSeries emptyTimeSeries = ImmutableZonedDateTimeDoubleTimeSeries.ofEmptyUTC();
			return emptyTimeSeries;
		}
	}
	
	private static List<ZonedDateTime> couponIborFixingDates(AnnuityCouponDefinition<? extends CouponFloatingDefinition> iborLeg, ZonedDateTime executionTimestamp) {
		List<ZonedDateTime> dateList = new ArrayList<>();
		for (int loop = 0; loop < iborLeg.getPayments().length; loop++) {
			LocalDate fixingDate = iborLeg.getNthPayment(loop).getFixingDate().toLocalDate();
			LocalDate paymentDate = iborLeg.getNthPayment(loop).getPaymentDate().toLocalDate();
			if (fixingDate.isBefore(executionTimestamp.toLocalDate()) && !paymentDate.isBefore(executionTimestamp.toLocalDate())) {
					dateList.add(iborLeg.getNthPayment(loop).getFixingDate().toLocalDate().atStartOfDay(ZoneOffset.UTC));
			}
		}
		return dateList;
	}
	
	private static List<ZonedDateTime> couponONFixingDates(AnnuityCouponONDefinition floatLeg, ZonedDateTime executionTimestamp) {
		List<ZonedDateTime> dateList = new ArrayList<>();
		for (CouponONDefinition coupon : floatLeg.getPayments()){
			ZonedDateTime[] fixingDates = coupon.getFixingPeriodDate();
			if (fixingDates[0].toLocalDate().isBefore(executionTimestamp.toLocalDate()) && !coupon.getPaymentDate().toLocalDate().isBefore(executionTimestamp.toLocalDate())){
				int datesLoop = 0;
				while (fixingDates[datesLoop].toLocalDate().isBefore(executionTimestamp.toLocalDate())){
					dateList.add(fixingDates[datesLoop].toLocalDate().atStartOfDay(ZoneOffset.UTC));
					datesLoop++;
				}
			}
		}
		return dateList;
	}
	
	private static List<ZonedDateTime> couponONSpreadFixingDates(AnnuityCouponONSpreadDefinition floatLeg, ZonedDateTime executionTimestamp) {
		List<ZonedDateTime> dateList = new ArrayList<>();
		for (CouponONSpreadDefinition coupon : floatLeg.getPayments()){
			ZonedDateTime[] fixingDates = coupon.getFixingPeriodDate();
			if (fixingDates[0].toLocalDate().isBefore(executionTimestamp.toLocalDate()) && !coupon.getPaymentDate().toLocalDate().isBefore(executionTimestamp.toLocalDate())){
				int datesLoop = 0;
				while (fixingDates[datesLoop].toLocalDate().isBefore(executionTimestamp.toLocalDate())){
					dateList.add(fixingDates[datesLoop].toLocalDate().atStartOfDay(ZoneOffset.UTC));
					datesLoop++;
				}
			}
		}
		return dateList;
	}
	
	private static List<ZonedDateTime> couponIborCompoundingFlatSpreadFixingDates(AnnuityDefinition<CouponIborCompoundingFlatSpreadDefinition> floatLeg, ZonedDateTime executionTimestamp) {
		List<ZonedDateTime> dateList = new ArrayList<>();
		for (CouponIborCompoundingFlatSpreadDefinition coupon : floatLeg.getPayments()) {
			ZonedDateTime[] fixingDates = coupon.getFixingDates();
			if (fixingDates[0].toLocalDate().isBefore(executionTimestamp.toLocalDate()) && !coupon.getPaymentDate().toLocalDate().isBefore(executionTimestamp.toLocalDate())){
				int datesLoop = 0;
				while (fixingDates[datesLoop].toLocalDate().isBefore(executionTimestamp.toLocalDate())){
					dateList.add(fixingDates[datesLoop].toLocalDate().atStartOfDay(ZoneOffset.UTC));
					datesLoop++;
				}
			}
		}
		return dateList;
	}
	
	/**
	 * Creates a timeseries array to return index fixing data for fixedIbor swaps. 
	 * @param FixedIbor swap definition
	 * @param Execution timestamp
	 * @return The fixing time series
	 */
	public static ZonedDateTimeDoubleTimeSeries[] makeFixingTimeSeries(SwapFixedIborDefinition swap, ZonedDateTime executionTimestamp) {
		AnnuityCouponIborDefinition floatLeg = swap.getIborLeg();
		List<ZonedDateTime> dateList = couponIborFixingDates(floatLeg, executionTimestamp);//new ArrayList<>();
		ZonedDateTime[] requiredFixing = null;
		IborIndex index = floatLeg.getIborIndex();
		requiredFixing = dateList.toArray(new ZonedDateTime[dateList.size()]);
		return new ZonedDateTimeDoubleTimeSeries[] {makeTimeSeries(requiredFixing, index)};
	}
	
	/**
	 * Creates a timeseries array to return index fixing data for fixedIborSpread swaps. 
	 * @param FixedIbor swap definition
	 * @param Execution timestamp
	 * @return The fixing time series
	 */
	public static ZonedDateTimeDoubleTimeSeries[] makeFixingTimeSeries(SwapFixedIborSpreadDefinition swap, ZonedDateTime executionTimestamp) {
		AnnuityCouponIborSpreadDefinition floatLeg = (AnnuityCouponIborSpreadDefinition) swap.getIborLeg();  
		List<ZonedDateTime> dateList = couponIborFixingDates(floatLeg, executionTimestamp);
		ZonedDateTime[] requiredFixing = null;
		IborIndex index = floatLeg.getIborIndex();	
		requiredFixing = dateList.toArray(new ZonedDateTime[dateList.size()]);
		return new ZonedDateTimeDoubleTimeSeries[] {makeTimeSeries(requiredFixing, index)};
	}
	/**
	 * Creates a timeseries array to return index fixing data for IborIbor swaps. 
	 * @param Ibor Ibor swap definition
	 * @param Execution timestamp
	 * @return The fixing time series
	 */
	public static ZonedDateTimeDoubleTimeSeries[] makeFixingTimeSeries(SwapIborIborDefinition swap, ZonedDateTime executionTimestamp) {
		AnnuityCouponIborSpreadDefinition firstLeg = swap.getFirstLeg();
		AnnuityCouponIborSpreadDefinition secondLeg = swap.getSecondLeg();
		List<ZonedDateTime> dateList1 = couponIborFixingDates(firstLeg, executionTimestamp);
		List<ZonedDateTime> dateList2 = couponIborFixingDates(secondLeg, executionTimestamp);
		ZonedDateTime[] requiredFixing1 = null;
		ZonedDateTime[] requiredFixing2 = null;
		IborIndex firstIndex = firstLeg.getIborIndex();
		IborIndex secondIndex = secondLeg.getIborIndex();
		requiredFixing1 = dateList1.toArray(new ZonedDateTime[dateList1.size()]);
		requiredFixing2 = dateList2.toArray(new ZonedDateTime[dateList2.size()]);
		return new ZonedDateTimeDoubleTimeSeries[] {makeTimeSeries(requiredFixing1, firstIndex), makeTimeSeries(requiredFixing2, secondIndex)};
	}
	
	//TODO: find a better way of dealing with these trades, all of these methods take swaps that are subclasses of SwapDefinition...we can only do this hack once. 
	public static ZonedDateTimeDoubleTimeSeries[] makeFixingTimeSeries(SwapDefinition swap, ZonedDateTime executionTimestamp) {
		AnnuityDefinition<? extends PaymentDefinition> firstLeg = swap.getFirstLeg();
		AnnuityDefinition<? extends PaymentDefinition> secondLeg = swap.getSecondLeg();
		List<ZonedDateTime> dateList1;
		List<ZonedDateTime> dateList2;
		IborIndex firstIndex;
		IborIndex secondIndex;
		if (firstLeg.getNthPayment(0) instanceof CouponIborCompoundingFlatSpreadDefinition) {
			dateList1 = couponIborCompoundingFlatSpreadFixingDates((AnnuityDefinition<CouponIborCompoundingFlatSpreadDefinition>) firstLeg, executionTimestamp);
			firstIndex = ((AnnuityDefinition<CouponIborCompoundingFlatSpreadDefinition>) firstLeg).getNthPayment(0).getIndex();
		} else if (firstLeg instanceof AnnuityCouponIborSpreadDefinition) {
			dateList1 = couponIborFixingDates((AnnuityCouponIborSpreadDefinition) firstLeg, executionTimestamp);
			firstIndex = ((AnnuityCouponIborSpreadDefinition) firstLeg).getIborIndex();
		} else {
			throw new IllegalArgumentException("This fixing method is meant from Ibor or Ibor Coupounding");
		}
		if (secondLeg.getNthPayment(0) instanceof CouponIborCompoundingFlatSpreadDefinition) {
			dateList2 = couponIborCompoundingFlatSpreadFixingDates((AnnuityDefinition<CouponIborCompoundingFlatSpreadDefinition>) secondLeg, executionTimestamp);
			secondIndex = ((AnnuityDefinition<CouponIborCompoundingFlatSpreadDefinition>) secondLeg).getNthPayment(0).getIndex();
		} else if (secondLeg instanceof AnnuityCouponIborSpreadDefinition) {
			dateList2 = couponIborFixingDates((AnnuityCouponIborSpreadDefinition) secondLeg, executionTimestamp);
			secondIndex = ((AnnuityCouponIborSpreadDefinition) secondLeg).getIborIndex();
		} else {
			throw new IllegalArgumentException("This fixing method is meant from Ibor or Ibor Coupounding");
		}
		ZonedDateTime[] requiredFixing1 = null;
		ZonedDateTime[] requiredFixing2 = null;
		requiredFixing1 = dateList1.toArray(new ZonedDateTime[dateList1.size()]);
		requiredFixing2 = dateList2.toArray(new ZonedDateTime[dateList2.size()]);
		return new ZonedDateTimeDoubleTimeSeries[] {makeTimeSeries(requiredFixing1, firstIndex), makeTimeSeries(requiredFixing2, secondIndex)};
	}
	
	public static ZonedDateTimeDoubleTimeSeries[] makeFixingTimeSeries(SwapXCcyIborIborDefinition swap, ZonedDateTime executionTimestamp) {
		//get array of payments
		PaymentDefinition[] firstLegPayments = swap.getFirstLeg().getPayments();
		PaymentDefinition[] secondLegPayments = swap.getSecondLeg().getPayments();
		// make arrays to hold the coupon ibor parts i.e. trimming the initial and final notional exchange
		CouponIborSpreadDefinition[] firstLegPaymentsNoNotional = new CouponIborSpreadDefinition[firstLegPayments.length - 2];
		CouponIborSpreadDefinition[] secondLegPaymentsNoNotional = new CouponIborSpreadDefinition[secondLegPayments.length - 2];
		for (int i = 1; i < firstLegPayments.length -1; i++) {
			firstLegPaymentsNoNotional[i-1] = (CouponIborSpreadDefinition) firstLegPayments[i];
		}
		for (int j = 1; j < secondLegPayments.length -1; j++) {
			secondLegPaymentsNoNotional[j-1] = (CouponIborSpreadDefinition) secondLegPayments[j];
		}
		// create annuity definition from these arrays
		AnnuityCouponIborSpreadDefinition firstLegNoNotional = new AnnuityCouponIborSpreadDefinition(firstLegPaymentsNoNotional, swap.getFirstLeg().getCalendar());
		AnnuityCouponIborSpreadDefinition secondLegNoNotional = new AnnuityCouponIborSpreadDefinition(secondLegPaymentsNoNotional, swap.getSecondLeg().getCalendar());
		ZonedDateTime[] requiredFixing1 = null;
		ZonedDateTime[] requiredFixing2 = null;
		List<ZonedDateTime> list1 = couponIborFixingDates(firstLegNoNotional, executionTimestamp);
		List<ZonedDateTime> list2 = couponIborFixingDates(secondLegNoNotional, executionTimestamp);
		requiredFixing1 = list1.toArray(new ZonedDateTime[list1.size()]);
		requiredFixing2 = list2.toArray(new ZonedDateTime[list2.size()]);
		IborIndex firstIndex = firstLegNoNotional.getIborIndex();
		IborIndex secondIndex = secondLegNoNotional.getIborIndex();
		return new ZonedDateTimeDoubleTimeSeries[] {makeTimeSeries(requiredFixing1, firstIndex), makeTimeSeries(requiredFixing2, secondIndex)};
	}
	
	public static ZonedDateTimeDoubleTimeSeries[] makeFixingTimeSeries(SwapXCcyFixedIborDefinition swap, ZonedDateTime executionTimestamp) {
		//get array of payments
		PaymentDefinition[] iborLegPayments = swap.getIborLeg().getPayments();	
		// make array to hold the coupon ibor parts i.e. trimming the initial and final notional exchange
		CouponIborSpreadDefinition[] iborLegPaymentsNoNotional = new CouponIborSpreadDefinition[iborLegPayments.length - 2];
		for (int i = 1; i < iborLegPayments.length -1; i++) {
			iborLegPaymentsNoNotional[i-1] = (CouponIborSpreadDefinition) iborLegPayments[i];
		}
		// create annuity definition from these arrays
		AnnuityCouponIborSpreadDefinition iborLegNoNotional = new AnnuityCouponIborSpreadDefinition(iborLegPaymentsNoNotional, swap.getFirstLeg().getCalendar());
		ZonedDateTime[] requiredFixing = null;
		List<ZonedDateTime> list = couponIborFixingDates(iborLegNoNotional, executionTimestamp);
		requiredFixing = list.toArray(new ZonedDateTime[list.size()]);
		IborIndex iborIndex = iborLegNoNotional.getIborIndex();
		return new ZonedDateTimeDoubleTimeSeries[] {makeTimeSeries(requiredFixing, iborIndex)};
	}
	
	/**
	 * Creates a timeseries array to return index fixing data for IborON swaps. 
	 * @param Ibor ON swap definition
	 * @param Execution timestamp
	 * @return The fixing time series
	 */
	public static ZonedDateTimeDoubleTimeSeries[] makeFixingTimeSeries(SwapIborONDefinition swap, ZonedDateTime executionTimestamp) {
		AnnuityCouponIborSpreadDefinition firstLeg = swap.getIborLeg();
		AnnuityCouponONDefinition secondLeg = swap.getOISLeg();
		List<ZonedDateTime> dateList1 = couponIborFixingDates(firstLeg, executionTimestamp);
		List<ZonedDateTime> dateList2 = couponONFixingDates(secondLeg, executionTimestamp);
		ZonedDateTime[] requiredFixing = null;
		ZonedDateTime[] requiredFixingOIS = null;
		requiredFixing = dateList1.toArray(new ZonedDateTime[dateList1.size()]);
		requiredFixingOIS = dateList2.toArray(new ZonedDateTime[dateList2.size()]);
		IborIndex firstIndex = firstLeg.getIborIndex();
		IndexON secondIndex = secondLeg.getOvernightIndex();
		return new ZonedDateTimeDoubleTimeSeries[] {makeTimeSeries(requiredFixing, firstIndex), makeTimeSeries(requiredFixingOIS, secondIndex)};
	}
	

	public static ZonedDateTimeDoubleTimeSeries[] makeFixingTimeSeries(SwapONSpreadIborSpreadDefinition swap, ZonedDateTime executionTimestamp) {
		AnnuityCouponONSpreadDefinition firstLeg = swap.getOISLeg();
		AnnuityCouponIborSpreadDefinition secondLeg = swap.getIborLeg();
		List<ZonedDateTime> dateList1 = couponONSpreadFixingDates(firstLeg, executionTimestamp);
		List<ZonedDateTime> dateList2 = couponIborFixingDates(secondLeg, executionTimestamp);
		ZonedDateTime[] requiredFixingOIS = null;
		ZonedDateTime[] requiredFixing = null;
		requiredFixingOIS = dateList1.toArray(new ZonedDateTime[dateList1.size()]);
		requiredFixing = dateList2.toArray(new ZonedDateTime[dateList2.size()]);
		IndexON firstIndex = firstLeg.getNthPayment(0).getIndex();
		IborIndex secondIndex = secondLeg.getIborIndex();
		return new ZonedDateTimeDoubleTimeSeries[] {makeTimeSeries(requiredFixingOIS, firstIndex), makeTimeSeries(requiredFixing, secondIndex)};
	}
	
	/**
	 * Creates a timeseries array to return index fixing data for FixedON swaps. 
	 * @param Ibor ON swap definition
	 * @param Execution timestamp
	 * @return The fixing time series
	 */
	public static ZonedDateTimeDoubleTimeSeries[] makeFixingTimeSeries(SwapFixedONDefinition swap, ZonedDateTime executionTimestamp) {
		AnnuityCouponONDefinition firstLeg = swap.getOISLeg();
		ZonedDateTime[] requiredFixingOIS = null;
		List<ZonedDateTime> list = couponONFixingDates(firstLeg, executionTimestamp);
		requiredFixingOIS = list.toArray(new ZonedDateTime[list.size()]);
		IndexON secondIndex = firstLeg.getOvernightIndex();
		return new ZonedDateTimeDoubleTimeSeries[] {makeTimeSeries(requiredFixingOIS, secondIndex)};
	}
}

