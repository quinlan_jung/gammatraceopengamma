package com.opengamma.gammatrace.rates.instrument;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.datamodel.Trade;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityDefinitionBuilder;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.payment.PaymentDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapXCcyIborIborDefinition;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.ArgumentChecker;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class SwapXCcyFixedFixedBuilder extends SwapBuilder {
private SwapInstrument instrument;
	
	/**
	 * Constructor from all the details contained in the trade map.
	 * @param trade
	 */
	public SwapXCcyFixedFixedBuilder(Trade trade) {
		super(trade);  //effectiveDate, maturityDate, executionTimestamp, tradeDate, fixedDayCountConvention, currency, notional, fixedRate, floatSpread
		instrument = makeInstrument(true);		
	}
	
	private SwapInstrument makeInstrument(Boolean isFixedPayer) {
		Swap<?, ?> derivative = makeSwap(isFixedPayer);
		return new SwapInstrument(derivative, executionTimestamp, effectiveDate, endDate, firstNotional, secondNotional, tradeDate, multiCcyUpfront, InstrumentDescription.XCCY_FIXED_FIXED_SWAP, makeAssetPair());
	}
	
	@Override
	protected Pair<String, String> makeAssetPair() {
		return Pair.of("FIXED", "FIXED");
	}
	
	/**
	 * Derivative maker from the fields. 
	 * @return The trade derivative on the trade date with the appropriate fixing information
	 */
	private Swap<?, ?> makeSwap(Boolean isFixedPayer) {
		Calendar calendar = new MondayToFridayCalendar("UTC");
		BusinessDayConvention businessDaysConvention = BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following");
		boolean isEOM = true;
		
		SwapDefinition swapDefinition = from(effectiveDate, endDate, firstNotional.getCurrency(), firstNotional.getAmount(), firstLegPaymentPeriod, fixedDayCountConvention, priceNotation1, secondNotional.getCurrency(),
				secondNotional.getAmount(), secondLegPaymentPeriod, priceNotation2, calendar, businessDaysConvention, isEOM, true);
		  	
    	//ZonedDateTimeDoubleTimeSeries[] iborFixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate);		// no fixing needed for fixed vs. fixed trade

    	return swapDefinition.toDerivative(tradeDate);	
	}
	
	/**
	 * Gets the Instrument. 
	 * @return The Instrument.
	 */
	public SwapInstrument getInstrument(){
		return instrument;
	}
	
	
	private static SwapDefinition from(final ZonedDateTime settlementDate, final ZonedDateTime maturityDate, final Currency firstCurrency, final double firstNotional, final Period firstPaymentPeriod, 
			final DayCount firstDayCount, final double firstRate, final Currency secondCurrency, final double secondNotional, final Period secondPaymentPeriod, final double secondRate, final Calendar calendar, 
			final BusinessDayConvention businessDayConvention, final boolean isEOM, final boolean isFirstLegPayer) {
		
		final AnnuityDefinition<PaymentDefinition> firstLegWithNotional = AnnuityDefinitionBuilder.annuityCouponFixedWithNotional(firstCurrency, settlementDate, maturityDate, firstPaymentPeriod, calendar, 
				firstDayCount, businessDayConvention, isEOM, firstNotional, firstRate, isFirstLegPayer);
		final AnnuityDefinition<PaymentDefinition> secondLegWithNotional = AnnuityDefinitionBuilder.annuityCouponFixedWithNotional(secondCurrency, settlementDate, maturityDate, secondPaymentPeriod, calendar, 
				getFixedBondBasisDayCountByCurrency(secondCurrency), businessDayConvention, isEOM, secondNotional, secondRate, !isFirstLegPayer);
		
		return new SwapDefinition(firstLegWithNotional, secondLegWithNotional);
		
	}
	
	private static DayCount getFixedBondBasisDayCountByCurrency(Currency ccy) {
		DayCountFactory factory = DayCountFactory.INSTANCE;
		if (ccy.equals(Currency.USD)){
			return factory.getDayCount("30U/360");
		} else if (ccy.equals(Currency.EUR)){
			return factory.getDayCount("30U/360");
		} else if (ccy.equals(Currency.GBP)) {
			return factory.getDayCount("Actual/365");
		} else if (ccy.equals(Currency.JPY)) {
			return factory.getDayCount("Actual/365");
		} else {
			return factory.getDayCount("30U/360");
		}
	}
}
