package com.opengamma.gammatrace.rates.instrument;

import org.threeten.bp.ZonedDateTime;

import com.gammatrace.datamodel.Trade;
import com.opengamma.analytics.financial.instrument.swaption.SwaptionPhysicalFixedIborSpreadDefinition;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.gammatrace.exceptions.UnsupportedOperation;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwaptionInstrument;

// we have to use the swap builder in here to get the swap definition (SwapInstrument doesn't contain the definition, only the derivative)
public class SwaptionPhysicalFixedIborBuilder extends SwaptionBuilder {
	
	private SwaptionInstrument instrument;
	
	public SwaptionPhysicalFixedIborBuilder(Trade trade) throws UnsupportedOperation{
		super(trade);
		instrument = makeInstrument();
	}
	
	private SwaptionInstrument makeInstrument() {
		SwaptionPhysicalFixedIbor[] swaption = makeSwaption();
		SwapFixedIborBuilder build = underlyingSwapBuilder[0];
		return new SwaptionInstrument(swaption, build.getExecutionTimestamp() , optionPremium, expirationDate, build.getEffectiveDate(), build.getEndDate(), 
				build.getFirstNotional(), build.getSecondNotional(), build.getTradeDate(), build.getUpfront(), InstrumentDescription.FIXED_IBOR_SWAPTION, build.makeAssetPair());	
	}
	
	private SwaptionPhysicalFixedIbor[] makeSwaption() {
		ZonedDateTime tradeDate = underlyingSwapBuilder[0].getTradeDate();
		SwaptionPhysicalFixedIbor[] swaption = new SwaptionPhysicalFixedIbor[underlyingSwapBuilder.length];
		for (int i = 0; i < underlyingSwapBuilder.length; i++) {
			SwaptionPhysicalFixedIborSpreadDefinition swaptionDefinition = SwaptionPhysicalFixedIborSpreadDefinition.from(expirationDate, underlyingSwapBuilder[i].getSwapDefinitionForPhysicalSwaption(), true);
			swaption[i] = swaptionDefinition.toDerivative(tradeDate);
		}
		return swaption;
	}
	
	public SwaptionInstrument getInstrument() {
		return instrument;
	}
	
}