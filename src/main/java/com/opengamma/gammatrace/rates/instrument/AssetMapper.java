package com.opengamma.gammatrace.rates.instrument;

import java.util.HashMap;
import java.util.Map;

import org.threeten.bp.Period;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexDeposit;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;
import com.opengamma.gammatrace.rates.indextemplate.ONTemplateByName;
import com.opengamma.lambdava.tuple.ObjectsPair;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;
import com.opengamma.util.tuple.Triple;

/**
 * Mapping of data asset names to Ibor/ ON indices from templates.
 */
public class AssetMapper {
	/**
	 * The method unique instance.
	 */
	private static final AssetMapper INSTANCE = new AssetMapper();

	/**
	 * Return the unique instance of the class.
	 * @return The instance.
	 */
	public static AssetMapper getInstance() {
	  return INSTANCE;
	}

	/**
	 * The map with the list of asset names and their indices.
	 */
	private final Map<Triple<String, Currency, Period>, IndexDeposit> _indices;
	
	/**
	 * The index templates.
	 */
	private final IborTemplateByName _iborIndexMaster;
	private final ONTemplateByName _indexONMaster;
	
	/**
	 * Private constructor.
	 */
	private AssetMapper() {
		_iborIndexMaster = IborTemplateByName.getInstance();
		_indexONMaster = ONTemplateByName.getInstance();
		
		_indices = new HashMap<Triple<String, Currency, Period>, IndexDeposit>();
	    //********* ON Indices*********/
		
		Pair<Currency, Period> pair = Pair.of(Currency.AUD, Period.ofYears(1));
		_indices.put(Triple.of("AUD-AONIA-OIS-COMPOUND", Currency.AUD, Period.ofYears(1)), _indexONMaster.getIndex(Indicies.RBA_ON));
		
	    _indices.put(Triple.of("AONIA", Currency.AUD, Period.ofMonths(12)), _indexONMaster.getIndex(Indicies.RBA_ON));
	    //EUR
	    _indices.put(Triple.of("EUR-EONIA-OIS-COMPOUND", Currency.EUR, Period.ofYears(1)), _indexONMaster.getIndex(Indicies.EONIA));
	    _indices.put(Triple.of("EONIA", Currency.EUR, Period.ofYears(1)), _indexONMaster.getIndex(Indicies.EONIA));
	    //GBP
	    _indices.put(Triple.of("GBP-WMBA-SONIA-COMPOUND", Currency.GBP, Period.ofYears(1)), _indexONMaster.getIndex(Indicies.SONIA));
	    //JPY
	    _indices.put(Triple.of("JPY-TONA-OIS-COMPOUND", Currency.JPY, Period.ofYears(1)), _indexONMaster.getIndex(Indicies.TONAR));
	    //USD.... combining fed funds and USD OIS (this is an approximation)
	    _indices.put(Triple.of("USD-Federal Funds-H.15-OIS-COMPOUND", Currency.USD, Period.ofYears(1)), _indexONMaster.getIndex(Indicies.FEDFUNDS));
	    _indices.put(Triple.of("USD-Federal Funds-H.15-OIS-COMPOUND", Currency.USD, Period.ofMonths(3)), _indexONMaster.getIndex(Indicies.FEDFUNDS));
	    _indices.put(Triple.of("USD-Federal Funds-H.15", Currency.USD, Period.ofYears(1)), _indexONMaster.getIndex(Indicies.FEDFUNDS));
	    _indices.put(Triple.of("USD-Federal Funds-H.15", Currency.USD, Period.ofMonths(3)), _indexONMaster.getIndex(Indicies.FEDFUNDS));
	    _indices.put(Triple.of("USD-Federal Funds-H.15", Currency.USD, Period.ofDays(1)), _indexONMaster.getIndex(Indicies.FEDFUNDS));	
	    _indices.put(Triple.of("US FED EFFECTIVE", Currency.USD, Period.ofYears(1)), _indexONMaster.getIndex(Indicies.FEDFUNDS));
	    _indices.put(Triple.of("US FED EFFECTIVE", Currency.USD, Period.ofMonths(3)), _indexONMaster.getIndex(Indicies.FEDFUNDS));
	    _indices.put(Triple.of("FEDFUNDS", Currency.USD, Period.ofYears(1)), _indexONMaster.getIndex(Indicies.FEDFUNDS));
	    _indices.put(Triple.of("FEDFUNDS", Currency.USD, Period.ofDays(1)), _indexONMaster.getIndex(Indicies.FEDFUNDS));
	    
	    //*******Ibor Indices**********/
	    //AED
	    //_indices.put(Pair.of("AED-EBOR-Reuters", Period.ofMonths(3)) , null);
	    //AUD
	    //_indices.put(Pair.of("AUD-BBR-BBSW", Period.ofMonths(1)), _iborIndexMaster.getIndex("AUDBB1M"));
	    _indices.put(Triple.of("AUD-BBR-BBSW", Currency.AUD, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.AUDBB3M));
	    _indices.put(Triple.of("AUD-BBR-BBSW", Currency.AUD, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.AUDBB6M));
	    //CAD
	    _indices.put(Triple.of("CAD-BA-CDOR", Currency.CAD, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.CADCDOR3M));
	    _indices.put(Triple.of("CAD-BA-CDOR", Currency.CAD, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.CADCDOR6M));
	    //EUR
	    _indices.put(Triple.of("EUR-EURIBOR-Reuters", Currency.EUR, Period.ofMonths(1)), _iborIndexMaster.getIndex(Indicies.EURIBOR1M));
	    _indices.put(Triple.of("EURIBOR", Currency.EUR, Period.ofMonths(1)), _iborIndexMaster.getIndex(Indicies.EURIBOR1M));
	    _indices.put(Triple.of("EUR-EURIBOR-Reuters", Currency.EUR, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.EURIBOR3M));
	    _indices.put(Triple.of("EURIBOR", Currency.EUR, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.EURIBOR3M));
	    _indices.put(Triple.of("EUR-EURIBOR-Reuters", Currency.EUR, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.EURIBOR6M));
	    _indices.put(Triple.of("EUR-EURIBOR-Telerate", Currency.EUR, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.EURIBOR6M));
	    _indices.put(Triple.of("EURIBOR", Currency.EUR, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.EURIBOR6M));
	    _indices.put(Triple.of("EUR-EURIBOR-Reuters", Currency.EUR, Period.ofMonths(12)), _iborIndexMaster.getIndex(Indicies.EURIBOR12M));
	    _indices.put(Triple.of("EURIBOR", Currency.EUR, Period.ofMonths(12)), _iborIndexMaster.getIndex(Indicies.EURIBOR12M));
	    //GBP
	    _indices.put(Triple.of("GBP-LIBOR-BBA", Currency.GBP, Period.ofMonths(1)), _iborIndexMaster.getIndex(Indicies.GBPLIBOR1M));
	    _indices.put(Triple.of("GBP-LIBOR-BBA", Currency.GBP, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.GBPLIBOR3M));
	    _indices.put(Triple.of("GBP-LIBOR-BBA", Currency.GBP, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.GBPLIBOR6M));
	    //JPY
	    _indices.put(Triple.of("JPY-LIBOR-BBA", Currency.JPY, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.JPYLIBOR3M));
	    _indices.put(Triple.of("LIBOR", Currency.JPY, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.JPYLIBOR3M));
	    _indices.put(Triple.of("JPY-LIBOR-BBA", Currency.JPY, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.JPYLIBOR6M));
	    _indices.put(Triple.of("LIBOR", Currency.JPY, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.JPYLIBOR6M));
	    //USD
	    _indices.put(Triple.of("USD-LIBOR-BBA", Currency.USD, Period.ofMonths(1)), _iborIndexMaster.getIndex(Indicies.USDLIBOR1M));
	    _indices.put(Triple.of("LIBOR", Currency.USD, Period.ofMonths(1)), _iborIndexMaster.getIndex(Indicies.USDLIBOR1M));
	    _indices.put(Triple.of("USD-LIBOR-T3750", Currency.USD, Period.ofMonths(1)), _iborIndexMaster.getIndex(Indicies.USDLIBOR1M));
	    _indices.put(Triple.of("USD-LIBOR-BBA", Currency.USD, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M));
	    _indices.put(Triple.of("LIBOR", Currency.USD, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M));
	    _indices.put(Triple.of("USD-LIBOR-T3750", Currency.USD, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M));
	    _indices.put(Triple.of("USD-LIBOR-BBA", Currency.USD, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.USDLIBOR6M));
	    _indices.put(Triple.of("LIBOR", Currency.USD, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.USDLIBOR6M));
	    _indices.put(Triple.of("USD-LIBOR-T3750", Currency.USD, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.USDLIBOR6M)); 
	}
	
	public IndexDeposit getIndex(final String code, final Currency ccy, final Period period) {
		final IndexDeposit indexNoCalendar = _indices.get(Triple.of(code, ccy, period));
		if (indexNoCalendar == null) {
			throw new OpenGammaRuntimeException("Could not get index for code: " + code +", currency: "+ccy+", period: "+period);
		}
		if (indexNoCalendar instanceof IndexON ) {
			return new IndexON(indexNoCalendar.getName(), indexNoCalendar.getCurrency(), ((IndexON) indexNoCalendar).getDayCount(), ((IndexON) indexNoCalendar).getPublicationLag());
		} else if (indexNoCalendar instanceof IborIndex) {
			return new IborIndex(indexNoCalendar.getCurrency(), ((IborIndex) indexNoCalendar).getTenor(), ((IborIndex) indexNoCalendar).getSpotLag(), ((IborIndex) indexNoCalendar).getDayCount(), 
					((IborIndex) indexNoCalendar).getBusinessDayConvention(), ((IborIndex) indexNoCalendar).isEndOfMonth(), indexNoCalendar.getName());
		} else {
			return null;
		}		
	}
}
