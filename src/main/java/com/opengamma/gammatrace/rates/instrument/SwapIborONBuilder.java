package com.opengamma.gammatrace.rates.instrument;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.datamodel.Trade;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponONDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponONSpreadDefinition;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexDeposit;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.analytics.financial.instrument.payment.CouponIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.payment.CouponONDefinition;
import com.opengamma.analytics.financial.instrument.payment.CouponONSpreadDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapIborIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapIborONDefinition;
import com.opengamma.analytics.financial.interestrate.annuity.derivative.Annuity;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Payment;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.ogextension.SwapONSpreadIborSpreadDefinition;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.ArgumentChecker;

public class SwapIborONBuilder extends SwapBuilder {

	private SwapInstrument instrument;
	
	public SwapIborONBuilder(Trade trade) {
		super(trade);
		instrument = makeInstrument(true);	
	}
	
	private SwapInstrument makeInstrument(Boolean isFirstLegPayer) {
		
		Swap<?, ?> derivative = makeSwap(isFirstLegPayer);
		return new SwapInstrument(derivative, executionTimestamp, effectiveDate, endDate, firstNotional, secondNotional, tradeDate, multiCcyUpfront, InstrumentDescription.IBOR_ON_SWAP, makeAssetPair());
	}
	
	private Swap<?, ?> makeSwap(Boolean isFirstLegPayer) {
		Calendar calendar = new MondayToFridayCalendar("UTC");
		BusinessDayConvention businessDaysConvention = BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following");
		boolean isEOM = true;
		// work out which leg is ibor and which is O/N
		IndexDeposit firstIndex = AssetMapper.getInstance().getIndex(firstAssetName, currency, firstLegResetPeriod);
		IndexDeposit secondIndex = AssetMapper.getInstance().getIndex(secondAssetName, currency, secondLegResetPeriod);
		IborIndex iborIndex;
		IndexON indexON;
		Period iborPaymentPeriod, oisPaymentPeriod;
		double iborSpread, oisSpread;
		if (firstIndex instanceof IborIndex && secondIndex instanceof IndexON) {		// TODO: do we need error checking here? The only way to get to this builder is after performing a similar check in InstrumentBuildManager
			iborIndex = (IborIndex) firstIndex;
			iborPaymentPeriod = firstLegPaymentPeriod;
			iborSpread = priceNotation1;
			indexON = (IndexON) secondIndex;
			oisPaymentPeriod = secondLegPaymentPeriod;
			oisSpread = priceNotation2;
		} else {
			iborIndex = (IborIndex) secondIndex;
			iborPaymentPeriod = secondLegPaymentPeriod;
			iborSpread = priceNotation2;
			indexON = (IndexON) firstIndex;
			oisPaymentPeriod = firstLegPaymentPeriod;
			oisSpread = priceNotation1;
		}
    	
		SwapONSpreadIborSpreadDefinition swapDefinition = SwapONSpreadIborSpreadDefinition.from(effectiveDate, endDate, firstNotional.getAmount(), oisPaymentPeriod, indexON, oisSpread, iborPaymentPeriod, iborIndex, iborSpread, 
				calendar, businessDaysConvention, isEOM, isFirstLegPayer);
    	ZonedDateTimeDoubleTimeSeries[] iborFixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate);
    	return swapDefinition.toDerivative(tradeDate, iborFixing);		
	}
	
	
	public SwapInstrument getInstrument() {
		return instrument;
	}
	@Deprecated
	private static SwapDefinition from(final ZonedDateTime settlementDate, final ZonedDateTime maturityDate, final double notional, final Period iborPaymentPeriod, final IborIndex iborIndex, final double iborSpread, 
			final Period oisPaymentPeriod, final IndexON indexON, final double oisSpread, final Calendar calendar, final BusinessDayConvention businessDayConvention, final boolean isEOM, final boolean isIborPayer) {
	    
		AnnuityCouponIborSpreadDefinition iborLeg = AnnuityCouponIborSpreadDefinition.from(settlementDate, maturityDate, iborPaymentPeriod, notional, iborIndex, isIborPayer, businessDayConvention, isEOM, 
				iborIndex.getDayCount(), iborSpread, calendar);
	    AnnuityCouponONSpreadDefinition oisLeg = AnnuityCouponONSpreadDefinition.from(settlementDate, maturityDate, notional, !isIborPayer, indexON, iborIndex.getSpotLag(), calendar, businessDayConvention, oisPaymentPeriod,
	    		isEOM, oisSpread);
	    return new SwapDefinition(iborLeg, oisLeg);
	}
}
