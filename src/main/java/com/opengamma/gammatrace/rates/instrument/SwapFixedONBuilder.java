package com.opengamma.gammatrace.rates.instrument;

import java.util.Map;
import java.util.regex.Pattern;

import org.threeten.bp.Instant;
import org.threeten.bp.Period;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.commonutils.TradeUtils;
import com.gammatrace.datamodel.Trade;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedONDefinition;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Coupon;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.interestrate.swap.derivative.SwapFixedCoupon;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.rates.indextemplate.ONTemplate;
import com.opengamma.timeseries.precise.zdt.ImmutableZonedDateTimeDoubleTimeSeries;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;

public class SwapFixedONBuilder extends SwapBuilder {

	private Period fixedPaymentPeriod;
	private Period floatPaymentPeriod;
	private Period floatResetPeriod;
	
	private SwapInstrument instrument;
	
	/**
	 * Constructor from all the details contained in the trade map.
	 * @param trade
	 */
	public SwapFixedONBuilder(Trade trade) {
		super(trade);
		fixedPaymentPeriod = TradeUtils.getFixedPaymentPeriod(trade);
		floatPaymentPeriod = TradeUtils.getFloatPaymentPeriod(trade);
		floatResetPeriod = TradeUtils.getFloatResetPeriod(trade);
		instrument = makeInstrument(true);
	}
	
	private SwapInstrument makeInstrument(Boolean isFixedPayer) {
		Swap<?, ?> derivative = makeSwap(isFixedPayer);
		return new SwapInstrument(derivative, executionTimestamp, effectiveDate, endDate, firstNotional, secondNotional, tradeDate, multiCcyUpfront, InstrumentDescription.FIXED_ON_SWAP, makeAssetPair());
	}
	
	private Swap<?, ?> makeSwap(Boolean isFixedPayer) {
    	Calendar UTC = new MondayToFridayCalendar("UTC");
		IndexON indexON = ONTemplate.getInstance().getIndex(currency);
    	// deal with this...
		BusinessDayConvention businessDaysConvention = BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following");
    	
		Boolean floatLegEOM = true; // check this!!!
    	Boolean fixedLegEOM = true; // check this!!!
    	
    	DayCount floatDayCountConvention = indexON.getDayCount(); 
    	// TODO: we are assuming daycount, businessdays, frequency, EOM convention is the same on both legs, design test to check this? 
    	GeneratorSwapFixedON generator = new GeneratorSwapFixedON("generator", indexON, fixedPaymentPeriod, fixedDayCountConvention, businessDaysConvention,
    			fixedLegEOM, 2, 2, UTC);
    	
    	SwapFixedONDefinition swapDefinition = SwapFixedONDefinition.from(effectiveDate, endDate, firstNotional.getAmount(), firstNotional.getAmount(), generator, priceNotation1, true);
    	
    	ZonedDateTimeDoubleTimeSeries[] iborFixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate); 
    	return swapDefinition.toDerivative(tradeDate, iborFixing);	
	}
	
	/**
	 * Gets the Instrument. 
	 * @return The Instrument.
	 */
	public SwapInstrument getInstrument(){
		return instrument;
	}
}
