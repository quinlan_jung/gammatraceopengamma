package com.opengamma.gammatrace.rates.instrument;

import java.util.Map;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.datamodel.Trade;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityDefinitionBuilder;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborIbor;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.analytics.financial.instrument.payment.CouponIborCompoundingFlatSpreadDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedONDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapIborIborDefinition;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Coupon;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.interestrate.swap.derivative.SwapFixedCoupon;
import com.opengamma.financial.convention.StubType;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.ArgumentChecker;

public class SwapIborIborBuilder extends SwapBuilder {
	
	private SwapInstrument instrument;
	
	/**
	 * Constructor from all the details contained in the trade map.
	 * @param trade
	 */
	public SwapIborIborBuilder(Trade trade) {
		super(trade);  //effectiveDate, maturityDate, tradeDate, fixedDayCountConvention, currency, notional, fixedRate, floatSpread
		instrument = makeInstrument(true);
				
	}
	
	private SwapInstrument makeInstrument(Boolean isFirstLegPayer) {
		Swap<?, ?> derivative = makeSwap(isFirstLegPayer);
		return new SwapInstrument(derivative, executionTimestamp, effectiveDate, endDate, firstNotional, secondNotional, tradeDate, multiCcyUpfront, InstrumentDescription.IBOR_IBOR_SWAP, makeAssetPair());
	}
	
	
	/**
	 * Derivative maker from the fields. 
	 * @return The trade derivative on the trade date with the appropriate fixing information
	 */
	private Swap<?, ?> makeSwap(Boolean isFirstLegPayer) {
		Calendar UTC = new MondayToFridayCalendar("UTC");
    	
		IborIndex firstIborIndex = (IborIndex) AssetMapper.getInstance().getIndex(firstAssetName, currency, firstLegResetPeriod);
		IborIndex secondIborIndex = (IborIndex) AssetMapper.getInstance().getIndex(secondAssetName, currency, secondLegResetPeriod);
    	
    	// deal with this...
		BusinessDayConvention businessDaysConvention = BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following");
		boolean firstIborLegEOM = true; 
    	boolean secondIborLegEOM = true;
    	
    	DayCount secondLegDayCountConvention = secondIborIndex.getDayCount();
    	
    	if (firstLegPaymentPeriod.equals(firstLegResetPeriod) && secondLegPaymentPeriod.equals(secondLegResetPeriod)) {
    		SwapIborIborDefinition swapDefinition = from(effectiveDate, endDate, firstLegPaymentPeriod, fixedDayCountConvention, businessDaysConvention, firstIborLegEOM,
        			firstNotional.getAmount(), firstIborIndex, priceNotation1, UTC, secondLegPaymentPeriod, secondLegDayCountConvention ,businessDaysConvention, secondIborLegEOM, secondNotional.getAmount(), 
        			secondIborIndex, priceNotation2, UTC, isFirstLegPayer);
    		ZonedDateTimeDoubleTimeSeries[] iborFixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate);
    		return swapDefinition.toDerivative(tradeDate, iborFixing);
    		
    	} else if (!firstLegPaymentPeriod.equals(firstLegResetPeriod)) {
    		AnnuityDefinition<CouponIborCompoundingFlatSpreadDefinition> firstLeg = AnnuityDefinitionBuilder.annuityIborCompoundingFlatSpreadFrom(effectiveDate, endDate, firstLegPaymentPeriod, firstNotional.getAmount(), 
    				priceNotation1, firstIborIndex, StubType.SHORT_START, isFirstLegPayer, businessDaysConvention, firstIborLegEOM, UTC, StubType.SHORT_START); //the first StubType doesn't seem to be used in the annuity builder.
    		AnnuityCouponIborSpreadDefinition secondLeg = AnnuityCouponIborSpreadDefinition.from(effectiveDate, endDate, secondLegPaymentPeriod, secondNotional.getAmount(), secondIborIndex,
    		        !isFirstLegPayer, businessDaysConvention, secondIborLegEOM, secondLegDayCountConvention, priceNotation2, UTC);
    		SwapDefinition swapDefinition = new SwapDefinition(firstLeg, secondLeg);
    		ZonedDateTimeDoubleTimeSeries[] iborFixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate);
    		return swapDefinition.toDerivative(tradeDate, iborFixing);
    		
    	} else if (!secondLegPaymentPeriod.equals(secondLegResetPeriod)) {
    		AnnuityCouponIborSpreadDefinition firstLeg = AnnuityCouponIborSpreadDefinition.from(effectiveDate, endDate, firstLegPaymentPeriod, firstNotional.getAmount(), firstIborIndex,
    		        isFirstLegPayer, businessDaysConvention, firstIborLegEOM, fixedDayCountConvention, priceNotation1, UTC);
    		AnnuityDefinition<CouponIborCompoundingFlatSpreadDefinition> secondLeg = AnnuityDefinitionBuilder.annuityIborCompoundingFlatSpreadFrom(effectiveDate, endDate, secondLegPaymentPeriod, secondNotional.getAmount(), 
    				priceNotation2, secondIborIndex, StubType.SHORT_START, !isFirstLegPayer, businessDaysConvention, secondIborLegEOM, UTC, StubType.SHORT_START);
    		SwapDefinition swapDefinition = new SwapDefinition(firstLeg, secondLeg);
    		ZonedDateTimeDoubleTimeSeries[] iborFixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate);
    		return swapDefinition.toDerivative(tradeDate, iborFixing);
    		
    	} else {
    		AnnuityDefinition<CouponIborCompoundingFlatSpreadDefinition> firstLeg = AnnuityDefinitionBuilder.annuityIborCompoundingFlatSpreadFrom(effectiveDate, endDate, firstLegPaymentPeriod, firstNotional.getAmount(), 
    				priceNotation1, firstIborIndex, StubType.SHORT_START, isFirstLegPayer, businessDaysConvention, firstIborLegEOM, UTC, StubType.SHORT_START);
    		AnnuityDefinition<CouponIborCompoundingFlatSpreadDefinition> secondLeg = AnnuityDefinitionBuilder.annuityIborCompoundingFlatSpreadFrom(effectiveDate, endDate, secondLegPaymentPeriod, secondNotional.getAmount(), 
    				priceNotation2, secondIborIndex, StubType.SHORT_START, !isFirstLegPayer, businessDaysConvention, secondIborLegEOM, UTC, StubType.SHORT_START);
    		SwapDefinition swapDefinition = new SwapDefinition(firstLeg, secondLeg);
    		ZonedDateTimeDoubleTimeSeries[] iborFixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate);
    		return swapDefinition.toDerivative(tradeDate, iborFixing);
    	}		
	}
	
	/**
	 * Gets the Instrument. 
	 * @return The Instrument.
	 */
	public SwapInstrument getInstrument(){
		return instrument;
	}
	
	/**
	 * @param settlementDate The settlement date.
	 * @param maturityDate The swap maturity date.
	 * @param firstLegPeriod The Ibor leg payment period.
	 * @param firstLegDayCount The Ibor leg day count convention.
	 * @param firstLegBusinessDayConvention The Ibor leg business day convention.
	 * @param firstLegEOM The Ibor leg end-of-month.
	 * @param firstLegNotional The Ibor leg notional.
	 * @param firstIborIndex The Ibor index.
	 * @param firstLegSpread The Ibor leg spread.
	 * @param firstLegCalendar The holiday calendar for the first ibor leg.
	 * @param secondLegPeriod The Ibor leg payment period.
	 * @param secondLegDayCount The Ibor leg day count convention.
	 * @param secondLegBusinessDayConvention The Ibor leg business day convention.
	 * @param secondLegEOM The Ibor leg end-of-month.
	 * @param secondLegNotional The Ibor leg notional.
	 * @param secondIborIndex The Ibor index.
	 * @param secondLegSpread The Ibor leg spread.
	 * @param secondLegCalendar The holiday calendar for the second ibor leg.
	 * @param isPayer The payer flag for the first leg.
	 */
	private static SwapIborIborDefinition from(final ZonedDateTime settlementDate, final ZonedDateTime maturityDate, final Period firstLegPeriod, final DayCount firstLegDayCount,
			final BusinessDayConvention firstLegBusinessDayConvention, final boolean firstLegEOM, final double firstLegNotional, final IborIndex firstIborIndex,
			final double firstLegSpread, final Calendar firstLegCalendar, final Period secondLegPeriod, final DayCount secondLegDayCount, final BusinessDayConvention secondLegBusinessDayConvention,
			final boolean secondLegEOM, final double secondLegNotional, final IborIndex secondIborIndex, final double secondLegSpread, final Calendar secondLegCalendar,
			final boolean isPayer) {
		ArgumentChecker.notNull(settlementDate, "settlement date");
		ArgumentChecker.notNull(maturityDate, "maturity");
		ArgumentChecker.notNull(firstIborIndex, "first ibor index");
		ArgumentChecker.notNull(secondIborIndex, "second ibor index");
		final AnnuityCouponIborSpreadDefinition firstLeg = AnnuityCouponIborSpreadDefinition.from(settlementDate, maturityDate, firstLegPeriod, firstLegNotional, firstIborIndex,
		        isPayer, firstLegBusinessDayConvention, firstLegEOM, firstLegDayCount, firstLegSpread, firstLegCalendar);
		final AnnuityCouponIborSpreadDefinition secondLeg = AnnuityCouponIborSpreadDefinition.from(settlementDate, maturityDate, secondLegPeriod, secondLegNotional, secondIborIndex,
		        !isPayer, secondLegBusinessDayConvention, secondLegEOM, secondLegDayCount, secondLegSpread, secondLegCalendar);
		return new SwapIborIborDefinition(firstLeg, secondLeg);
	}
		
}
