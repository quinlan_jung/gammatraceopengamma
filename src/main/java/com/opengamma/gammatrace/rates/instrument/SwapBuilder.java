package com.opengamma.gammatrace.rates.instrument;

import org.threeten.bp.Instant;
import org.threeten.bp.Period;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.datamodel.Trade;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.gammatrace.rates.instrument.AssetMapper;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.tuple.Pair;

public abstract class SwapBuilder {
	
	protected ZonedDateTime effectiveDate;
	protected ZonedDateTime endDate;
	protected ZonedDateTime executionTimestamp;
	protected ZonedDateTime tradeDate;
	protected DayCount fixedDayCountConvention;
	protected Currency currency;
	protected CurrencyAmount firstNotional;
	protected CurrencyAmount secondNotional;
	protected Currency firstNotionalCcy;
	protected Currency secondNotionalCcy;
	protected double priceNotation1;
	protected double priceNotation2;
	protected final String firstAssetName;
	protected final String secondAssetName;
	protected final Period firstLegPaymentPeriod;
	protected final Period secondLegPaymentPeriod;
	protected final Period firstLegResetPeriod;
	protected final Period secondLegResetPeriod;
	
	protected MultipleCurrencyAmount multiCcyUpfront;
	
	public SwapBuilder (Trade trade) {
		effectiveDate = epochToDateMidnight(trade.getEffective_date());
		endDate = epochToDateMidnight(trade.getEnd_date());
		executionTimestamp = epochToDate(trade.getExecution_timestamp());
		tradeDate = epochToDateMidnight(trade.getExecution_timestamp());
		fixedDayCountConvention = dayCountMapping(trade.getDay_count_convention());
		currency = Currency.parse(trade.getSettlement_currency());
		firstNotionalCcy = Currency.parse(trade.getNotional_currency_1());
		secondNotionalCcy = Currency.parse(trade.getNotional_currency_2());
		firstNotional = CurrencyAmount.of(firstNotionalCcy, trade.getRounded_notional_amount_1());
		secondNotional = CurrencyAmount.of(secondNotionalCcy, trade.getRounded_notional_amount_2());
		priceNotation1 = trade.getPrice_notation();
		priceNotation2 = trade.getPrice_notation2(); // TODO: look at the diff: correct?
		firstAssetName = trade.getUnderlying_asset_1();
		secondAssetName = trade.getUnderlying_asset_2();
		firstLegPaymentPeriod = trade.getPayment_frequency_1_asPeriod();
		secondLegPaymentPeriod = trade.getPayment_frequency_2_asPeriod();
		firstLegResetPeriod = trade.getReset_frequency_1_asPeriod();
		secondLegResetPeriod = trade.getReset_frequency_2_asPeriod();
		
		String additionalPriceNotationType = trade.getAdditional_price_notation_type();
		if (additionalPriceNotationType == null || additionalPriceNotationType.isEmpty()) {
			multiCcyUpfront = MultipleCurrencyAmount.of(currency, 0.0);
		} else {
			Currency upfrontCcy = Currency.parse(trade.getAdditional_price_notation_type());
			double upfront = trade.getAdditional_price_notation();
			multiCcyUpfront = MultipleCurrencyAmount.of(upfrontCcy, upfront);
		}
	}
	
	public CurrencyAmount getFirstNotional() {
		return firstNotional;
	}
	public CurrencyAmount getSecondNotional() {
		return secondNotional;
	}
	
	public ZonedDateTime getEffectiveDate(){
		return effectiveDate;
	}
	
	public ZonedDateTime getEndDate() {
		return endDate;
	}
	
	public ZonedDateTime getExecutionTimestamp() {
		return executionTimestamp;
	}
	
	public ZonedDateTime getTradeDate() {
		return tradeDate;
	}
	
	public MultipleCurrencyAmount getUpfront() {
		return multiCcyUpfront;
	}

	protected Pair<String, String> makeAssetPair() {
		Pair<String, String> assetPair;
		if (firstAssetName.equalsIgnoreCase("FIXED")) {
			assetPair = Pair.of("FIXED", AssetMapper.getInstance().getIndex(secondAssetName, secondNotionalCcy, secondLegResetPeriod).getName());
		} else if (secondAssetName.equalsIgnoreCase("FIXED")) {
			assetPair = Pair.of(AssetMapper.getInstance().getIndex(firstAssetName, firstNotionalCcy, firstLegResetPeriod).getName(), "FIXED");
		} else {
			assetPair = Pair.of(AssetMapper.getInstance().getIndex(firstAssetName, firstNotionalCcy, firstLegResetPeriod).getName(), 
					AssetMapper.getInstance().getIndex(secondAssetName, secondNotionalCcy, secondLegResetPeriod).getName());
		}
		return assetPair;
	}
	
	private ZonedDateTime epochToDate (long epoch) {
		ZonedDateTime dateTime = ZonedDateTime.ofInstant(Instant.ofEpochSecond(epoch), ZoneId.of("UTC"));
		return dateTime;
	}

	/**
	 * Convert epoch to a ZonedDateTime with time set to midnight.
	 * @param epoch
	 * @return The zoned date-time
	 */
	private ZonedDateTime epochToDateMidnight (long epoch) {
		ZonedDateTime dateMidnight = ZonedDateTime.ofInstant(Instant.ofEpochSecond(epoch), ZoneId.of("UTC")).toLocalDate().atStartOfDay(ZoneId.of("UTC"));
		return dateMidnight;	
	}
	
	/**
	 * Maps FpML (and some misc labeling) day count convention code strings to OG DayCount.
	 * @param dayCountConvention string 
	 * @return The DayCount 
	 */
	private DayCount dayCountMapping(String dayCountConvention) {
		DayCountFactory factory = DayCountFactory.INSTANCE;
		if (dayCountConvention.equalsIgnoreCase("ACT/ACT.ISDA")){
			return factory.getDayCount("Actual/Actual ISDA");
		} else if (dayCountConvention.equalsIgnoreCase("ACT/ACT.AFB")){
			return factory.getDayCount("Actual/Actual AFB");
		} else if (dayCountConvention.equalsIgnoreCase("ACT/ACT.ICMA")){
			return factory.getDayCount("Actual/Actual ICMA");	
		} else if (dayCountConvention.equalsIgnoreCase("ACT/ACT.ICMA")){
			return factory.getDayCount("Actual/Actual ICMA");	
		} else if (dayCountConvention.equalsIgnoreCase("ACT/365.FIXED")){
			return factory.getDayCount("Actual/365");
		} else if (dayCountConvention.equalsIgnoreCase("ACT/360")){
			return factory.getDayCount("Actual/360");
		} else if (dayCountConvention.equalsIgnoreCase("30/360")){
			return factory.getDayCount("30U/360");
		} else if (dayCountConvention.equalsIgnoreCase("30E/360")){
			return factory.getDayCount("30E/360");
		} else if (dayCountConvention.equalsIgnoreCase("30E/360.ISDA")){
			return factory.getDayCount("30E/360 ISDA");
		} else if (dayCountConvention.equalsIgnoreCase("1/1")){
			return factory.getDayCount("1/1");
		} else if (dayCountConvention.equalsIgnoreCase("BUS/252")){
			return factory.getDayCount("BUS/252");
		} else if (dayCountConvention.equalsIgnoreCase("BD/252")){
			return factory.getDayCount("BUS/252");	// non FpML, but seen in the data
		} else {
			throw new IllegalArgumentException("Invalid Day Count Convention given: " + dayCountConvention);
		}
	}
}
