package com.opengamma.gammatrace.rates.instrument;

import org.threeten.bp.Period;

import com.gammatrace.datamodel.Trade;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.ogextension.SwapXCcyFixedIborDefinition;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.time.DateUtils;

public class SwapXCcyFixedIborBuilder extends SwapBuilder {

	private SwapInstrument instrument;

	/**
	 * Constructor from all the details contained in the trade map.
	 * @param trade
	 */
	public SwapXCcyFixedIborBuilder(Trade trade) {
		super(trade); 
		instrument = makeInstrument(true);		
	}
	
	private SwapInstrument makeInstrument(Boolean isFixedPayer) {
		Swap<?, ?> derivative = makeSwap(isFixedPayer);
		return new SwapInstrument(derivative, executionTimestamp, effectiveDate, endDate, firstNotional, secondNotional, tradeDate, multiCcyUpfront, InstrumentDescription.XCCY_FIXED_IBOR_SWAP, makeAssetPair());
	}
	
	/**
	 * Derivative maker from the fields. 
	 * @return The trade derivative on the trade date with the appropriate fixing information
	 */
	private Swap<?, ?> makeSwap(Boolean isFixedPayer) {
		Calendar calendar = new MondayToFridayCalendar("UTC");
		BusinessDayConvention businessDaysConvention = BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following");
		boolean isEOM = true;
		
		CurrencyAmount fixedNotional, iborNotional ;
		IborIndex iborIndex;
		Period fixedPaymentPeriod;
		if (firstAssetName.equalsIgnoreCase("FIXED")) {
			fixedNotional = firstNotional;
			iborNotional = secondNotional;
			fixedPaymentPeriod = firstLegPaymentPeriod;
			iborIndex = (IborIndex) AssetMapper.getInstance().getIndex(secondAssetName, secondNotional.getCurrency(), secondLegResetPeriod);
		} else {
			fixedNotional = secondNotional;
			iborNotional = firstNotional;
			fixedPaymentPeriod = secondLegPaymentPeriod;
			iborIndex = (IborIndex) AssetMapper.getInstance().getIndex(firstAssetName, firstNotional.getCurrency(), firstLegResetPeriod);
		}
		SwapXCcyFixedIborDefinition swapDefinition = SwapXCcyFixedIborDefinition.from(effectiveDate, endDate, fixedNotional.getCurrency(), fixedNotional.getAmount(), fixedPaymentPeriod, fixedDayCountConvention, 
			priceNotation1, iborNotional.getAmount(), iborIndex, priceNotation2, calendar, businessDaysConvention, isEOM, true);
		ZonedDateTimeDoubleTimeSeries[] iborFixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate); 
    	return swapDefinition.toDerivative(tradeDate, iborFixing);	
	}
	
	/**
	 * Gets the Instrument. 
	 * @return The Instrument.
	 */
	public SwapInstrument getInstrument(){
		return instrument;
	}	
}
