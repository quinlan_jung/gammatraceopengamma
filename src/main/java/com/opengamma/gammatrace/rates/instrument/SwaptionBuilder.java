package com.opengamma.gammatrace.rates.instrument;

import org.threeten.bp.Instant;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.datamodel.Trade;
import com.opengamma.gammatrace.exceptions.UnsupportedOperation;
import com.opengamma.gammatrace.instrument.SwapInstrument;
//we have to use the swap builder in here to get the swap definition (instrument doesn't contain the definition, only the derivative)
public class SwaptionBuilder {
	
	protected ZonedDateTime expirationDate;
	protected double optionPremium;
	protected String optionType;
	protected SwapFixedIborBuilder[] underlyingSwapBuilder;
	
	public SwaptionBuilder(Trade trade) throws UnsupportedOperation {		
		expirationDate = epochToDateMidnight(trade.getOption_expiration_date());
		optionPremium = (trade.getOption_premium() == null)? trade.getAdditional_price_notation() : trade.getOption_premium();
		optionType = trade.getOption_type(); 
		
		if (optionType.equalsIgnoreCase("PF")){
			underlyingSwapBuilder = new SwapFixedIborBuilder[] {new SwapFixedIborBuilder(trade, true)};
		} else if (optionType.equalsIgnoreCase("RF")) {
			underlyingSwapBuilder = new SwapFixedIborBuilder[] {new SwapFixedIborBuilder(trade, false)};
		} else if (optionType.equalsIgnoreCase("D-")) {
			underlyingSwapBuilder = new SwapFixedIborBuilder[] {new SwapFixedIborBuilder(trade, true), new SwapFixedIborBuilder(trade, false)};
		} else {
			throw new UnsupportedOperation("Unsupported option type:" +optionType);
		}
	}
	public SwapFixedIborBuilder[] getUnderlyingSwapBuilder() {
		return underlyingSwapBuilder;
	}
	
	public SwapInstrument[] getUnderlyingSwap() {
		SwapInstrument[] underlyingSwap = new SwapInstrument[underlyingSwapBuilder.length];
		int i = 0;
		for (SwapFixedIborBuilder eachBuilder : underlyingSwapBuilder) {
			underlyingSwap[i] = eachBuilder.getInstrument();
			i++;
		}
		return underlyingSwap;
	}
	
	public ZonedDateTime getExpirationDate() {
		return expirationDate;
	}

	// TODO: how to determine between spot and forward premium???????
	public double getOptionPremium() {
		return optionPremium;
	}
	
	
	/**
	 * Convert epoch to a ZonedDateTime with time set to midnight.
	 * @param epoch
	 * @return The zoned date-time
	 */
	private ZonedDateTime epochToDateMidnight (long epoch) {
		ZonedDateTime dateMidnight = ZonedDateTime.ofInstant(Instant.ofEpochSecond(epoch), ZoneId.of("UTC")).toLocalDate().atStartOfDay(ZoneId.of("UTC"));
		return dateMidnight;	
	}
	
}
