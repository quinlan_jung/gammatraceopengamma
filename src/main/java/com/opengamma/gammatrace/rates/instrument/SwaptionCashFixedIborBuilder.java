package com.opengamma.gammatrace.rates.instrument;

import org.threeten.bp.ZonedDateTime;

import com.gammatrace.datamodel.Trade;
import com.opengamma.analytics.financial.instrument.swaption.SwaptionCashFixedIborDefinition;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionCashFixedIbor;
import com.opengamma.gammatrace.exceptions.UnsupportedOperation;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwaptionInstrument;

//we have to use the swap builder in here to get the swap definition (SwapInstrument doesn't contain the definition, only the derivative)
public class SwaptionCashFixedIborBuilder extends SwaptionBuilder {
	private SwaptionInstrument instrument;
	
	public SwaptionCashFixedIborBuilder(Trade trade) throws UnsupportedOperation{
		super(trade);
		instrument = makeInstrument();
	}
	
	private SwaptionInstrument makeInstrument() {
		SwaptionCashFixedIbor[] swaption = makeSwaption();
		SwapFixedIborBuilder build = underlyingSwapBuilder[0];
		return new SwaptionInstrument(swaption, build.getExecutionTimestamp() , optionPremium, expirationDate, build.getEffectiveDate(), build.getEndDate(), 
				build.getFirstNotional(), build.getSecondNotional(), build.getTradeDate(), build.getUpfront(), InstrumentDescription.FIXED_IBOR_CASH_SWAPTION, build.makeAssetPair());	
	}
	
	private SwaptionCashFixedIbor[] makeSwaption() {
		ZonedDateTime tradeDate = underlyingSwapBuilder[0].getTradeDate();
		SwaptionCashFixedIbor[] swaption = new SwaptionCashFixedIbor[underlyingSwapBuilder.length];
		for (int i = 0; i < underlyingSwapBuilder.length; i++) {
			SwaptionCashFixedIborDefinition swaptionDefinition = SwaptionCashFixedIborDefinition.from(expirationDate, underlyingSwapBuilder[i].getSwapDefinitionForCashSwaption(), true);
			swaption[i] = swaptionDefinition.toDerivative(tradeDate);
		} 
		return swaption;
	}
	
	public SwaptionInstrument getInstrument() {
		return instrument;
	}
}
