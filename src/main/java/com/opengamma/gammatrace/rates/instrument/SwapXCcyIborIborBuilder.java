package com.opengamma.gammatrace.rates.instrument;

import java.util.Map;

import org.threeten.bp.ZoneOffset;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.datamodel.Trade;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityDefinitionBuilder;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.payment.PaymentDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapXCcyIborIborDefinition;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.ArgumentChecker;

public class SwapXCcyIborIborBuilder extends SwapBuilder {
	
	private SwapInstrument instrument;
	
	/**
	 * Constructor from all the details contained in the trade map.
	 * @param trade
	 */
	public SwapXCcyIborIborBuilder(Trade trade) {
		super(trade);  //effectiveDate, maturityDate, executionTimestamp, tradeDate, fixedDayCountConvention, currency, notional, fixedRate, floatSpread
		instrument = makeInstrument(true);		
	}
	
	private SwapInstrument makeInstrument(Boolean isFixedPayer) {
		Swap<?, ?> derivative = makeSwap(isFixedPayer);
		return new SwapInstrument(derivative, executionTimestamp, effectiveDate, endDate, firstNotional, secondNotional, tradeDate, multiCcyUpfront, InstrumentDescription.XCCY_IBOR_IBOR_SWAP, makeAssetPair());
	}
	
	/**
	 * Derivative maker from the fields. 
	 * @return The trade derivative on the trade date with the appropriate fixing information
	 */
	private Swap<?, ?> makeSwap(Boolean isFixedPayer) {
		Calendar UTC = new MondayToFridayCalendar("UTC");
		IborIndex firstIborIndex = (IborIndex) AssetMapper.getInstance().getIndex(firstAssetName, firstNotionalCcy, firstLegResetPeriod);
		IborIndex secondIborIndex = (IborIndex) AssetMapper.getInstance().getIndex(secondAssetName, secondNotionalCcy, secondLegResetPeriod);
    	SwapXCcyIborIborDefinition swapDefinition = from(effectiveDate, endDate, firstIborIndex, secondIborIndex, firstNotional.getAmount(), secondNotional.getAmount(), priceNotation1, 
    			priceNotation2, UTC, UTC, true);
    	ZonedDateTimeDoubleTimeSeries[] iborFixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, tradeDate);
    	return swapDefinition.toDerivative(tradeDate, iborFixing);	
	}
	
	/**
	 * Gets the Instrument. 
	 * @return The Instrument.
	 */
	public SwapInstrument getInstrument(){
		return instrument;
	}
	
	/**
	   * Builder from the settlement date and a generator. The legs have different notionals.
	   * The notionals are paid on the settlement date and final payment date of each leg.
	   * @param settlementDate The settlement date.
	   * @param maturityDate The swap maturity date.
	   * @param generator The Ibor/Ibor swap generator.
	   * @param notional1 The first leg notional.
	   * @param notional2 The second leg notional.
	   * @param spread1 The spread to be applied to the first leg.
	   * @param spread2 The spread to be applied to the second leg.
	   * @param isPayer The payer flag for the first leg.
	   * @return The swap.
	   */
	public static SwapXCcyIborIborDefinition from(final ZonedDateTime settlementDate, final ZonedDateTime maturityDate, final IborIndex iborIndex1, final IborIndex iborIndex2, final double notional1,
			final double notional2, final double spread1, final double spread2, final Calendar calendar1, final Calendar calendar2, final boolean isPayer) {
	    ArgumentChecker.notNull(settlementDate, "settlement date");
	    ArgumentChecker.notNull(maturityDate, "Maturity date");
	    
	    // TODO: create a mechanism for the simultaneous payments on both legs, i.e. joint calendar
	    final AnnuityDefinition<PaymentDefinition> firstLegNotional = AnnuityDefinitionBuilder.annuityIborSpreadWithNotionalFrom(settlementDate, maturityDate,
	        notional1, iborIndex1, spread1, isPayer, calendar1);
	    final AnnuityDefinition<PaymentDefinition> secondLegNotional = AnnuityDefinitionBuilder.annuityIborSpreadWithNotionalFrom(settlementDate, maturityDate,
	        notional2, iborIndex2, spread2, !isPayer, calendar2);
	    return new SwapXCcyIborIborDefinition(firstLegNotional, secondLegNotional);
	}
}
