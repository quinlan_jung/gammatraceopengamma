package com.opengamma.gammatrace.rates.generatortemplate;

import java.util.HashMap;
import java.util.Map;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborON;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.CalendarNoHoliday;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;
import com.opengamma.gammatrace.rates.indextemplate.ONTemplateByName;

/**
 * A list of Ibor vs. ON basis swap generators to be used to create the instruments required to describe the market inputs 
 * to a curve.
 */
public final class GeneratorSwapIborONTemplate {

  /**
   * The method unique instance.
   */
  private static final GeneratorSwapIborONTemplate INSTANCE = new GeneratorSwapIborONTemplate();

  /**
   * Return the unique instance of the class.
   * @return The instance.
   */
  public static GeneratorSwapIborONTemplate getInstance() {
    return INSTANCE;
  }

  /**
   * The map with the list of names and the swap generators.
   */
  private final Map<Generators, GeneratorSwapIborON> _generatorSwap;

  /**
   * The list of Ibor indices.
   */
  private final IborTemplateByName _iborIndexMaster;
  
  /**
   * The list of ON indices.
   */
  private final ONTemplateByName _indexONMaster;
  
  /**
   * Private constructor.
   */
  private GeneratorSwapIborONTemplate() {
    _iborIndexMaster = IborTemplateByName.getInstance();
    _indexONMaster = ONTemplateByName.getInstance();
    final Calendar baseCalendar = new CalendarNoHoliday("No Holidays");
    _generatorSwap = new HashMap<>();
    _generatorSwap.put(Generators.USD3MON, new GeneratorSwapIborON("USD3MON", _iborIndexMaster.getIndex(Indicies.USDLIBOR3M), _indexONMaster.getIndex(Indicies.FEDFUNDS),
    		_iborIndexMaster.getIndex(Indicies.USDLIBOR3M).getBusinessDayConvention(),
    		_iborIndexMaster.getIndex(Indicies.USDLIBOR3M).isEndOfMonth(), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M).getSpotLag(), 
    		baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.JPY3MON, new GeneratorSwapIborON("JPY3MON", _iborIndexMaster.getIndex(Indicies.JPYLIBOR3M), _indexONMaster.getIndex(Indicies.TONAR),
    		_iborIndexMaster.getIndex(Indicies.JPYLIBOR3M).getBusinessDayConvention(),
    		_iborIndexMaster.getIndex(Indicies.JPYLIBOR3M).isEndOfMonth(), _iborIndexMaster.getIndex(Indicies.JPYLIBOR3M).getSpotLag(), 
    		baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.EUR3MON, new GeneratorSwapIborON("EUR3MON", _iborIndexMaster.getIndex(Indicies.EURIBOR3M), _indexONMaster.getIndex(Indicies.EONIA),
    		_iborIndexMaster.getIndex(Indicies.EURIBOR3M).getBusinessDayConvention(),
    		_iborIndexMaster.getIndex(Indicies.EURIBOR3M).isEndOfMonth(), _iborIndexMaster.getIndex(Indicies.EURIBOR3M).getSpotLag(), 
    		baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.AUD3MON, new GeneratorSwapIborON("AUD3MON", _iborIndexMaster.getIndex(Indicies.AUDBB3M), _indexONMaster.getIndex(Indicies.RBA_ON),
    		_iborIndexMaster.getIndex(Indicies.AUDBB3M).getBusinessDayConvention(),
    		_iborIndexMaster.getIndex(Indicies.AUDBB3M).isEndOfMonth(), _iborIndexMaster.getIndex(Indicies.AUDBB3M).getSpotLag(), 
    		baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.GBP3MON, new GeneratorSwapIborON("GBP3MON", _iborIndexMaster.getIndex(Indicies.GBPLIBOR3M), _indexONMaster.getIndex(Indicies.SONIA),
    		_iborIndexMaster.getIndex(Indicies.GBPLIBOR3M).getBusinessDayConvention(),
    		_iborIndexMaster.getIndex(Indicies.GBPLIBOR3M).isEndOfMonth(), _iborIndexMaster.getIndex(Indicies.GBPLIBOR3M).getSpotLag(), 
    		baseCalendar, baseCalendar));
  }
  
  public GeneratorSwapIborON getGenerator(final Generators generatorName, final Calendar cal) {
    final GeneratorSwapIborON generatorNoCalendar = _generatorSwap.get(generatorName);
    if (generatorNoCalendar == null) {
      throw new OpenGammaRuntimeException("Could not get Ibor index for " + generatorName.toString());
    }
    IborIndex IborLeg = generatorNoCalendar.getIndexIbor();
    IndexON ONLeg = generatorNoCalendar.getIndexON();
    BusinessDayConvention busDays = generatorNoCalendar.getIndexIbor().getBusinessDayConvention();
    Boolean EOM = generatorNoCalendar.getIndexIbor().isEndOfMonth();
    int spotLag = generatorNoCalendar.getIndexIbor().getSpotLag();
    
    return new GeneratorSwapIborON(generatorNoCalendar.getName(), IborLeg, ONLeg, busDays, EOM, spotLag, cal,cal); 
  }

}
