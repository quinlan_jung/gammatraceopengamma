package com.opengamma.gammatrace.rates.generatortemplate;

import java.util.HashMap;
import java.util.Map;

import org.threeten.bp.Period;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.CalendarNoHoliday;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;

public class GeneratorSwapXCcyIborIborTemplate {
	/**
	 * The method unique instance.
	 */
	private static final GeneratorSwapXCcyIborIborTemplate INSTANCE = new GeneratorSwapXCcyIborIborTemplate();
	
	/**
	 * Return the unique instance of the class.
	 * @return The instance.
	 */
	public static GeneratorSwapXCcyIborIborTemplate getInstance() {
		return INSTANCE;
	}
	
	/**
	 * The map with the list of names and the swap generators.
	 */
	private final Map<Generators, GeneratorSwapXCcyIborIbor> _generatorSwap;
	
	/**
	 * The list of Ibor indexes for test purposes.
	 */
	 private final IborTemplateByName _iborIndexMaster;
	
	/**
	 * Private constructor.
	 */
	 // first leg should be the leg on which the spread is to be applied... i.e. normally NOT the USD leg. 
	private GeneratorSwapXCcyIborIborTemplate() {
		_iborIndexMaster = IborTemplateByName.getInstance();
		final Calendar baseCalendar = new CalendarNoHoliday("No Holidays");
		_generatorSwap = new HashMap<>();
		_generatorSwap.put(Generators.EUR3MUSD3M, 
				new GeneratorSwapXCcyIborIbor("EUR3MUSD3M", _iborIndexMaster.getIndex(Indicies.EURIBOR3M), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M).getBusinessDayConvention(),
						_iborIndexMaster.getIndex(Indicies.USDLIBOR3M).isEndOfMonth(), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M).getSpotLag(), baseCalendar, baseCalendar));
		_generatorSwap.put(Generators.GBP3MUSD3M,
				new GeneratorSwapXCcyIborIbor("GBP3MUSD3M", _iborIndexMaster.getIndex(Indicies.GBPLIBOR3M), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M).getBusinessDayConvention(),
						_iborIndexMaster.getIndex(Indicies.USDLIBOR3M).isEndOfMonth(), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M).getSpotLag(), baseCalendar, baseCalendar));
		_generatorSwap.put(Generators.GBP3MEUR3M,
				new GeneratorSwapXCcyIborIbor("GBP3MEUR3M", _iborIndexMaster.getIndex(Indicies.GBPLIBOR3M), _iborIndexMaster.getIndex(Indicies.EURIBOR3M), _iborIndexMaster.getIndex(Indicies.EURIBOR3M).getBusinessDayConvention(),
						_iborIndexMaster.getIndex(Indicies.EURIBOR3M).isEndOfMonth(), _iborIndexMaster.getIndex(Indicies.EURIBOR3M).getSpotLag(), baseCalendar, baseCalendar));
		_generatorSwap.put(Generators.JPY3MUSD3M,
				new GeneratorSwapXCcyIborIbor("JPY3MUSD3M", _iborIndexMaster.getIndex(Indicies.JPYLIBOR3M), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M).getBusinessDayConvention(),
						_iborIndexMaster.getIndex(Indicies.USDLIBOR3M).isEndOfMonth(), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M).getSpotLag(), baseCalendar, baseCalendar));
		_generatorSwap.put(Generators.JPY3MEUR3M,
				new GeneratorSwapXCcyIborIbor("JPY3MEUR3M", _iborIndexMaster.getIndex(Indicies.JPYLIBOR3M), _iborIndexMaster.getIndex(Indicies.EURIBOR3M), _iborIndexMaster.getIndex(Indicies.EURIBOR3M).getBusinessDayConvention(),
						_iborIndexMaster.getIndex(Indicies.EURIBOR3M).isEndOfMonth(), _iborIndexMaster.getIndex(Indicies.EURIBOR3M).getSpotLag(), baseCalendar, baseCalendar));
	}
	
	public GeneratorSwapXCcyIborIbor getGenerator(final Generators generatorName, final Calendar firstCalendar, final Calendar secondCalendar) {
	    final GeneratorSwapXCcyIborIbor generatorNoCalendar = _generatorSwap.get(generatorName);
	    if (generatorNoCalendar == null) {
	    	throw new OpenGammaRuntimeException("Could not get XCcy generator for " + generatorName);
	    }
	    return new GeneratorSwapXCcyIborIbor(generatorNoCalendar.getName(), generatorNoCalendar.getIborIndex1(), generatorNoCalendar.getIborIndex2(), generatorNoCalendar.getIborIndex2().getBusinessDayConvention(),
	    		generatorNoCalendar.getIborIndex2().isEndOfMonth(), generatorNoCalendar.getIborIndex2().getSpotLag(), firstCalendar, secondCalendar);
	}

}
