package com.opengamma.gammatrace.rates.generatortemplate;

import java.util.HashMap;
import java.util.Map;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.instrument.index.GeneratorForexSwap;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.CalendarNoHoliday;
import com.opengamma.util.money.Currency;

public class GeneratorForexSwapTemplate {
	/**
	 * The method unique instance.
	 */
	private static final GeneratorForexSwapTemplate INSTANCE = new GeneratorForexSwapTemplate();
	
	/**
	 * Return the unique instance of the class.
	 * @return The instance.
	 */
	public static GeneratorForexSwapTemplate getInstance() {
		return INSTANCE;
	}
	
	/**
	 * The map with the list of names and the swap generators.
	 */
	private final Map<Generators, GeneratorForexSwap> _generatorSwap;
	
	/**
	 * Private constructor.
	 */
	private GeneratorForexSwapTemplate() {
		final Calendar baseCalendar = new CalendarNoHoliday("No Holidays"); // joint calendar
		final BusinessDayConvention modFol = BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following");
		_generatorSwap = new HashMap<>();
		_generatorSwap.put(Generators.EURUSD, new GeneratorForexSwap("EURUSD", Currency.USD, Currency.EUR, baseCalendar, 2, modFol, true));
		_generatorSwap.put(Generators.GBPUSD, new GeneratorForexSwap("GBPUSD", Currency.USD, Currency.GBP, baseCalendar, 2, modFol, true));
		_generatorSwap.put(Generators.JPYUSD, new GeneratorForexSwap("JPYUSD", Currency.USD, Currency.JPY, baseCalendar, 2, modFol, true));
	}
	
	public GeneratorForexSwap getGenerator(final Generators generatorName, final Calendar jointCalendar) {
	    final GeneratorForexSwap generatorNoCalendar = _generatorSwap.get(generatorName);
	    if (generatorNoCalendar == null) {
	    	throw new OpenGammaRuntimeException("Could not get fx swap generator for " + generatorName.toString());
	    }
	    return new GeneratorForexSwap(generatorNoCalendar.getName(), generatorNoCalendar.getCurrency1(), generatorNoCalendar.getCurrency2(),
	        jointCalendar, generatorNoCalendar.getSpotLag(), generatorNoCalendar.getBusinessDayConvention(), true);
	}
}
