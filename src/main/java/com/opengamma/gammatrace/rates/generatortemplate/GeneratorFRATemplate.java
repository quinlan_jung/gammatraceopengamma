package com.opengamma.gammatrace.rates.generatortemplate;

import java.util.HashMap;
import java.util.Map;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.instrument.index.GeneratorFRA;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.CalendarNoHoliday;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;

public class GeneratorFRATemplate {
	/**
	 * The method unique instance.
	 */
	private static final GeneratorFRATemplate INSTANCE = new GeneratorFRATemplate();
	
	/**
	 * Return the unique instance of the class.
	 * @return The instance.
	 */
	public static GeneratorFRATemplate getInstance() {
		return INSTANCE;
	}
	
	/**
	 * The map with the list of names and the swap generators.
	 */
	private final Map<Generators, GeneratorFRA> _generatorFRA;
	
	/**
	 * The list of Ibor indexes for test purposes.
	 */
	 private final IborTemplateByName _iborIndexMaster;
	
	/**
	 * Private constructor.
	 */
	private GeneratorFRATemplate() {
	_iborIndexMaster = IborTemplateByName.getInstance();
	final Calendar baseCalendar = new CalendarNoHoliday("No Holidays");
	_generatorFRA = new HashMap<>();
	_generatorFRA.put(Generators.USDLIBOR3MFRA, new GeneratorFRA("USDLIBOR3MFRA", _iborIndexMaster.getIndex(Indicies.USDLIBOR3M), baseCalendar));
	
	_generatorFRA.put(Generators.EURIBOR3MFRA, new GeneratorFRA("EURIBOR3MFRA", _iborIndexMaster.getIndex(Indicies.EURIBOR3M), baseCalendar));
	_generatorFRA.put(Generators.EURIBOR6MFRA, new GeneratorFRA("EURIBOR6MFRA", _iborIndexMaster.getIndex(Indicies.EURIBOR6M), baseCalendar));
	
	_generatorFRA.put(Generators.GBPLIBOR3MFRA, new GeneratorFRA("GBPLIBOR3MFRA", _iborIndexMaster.getIndex(Indicies.GBPLIBOR3M), baseCalendar));
	_generatorFRA.put(Generators.GBPLIBOR6MFRA, new GeneratorFRA("GBPLIBOR6MFRA", _iborIndexMaster.getIndex(Indicies.GBPLIBOR6M), baseCalendar));
	 }
	
	public GeneratorFRA getGenerator(final Generators generatorName, final Calendar cal) {
	    final GeneratorFRA generatorNoCalendar = _generatorFRA.get(generatorName);
	    if (generatorNoCalendar == null) {
	      throw new OpenGammaRuntimeException("Could not get FRA Generator for " + generatorName);
	    }
	    return new GeneratorFRA(generatorNoCalendar.getName(), generatorNoCalendar.getIborIndex(), cal);
	}
}
