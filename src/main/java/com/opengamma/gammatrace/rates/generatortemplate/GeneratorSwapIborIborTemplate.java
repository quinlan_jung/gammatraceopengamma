package com.opengamma.gammatrace.rates.generatortemplate;

import java.util.HashMap;
import java.util.Map;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborIbor;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.CalendarNoHoliday;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;

/**
 * A list of Ibor vs Ibor basis swap generators to be used to create the instruments required to describe the market inputs 
 * to a curve.
 */
public final class GeneratorSwapIborIborTemplate {

  /**
   * The method unique instance.
   */
  private static final GeneratorSwapIborIborTemplate INSTANCE = new GeneratorSwapIborIborTemplate();

  /**
   * Return the unique instance of the class.
   * @return The instance.
   */
  public static GeneratorSwapIborIborTemplate getInstance() {
    return INSTANCE;
  }

  /**
   * The map with the list of names and the swap generators.
   */
  private final Map<Generators, GeneratorSwapIborIbor> _generatorSwap;

  /**
   * The list of Ibor indices.
   */
  private final IborTemplateByName _iborIndexMaster;

  /**
   * Private constructor.
   */
  private GeneratorSwapIborIborTemplate() {
    _iborIndexMaster = IborTemplateByName.getInstance();
    final Calendar baseCalendar = new CalendarNoHoliday("No Holidays");
    _generatorSwap = new HashMap<>();
    // when constructing the definition the spread is applied on the first leg.
    _generatorSwap.put(Generators.USD1M3M, new GeneratorSwapIborIbor("USD1M3M", _iborIndexMaster.getIndex(Indicies.USDLIBOR1M), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M),
            baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.USD3M1M, new GeneratorSwapIborIbor("USD3M1M", _iborIndexMaster.getIndex(Indicies.USDLIBOR3M), _iborIndexMaster.getIndex(Indicies.USDLIBOR1M),
            baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.USD3M6M, new GeneratorSwapIborIbor("USD3M6M", _iborIndexMaster.getIndex(Indicies.USDLIBOR3M), _iborIndexMaster.getIndex(Indicies.USDLIBOR6M),
        baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.USD3M12M, new GeneratorSwapIborIbor("USD3M12M", _iborIndexMaster.getIndex(Indicies.USDLIBOR3M), _iborIndexMaster.getIndex(Indicies.USDLIBOR12M),
            baseCalendar, baseCalendar));
    
    _generatorSwap.put(Generators.JPY1M3M, new GeneratorSwapIborIbor("JPY1M3M", _iborIndexMaster.getIndex(Indicies.JPYLIBOR1M), _iborIndexMaster.getIndex(Indicies.JPYLIBOR3M),
            baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.JPY3M1M, new GeneratorSwapIborIbor("JPY3M1M", _iborIndexMaster.getIndex(Indicies.JPYLIBOR3M), _iborIndexMaster.getIndex(Indicies.JPYLIBOR1M),
            baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.JPY3M6M, new GeneratorSwapIborIbor("JPY3M6M", _iborIndexMaster.getIndex(Indicies.JPYLIBOR3M), _iborIndexMaster.getIndex(Indicies.JPYLIBOR6M),
        baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.JPY3M12M, new GeneratorSwapIborIbor("JPY3M6M", _iborIndexMaster.getIndex(Indicies.JPYLIBOR3M), _iborIndexMaster.getIndex(Indicies.JPYLIBOR12M),
            baseCalendar, baseCalendar));
    
    _generatorSwap.put(Generators.EUR1M3M, new GeneratorSwapIborIbor("EUR1M3M", _iborIndexMaster.getIndex(Indicies.EURIBOR1M), _iborIndexMaster.getIndex(Indicies.EURIBOR3M),
            baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.EUR3M1M, new GeneratorSwapIborIbor("EUR3M1M", _iborIndexMaster.getIndex(Indicies.EURIBOR3M), _iborIndexMaster.getIndex(Indicies.EURIBOR1M),
            baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.EUR3M6M, new GeneratorSwapIborIbor("EUR3M6M", _iborIndexMaster.getIndex(Indicies.EURIBOR3M), _iborIndexMaster.getIndex(Indicies.EURIBOR6M),
            baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.EUR3M12M, new GeneratorSwapIborIbor("EUR3M12M", _iborIndexMaster.getIndex(Indicies.EURIBOR3M), _iborIndexMaster.getIndex(Indicies.EURIBOR12M),
                baseCalendar, baseCalendar));
    
    _generatorSwap.put(Generators.AUD3M6M, new GeneratorSwapIborIbor("AUD3M6M", _iborIndexMaster.getIndex(Indicies.AUDBB3M), _iborIndexMaster.getIndex(Indicies.AUDBB6M),
            baseCalendar, baseCalendar));
    
    _generatorSwap.put(Generators.GBP1M3M, new GeneratorSwapIborIbor("GBP1M3M", _iborIndexMaster.getIndex(Indicies.GBPLIBOR1M), _iborIndexMaster.getIndex(Indicies.GBPLIBOR3M),
            baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.GBP1M6M, new GeneratorSwapIborIbor("GBP1M6M", _iborIndexMaster.getIndex(Indicies.GBPLIBOR1M), _iborIndexMaster.getIndex(Indicies.GBPLIBOR6M),
            baseCalendar, baseCalendar));
    _generatorSwap.put(Generators.GBP3M6M, new GeneratorSwapIborIbor("GBP3M6M", _iborIndexMaster.getIndex(Indicies.GBPLIBOR3M), _iborIndexMaster.getIndex(Indicies.GBPLIBOR6M),
            baseCalendar, baseCalendar));
  }

  public GeneratorSwapIborIbor getGenerator(final Generators generatorName, final Calendar cal) {
    final GeneratorSwapIborIbor generatorNoCalendar = _generatorSwap.get(generatorName);
    if (generatorNoCalendar == null) {
      throw new OpenGammaRuntimeException("Could not get Ibor index for " + generatorName);
    }
    return new GeneratorSwapIborIbor(generatorNoCalendar.getName(), generatorNoCalendar.getIborIndex1(), generatorNoCalendar.getIborIndex2(), cal, cal);
  }

}
