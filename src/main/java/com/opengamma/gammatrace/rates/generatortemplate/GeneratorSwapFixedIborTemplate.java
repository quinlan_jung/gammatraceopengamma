package com.opengamma.gammatrace.rates.generatortemplate;

import java.util.HashMap;
import java.util.Map;

import org.threeten.bp.Period;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.CalendarNoHoliday;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;

/**
 * A list of swap generators that can be used in the tests.
 */
public class GeneratorSwapFixedIborTemplate {
	/**
	 * The method unique instance.
	 */
	private static final GeneratorSwapFixedIborTemplate INSTANCE = new GeneratorSwapFixedIborTemplate();
	
	/**
	 * Return the unique instance of the class.
	 * @return The instance.
	 */
	public static GeneratorSwapFixedIborTemplate getInstance() {
		return INSTANCE;
	}
	
	/**
	 * The map with the list of names and the swap generators.
	 */
	private final Map<Generators, GeneratorSwapFixedIbor> _generatorSwap;
	
	/**
	 * The list of Ibor indexes for test purposes.
	 */
	 private final IborTemplateByName _iborIndexMaster;
	
	/**
	 * Private constructor.
	 */
	private GeneratorSwapFixedIborTemplate() {
	  _iborIndexMaster = IborTemplateByName.getInstance();
	  final Calendar baseCalendar = new CalendarNoHoliday("No Holidays");
	_generatorSwap = new HashMap<>();
	_generatorSwap.put(Generators.USD6MLIBOR1M,
			new GeneratorSwapFixedIbor("USD6MLIBOR1M", Period.ofMonths(6), DayCountFactory.INSTANCE.getDayCount("30/360"), _iborIndexMaster.getIndex(Indicies.USDLIBOR1M), baseCalendar));
	_generatorSwap.put(Generators.USD6MLIBOR3M,
		new GeneratorSwapFixedIbor("USD6MLIBOR3M", Period.ofMonths(6), DayCountFactory.INSTANCE.getDayCount("30/360"), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M), baseCalendar));
	_generatorSwap.put(Generators.USD1YLIBOR3M,
	    new GeneratorSwapFixedIbor("USD1YLIBOR3M", Period.ofMonths(12), DayCountFactory.INSTANCE.getDayCount("30/360"),_iborIndexMaster.getIndex(Indicies.USDLIBOR3M), baseCalendar));
	_generatorSwap.put(Generators.USD6MLIBOR6M,
	    new GeneratorSwapFixedIbor("USD6MLIBOR6M", Period.ofMonths(6), DayCountFactory.INSTANCE.getDayCount("30/360"), _iborIndexMaster.getIndex(Indicies.USDLIBOR6M), baseCalendar));
	// EUR
	_generatorSwap.put(Generators.EUR1YEURIBOR1M,
		    new GeneratorSwapFixedIbor("EUR1YEURIBOR1M", Period.ofMonths(12), DayCountFactory.INSTANCE.getDayCount("30/360"), _iborIndexMaster.getIndex(Indicies.EURIBOR1M), baseCalendar));
	_generatorSwap.put(Generators.EUR1YEURIBOR3M,
	    new GeneratorSwapFixedIbor("EUR1YEURIBOR3M", Period.ofMonths(12), DayCountFactory.INSTANCE.getDayCount("30/360"), _iborIndexMaster.getIndex(Indicies.EURIBOR3M), baseCalendar));
	_generatorSwap.put(Generators.EUR1YEURIBOR6M,
	    new GeneratorSwapFixedIbor("EUR1YEURIBOR6M", Period.ofMonths(12), DayCountFactory.INSTANCE.getDayCount("30/360"), _iborIndexMaster.getIndex(Indicies.EURIBOR6M), baseCalendar));
	//GBP
	_generatorSwap.put(Generators.GBP1YLIBOR3M,
	    new GeneratorSwapFixedIbor("GBP1YLIBOR3M", Period.ofMonths(12), DayCountFactory.INSTANCE.getDayCount("ACT/365"), _iborIndexMaster.getIndex(Indicies.GBPLIBOR3M), baseCalendar));
	_generatorSwap.put(Generators.GBP6MLIBOR6M,
	    new GeneratorSwapFixedIbor("GBP6MLIBOR6M", Period.ofMonths(6), DayCountFactory.INSTANCE.getDayCount("ACT/365"), _iborIndexMaster.getIndex(Indicies.GBPLIBOR6M), baseCalendar));
	//JPY
	_generatorSwap.put(Generators.JPY6MLIBOR3M,
	    new GeneratorSwapFixedIbor("JPY6MLIBOR3M", Period.ofMonths(6), DayCountFactory.INSTANCE.getDayCount("ACT/365"), _iborIndexMaster.getIndex(Indicies.JPYLIBOR3M), baseCalendar));
	_generatorSwap.put(Generators.JPY6MLIBOR6M,
	    new GeneratorSwapFixedIbor("JPY6MLIBOR6M", Period.ofMonths(6), DayCountFactory.INSTANCE.getDayCount("ACT/365"), _iborIndexMaster.getIndex(Indicies.JPYLIBOR6M), baseCalendar));
	//DKK
	_generatorSwap.put(Generators.DKK1YCIBOR6M,
	    new GeneratorSwapFixedIbor("DKK1YCIBOR6M", Period.ofMonths(12), DayCountFactory.INSTANCE.getDayCount("30/360"), _iborIndexMaster.getIndex(Indicies.DKKCIBOR6M), baseCalendar));
	//AUD
	_generatorSwap.put(Generators.AUD3MBBSW3M,
	    new GeneratorSwapFixedIbor("AUD3MBBSW3M", Period.ofMonths(3), DayCountFactory.INSTANCE.getDayCount("ACT/365"), _iborIndexMaster.getIndex(Indicies.AUDBB3M), baseCalendar));
	_generatorSwap.put(Generators.AUD6MBBSW6M,
	    new GeneratorSwapFixedIbor("AUD6MBBSW6M", Period.ofMonths(6), DayCountFactory.INSTANCE.getDayCount("ACT/365"), _iborIndexMaster.getIndex(Indicies.AUDBB6M), baseCalendar));
	  }
	
	  public GeneratorSwapFixedIbor getGenerator(final Generators generatorName, final Calendar cal) {
	    final GeneratorSwapFixedIbor generatorNoCalendar = _generatorSwap.get(generatorName);
	    if (generatorNoCalendar == null) {
	      throw new OpenGammaRuntimeException("Could not get generator for " + generatorName.toString());
	    }
	    return new GeneratorSwapFixedIbor(generatorNoCalendar.getName(), generatorNoCalendar.getFixedLegPeriod(), generatorNoCalendar.getFixedLegDayCount(),
	        generatorNoCalendar.getIborIndex(), cal);
	  }
}


