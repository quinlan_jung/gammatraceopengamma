package com.opengamma.gammatrace.rates.generatortemplate;

import java.util.HashMap;
import java.util.Map;

import org.threeten.bp.Period;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.CalendarNoHoliday;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;
import com.opengamma.gammatrace.rates.indextemplate.ONTemplateByName;

/**
 * A list of generators for swaps Fixed/ON available for tests.
 */
public final class GeneratorSwapFixedONTemplate {

  /**
   * The method unique instance.
   */
  private static final GeneratorSwapFixedONTemplate INSTANCE = new GeneratorSwapFixedONTemplate();

  /**
   * Return the unique instance of the class.
   * @return The instance.
   */
  public static GeneratorSwapFixedONTemplate getInstance() {
    return INSTANCE;
  }

  /**
   * The map with the list of names and the swap generators.
   */
  private final Map<Generators, GeneratorSwapFixedON> _generatorSwap;

  /**
   * Private constructor.
   */
  private GeneratorSwapFixedONTemplate() {
    final ONTemplateByName indexONMaster = ONTemplateByName.getInstance();
    final Calendar baseCalendar = new CalendarNoHoliday("No Holidays");
    final DayCount act360 = DayCountFactory.INSTANCE.getDayCount("Actual/360");
    final DayCount act365 = DayCountFactory.INSTANCE.getDayCount("Actual/365");
    final BusinessDayConvention modFol = BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following");
    _generatorSwap = new HashMap<>();
    
    _generatorSwap.put(Generators.USD1YFEDFUND, new GeneratorSwapFixedON("USD1YFEDFUND", indexONMaster.getIndex(Indicies.FEDFUNDS), Period.ofMonths(12), act360, modFol, true, 2, 2, baseCalendar));
    _generatorSwap.put(Generators.EUR1YEONIA, new GeneratorSwapFixedON("EUR1YEONIA", indexONMaster.getIndex(Indicies.EONIA), Period.ofMonths(12), act360, modFol, true, 2, 2, baseCalendar));
    _generatorSwap.put(Generators.GBP1YSONIA, new GeneratorSwapFixedON("GBP1YSONIA",  indexONMaster.getIndex(Indicies.SONIA), Period.ofMonths(12), act365, modFol, true, 0, 1, baseCalendar));
    _generatorSwap.put(Generators.AUD1YRBAON, new GeneratorSwapFixedON("AUD1YRBAON", indexONMaster.getIndex(Indicies.RBA_ON), Period.ofMonths(12), act365, modFol, true, 2, 1, baseCalendar));
    _generatorSwap.put(Generators.JPY1YTONAR, new GeneratorSwapFixedON("JPY1YTONAR", indexONMaster.getIndex(Indicies.TONAR), Period.ofMonths(12), act365, modFol, true, 2, 1, baseCalendar));
  }

  public GeneratorSwapFixedON getGenerator(final Generators generatorName, final Calendar cal) {
    final GeneratorSwapFixedON generatorNoCalendar = _generatorSwap.get(generatorName);
    if (generatorNoCalendar == null) {
      throw new OpenGammaRuntimeException("Could not get Swap Fixed/ON generator for " + generatorName);
    }    
    return new GeneratorSwapFixedON(generatorNoCalendar.getName(), generatorNoCalendar.getIndex(), generatorNoCalendar.getLegsPeriod(), generatorNoCalendar.getFixedLegDayCount(),
        generatorNoCalendar.getBusinessDayConvention(), generatorNoCalendar.isEndOfMonth(), generatorNoCalendar.getSpotLag(), generatorNoCalendar.getPaymentLag(), cal);
  }

}
