package com.opengamma.gammatrace.rates.indextemplate;

import java.util.HashMap;
import java.util.Map;

import org.threeten.bp.Period;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;
//TODO: should we change the map entries here to call IborTemplateByName? Then only need to maintain definitions in one place.
/**
 * Description of Ibor indexes available.
 */
public final class IborTemplate {

  /**
   * The method unique instance.
   */
  private static final IborTemplate INSTANCE = new IborTemplate();

  /**
   * Return the unique instance of the class.
   * @return The instance.
   */
  public static IborTemplate getInstance() {
    return INSTANCE;
  }

  /**
   * The map with the list of Ibor Indexes and their conventions.
   */
  private final Map<Pair<Currency, Period>, IborIndex> _ibor;

  /**
   * The list of Ibor indexes for test purposes.
   */
  private final IborTemplateByName _iborIndexMaster;
  
  /**
   * Private constructor.
   */
  private IborTemplate() {
    _ibor = new HashMap<Pair<Currency, Period>, IborIndex>();
    _iborIndexMaster = IborTemplateByName.getInstance();
    _ibor.put(Pair.of(Currency.AUD, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.AUDBB3M));
    _ibor.put(Pair.of(Currency.AUD, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.AUDBB6M));
    _ibor.put(Pair.of(Currency.CAD, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.CADCDOR3M));
    _ibor.put(Pair.of(Currency.CAD, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.CADCDOR6M));
    _ibor.put(Pair.of(Currency.EUR, Period.ofMonths(1)), _iborIndexMaster.getIndex(Indicies.EURIBOR1M));
    _ibor.put(Pair.of(Currency.EUR, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.EURIBOR3M));
    _ibor.put(Pair.of(Currency.EUR, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.EURIBOR6M));
    _ibor.put(Pair.of(Currency.EUR, Period.ofMonths(12)), _iborIndexMaster.getIndex(Indicies.EURIBOR12M));
    _ibor.put(Pair.of(Currency.USD, Period.ofMonths(1)), _iborIndexMaster.getIndex(Indicies.USDLIBOR1M));
    _ibor.put(Pair.of(Currency.USD, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.USDLIBOR3M));
    _ibor.put(Pair.of(Currency.USD, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.USDLIBOR6M));
    _ibor.put(Pair.of(Currency.USD, Period.ofMonths(12)), _iborIndexMaster.getIndex(Indicies.USDLIBOR12M));
    _ibor.put(Pair.of(Currency.GBP, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.GBPLIBOR3M));
    _ibor.put(Pair.of(Currency.GBP, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.GBPLIBOR6M));
    _ibor.put(Pair.of(Currency.DKK, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.DKKCIBOR3M));
    _ibor.put(Pair.of(Currency.DKK, Period.ofMonths(6)),_iborIndexMaster.getIndex(Indicies.DKKCIBOR6M));
    _ibor.put(Pair.of(Currency.JPY, Period.ofMonths(1)), _iborIndexMaster.getIndex(Indicies.JPYLIBOR1M));
    _ibor.put(Pair.of(Currency.JPY, Period.ofMonths(3)), _iborIndexMaster.getIndex(Indicies.JPYLIBOR3M));
    _ibor.put(Pair.of(Currency.JPY, Period.ofMonths(6)), _iborIndexMaster.getIndex(Indicies.JPYLIBOR6M));
    _ibor.put(Pair.of(Currency.JPY, Period.ofMonths(12)), _iborIndexMaster.getIndex(Indicies.JPYLIBOR12M));
  }

  public IborIndex getIndex(final Currency ccy, Period period) {
    final IborIndex indexNoCalendar = _ibor.get(Pair.of(ccy, period));
    if (indexNoCalendar == null) {
      throw new OpenGammaRuntimeException("Could not get Ibor index for " + ccy + period);
    }
    return new IborIndex(indexNoCalendar.getCurrency(), indexNoCalendar.getTenor(), indexNoCalendar.getSpotLag(), indexNoCalendar.getDayCount(), indexNoCalendar.getBusinessDayConvention(),
        indexNoCalendar.isEndOfMonth(), indexNoCalendar.getName());
  }

}


