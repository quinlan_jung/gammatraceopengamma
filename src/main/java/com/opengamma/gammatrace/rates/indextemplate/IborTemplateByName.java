package com.opengamma.gammatrace.rates.indextemplate;

import java.util.HashMap;
import java.util.Map;

import org.threeten.bp.Period;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.util.money.Currency;


/**
 * Description of Ibor indexes available.
 */
public final class IborTemplateByName {

  /**
   * The method unique instance.
   */
  private static final IborTemplateByName INSTANCE = new IborTemplateByName();

  /**
   * Return the unique instance of the class.
   * @return The instance.
   */
  public static IborTemplateByName getInstance() {
    return INSTANCE;
  }

  /**
   * The map with the list of Ibor Indexes and their conventions.
   */
  private final Map<Indicies, IborIndex> _ibor;

  /**
   * Private constructor.
   */
  private IborTemplateByName() {
    _ibor = new HashMap<Indicies, IborIndex>();
    _ibor.put(
    		Indicies.AUDBB3M,
        new IborIndex(Currency.AUD, Period.ofMonths(3), 1, DayCountFactory.INSTANCE.getDayCount("Actual/365"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "AUDBB3M"));
    _ibor.put(
    		Indicies.AUDBB6M,
        new IborIndex(Currency.AUD, Period.ofMonths(6), 1, DayCountFactory.INSTANCE.getDayCount("Actual/365"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "AUDBB6M"));
    _ibor.put(
    		Indicies.CADCDOR3M,
        new IborIndex(Currency.CAD, Period.ofMonths(3), 0, DayCountFactory.INSTANCE.getDayCount("Actual/365"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "CADCDOR3M"));
    _ibor.put(
    		Indicies.CADCDOR6M,
            new IborIndex(Currency.CAD, Period.ofMonths(6), 0, DayCountFactory.INSTANCE.getDayCount("Actual/365"), BusinessDayConventionFactory.INSTANCE
                .getBusinessDayConvention("Modified Following"), true, "CADCDOR6M"));
    _ibor.put(
    		Indicies.EURIBOR1M,
        new IborIndex(Currency.EUR, Period.ofMonths(1), 2, DayCountFactory.INSTANCE.getDayCount("Actual/360"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "EURIBOR1M"));
    _ibor.put(
    		Indicies.EURIBOR3M,
        new IborIndex(Currency.EUR, Period.ofMonths(3), 2, DayCountFactory.INSTANCE.getDayCount("Actual/360"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "EURIBOR3M"));
    _ibor.put(
    		Indicies.EURIBOR6M,
        new IborIndex(Currency.EUR, Period.ofMonths(6), 2, DayCountFactory.INSTANCE.getDayCount("Actual/360"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "EURIBOR6M"));
    _ibor.put(
    		Indicies.EURIBOR12M,
        new IborIndex(Currency.EUR, Period.ofMonths(12), 2, DayCountFactory.INSTANCE.getDayCount("Actual/360"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "EURIBOR12M"));
    _ibor.put(
    		Indicies.USDLIBOR1M,
        new IborIndex(Currency.USD, Period.ofMonths(1), 2, DayCountFactory.INSTANCE.getDayCount("Actual/360"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "USDLIBOR1M"));
    _ibor.put(
    		Indicies.USDLIBOR3M,
        new IborIndex(Currency.USD, Period.ofMonths(3), 2, DayCountFactory.INSTANCE.getDayCount("Actual/360"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "USDLIBOR3M"));
    _ibor.put(
    		Indicies.USDLIBOR6M,
        new IborIndex(Currency.USD, Period.ofMonths(6), 2, DayCountFactory.INSTANCE.getDayCount("Actual/360"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "USDLIBOR6M"));
    _ibor.put(
    		Indicies.USDLIBOR12M,
        new IborIndex(Currency.USD, Period.ofMonths(12), 2, DayCountFactory.INSTANCE.getDayCount("Actual/360"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "USDLIBOR12M"));
    _ibor.put(
    		Indicies.GBPLIBOR1M,
        new IborIndex(Currency.GBP, Period.ofMonths(1), 0, DayCountFactory.INSTANCE.getDayCount("Actual/365"), BusinessDayConventionFactory.INSTANCE
                .getBusinessDayConvention("Modified Following"), true, "GBPLIBOR1M"));
    _ibor.put(
    		Indicies.GBPLIBOR3M,
        new IborIndex(Currency.GBP, Period.ofMonths(3), 0, DayCountFactory.INSTANCE.getDayCount("Actual/365"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "GBPLIBOR3M"));
    _ibor.put(
    		Indicies.GBPLIBOR6M,
        new IborIndex(Currency.GBP, Period.ofMonths(6), 0, DayCountFactory.INSTANCE.getDayCount("Actual/365"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "GBPLIBOR6M"));
    _ibor.put(
    		Indicies.DKKCIBOR3M,
        new IborIndex(Currency.DKK, Period.ofMonths(3), 2, DayCountFactory.INSTANCE.getDayCount("Actual/360"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "DKKCIBOR3M"));
    _ibor.put(
    		Indicies.DKKCIBOR6M,
        new IborIndex(Currency.DKK, Period.ofMonths(6), 2, DayCountFactory.INSTANCE.getDayCount("Actual/360"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "DKKCIBOR6M"));
    _ibor.put(
    		Indicies.JPYLIBOR1M,
        new IborIndex(Currency.JPY, Period.ofMonths(1), 2, DayCountFactory.INSTANCE.getDayCount("Actual/365"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "JPYLIBOR1M"));
    _ibor.put(
    		Indicies.JPYLIBOR3M,
        new IborIndex(Currency.JPY, Period.ofMonths(3), 2, DayCountFactory.INSTANCE.getDayCount("Actual/365"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "JPYLIBOR3M"));
    _ibor.put(
    		Indicies.JPYLIBOR6M,
        new IborIndex(Currency.JPY, Period.ofMonths(6), 2, DayCountFactory.INSTANCE.getDayCount("Actual/365"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "JPYLIBOR6M"));
    _ibor.put(
    		Indicies.JPYLIBOR12M,
        new IborIndex(Currency.JPY, Period.ofMonths(12), 2, DayCountFactory.INSTANCE.getDayCount("Actual/365"), BusinessDayConventionFactory.INSTANCE
            .getBusinessDayConvention("Modified Following"), true, "JPYLIBOR12M"));
  
  }

  public IborIndex getIndex(final Indicies name) {
    final IborIndex indexNoCalendar = _ibor.get(name);
    if (indexNoCalendar == null) {
      throw new OpenGammaRuntimeException("Could not get Ibor index for " + name.toString());
    }
    return new IborIndex(indexNoCalendar.getCurrency(), indexNoCalendar.getTenor(), indexNoCalendar.getSpotLag(), indexNoCalendar.getDayCount(), indexNoCalendar.getBusinessDayConvention(),
        indexNoCalendar.isEndOfMonth(), indexNoCalendar.getName());
  }

}

