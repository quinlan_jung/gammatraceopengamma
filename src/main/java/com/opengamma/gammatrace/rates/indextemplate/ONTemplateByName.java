package com.opengamma.gammatrace.rates.indextemplate;

import java.util.HashMap;
import java.util.Map;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.util.money.Currency;

/**
 * Description of ON indexes.
 */
public final class ONTemplateByName {

  /**
   * The method unique instance.
   */
  private static final ONTemplateByName INSTANCE = new ONTemplateByName();

  /**
   * Return the unique instance of the class.
   * @return The instance.
   */
  public static ONTemplateByName getInstance() {
    return INSTANCE;
  }

  /**
   * The map with the list of Ibor Indexes and their conventions.
   */
  private final Map<Indicies, IndexON> _on;

  /**
   * Private constructor.
   */
  private ONTemplateByName() {
    final DayCount act360 = DayCountFactory.INSTANCE.getDayCount("Actual/360");
    final DayCount act365 = DayCountFactory.INSTANCE.getDayCount("Actual/365");
    final DayCount Bus252 = DayCountFactory.INSTANCE.getDayCount("Business/252");
    _on = new HashMap<>();
    _on.put(Indicies.EONIA, new IndexON("EONIA", Currency.EUR, act360, 0));
    _on.put(Indicies.FEDFUNDS, new IndexON("FED FUND", Currency.USD, act360, 1));
    _on.put(Indicies.SONIA, new IndexON("SONIA", Currency.GBP, act365, 0));
    _on.put(Indicies.RBA_ON, new IndexON("RBA ON", Currency.AUD, act365, 0));
    _on.put(Indicies.DKK_TN, new IndexON("DKK TN", Currency.DKK, act360, 1));
    _on.put(Indicies.TONAR, new IndexON("TONAR", Currency.JPY, act365, 0));
    _on.put(Indicies.CDI, new IndexON("CDI", Currency.BRL, Bus252, 0));
  }

  public IndexON getIndex(final Indicies name) {
    final IndexON indexNoCalendar = _on.get(name);
    if (indexNoCalendar == null) {
      throw new OpenGammaRuntimeException("Could not get ON index for " + name.toString());
    }
    return new IndexON(indexNoCalendar.getName(), indexNoCalendar.getCurrency(), indexNoCalendar.getDayCount(), indexNoCalendar.getPublicationLag());
  }

}
