package com.opengamma.gammatrace.forex.instrument;

import org.threeten.bp.Instant;
import org.threeten.bp.Period;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.datamodel.Trade;
import com.opengamma.analytics.financial.forex.definition.ForexDefinition;
import com.opengamma.analytics.financial.forex.definition.ForexOptionVanillaDefinition;
import com.opengamma.analytics.financial.forex.derivative.ForexOptionVanilla;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.instrument.ForexInstrument;
import com.opengamma.util.money.CurrencyAmount;

public class ForexOptionVanillaBuilder {

	private ForexInstrument instrument;
	
	/* it looks like option_strike_price is a derived quantity because
	 * 1) there is some rounding error vs notional1/notional2 or notional2/notional
	 * 2) lacking consistency in ccy order. For example a USDJPY put @ 116.5 = JPYUSD call @ 1/116.5, but even when the currency order
	 * is reversed, the option_strike_price is still 116.5 (see 22984862 and 22984863)
	 * Therefore ignore option_strike_price and use the notionals and notional_currency_1 = foreign ccy and notional_currency_2 = domestic
	 * ccy, and C- and P- are calls and puts on this pair.
	 */
	
	private ZonedDateTime effectiveDate;
	private ZonedDateTime optionExpiry;
	private ZonedDateTime executionTimestamp;
	private ZonedDateTime tradeDate;
	private CurrencyAmount foreignNotional;
	private CurrencyAmount domesticNotional;
	private boolean isCall;
	private CurrencyAmount optionPremium;
	
	public ForexOptionVanillaBuilder(Trade trade) {
		effectiveDate = epochToDateMidnight(trade.getEffective_date());
		optionExpiry = epochToDateMidnight(trade.getOption_expiration_date());
		executionTimestamp = epochToDateTime(trade.getExecution_timestamp());
		tradeDate = epochToDateMidnight(trade.getExecution_timestamp());
		foreignNotional = CurrencyAmount.of(trade.getNotional_currency_1(), trade.getRounded_notional_amount_1());
		domesticNotional = CurrencyAmount.of(trade.getNotional_currency_2(), trade.getRounded_notional_amount_2());
		isCall = (trade.getOption_type().equalsIgnoreCase("C-"))? true : false;
		optionPremium = CurrencyAmount.of(trade.getOption_currency(), trade.getOption_premium());
	}
	
	private ForexInstrument makeInstrument() {
		ForexOptionVanilla derivative = makeDerivative();
		return new ForexInstrument(derivative, executionTimestamp, optionPremium);
	}
	
	private ForexOptionVanilla makeDerivative() {
		Calendar UTC = new MondayToFridayCalendar("UTC");
		ZonedDateTime optionPayDate = ScheduleCalculator.getAdjustedDate(optionExpiry, 2, UTC);
		final double strike = domesticNotional.getAmount() / foreignNotional.getAmount(); 
		ForexDefinition forexDefinition = new ForexDefinition(foreignNotional.getCurrency(), domesticNotional.getCurrency(), optionPayDate, foreignNotional.getAmount(), strike);
		ForexOptionVanillaDefinition forexOptionCallDefinition = new ForexOptionVanillaDefinition(forexDefinition, optionExpiry, isCall, true);
		return forexOptionCallDefinition.toDerivative(tradeDate);
	}
	
	public ForexInstrument getInstrument() {
		return instrument;
	}
	
	//TODO: move these to some common place, this is also used in rates SwapBuilder.
	/**
	 * Convert epoch to a ZonedDateTime with time set to midnight.
	 * @param epoch
	 * @return The zoned date-time
	 */
	private ZonedDateTime epochToDateMidnight (long epoch) {
		ZonedDateTime dateMidnight = ZonedDateTime.ofInstant(Instant.ofEpochSecond(epoch), ZoneId.of("UTC")).toLocalDate().atStartOfDay(ZoneId.of("UTC"));
		return dateMidnight;	
	}
	private ZonedDateTime epochToDateTime (long epoch) {
		ZonedDateTime dateTime = ZonedDateTime.ofInstant(Instant.ofEpochSecond(epoch), ZoneId.of("UTC"));
		return dateTime;
	}
	
}
