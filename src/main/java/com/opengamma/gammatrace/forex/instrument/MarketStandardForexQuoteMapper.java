package com.opengamma.gammatrace.forex.instrument;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.threeten.bp.Period;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexDeposit;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.ONTemplateByName;
import com.opengamma.gammatrace.rates.instrument.AssetMapper;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;
import com.opengamma.util.tuple.Triple;

/**
 * Mapping of data asset names to Ibor/ ON indices from templates.
 */
public class MarketStandardForexQuoteMapper {
	/**
	 * The method unique instance.
	 */
	private static final MarketStandardForexQuoteMapper INSTANCE = new MarketStandardForexQuoteMapper();

	/**
	 * Return the unique instance of the class.
	 * @return The instance.
	 */
	public static MarketStandardForexQuoteMapper getInstance() {
	  return INSTANCE;
	}
	
	/**
	 * The map with the list of asset names and their indices.
	 */
	private final ArrayList<ForexPair> _standardForexPairs;
	
	/**
	 * Private constructor.
	 */
	// REVIEW: alternatively could define a priority of individual currencies
	
	private MarketStandardForexQuoteMapper() {	
		_standardForexPairs = new ArrayList<ForexPair>();

		_standardForexPairs.add(new ForexPair(Currency.EUR, Currency.USD));
		_standardForexPairs.add(new ForexPair(Currency.GBP, Currency.USD));
		_standardForexPairs.add(new ForexPair(Currency.AUD, Currency.USD));
		_standardForexPairs.add(new ForexPair(Currency.NZD, Currency.USD));
		
		_standardForexPairs.add(new ForexPair(Currency.USD, Currency.CAD));
		_standardForexPairs.add(new ForexPair(Currency.USD, Currency.CHF));
		_standardForexPairs.add(new ForexPair(Currency.USD, Currency.JPY));
		
	}
	/**
	 * 
	 * @param input the ForexPair representing the quote convention of the original trade
	 * @return the ForexPair in the same convention as market standard e.g. USDEUR will be switched to EURUSD
	 */
	public ForexPair getMarketStandardForexQuote(ForexPair input) {
		if (_standardForexPairs.contains(input)) {
			return input;
		}
		ForexPair inverted = invertForexPair(input);
		if (_standardForexPairs.contains(inverted)) {
			return inverted;
		}
		return null;
	}
	
	private ForexPair invertForexPair(ForexPair input) {
		return new ForexPair(input.getDomesticCurrency(), input.getForeignCurrency());
	}
}


