package com.opengamma.gammatrace.forex.instrument;

import com.opengamma.util.ArgumentChecker;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class ForexPair {

	private Currency domesticCurrency;
	private Currency foreignCurrency;
	
	public ForexPair (Currency firstCcy, Currency secondCcy) {
		ArgumentChecker.notNull(firstCcy, "firstCcy");
		ArgumentChecker.notNull(secondCcy, "secondCcy");
		domesticCurrency = secondCcy;
		foreignCurrency = firstCcy;
	}
	
	public ForexPair (Pair<Currency, Currency> currencyPair) {
		domesticCurrency = currencyPair.getSecond();
		foreignCurrency = currencyPair.getFirst();
	}
	
	public Currency getDomesticCurrency() {
		return domesticCurrency;
	}
	
	public Currency getForeignCurrency() {
		return foreignCurrency;
	}

	@Override
	public String toString() {
		return domesticCurrency+"/"+foreignCurrency;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((domesticCurrency == null) ? 0 : domesticCurrency.hashCode());
		result = prime * result + ((foreignCurrency == null) ? 0 : foreignCurrency.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ForexPair other = (ForexPair) obj;
		if (domesticCurrency == null) {
			if (other.domesticCurrency != null)
				return false;
		} else if (!domesticCurrency.equals(other.domesticCurrency))
			return false;
		if (foreignCurrency == null) {
			if (other.foreignCurrency != null)
				return false;
		} else if (!foreignCurrency.equals(other.foreignCurrency))
			return false;
		return true;
	}
}
