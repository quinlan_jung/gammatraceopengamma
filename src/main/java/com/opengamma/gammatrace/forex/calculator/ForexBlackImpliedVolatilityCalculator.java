package com.opengamma.gammatrace.forex.calculator;

import com.opengamma.analytics.financial.forex.derivative.ForexOptionVanilla;
import com.opengamma.analytics.financial.model.volatility.BlackFormulaRepository;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.util.money.Currency;

public class ForexBlackImpliedVolatilityCalculator {

	public static double getImpliedVolatility (ForexOptionVanilla option, double optionPremium, MulticurveProviderDiscount curve) {
		
		Currency forCcy = option.getCurrency1();
		Currency domCcy = option.getCurrency2();
		
		double discountFactor = curve.getDiscountFactor(domCcy, option.getTimeToExpiry());
		boolean isCall = option.isCall();
		
		
		double spot = curve.getFxRate(forCcy, domCcy);
		System.out.println(forCcy.toString()+domCcy.toString()+" "+spot);
		double f = spot * curve.getDiscountFactor(forCcy, option.getTimeToExpiry()) / curve.getDiscountFactor(domCcy, option.getTimeToExpiry());
		
		double k = option.getStrike();
		double t = option.getTimeToExpiry();
		
		double fwdPrice = optionPremium / discountFactor / option.getUnderlyingForex().getPaymentCurrency1().getAmount();
		
		System.out.println("optionPremium = "+optionPremium+"  discountFactor = "+discountFactor+"  fwdprice = "+fwdPrice+"  fwd = "+f+"  Strike = "+k+"  isCall? = "+isCall);
		
		return BlackFormulaRepository.impliedVolatility(fwdPrice, f, k, t, isCall);
	}
}
