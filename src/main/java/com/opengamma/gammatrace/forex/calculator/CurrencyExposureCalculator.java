package com.opengamma.gammatrace.forex.calculator;

import com.opengamma.analytics.financial.forex.derivative.ForexOptionVanilla;
import com.opengamma.analytics.financial.provider.calculator.blackforex.CurrencyExposureForexBlackSmileCalculator;
import com.opengamma.analytics.financial.provider.description.forex.BlackForexSmileProviderDiscount;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;

public class CurrencyExposureCalculator {
	private static final CurrencyExposureForexBlackSmileCalculator CEFBC = CurrencyExposureForexBlackSmileCalculator.getInstance();
	
	private Currency foreignCurrency;
	private Currency domesticCurrency;
	private MultipleCurrencyAmount currencyExposure;
	
	public CurrencyExposureCalculator(ForexOptionVanilla option, BlackForexSmileProviderDiscount smileMulticurve) {
		foreignCurrency = option.getCurrency1();
		domesticCurrency = option.getCurrency2();
		currencyExposure = option.accept(CEFBC, smileMulticurve);
	}
	
	public CurrencyAmount getForeignDelta() {
		return currencyExposure.getCurrencyAmount(foreignCurrency);
	}
	
}
