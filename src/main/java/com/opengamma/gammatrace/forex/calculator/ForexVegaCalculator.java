package com.opengamma.gammatrace.forex.calculator;

import java.text.DecimalFormat;

import com.opengamma.analytics.financial.forex.derivative.ForexOptionVanilla;
import com.opengamma.analytics.financial.forex.method.PresentValueForexBlackVolatilityQuoteSensitivityDataBundle;
import com.opengamma.analytics.financial.provider.calculator.blackforex.QuoteBucketedVegaForexBlackSmileCalculator;
import com.opengamma.analytics.financial.provider.description.forex.BlackForexSmileProviderDiscount;
// TODO: make a data model for vega that stores the PresentValueForex.... and the String[] containing the labels (or maybe just the double[][] of vega, the labels and Period[] for the expiries)
public class ForexVegaCalculator {
	private static final QuoteBucketedVegaForexBlackSmileCalculator QBVFBSC = QuoteBucketedVegaForexBlackSmileCalculator.getInstance();
	
	private PresentValueForexBlackVolatilityQuoteSensitivityDataBundle vega;
	
	public ForexVegaCalculator(ForexOptionVanilla option, BlackForexSmileProviderDiscount smileMulticurve) {
		vega = option.accept(QBVFBSC, smileMulticurve);
	}

	public PresentValueForexBlackVolatilityQuoteSensitivityDataBundle getVega() {
		return vega;
	}
	
	public String[] getVegaLabels() {
		double[] delta = vega.getDelta();
		String[] labels = new String[delta.length];
		labels[0] = "DN";
		int numStrikes = 0;
		while (delta[numStrikes] < 0.5) {
			numStrikes++;
		}
		DecimalFormat wholeNumber = new DecimalFormat("0");
		
		for (int i = 0; i < numStrikes; i++) {
			StringBuilder risky = new StringBuilder();
			risky.append(wholeNumber.format(delta[i]*100));
			risky.append("R");
			labels[i+1] = risky.toString();
			StringBuilder fly = new StringBuilder();
			fly.append(wholeNumber.format(delta[i]*100));
			fly.append("F");
			labels[i+1+numStrikes] = fly.toString();	
		}
		return labels;
	}
}
