package com.opengamma.gammatrace.exceptions;

public class UnsupportedOperation extends Exception{

	public UnsupportedOperation(String msg){
		super(msg);
		System.out.println("This operation is not supported:" + msg);
	}
}
