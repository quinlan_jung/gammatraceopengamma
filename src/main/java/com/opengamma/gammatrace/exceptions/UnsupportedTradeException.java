package com.opengamma.gammatrace.exceptions;

public class UnsupportedTradeException extends Exception {
	public UnsupportedTradeException(String msg){
		super(msg);
		System.out.println("This trade is not supported:" + msg);
	}
}
