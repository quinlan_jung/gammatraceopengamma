package com.opengamma.gammatrace.marketconstruction.curveconfiguration;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.threeten.bp.Period;

import com.opengamma.analytics.financial.instrument.index.GeneratorAttribute;
import com.opengamma.analytics.financial.instrument.index.GeneratorForexSwap;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.gammatrace.marketconstruction.quote.SwapQuote;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorForexSwapTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapXCcyIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class GBPMultiCcyConfiguration extends GBPConfiguration {
	
	protected static final Calendar NYC = new MondayToFridayCalendar("NYC");
	
	private static final GeneratorForexSwapTemplate GENERATOR_FXSWAP_MASTER = GeneratorForexSwapTemplate.getInstance();
    private static final GeneratorSwapXCcyIborIborTemplate GENERATOR_XCCY_MASTER = GeneratorSwapXCcyIborIborTemplate.getInstance();
 
	private static final GeneratorSwapXCcyIborIbor GBP3MUSD3M = GENERATOR_XCCY_MASTER.getGenerator(Generators.GBP3MUSD3M, LOCAL_CALENDAR, NYC);
	private static final GeneratorForexSwap GBPUSD = GENERATOR_FXSWAP_MASTER.getGenerator(Generators.GBPUSD, LOCAL_CALENDAR);    
	
	public GBPMultiCcyConfiguration() {
		super();
		
		// Configuration of Xccy discounting
		SWAP_QUOTES[0] = new SwapQuote[] {	new SwapQuote(Period.ofYears(1), GBP3MUSD3M, "GBPUSL1Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(2), GBP3MUSD3M, "GBPUSL2Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(3), GBP3MUSD3M, "GBPUSL3Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(4), GBP3MUSD3M, "GBPUSL4Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(5), GBP3MUSD3M, "GBPUSL5Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(7), GBP3MUSD3M, "GBPUSL7Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(10), GBP3MUSD3M, "GBPUSL10Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(15), GBP3MUSD3M, "GBPUSL15Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(20), GBP3MUSD3M, "GBPUSL20Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(30), GBP3MUSD3M, "GBPUSL30Y=ICIR", QuoteInstrumentType.XCCYSWAP)};
	}
	
    // no entry into ON map because we are now building the discounting curve from xccy swaps, not from OIS
    //TODO: review: will we ever need xccy overnight index trades? If so need to include OIS swaps as well
    @Override
    public LinkedHashMap<String, IndexON[]> getIndexONMap(){
    	LinkedHashMap<String, IndexON[]> FWD_ON_MAP = new LinkedHashMap<>();	
		return FWD_ON_MAP;	
	}
    @Override
    public HashMap<String, Pair<Currency, String>> getCurveRelations() {
 		HashMap<String, Pair<Currency, String>> curveRelations = super.getCurveRelations();
 		curveRelations.put(CURVE_NAMES[0], Pair.of(Currency.GBP, "XCcy"));
 		return curveRelations;
 	}
}
