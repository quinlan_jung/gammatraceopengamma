package com.opengamma.gammatrace.marketconstruction.curveconfiguration;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.threeten.bp.Period;

import com.opengamma.analytics.financial.instrument.index.GeneratorAttribute;
import com.opengamma.analytics.financial.instrument.index.GeneratorDepositIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorDepositON;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborON;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.gammatrace.marketconstruction.quote.SwapQuote;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;
import com.opengamma.gammatrace.rates.indextemplate.ONTemplateByName;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class GBPConfiguration extends CurveConfiguration {
	//TODO: this calendar needs to be sorted out, and calendar should only be generated here and this passed around 
	protected static final Calendar LOCAL_CALENDAR = new MondayToFridayCalendar("TARGET");
    private static final Currency CURRENCY = Currency.GBP;
    
    //Generator Template instances
    private static final GeneratorSwapFixedIborTemplate GENERATOR_SWAP_MASTER = GeneratorSwapFixedIborTemplate.getInstance();
    private static final GeneratorSwapIborIborTemplate GENERATOR_BASIS_MASTER = GeneratorSwapIborIborTemplate.getInstance();
    private static final GeneratorSwapFixedONTemplate GENERATOR_OIS_MASTER = GeneratorSwapFixedONTemplate.getInstance();
    private static final GeneratorSwapIborONTemplate GENERATOR_OISBASIS_MASTER = GeneratorSwapIborONTemplate.getInstance();
    private static final IborTemplateByName IBOR_MASTER = IborTemplateByName.getInstance();
    private static final ONTemplateByName ON_MASTER = ONTemplateByName.getInstance();
    
    // Generators for Libors / OIS
    private static final IndexON ON_INDEX = ON_MASTER.getIndex(Indicies.SONIA);
    private static final IborIndex IBOR_1M = IBOR_MASTER.getIndex(Indicies.GBPLIBOR1M);
    private static final IborIndex IBOR_3M = IBOR_MASTER.getIndex(Indicies.GBPLIBOR3M);
    private static final IborIndex IBOR_6M = IBOR_MASTER.getIndex(Indicies.GBPLIBOR6M);

    //Generators for deposits 
    //TODO: is it worth factoring this out into a deposit generator class?
    private static final GeneratorDepositON DEPO_ON_INDEX = new GeneratorDepositON("DEPO_ON_INDEX", CURRENCY, LOCAL_CALENDAR, ON_INDEX.getDayCount());
    private static final GeneratorDepositIbor DEPO_IBOR_3M = new GeneratorDepositIbor("GBPLIBOR3M", IBOR_3M, LOCAL_CALENDAR);
    private static final GeneratorDepositIbor DEPO_IBOR_6M = new GeneratorDepositIbor("GBPLIBOR6M", IBOR_6M, LOCAL_CALENDAR);
    
 	// Generators for Swaps  	
    private static final GeneratorSwapFixedIbor IBOR3M_FIXED = GENERATOR_SWAP_MASTER.getGenerator(Generators.GBP1YLIBOR3M, LOCAL_CALENDAR);
    private static final GeneratorSwapFixedIbor IBOR6M_FIXED = GENERATOR_SWAP_MASTER.getGenerator(Generators.GBP6MLIBOR6M, LOCAL_CALENDAR);
    private static final GeneratorSwapFixedON GBP1YSONIA = GENERATOR_OIS_MASTER.getGenerator(Generators.GBP1YSONIA, LOCAL_CALENDAR);
    private static final GeneratorSwapIborON IBOR3M_ON = GENERATOR_OISBASIS_MASTER.getGenerator(Generators.GBP3MON, LOCAL_CALENDAR);
    private static final GeneratorSwapIborIbor IBOR3M_IBOR6M = GENERATOR_BASIS_MASTER.getGenerator(Generators.GBP3M6M, LOCAL_CALENDAR);
    private static final GeneratorSwapIborIbor IBOR1M_IBOR6M = GENERATOR_BASIS_MASTER.getGenerator(Generators.GBP1M6M, LOCAL_CALENDAR);
    
    public GBPConfiguration() {
    	super(new String[] {"GBP DSC", "GBP fwd 1m", "GBP fwd 3m", "GBP fwd 6m"});
    	// Configuration of discounting and overnight index projection
    	
    	SWAP_QUOTES[0] = new SwapQuote[] {	new SwapQuote(Period.ofMonths(1), GBP1YSONIA,"GBP1MOI=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofMonths(2), GBP1YSONIA,"GBP2MOI=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofMonths(3), GBP1YSONIA,"GBP3MOI=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofMonths(6), GBP1YSONIA,"GBP6MOI=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofMonths(12), GBP1YSONIA,"GBP12MOI=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(2), GBP1YSONIA,"GBP2YOI=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(3), GBP1YSONIA,"GBP3YOI=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(4), GBP1YSONIA,"GBP4YOI=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(5), GBP1YSONIA,"GBP5YOI=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(7), GBP1YSONIA,"GBP7YOI=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(10), GBP1YSONIA,"GBP10YOI=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(15), GBP1YSONIA,"GBP15YOI=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(20), GBP1YSONIA,"GBP20YOI=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(30), GBP1YSONIA,"GBP30YOI=ICIR",QuoteInstrumentType.SWAP)};
    	
    	SWAP_QUOTES[1] = new SwapQuote[] {	new SwapQuote(Period.ofYears(1), IBOR1M_IBOR6M, "GBP6USL11Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(2), IBOR1M_IBOR6M, "GBP6USL12Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(3), IBOR1M_IBOR6M, "GBP6USL13Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(4), IBOR1M_IBOR6M, "GBP6USL14Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(5), IBOR1M_IBOR6M, "GBP6USL15Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(7), IBOR1M_IBOR6M, "GBP6USL17Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(10), IBOR1M_IBOR6M, "GBP6USL110Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(15), IBOR1M_IBOR6M, "GBP6USL115Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(20), IBOR1M_IBOR6M, "GBP6USL120Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(30), IBOR1M_IBOR6M, "GBP6USL130Y=ICIR", QuoteInstrumentType.BASISSWAP)};
				
		SWAP_QUOTES[2] = new SwapQuote[] { 	new SwapQuote(Period.ofYears(1), IBOR3M_FIXED, "GBP1Y3W=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(2), IBOR3M_FIXED, "GBP2Y3W=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(3), IBOR3M_FIXED, "GBP3Y3W=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(4), IBOR3M_FIXED, "GBP4Y3W=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(5), IBOR3M_FIXED, "GBP5Y3W=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(7), IBOR3M_FIXED, "GBP7Y3W=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(10), IBOR3M_FIXED, "GBP10Y3W=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(15), IBOR3M_FIXED, "GBP15Y3W=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(20), IBOR3M_FIXED, "GBP20Y3W=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(30), IBOR3M_FIXED, "GBP30Y3W=ICIR", QuoteInstrumentType.SWAP)};
		
		SWAP_QUOTES[3] = new SwapQuote[] {	new SwapQuote(Period.ofYears(2), IBOR6M_FIXED, "GBP2YW=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(3), IBOR6M_FIXED, "GBP3YW=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(4), IBOR6M_FIXED, "GBP4YW=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(5), IBOR6M_FIXED, "GBP5YW=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(7), IBOR6M_FIXED, "GBP7YW=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(10), IBOR6M_FIXED, "GBP10YW=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(15), IBOR6M_FIXED, "GBP15YW=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(20), IBOR6M_FIXED, "GBP20YW=ICIR", QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(30), IBOR6M_FIXED, "GBP30YW=ICIR", QuoteInstrumentType.SWAP)};
		
        FX_ORDER = Pair.of(Currency.GBP, Currency.USD);	//1 ccy1 = fxRate * ccy2. MUST be against USD because of the way MultiCurrencyCurveMarketDataProvider is implemented.
       	FX_TICKER = "GBP=ICGC";
    }
    
    public LinkedHashMap<String, Currency> getDiscountMap(){
    	LinkedHashMap<String, Currency> DSC_MAP = new LinkedHashMap<>();
		DSC_MAP.put(CURVE_NAMES[0], CURRENCY);
		return DSC_MAP;
	}
    public LinkedHashMap<String, IndexON[]> getIndexONMap(){
    	LinkedHashMap<String, IndexON[]> FWD_ON_MAP = new LinkedHashMap<>();
    	FWD_ON_MAP.put(CURVE_NAMES[0], new IndexON[] {ON_INDEX });	
		return FWD_ON_MAP;
	}
	public LinkedHashMap<String, IborIndex[]> getIborIndexMap(){
		LinkedHashMap<String, IborIndex[]> FWD_IBOR_MAP = new LinkedHashMap<>();
		FWD_IBOR_MAP.put(CURVE_NAMES[1], new IborIndex[] {IBOR_1M });
		FWD_IBOR_MAP.put(CURVE_NAMES[2], new IborIndex[] {IBOR_3M });
		FWD_IBOR_MAP.put(CURVE_NAMES[3], new IborIndex[] {IBOR_6M });
		return FWD_IBOR_MAP;
	}

 	public HashMap<String, Pair<Currency, String>> getCurveRelations() {
 		HashMap<String, Pair<Currency, String>> curveRelations = new HashMap<>();
 		curveRelations.put(CURVE_NAMES[0], Pair.of(Currency.GBP, "Fixed"));
 		curveRelations.put(CURVE_NAMES[1], Pair.of(Currency.GBP, CURVE_NAMES[3]));
 		curveRelations.put(CURVE_NAMES[2], Pair.of(Currency.GBP, "Fixed"));
 		curveRelations.put(CURVE_NAMES[3], Pair.of(Currency.GBP, "Fixed"));
 		return curveRelations;
 	}
}
