package com.opengamma.gammatrace.marketconstruction.curveconfiguration;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.threeten.bp.Period;

import com.opengamma.analytics.financial.instrument.index.GeneratorAttribute;
import com.opengamma.analytics.financial.instrument.index.GeneratorDepositIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorDepositON;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborON;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;
import com.opengamma.gammatrace.rates.indextemplate.ONTemplateByName;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class AUDConfiguration extends CurveConfiguration {
	//TODO: this calendar needs to be sorted out, and calendar should only be generated here and this passed around 
    private static final Calendar CALENDAR = new MondayToFridayCalendar("NYC");
    private static final Currency CURRENCY = Currency.AUD;
    
    //Generator Template instances
    private static final GeneratorSwapFixedIborTemplate GENERATOR_SWAP_MASTER = GeneratorSwapFixedIborTemplate.getInstance();
    private static final GeneratorSwapIborIborTemplate GENERATOR_BASIS_MASTER = GeneratorSwapIborIborTemplate.getInstance();
    private static final GeneratorSwapIborONTemplate GENERATOR_OISBASIS_MASTER = GeneratorSwapIborONTemplate.getInstance();
    private static final IborTemplateByName IBOR_MASTER = IborTemplateByName.getInstance();
    private static final ONTemplateByName ON_MASTER = ONTemplateByName.getInstance();
    
    // Generators for Libors / OIS
    private static final IndexON ON_INDEX = ON_MASTER.getIndex(Indicies.RBA_ON);
    private static final IborIndex IBOR_3M = IBOR_MASTER.getIndex(Indicies.AUDBB3M);
    private static final IborIndex IBOR_6M = IBOR_MASTER.getIndex(Indicies.AUDBB6M);

    //Generators for deposits 
    //TODO: is it worth factoring this out into a deposit generator class?
    private static final GeneratorDepositON DEPO_ON_INDEX = new GeneratorDepositON("DEPO_ON_INDEX", CURRENCY, CALENDAR, ON_INDEX.getDayCount());
    private static final GeneratorDepositIbor DEPO_IBOR_3M = new GeneratorDepositIbor("AUDBB3M", IBOR_3M, CALENDAR);
    private static final GeneratorDepositIbor DEPO_IBOR_6M = new GeneratorDepositIbor("AUDBB6M", IBOR_6M, CALENDAR);
    
 	// Generators for Swaps  	
    private static final GeneratorSwapFixedIbor IBOR3M_FIXED = GENERATOR_SWAP_MASTER.getGenerator(Generators.AUD3MBBSW3M, CALENDAR);
    private static final GeneratorSwapIborON IBOR3M_ON = GENERATOR_OISBASIS_MASTER.getGenerator(Generators.AUD3MON, CALENDAR);
    private static final GeneratorSwapIborIbor IBOR3M_IBOR6M = GENERATOR_BASIS_MASTER.getGenerator(Generators.AUD3M6M, CALENDAR);
    
    public AUDConfiguration() {
    	super(new String[] {"AUD DSC", "AUD fwd 3m", "AUD fwd 6m"});
    }
    
    public LinkedHashMap<String, Currency> getDiscountMap(){	//unchanged
    	LinkedHashMap<String, Currency> DSC_MAP = new LinkedHashMap<>();
		DSC_MAP.put(CURVE_NAMES[0], CURRENCY);
		return DSC_MAP;
	}
    
    public LinkedHashMap<String, IndexON[]> getIndexONMap(){	//TO CHANGE
    	LinkedHashMap<String, IndexON[]> FWD_ON_MAP = new LinkedHashMap<>();
    	FWD_ON_MAP.put(CURVE_NAMES[0], new IndexON[] {ON_INDEX });	
		return FWD_ON_MAP;
	}
	
	public LinkedHashMap<String, IborIndex[]> getIborIndexMap(){	//unchanged
		LinkedHashMap<String, IborIndex[]> FWD_IBOR_MAP = new LinkedHashMap<>();
		FWD_IBOR_MAP.put(CURVE_NAMES[1], new IborIndex[] {IBOR_3M });
	    FWD_IBOR_MAP.put(CURVE_NAMES[2], new IborIndex[] {IBOR_6M });
		return FWD_IBOR_MAP;
	}
	 	public HashMap<String, Pair<Currency, String>> getCurveRelations() {
 		HashMap<String, Pair<Currency,String>> curveRelations = new HashMap<>();
 		curveRelations.put(CURVE_NAMES[0], Pair.of(Currency.AUD, "Fixed"));
 		curveRelations.put(CURVE_NAMES[1], Pair.of(Currency.AUD, CURVE_NAMES[2]));
 		curveRelations.put(CURVE_NAMES[2], Pair.of(Currency.AUD, "Fixed"));
 		return curveRelations;
 	}
}
