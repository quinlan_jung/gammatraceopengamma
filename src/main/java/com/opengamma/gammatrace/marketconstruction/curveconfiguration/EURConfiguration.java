package com.opengamma.gammatrace.marketconstruction.curveconfiguration;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.threeten.bp.Period;

import com.opengamma.analytics.financial.instrument.index.GeneratorDepositIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorDepositON;
import com.opengamma.analytics.financial.instrument.index.GeneratorFRA;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborON;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.gammatrace.marketconstruction.quote.SwapQuote;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorFRATemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;
import com.opengamma.gammatrace.rates.indextemplate.ONTemplateByName;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class EURConfiguration extends CurveConfiguration {
	//TODO: this calendar needs to be sorted out, and calendar should only be generated here and this passed around 
    protected static final Calendar TARGET = new MondayToFridayCalendar("TARGET");
    protected static final Calendar NYC = new MondayToFridayCalendar("NYC");
    private static final Currency EUR = Currency.EUR;
    
    //Generator Template instances
    private static final GeneratorSwapFixedIborTemplate GENERATOR_SWAP_MASTER = GeneratorSwapFixedIborTemplate.getInstance();
    private static final GeneratorSwapFixedONTemplate GENERATOR_OIS_MASTER = GeneratorSwapFixedONTemplate.getInstance();
    private static final GeneratorFRATemplate GENERATOR_FRA_MASTER = GeneratorFRATemplate.getInstance();
    private static final GeneratorSwapIborIborTemplate GENERATOR_BASIS_MASTER = GeneratorSwapIborIborTemplate.getInstance();
    private static final GeneratorSwapIborONTemplate GENERATOR_OISBASIS_MASTER = GeneratorSwapIborONTemplate.getInstance();
    
    private static final IborTemplateByName IBOR_MASTER = IborTemplateByName.getInstance();
    private static final ONTemplateByName ON_MASTER = ONTemplateByName.getInstance();
    
    // Generators for Libors / OIS
    private static final IndexON EONIA = ON_MASTER.getIndex(Indicies.EONIA);
    private static final IborIndex EURIBOR1M = IBOR_MASTER.getIndex(Indicies.EURIBOR1M);
    private static final IborIndex EURIBOR3M = IBOR_MASTER.getIndex(Indicies.EURIBOR3M);
    private static final IborIndex EURIBOR6M = IBOR_MASTER.getIndex(Indicies.EURIBOR6M);

    //Generators for deposits 
    private static final GeneratorDepositON DEPO_EONIA = new GeneratorDepositON("DEPO_EONIA", EUR, TARGET, EONIA.getDayCount());
    private static final GeneratorDepositIbor DEPO_EURIBOR1M = new GeneratorDepositIbor("EURIBOR1M", EURIBOR1M, TARGET);
    private static final GeneratorDepositIbor DEPO_EURIBOR3M = new GeneratorDepositIbor("EURIBOR3M", EURIBOR3M, TARGET);
    private static final GeneratorDepositIbor DEPO_EURIBOR6M = new GeneratorDepositIbor("EURIBOR6M", EURIBOR6M, TARGET);
    
    // Generators FRAs
    private static final GeneratorFRA EURIBOR6MFRA = GENERATOR_FRA_MASTER.getGenerator(Generators.EURIBOR6MFRA, TARGET);
    
 	// Generators for Swaps  	
    private static final GeneratorSwapFixedIbor EUR1YEURIBOR6M = GENERATOR_SWAP_MASTER.getGenerator(Generators.EUR1YEURIBOR6M, TARGET);
    private static final GeneratorSwapFixedON EUR1YEONIA = GENERATOR_OIS_MASTER.getGenerator(Generators.EUR1YEONIA, TARGET);
    private static final GeneratorSwapIborON EUR3MON = GENERATOR_OISBASIS_MASTER.getGenerator(Generators.EUR3MON, TARGET);
    private static final GeneratorSwapIborIbor EUR3M1M = GENERATOR_BASIS_MASTER.getGenerator(Generators.EUR3M1M, TARGET);
    private static final GeneratorSwapIborIbor EUR3M6M = GENERATOR_BASIS_MASTER.getGenerator(Generators.EUR3M6M, TARGET);
  
    public EURConfiguration() {
    	super(new String[] {"EUR DSC", "EUR fwd 3m", "EUR fwd 1m", "EUR fwd 6m"});
    
    	SWAP_QUOTES[0] = new SwapQuote[] {	new SwapQuote(Period.ofMonths(1), EUR1YEONIA,"EUR1MOI=ICIR",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofMonths(2), EUR1YEONIA,"EUR2MOI=ICIR",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofMonths(3), EUR1YEONIA,"EUR3MOI=ICIR",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofMonths(6), EUR1YEONIA,"EUR6MOI=ICIR",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(1), EUR1YEONIA,"EONIA-1Y=ICIR",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(2), EUR1YEONIA,"EONIA-2Y=ICIR",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(3), EUR1YEONIA,"EONIA-3Y=ICIR",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(4), EUR1YEONIA,"EONIA-4Y=ICIR",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(5), EUR1YEONIA,"EONIA-5Y=ICIR",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(7), EUR1YEONIA,"EONIA-7Y=ICIR",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(10), EUR1YEONIA,"EONIA-10Y=ICIR",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(15), EUR1YEONIA,"EONIA-15Y=ICIR",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(20), EUR1YEONIA,"EONIA-20Y=ICIR",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(30), EUR1YEONIA,"EONIA-30Y=ICIR",QuoteInstrumentType.SWAP)};
    	
    	SWAP_QUOTES[1] = new SwapQuote[] {	new SwapQuote(Period.ofYears(1), EUR3M6M, "EUR1EUL61SW1Y=ICIR", QuoteInstrumentType.BASISSWAP),		//TODO: replace with 2 swap quotes?
    										new SwapQuote(Period.ofYears(2), EUR3M6M, "EUR1EUL61SW2Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(3), EUR3M6M, "EUR1EUL61SW3Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(4), EUR3M6M, "EUR1EUL61SW4Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(5), EUR3M6M, "EUR1EUL61SW5Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(7), EUR3M6M, "EUR1EUL61SW7Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(10), EUR3M6M, "EUR1EUL61SW10Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(15), EUR3M6M, "EUR1EUL61SW15Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(20), EUR3M6M, "EUR1EUL61SW20Y=ICIR", QuoteInstrumentType.BASISSWAP),
    										new SwapQuote(Period.ofYears(30), EUR3M6M, "EUR1EUL61SW30Y=ICIR", QuoteInstrumentType.BASISSWAP)};
    	
    	SWAP_QUOTES[2] = new SwapQuote[] {	new SwapQuote(Period.ofYears(1), EUR3M1M, "EUR1EUL31SW1Y=ICIR", QuoteInstrumentType.BASISSWAP),		//TODO: replace with 2 swap quotes?
											new SwapQuote(Period.ofYears(2), EUR3M1M, "EUR1EUL31SW2Y=ICIR", QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(3), EUR3M1M, "EUR1EUL31SW3Y=ICIR", QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(4), EUR3M1M, "EUR1EUL31SW4Y=ICIR", QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(5), EUR3M1M, "EUR1EUL31SW5Y=ICIR", QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(7), EUR3M1M, "EUR1EUL31SW7Y=ICIR", QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(10), EUR3M1M, "EUR1EUL31SW10Y=ICIR", QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(15), EUR3M1M, "EUR1EUL31SW15Y=ICIR", QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(20), EUR3M1M, "EUR1EUL31SW20Y=ICIR", QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(30), EUR3M1M, "EUR1EUL31SW30Y=ICIR", QuoteInstrumentType.BASISSWAP)};
    	
    	SWAP_QUOTES[3] = new SwapQuote[] {	new SwapQuote(Period.ofMonths(9), EURIBOR6MFRA, "EUR3X9FRA=ICIR", QuoteInstrumentType.FRA),
    										new SwapQuote(Period.ofYears(1), EUR1YEURIBOR6M, "EUR1YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(2), EUR1YEURIBOR6M, "EUR2YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(3), EUR1YEURIBOR6M, "EUR3YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(4), EUR1YEURIBOR6M, "EUR4YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(5), EUR1YEURIBOR6M, "EUR5YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(7), EUR1YEURIBOR6M, "EUR7YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(10), EUR1YEURIBOR6M, "EUR10YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(15), EUR1YEURIBOR6M, "EUR15YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(20), EUR1YEURIBOR6M, "EUR20YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(30), EUR1YEURIBOR6M, "EUR30YW=ICIR", QuoteInstrumentType.SWAP)};
    	
    	FX_ORDER = Pair.of(Currency.EUR, Currency.USD);	//1 ccy1 = fxRate * ccy2. MUST be against USD because of the way MultiCurrencyCurveMarketDataProvider is implemented.
       	FX_TICKER = "EUR=ICGC";
    }
    
    public LinkedHashMap<String, Currency> getDiscountMap(){	//unchanged
    	LinkedHashMap<String, Currency> DSC_MAP = new LinkedHashMap<>();
		DSC_MAP.put(CURVE_NAMES[0], EUR);
		return DSC_MAP;
	}
    
    public LinkedHashMap<String, IndexON[]> getIndexONMap(){	//TO CHANGE
    	LinkedHashMap<String, IndexON[]> FWD_ON_MAP = new LinkedHashMap<>();
    	FWD_ON_MAP.put(CURVE_NAMES[0], new IndexON[] {EONIA });	
		return FWD_ON_MAP;
	}
	
	public LinkedHashMap<String, IborIndex[]> getIborIndexMap(){	//unchanged
		LinkedHashMap<String, IborIndex[]> FWD_IBOR_MAP = new LinkedHashMap<>();
		FWD_IBOR_MAP.put(CURVE_NAMES[1], new IborIndex[] {EURIBOR3M });
	    FWD_IBOR_MAP.put(CURVE_NAMES[2], new IborIndex[] {EURIBOR1M });
	    FWD_IBOR_MAP.put(CURVE_NAMES[3], new IborIndex[] {EURIBOR6M });
		return FWD_IBOR_MAP;
	}
	 	public HashMap<String, Pair<Currency, String>> getCurveRelations() {
 		HashMap<String, Pair<Currency,String>> curveRelations = new HashMap<>();
 		curveRelations.put(CURVE_NAMES[0], Pair.of(Currency.EUR, "Fixed"));
 		curveRelations.put(CURVE_NAMES[1], Pair.of(Currency.EUR, CURVE_NAMES[3]));
 		curveRelations.put(CURVE_NAMES[2], Pair.of(Currency.EUR, CURVE_NAMES[3]));
 		curveRelations.put(CURVE_NAMES[3], Pair.of(Currency.EUR, "Fixed"));
 		return curveRelations;
 	}
}
