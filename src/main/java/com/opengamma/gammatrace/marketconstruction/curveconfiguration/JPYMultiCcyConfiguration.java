package com.opengamma.gammatrace.marketconstruction.curveconfiguration;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.threeten.bp.Period;

import com.opengamma.analytics.financial.instrument.index.GeneratorAttribute;
import com.opengamma.analytics.financial.instrument.index.GeneratorForexSwap;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.gammatrace.marketconstruction.quote.SwapQuote;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorForexSwapTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapXCcyIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class JPYMultiCcyConfiguration extends JPYConfiguration {

	protected static final Calendar NYC = new MondayToFridayCalendar("NYC");
	
	private static final GeneratorForexSwapTemplate GENERATOR_FXSWAP_MASTER = GeneratorForexSwapTemplate.getInstance();
    private static final GeneratorSwapXCcyIborIborTemplate GENERATOR_XCCY_MASTER = GeneratorSwapXCcyIborIborTemplate.getInstance();
    
	private static final GeneratorSwapXCcyIborIbor JPY3MUSD3M = GENERATOR_XCCY_MASTER.getGenerator(Generators.JPY3MUSD3M, LOCAL_CALENDAR, NYC);
	private static final GeneratorForexSwap JPYUSD = GENERATOR_FXSWAP_MASTER.getGenerator(Generators.JPYUSD, LOCAL_CALENDAR);    
	
	public JPYMultiCcyConfiguration() {
		super();
		
		// Configuration of Xccy discounting
		SWAP_QUOTES[0] = new SwapQuote[] {	new SwapQuote(Period.ofYears(1), JPY3MUSD3M, "JPYUSL1Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(2), JPY3MUSD3M, "JPYUSL2Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(3), JPY3MUSD3M, "JPYUSL3Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(4), JPY3MUSD3M, "JPYUSL4Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(5), JPY3MUSD3M, "JPYUSL5Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(7), JPY3MUSD3M, "JPYUSL7Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(10), JPY3MUSD3M, "JPYUSL10Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(15), JPY3MUSD3M, "JPYUSL15Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(20), JPY3MUSD3M, "JPYUSL20Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(30), JPY3MUSD3M, "JPYUSL30Y=ICIR", QuoteInstrumentType.XCCYSWAP)};
	}
	
    // no entry into ON map because we are now building the discounting curve from xccy swaps, not from OIS
    //TODO: review: will we ever need xccy overnight index trades? If so need to include OIS swaps as well
    @Override
    public LinkedHashMap<String, IndexON[]> getIndexONMap(){
    	LinkedHashMap<String, IndexON[]> FWD_ON_MAP = new LinkedHashMap<>();	
		return FWD_ON_MAP;	
	}    
    
    @Override
    public HashMap<String, Pair<Currency, String>> getCurveRelations() {
 		HashMap<String, Pair<Currency, String>> curveRelations = new HashMap<>();
 		curveRelations.put(CURVE_NAMES[0], Pair.of(Currency.JPY, "XCcy"));
 		curveRelations.put(CURVE_NAMES[1], Pair.of(Currency.JPY, "Fixed"));
 		return curveRelations;
 	}
}
