package com.opengamma.gammatrace.marketconstruction.curveconfiguration;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.threeten.bp.Period;

import com.opengamma.analytics.financial.instrument.index.GeneratorAttribute;
import com.opengamma.analytics.financial.instrument.index.GeneratorDepositIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorDepositON;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborON;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.gammatrace.marketconstruction.quote.SwapQuote;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;
import com.opengamma.gammatrace.rates.indextemplate.ONTemplateByName;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class JPYConfiguration extends CurveConfiguration {
	//TODO: this calendar needs to be sorted out, and calendar should only be generated here and this passed around 
    protected static final Calendar LOCAL_CALENDAR = new MondayToFridayCalendar("TOK");
    protected static final Currency JPY = Currency.JPY;
    
  //Generator Template instances
    private static final GeneratorSwapFixedIborTemplate GENERATOR_SWAP_MASTER = GeneratorSwapFixedIborTemplate.getInstance();
    private static final GeneratorSwapIborIborTemplate GENERATOR_BASIS_MASTER = GeneratorSwapIborIborTemplate.getInstance();
    private static final GeneratorSwapFixedONTemplate GENERATOR_OIS_MASTER = GeneratorSwapFixedONTemplate.getInstance();
    private static final GeneratorSwapIborONTemplate GENERATOR_OISBASIS_MASTER = GeneratorSwapIborONTemplate.getInstance();
    private static final IborTemplateByName IBOR_MASTER = IborTemplateByName.getInstance();
    private static final ONTemplateByName ON_MASTER = ONTemplateByName.getInstance();
    
    // Generators for Libors / OIS
    private static final IndexON TONAR = ON_MASTER.getIndex(Indicies.TONAR);
    private static final IborIndex JPYLIBOR1M = IBOR_MASTER.getIndex(Indicies.JPYLIBOR1M);
    protected static final IborIndex JPYLIBOR3M = IBOR_MASTER.getIndex(Indicies.JPYLIBOR3M);
    protected static final IborIndex JPYLIBOR6M = IBOR_MASTER.getIndex(Indicies.JPYLIBOR6M);

    //Generators for deposits 
    //TODO: is it worth factoring this out into a deposit generator class?
    private static final GeneratorDepositON DEPO_TONAR = new GeneratorDepositON("DEPO_TONAR", JPY, LOCAL_CALENDAR, TONAR.getDayCount());
    private static final GeneratorDepositIbor DEPO_JPYLIBOR1M = new GeneratorDepositIbor("JPYLIBOR1M", JPYLIBOR1M, LOCAL_CALENDAR);
    private static final GeneratorDepositIbor DEPO_JPYLIBOR3M = new GeneratorDepositIbor("JPYLIBOR3M", JPYLIBOR3M, LOCAL_CALENDAR);
    private static final GeneratorDepositIbor DEPO_JPYLIBOR6M = new GeneratorDepositIbor("JPYLIBOR6M", JPYLIBOR6M, LOCAL_CALENDAR);
    
 	// Generators for Swaps  	
    private static final GeneratorSwapFixedIbor JPY6MLIBOR6M = GENERATOR_SWAP_MASTER.getGenerator(Generators.JPY6MLIBOR6M, LOCAL_CALENDAR);
    private static final GeneratorSwapIborON JPY3MON = GENERATOR_OISBASIS_MASTER.getGenerator(Generators.JPY3MON, LOCAL_CALENDAR);
    private static final GeneratorSwapFixedON JPY1YTONAR = GENERATOR_OIS_MASTER.getGenerator(Generators.JPY1YTONAR, LOCAL_CALENDAR);
    private static final GeneratorSwapIborIbor JPY3M1M = GENERATOR_BASIS_MASTER.getGenerator(Generators.JPY3M1M, LOCAL_CALENDAR);
    private static final GeneratorSwapIborIbor JPY3M6M = GENERATOR_BASIS_MASTER.getGenerator(Generators.JPY3M6M, LOCAL_CALENDAR);
    
    public JPYConfiguration() {
    	super(new String[] {"JPY DSC", "JPY fwd 6m"});
    	
    	SWAP_QUOTES[0] = new SwapQuote[] {	new SwapQuote(Period.ofMonths(1), JPY1YTONAR,"JPY1MOI=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofMonths(2), JPY1YTONAR,"JPY2MOI=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofMonths(3), JPY1YTONAR,"JPY3MOI=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofMonths(6), JPY1YTONAR,"JPY6MOI=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofMonths(12), JPY1YTONAR,"JPY12MOI=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(2), JPY1YTONAR,"JPY2YOI=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(3), JPY1YTONAR,"JPY3YOI=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(4), JPY1YTONAR,"JPY4YOI=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(5), JPY1YTONAR,"JPY5YOI=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(7), JPY1YTONAR,"JPY7YOI=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(10), JPY1YTONAR,"JPY10YOI=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(15), JPY1YTONAR,"JPY15YOI=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(20), JPY1YTONAR,"JPY20YOI=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(30), JPY1YTONAR,"JPY30YOI=ICIR", QuoteInstrumentType.SWAP)};
    	
    	SWAP_QUOTES[1] = new SwapQuote[] {	new SwapQuote(Period.ofYears(1), JPY6MLIBOR6M, "JPY1YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(2), JPY6MLIBOR6M, "JPY2YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(3), JPY6MLIBOR6M, "JPY3YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(4), JPY6MLIBOR6M, "JPY4YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(5), JPY6MLIBOR6M, "JPY5YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(7), JPY6MLIBOR6M, "JPY7YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(10), JPY6MLIBOR6M, "JPY10YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(15), JPY6MLIBOR6M, "JPY15YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(20), JPY6MLIBOR6M, "JPY20YW=ICIR", QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(30), JPY6MLIBOR6M, "JPY30YW=ICIR", QuoteInstrumentType.SWAP)};

    	
        FX_ORDER = Pair.of(Currency.USD, Currency.JPY);	//1 ccy1 = fxRate * ccy2. MUST be against USD because of the way MultiCurrencyCurveMarketDataProvider is implemented.
       	FX_TICKER = "JPY=ICGC";
    }
   
    public LinkedHashMap<String, Currency> getDiscountMap(){
    	LinkedHashMap<String, Currency> DSC_MAP = new LinkedHashMap<>();
		DSC_MAP.put(CURVE_NAMES[0], JPY);
		return DSC_MAP;
	}
    
    public LinkedHashMap<String, IndexON[]> getIndexONMap(){
    	LinkedHashMap<String, IndexON[]> FWD_ON_MAP = new LinkedHashMap<>();
    	FWD_ON_MAP.put(CURVE_NAMES[0], new IndexON[] {TONAR });	
		return FWD_ON_MAP;
	}
	
	public LinkedHashMap<String, IborIndex[]> getIborIndexMap(){
		LinkedHashMap<String, IborIndex[]> FWD_IBOR_MAP = new LinkedHashMap<>();
		FWD_IBOR_MAP.put(CURVE_NAMES[1], new IborIndex[] {JPYLIBOR6M });
		return FWD_IBOR_MAP;
	}
 	public HashMap<String, Pair<Currency, String>> getCurveRelations() {
 		HashMap<String, Pair<Currency, String>> curveRelations = new HashMap<>();
 		curveRelations.put(CURVE_NAMES[0], Pair.of(Currency.JPY, "Fixed"));
 		curveRelations.put(CURVE_NAMES[1], Pair.of(Currency.JPY, "Fixed"));
 		return curveRelations;
 	}
}
