package com.opengamma.gammatrace.marketconstruction.curveconfiguration;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.threeten.bp.Period;

import com.opengamma.analytics.financial.instrument.index.GeneratorForexSwap;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.gammatrace.marketconstruction.quote.SwapQuote;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorForexSwapTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapXCcyIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class EURMultiCcyConfiguration extends EURConfiguration {
	private static final GeneratorForexSwapTemplate GENERATOR_FXSWAP_MASTER = GeneratorForexSwapTemplate.getInstance();
    private static final GeneratorSwapXCcyIborIborTemplate GENERATOR_XCCY_MASTER = GeneratorSwapXCcyIborIborTemplate.getInstance();
    
	private static final GeneratorSwapXCcyIborIbor EUR3MUSD3M = GENERATOR_XCCY_MASTER.getGenerator(Generators.EUR3MUSD3M, TARGET, NYC);
	private static final GeneratorForexSwap EURUSD = GENERATOR_FXSWAP_MASTER.getGenerator(Generators.EURUSD, TARGET);    
	
	public EURMultiCcyConfiguration() {
		super();
		
		SWAP_QUOTES[0] = new SwapQuote[] {	//new SwapQuote(Period.ofMonths(1), EURUSD, "EUR1MF=ICG7", QuoteInstrumentType.FXSWAP),
											//new SwapQuote(Period.ofMonths(3), EURUSD, "EUR3MF=ICG7", QuoteInstrumentType.FXSWAP),
											//new SwapQuote(Period.ofMonths(6), EURUSD, "EUR6MF=ICG7", QuoteInstrumentType.FXSWAP),
											new SwapQuote(Period.ofYears(1), EUR3MUSD3M, "EUL3USL31Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(2), EUR3MUSD3M, "EUL3USL32Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(3), EUR3MUSD3M, "EUL3USL33Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(5), EUR3MUSD3M, "EUL3USL35Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(10), EUR3MUSD3M, "EUL3USL310Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(15), EUR3MUSD3M, "EUL3USL315Y=ICIR", QuoteInstrumentType.XCCYSWAP),
											new SwapQuote(Period.ofYears(30), EUR3MUSD3M, "EUL3USL330Y=ICIR", QuoteInstrumentType.XCCYSWAP)};
	}
    // no entry into ON map because we are now building the discounting curve from xccy swaps, not from OIS
    //TODO: review: will we ever need xccy overnight index trades? If so need to include OIS swaps as well
    @Override
    public LinkedHashMap<String, IndexON[]> getIndexONMap(){
    	LinkedHashMap<String, IndexON[]> FWD_ON_MAP = new LinkedHashMap<>();
		return FWD_ON_MAP;
	}
    @Override
    public HashMap<String, Pair<Currency, String>> getCurveRelations() {
 		HashMap<String, Pair<Currency, String>> curveRelations = new HashMap<>();
 		curveRelations.put(CURVE_NAMES[0], Pair.of(Currency.EUR, "XCcy"));
 		curveRelations.put(CURVE_NAMES[1], Pair.of(Currency.EUR, CURVE_NAMES[3]));
 		curveRelations.put(CURVE_NAMES[2], Pair.of(Currency.EUR, CURVE_NAMES[3]));
 		curveRelations.put(CURVE_NAMES[3], Pair.of(Currency.EUR, "Fixed"));
 		return curveRelations;
 	}	
}
