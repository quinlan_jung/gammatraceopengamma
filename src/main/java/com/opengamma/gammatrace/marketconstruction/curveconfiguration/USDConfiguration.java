package com.opengamma.gammatrace.marketconstruction.curveconfiguration;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.threeten.bp.Period;

import com.opengamma.analytics.financial.instrument.index.GeneratorDepositIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorDepositON;
import com.opengamma.analytics.financial.instrument.index.GeneratorFRA;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborON;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.gammatrace.marketconstruction.quote.SwapQuote;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorFRATemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;
import com.opengamma.gammatrace.rates.indextemplate.ONTemplateByName;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class USDConfiguration extends CurveConfiguration {
	//TODO: this calendar needs to be sorted out, and calendar should only be generated here and this passed around 
    private static final Calendar NYC = new MondayToFridayCalendar("NYC");
    private static final Currency USD = Currency.USD;
    
    //Generator Template instances
    private static final GeneratorSwapFixedIborTemplate GENERATOR_SWAP_MASTER = GeneratorSwapFixedIborTemplate.getInstance();
    private static final GeneratorSwapFixedONTemplate GENERATOR_OIS_MASTER = GeneratorSwapFixedONTemplate.getInstance();
    private static final GeneratorFRATemplate GENERATOR_FRA_MASTER = GeneratorFRATemplate.getInstance();
    private static final GeneratorSwapIborIborTemplate GENERATOR_BASIS_MASTER = GeneratorSwapIborIborTemplate.getInstance();
    private static final GeneratorSwapIborONTemplate GENERATOR_OISBASIS_MASTER = GeneratorSwapIborONTemplate.getInstance();
    private static final IborTemplateByName IBOR_MASTER = IborTemplateByName.getInstance();
    private static final ONTemplateByName ON_MASTER = ONTemplateByName.getInstance();
    
    // Generators for Libors / OIS
    private static final IndexON FEDFUNDS = ON_MASTER.getIndex(Indicies.FEDFUNDS);
    private static final IborIndex USDLIBOR1M = IBOR_MASTER.getIndex(Indicies.USDLIBOR1M);
    private static final IborIndex USDLIBOR3M = IBOR_MASTER.getIndex(Indicies.USDLIBOR3M);
    private static final IborIndex USDLIBOR6M = IBOR_MASTER.getIndex(Indicies.USDLIBOR6M);

    //Generators for deposits 
    //TODO: is it worth factoring this out into a deposit generator class?
    private static final GeneratorDepositON DEPO_FEDFUNDS = new GeneratorDepositON("DEPO_FEDFUNDS", USD, NYC, FEDFUNDS.getDayCount());
    private static final GeneratorDepositIbor DEPO_USDLIBOR1M = new GeneratorDepositIbor("USDLIBOR1M", USDLIBOR1M, NYC);
    private static final GeneratorDepositIbor DEPO_USDLIBOR3M = new GeneratorDepositIbor("USDLIBOR3M", USDLIBOR3M, NYC);
    private static final GeneratorDepositIbor DEPO_USDLIBOR6M = new GeneratorDepositIbor("USDLIBOR6M", USDLIBOR6M, NYC);
    
    // Generators FRAs
    private static final GeneratorFRA USDLIBOR3MFRA = GENERATOR_FRA_MASTER.getGenerator(Generators.USDLIBOR3MFRA, NYC);

 	// Generators for Swaps  	
    private static final GeneratorSwapFixedIbor USD6MLIBOR3M = GENERATOR_SWAP_MASTER.getGenerator(Generators.USD6MLIBOR3M, NYC);
    private static final GeneratorSwapFixedON USD1YFEDFUND = GENERATOR_OIS_MASTER.getGenerator(Generators.USD1YFEDFUND, NYC);
    private static final GeneratorSwapIborON USD3MON = GENERATOR_OISBASIS_MASTER.getGenerator(Generators.USD3MON, NYC);
    private static final GeneratorSwapIborIbor USD1M3M = GENERATOR_BASIS_MASTER.getGenerator(Generators.USD1M3M, NYC);
    private static final GeneratorSwapIborIbor USD3M6M = GENERATOR_BASIS_MASTER.getGenerator(Generators.USD3M6M, NYC);
    
    public USDConfiguration() {
    	super(new String[] {"USD DSC", "USD fwd 3m", "USD fwd 1m", "USD fwd 6m"});
    	
    	SWAP_QUOTES[0] = new SwapQuote[] {	new SwapQuote(Period.ofMonths(1), USD1YFEDFUND,"USD1MOI=ICIU",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofMonths(2), USD1YFEDFUND,"USD2MOI=ICIU",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofMonths(3), USD1YFEDFUND,"USD3MOI=ICIU",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofMonths(6), USD1YFEDFUND,"USD6MOI=ICIU",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(1), USD1YFEDFUND,"USD12MOI=ICIU",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(2), USD1YFEDFUND,"USD24MOI=ICIU",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(3), USD1YFEDFUND,"USD3YOI=ICIU",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(4), USD1YFEDFUND,"USD4YOI=ICIU",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(5), USD1YFEDFUND,"USD5YOI=ICIU",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(7), USD1YFEDFUND,"USD7YOI=ICIU",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(10), USD1YFEDFUND,"USD10YOI=ICIU",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(15), USD1YFEDFUND,"USD15YOI=ICIU",QuoteInstrumentType.SWAP),
    										new SwapQuote(Period.ofYears(30), USD1YFEDFUND,"USD30YOI=ICIU",QuoteInstrumentType.SWAP)};
    	
    	SWAP_QUOTES[1] = new SwapQuote[] {	new SwapQuote(Period.ofMonths(6), USDLIBOR3MFRA,"USD3X6FRA=ICIU",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofMonths(9), USDLIBOR3MFRA,"USD6X9FRA=ICIU",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(1), USD6MLIBOR3M,"USD1YW=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(2), USD6MLIBOR3M,"USD2YW=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(3), USD6MLIBOR3M,"USD3YW=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(4), USD6MLIBOR3M,"USD4YW=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(5), USD6MLIBOR3M,"USD5YW=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(7), USD6MLIBOR3M,"USD7YW=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(10), USD6MLIBOR3M,"USD10YW=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(15), USD6MLIBOR3M,"USD15YW=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(20), USD6MLIBOR3M,"USD20YW=ICIR",QuoteInstrumentType.SWAP),
											new SwapQuote(Period.ofYears(30), USD6MLIBOR3M,"USD30YW=ICIR",QuoteInstrumentType.SWAP)};
    	
    	SWAP_QUOTES[2] = new SwapQuote[] {	new SwapQuote(Period.ofMonths(6), USD1M3M,"USD3USL16M=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(1), USD1M3M,"USD3USL11Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(2), USD1M3M,"USD3USL12Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(3), USD1M3M,"USD3USL13Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(5), USD1M3M,"USD3USL15Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(7), USD1M3M,"USD3USL17Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(10), USD1M3M,"USD3USL110Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(15), USD1M3M,"USD3USL115Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(30), USD1M3M,"USD3USL130Y=ICIR",QuoteInstrumentType.BASISSWAP)};
    	
    	SWAP_QUOTES[3] = new SwapQuote[] {	new SwapQuote(Period.ofYears(1), USD3M6M,"USD6USL31Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(2), USD3M6M,"USD6USL32Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(3), USD3M6M,"USD6USL33Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(5), USD3M6M,"USD6USL35Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(7), USD3M6M,"USD6USL37Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(10), USD3M6M,"USD6USL310Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(15), USD3M6M,"USD6USL315Y=ICIR",QuoteInstrumentType.BASISSWAP),
											new SwapQuote(Period.ofYears(30), USD3M6M,"USD6USL330Y=ICIR",QuoteInstrumentType.BASISSWAP)};
    
    	FX_ORDER = null;	//1 ccy1 = fxRate * ccy2. MUST be against USD because of the way MultiCurrencyCurveMarketDataProvider is implemented.
       	FX_TICKER = null;
    }    	
   
    public LinkedHashMap<String, Currency> getDiscountMap() {
    	LinkedHashMap<String, Currency> DSC_MAP = new LinkedHashMap<>();
		DSC_MAP.put(CURVE_NAMES[0], USD);
		return DSC_MAP;
	}
    
    public LinkedHashMap<String, IndexON[]> getIndexONMap(){
    	LinkedHashMap<String, IndexON[]> FWD_ON_MAP = new LinkedHashMap<>();
    	FWD_ON_MAP.put(CURVE_NAMES[0], new IndexON[] {FEDFUNDS });	
		return FWD_ON_MAP;
	}
	
	public LinkedHashMap<String, IborIndex[]> getIborIndexMap(){
		LinkedHashMap<String, IborIndex[]> FWD_IBOR_MAP = new LinkedHashMap<>();
		FWD_IBOR_MAP.put(CURVE_NAMES[1], new IborIndex[] {USDLIBOR3M });
	    FWD_IBOR_MAP.put(CURVE_NAMES[2], new IborIndex[] {USDLIBOR1M });
	    FWD_IBOR_MAP.put(CURVE_NAMES[3], new IborIndex[] {USDLIBOR6M });
		return FWD_IBOR_MAP;
	}

 	public HashMap<String, Pair<Currency, String>> getCurveRelations() {
 		HashMap<String, Pair<Currency, String>> curveRelations = new HashMap<>();
 		curveRelations.put(CURVE_NAMES[0], Pair.of(Currency.USD, "Fixed"));
 		curveRelations.put(CURVE_NAMES[1], Pair.of(Currency.USD, "Fixed"));
 		curveRelations.put(CURVE_NAMES[2], Pair.of(Currency.USD, CURVE_NAMES[1]));
 		curveRelations.put(CURVE_NAMES[3], Pair.of(Currency.USD, CURVE_NAMES[1]));
 		return curveRelations;
 	}
}
