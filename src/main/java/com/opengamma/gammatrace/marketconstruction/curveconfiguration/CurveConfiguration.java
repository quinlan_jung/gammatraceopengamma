package com.opengamma.gammatrace.marketconstruction.curveconfiguration;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.gammatrace.marketconstruction.quote.SwapQuote;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public abstract class CurveConfiguration {
	protected String[] CURVE_NAMES;
    protected SwapQuote[][] SWAP_QUOTES;
	
    protected Pair<Currency, Currency> FX_ORDER;
    protected String FX_TICKER;
    
    public CurveConfiguration(String[] names) {
    	CURVE_NAMES = names;
    	SWAP_QUOTES = new SwapQuote[CURVE_NAMES.length][];
    	
    }
    
	public abstract LinkedHashMap<String, Currency> getDiscountMap();
    public abstract LinkedHashMap<String, IndexON[]> getIndexONMap();
	public abstract LinkedHashMap<String, IborIndex[]> getIborIndexMap();
	public abstract HashMap<String, Pair<Currency, String>> getCurveRelations();
	
	public LinkedHashMap<String, SwapQuote[]> getSwapQuotes() {
		LinkedHashMap<String, SwapQuote[]> map = new LinkedHashMap<>();
		for (int i = 0; i < CURVE_NAMES.length; i++) {
			map.put(CURVE_NAMES[i], SWAP_QUOTES[i]);
		}
		return map;
	}
	
	public String[] getNames() {
    	return CURVE_NAMES;
    }
	public Pair<Currency, Currency> getFxOrder() {
    	return FX_ORDER;
    }
    public String getFxTicker() {
    	return FX_TICKER;
    }
}
