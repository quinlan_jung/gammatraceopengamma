package com.opengamma.gammatrace.marketconstruction.forexvol;

import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.model.volatility.surface.SmileDeltaTermStructureParametersStrikeInterpolation;
import com.opengamma.analytics.financial.provider.description.forex.BlackForexSmileProviderDiscount;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class ForexSmileCurveMaker {

	private Currency firstCcy;
	private Currency secondCcy;
	private ForexSmileMarketDataProvider volProvider;
	private CurveMaker curveMaker;
	
	public ForexSmileCurveMaker(Currency firstCcy, Currency secondCcy, ZonedDateTime curveTime) {
		this.firstCcy = firstCcy;
		this.secondCcy = secondCcy;
		volProvider = new ForexSmileMarketDataProvider(firstCcy, secondCcy, curveTime);
		curveMaker = new CurveMaker(firstCcy, secondCcy, curveTime);
	}
	
	public BlackForexSmileProviderDiscount makeForexSmileCurve() {
		return new BlackForexSmileProviderDiscount(curveMaker.makeCurve().getFirst(), volProvider.getSmileTermstructure(), Pair.of(firstCcy, secondCcy));
	}
	
	public double[] getDeltaPoints() {
		return volProvider.getDeltaPoints();
	}
	
}
