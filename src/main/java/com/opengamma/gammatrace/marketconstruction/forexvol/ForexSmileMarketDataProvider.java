package com.opengamma.gammatrace.marketconstruction.forexvol;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.model.volatility.surface.SmileDeltaTermStructureParametersStrikeInterpolation;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.analytics.util.time.TimeCalculator;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.gammatrace.exceptions.UnsupportedOperation;
import com.opengamma.gammatrace.marketconstruction.dataprovider.RandomGenerator;
import com.opengamma.gammatrace.marketconstruction.forexvolconfiguration.EURUSDVolConfiguration;
import com.opengamma.gammatrace.marketconstruction.forexvolconfiguration.ForexVolConfiguration;
import com.opengamma.gammatrace.marketconstruction.quote.ForexVolQuoteInstrumentType;
import com.opengamma.util.money.Currency;
//TODO: update with Sungard API
public class ForexSmileMarketDataProvider {
	private ZonedDateTime curveTime;
	private ForexVolConfiguration configuration;
	
	public ForexSmileMarketDataProvider(Currency firstCcy, Currency secondCcy, ZonedDateTime curveTime) {
		Set<Currency> set = new HashSet<>(); 
		set.add(firstCcy); 
		set.add(secondCcy);
		configuration = setConfiguration(set);
		this.curveTime = curveTime;
	}
	
	public SmileDeltaTermStructureParametersStrikeInterpolation getSmileTermstructure() {
		// create the time to expiry array
		BusinessDayConvention busDays = BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following");
		Calendar cal = configuration.getCalendar();
		int settlementLag = configuration.getSettlementLag();
		Period[] expiryPeriod = configuration.getInstrumentTenors();
		
		ZonedDateTime spotReferenceDate = ScheduleCalculator.getAdjustedDate(curveTime, settlementLag, cal);
		ZonedDateTime[] payDate = new ZonedDateTime[expiryPeriod.length];
		double[] timeToExpiration = new double[expiryPeriod.length];
		for (int i = 0; i < expiryPeriod.length; i++) {
			payDate[i] = ScheduleCalculator.getAdjustedDate(ScheduleCalculator.getAdjustedDate(spotReferenceDate, expiryPeriod[i], busDays, cal), -settlementLag, cal);
		    timeToExpiration[i] = TimeCalculator.getTimeBetween(curveTime, payDate[i]);
		}
		
		// get the values for the vol quotes
		RandomGenerator randomGenerator = new RandomGenerator(curveTime);
		double[] delta = configuration.getDeltaPoints();
		double[] atmVol = new double[timeToExpiration.length];
		double[][] riskReversalVol = new double[delta.length][timeToExpiration.length];
		double[][] flyVol = new double[delta.length][timeToExpiration.length];
		
		LinkedHashMap<ForexVolQuoteInstrumentType, String[][]> tickerMap = configuration.getInstrumentTickers();
		for (Map.Entry<ForexVolQuoteInstrumentType, String[][]> entry : tickerMap.entrySet()) {
			if (entry.getKey().equals(ForexVolQuoteInstrumentType.ATM)) {
				atmVol = randomGenerator.getFxVolQuotes(entry.getValue()[0]);	
			} else if (entry.getKey().equals(ForexVolQuoteInstrumentType.RISK_REVERSAL)){
				riskReversalVol = randomGenerator.getFxVolQuotesRR();
			} else {
				flyVol = randomGenerator.getFxVolQuotesFly();
			}
		}
		
		return new SmileDeltaTermStructureParametersStrikeInterpolation(timeToExpiration, delta, atmVol, riskReversalVol, flyVol);
	}
	
	
	public double[] getDeltaPoints() {
		return configuration.getDeltaPoints();
	}
	public String[] getNames() {	
    	return configuration.getNames();
    }
	public ForexVolQuoteInstrumentType[] getQuoteInstrumentType() {	
    	return configuration.getQuoteInstrumentType();
    }
	
	private ForexVolConfiguration setConfiguration(Set<Currency> set) {
		if (set.contains(Currency.USD) && set.contains(Currency.EUR)) {
			return new EURUSDVolConfiguration();
		}
		System.out.println("Don't have this fx vol configuration");
		return null; 
	}
}
