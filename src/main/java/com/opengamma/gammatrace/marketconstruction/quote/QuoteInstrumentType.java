package com.opengamma.gammatrace.marketconstruction.quote;

public enum QuoteInstrumentType {
	DEPOSIT("Deposit"),
	FUTURE("Future"),
	FRA("FRA"),
	SWAP("Swap"),
	BASISSWAP("Basis swap"),
	XCCYSWAP("Xccy swap"),
	FXSWAP("Fx swap");
	
	private String description;
	private QuoteInstrumentType(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return description;
	}
}



