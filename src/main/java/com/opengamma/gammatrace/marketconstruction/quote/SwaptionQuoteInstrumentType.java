package com.opengamma.gammatrace.marketconstruction.quote;

public enum SwaptionQuoteInstrumentType {
	STRADDLE_PHYSICAL("ATM straddle physically settled"),
	STRADDLE_CASH("ATM straddle cash settled");
	
	private String description;
	private SwaptionQuoteInstrumentType(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return description;
	}
}
