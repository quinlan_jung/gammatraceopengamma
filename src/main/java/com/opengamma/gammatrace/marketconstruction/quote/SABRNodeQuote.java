package com.opengamma.gammatrace.marketconstruction.quote;

import org.threeten.bp.Period;

public class SABRNodeQuote {

	private Period expiry;
	private Period underlyingTenor;
	private String straddleATMPremiumTicker;
	private String betaTicker;
	private String rhoTicker;
	private String nuTicker;

	public SABRNodeQuote(Period expiry, Period underlyingTenor, String alphaTicker, String betaTicker, String rhoTicker, String nuTicker) {
		this.expiry = expiry;
		this.underlyingTenor = underlyingTenor;
		this.straddleATMPremiumTicker = alphaTicker;
		this.betaTicker = betaTicker;
		this.rhoTicker = rhoTicker;
		this.nuTicker = nuTicker;
	}
	
	
	public Period getExpiry() {
		return expiry;
	}
	public Period getUnderlyingTenor() {
		return underlyingTenor;
	}
	
	public String getStraddleATMPremiumTicker() {
		return straddleATMPremiumTicker;
	}
	public String getBetaTicker() {
		return betaTicker;
	}
	public String getRhoTicker() {
		return rhoTicker;
	}
	public String getNuTicker() {
		return nuTicker;
	}

	
	public static Period[] getExpiriesFromArray(SABRNodeQuote[] sabrNodeQuoteArray) {
		Period[] expiries = new Period[sabrNodeQuoteArray.length];
		for (int i = 0; i < sabrNodeQuoteArray.length; i++) {
			expiries[i] = sabrNodeQuoteArray[i].getExpiry();
		}
		return expiries;
	}
	public static Period[] getUnderlyingTenorFromArray(SABRNodeQuote[] sabrNodeQuoteArray) {
		Period[] tenors = new Period[sabrNodeQuoteArray.length];
		for (int i = 0; i < sabrNodeQuoteArray.length; i++) {
			tenors[i] = sabrNodeQuoteArray[i].getUnderlyingTenor();
		}
		return tenors;
	}	
	public static String[] getStraddleATMPremiumTickersFromArray(SABRNodeQuote[] sabrNodeQuoteArray) {
		String[] tickers = new String[sabrNodeQuoteArray.length];
		for (int i = 0; i < sabrNodeQuoteArray.length; i++) {
			tickers[i] = sabrNodeQuoteArray[i].getStraddleATMPremiumTicker();
		}
		return tickers;
	}
	public static String[] getBetaTickersFromArray(SABRNodeQuote[] sabrNodeQuoteArray) {
		String[] tickers = new String[sabrNodeQuoteArray.length];
		for (int i = 0; i < sabrNodeQuoteArray.length; i++) {
			tickers[i] = sabrNodeQuoteArray[i].getBetaTicker();
		}
		return tickers;
	}
	
}
