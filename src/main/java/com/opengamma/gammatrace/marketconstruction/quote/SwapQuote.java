package com.opengamma.gammatrace.marketconstruction.quote;

import org.threeten.bp.Period;

import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;

public class SwapQuote {
	private Period period;
	private GeneratorInstrument<?> generator;
	private String ticker;
	private QuoteInstrumentType description;
	private double sungardValue = -999.99;
	private double conversionFactor;
	private double initialSearchPeriod;
	
	public SwapQuote(Period period, GeneratorInstrument<?> generator, String ticker, QuoteInstrumentType description) {
		this.period = period;
		this.generator = generator;
		this.ticker = ticker;
		this.description = description;
		conversionFactor = setConversionFactor();
	}

	public SwapQuote(Period period, GeneratorInstrument<?> generator, String ticker, QuoteInstrumentType description, double conversionFactor) {
		this.period = period;
		this.generator = generator;
		this.ticker = ticker;
		this.description = description;
		this.conversionFactor = conversionFactor;
	}
	
	public Period getPeriod() {
		return period;
	}

	public GeneratorInstrument getGenerator() {
		return generator;
	}

	public String getTicker() {
		return ticker;
	}

	public QuoteInstrumentType getDescription() {
		return description;
	}
	
	public double getValue() {
		if (sungardValue == -999.99) {
			throw new IllegalStateException("Quote value not set");
		}
		return sungardValue * conversionFactor;
	}
	
	public void setSungardValue(double sungardValue) {
		this.sungardValue = sungardValue;
	}
	
	private double setConversionFactor() {
		switch(description) {
		case DEPOSIT:
			return 0.01;
		case FUTURE:
			return 0.01;
		case FRA:
			return 0.01;
		case SWAP:
			return 0.01;
		case BASISSWAP:
			return 0.0001;
		case XCCYSWAP:
			return 0.0001;
		case FXSWAP:
			return 0.0001;			// TODO: this is not true for all ccy pairs, see conventions guide.
		default:
			return 1;
		}
	}
	
}
