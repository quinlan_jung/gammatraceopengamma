package com.opengamma.gammatrace.marketconstruction.quote;

public enum ForexVolQuoteInstrumentType {
	ATM("Delta Neutral straddle"),
	RISK_REVERSAL("Risk reversal"),
	FLY("Fly");
	
	private String description;
	private ForexVolQuoteInstrumentType(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return description;
	}
}
