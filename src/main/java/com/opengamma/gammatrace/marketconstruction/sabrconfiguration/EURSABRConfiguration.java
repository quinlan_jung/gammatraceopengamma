package com.opengamma.gammatrace.marketconstruction.sabrconfiguration;

import org.threeten.bp.Period;

import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.quote.SABRNodeQuote;
import com.opengamma.gammatrace.marketconstruction.quote.SwaptionQuoteInstrumentType;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;

public class EURSABRConfiguration extends SABRConfiguration {
	public EURSABRConfiguration() {
		calendar = new MondayToFridayCalendar("TARGET");
		underlyingGenerator = GeneratorSwapFixedIborTemplate.getInstance().getGenerator(Generators.EUR1YEURIBOR6M, calendar);
		quoteInstrumentType = SwaptionQuoteInstrumentType.STRADDLE_CASH;
		sabrNodeQuotes = new SABRNodeQuote[] {	new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(1), "EUR-1M-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(2), "EUR-1M-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(3), "EUR-1M-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(4), "EUR-1M-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(5), "EUR-1M-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(7), "EUR-1M-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(10), "EUR-1M-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(15), "EUR-1M-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(20), "EUR-1M-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(30), "EUR-1M-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(1), "EUR-3M-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(2), "EUR-3M-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(3), "EUR-3M-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(4), "EUR-3M-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(5), "EUR-3M-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(7), "EUR-3M-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(10), "EUR-3M-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(15), "EUR-3M-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(20), "EUR-3M-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(30), "EUR-3M-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(1), "EUR-6M-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(2), "EUR-6M-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(3), "EUR-6M-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(4), "EUR-6M-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(5), "EUR-6M-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(7), "EUR-6M-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(10), "EUR-6M-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(15), "EUR-6M-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(20), "EUR-6M-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(30), "EUR-6M-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(1), "EUR-1Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(2), "EUR-1Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(3), "EUR-1Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(4), "EUR-1Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(5), "EUR-1Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(7), "EUR-1Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(10), "EUR-1Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(15), "EUR-1Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(20), "EUR-1Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(30), "EUR-1Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(1), "EUR-2Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(2), "EUR-2Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(3), "EUR-2Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(4), "EUR-2Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(5), "EUR-2Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(7), "EUR-2Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(10), "EUR-2Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(15), "EUR-2Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(20), "EUR-2Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(30), "EUR-2Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(1), "EUR-3Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(2), "EUR-3Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(3), "EUR-3Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(4), "EUR-3Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(5), "EUR-3Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(7), "EUR-3Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(10), "EUR-3Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(15), "EUR-3Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(20), "EUR-3Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(30), "EUR-3Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(1), "EUR-4Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(2), "EUR-4Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(3), "EUR-4Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(4), "EUR-4Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(5), "EUR-4Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(7), "EUR-4Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(10), "EUR-4Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(15), "EUR-4Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(20), "EUR-4Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(30), "EUR-4Y-30Y=ICIO", "beta", "rho", "nu"),									
												
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(1), "EUR-5Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(2), "EUR-5Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(3), "EUR-5Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(4), "EUR-5Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(5), "EUR-5Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(7), "EUR-5Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(10), "EUR-5Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(15), "EUR-5Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(20), "EUR-5Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(30), "EUR-5Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(1), "EUR-7Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(2), "EUR-7Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(3), "EUR-7Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(4), "EUR-7Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(5), "EUR-7Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(7), "EUR-7Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(10), "EUR-7Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(15), "EUR-7Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(20), "EUR-7Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(30), "EUR-7Y-30Y=ICIO", "beta", "rho", "nu"),
										
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(1), "EUR-10Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(2), "EUR-10Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(3), "EUR-10Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(4), "EUR-10Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(5), "EUR-10Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(7), "EUR-10Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(10), "EUR-10Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(15), "EUR-10Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(20), "EUR-10Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(30), "EUR-10Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(1), "EUR-15Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(2), "EUR-15Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(3), "EUR-15Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(4), "EUR-15Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(5), "EUR-15Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(7), "EUR-15Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(10), "EUR-15Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(15), "EUR-15Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(20), "EUR-15Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(30), "EUR-15Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(1), "EUR-20Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(2), "EUR-20Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(3), "EUR-20Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(4), "EUR-20Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(5), "EUR-20Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(7), "EUR-20Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(10), "EUR-20Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(15), "EUR-20Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(20), "EUR-20Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(30), "EUR-20Y-30Y=ICIO", "beta", "rho", "nu")};
	}	
}
