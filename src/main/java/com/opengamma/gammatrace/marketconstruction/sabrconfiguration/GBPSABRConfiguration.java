package com.opengamma.gammatrace.marketconstruction.sabrconfiguration;

import org.threeten.bp.Period;

import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.quote.SABRNodeQuote;
import com.opengamma.gammatrace.marketconstruction.quote.SwaptionQuoteInstrumentType;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;

public class GBPSABRConfiguration extends SABRConfiguration {
	public GBPSABRConfiguration() {
		calendar = new MondayToFridayCalendar("LONDON");
		underlyingGenerator = GeneratorSwapFixedIborTemplate.getInstance().getGenerator(Generators.GBP6MLIBOR6M, calendar);
		quoteInstrumentType = SwaptionQuoteInstrumentType.STRADDLE_CASH;
		sabrNodeQuotes = new SABRNodeQuote[] {	new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(1), "GBP-1M-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(2), "GBP-1M-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(3), "GBP-1M-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(4), "GBP-1M-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(5), "GBP-1M-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(7), "GBP-1M-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(10), "GBP-1M-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(15), "GBP-1M-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(20), "GBP-1M-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(30), "GBP-1M-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(1), "GBP-3M-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(2), "GBP-3M-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(3), "GBP-3M-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(4), "GBP-3M-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(5), "GBP-3M-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(7), "GBP-3M-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(10), "GBP-3M-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(15), "GBP-3M-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(20), "GBP-3M-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(30), "GBP-3M-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(1), "GBP-6M-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(2), "GBP-6M-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(3), "GBP-6M-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(4), "GBP-6M-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(5), "GBP-6M-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(7), "GBP-6M-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(10), "GBP-6M-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(15), "GBP-6M-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(20), "GBP-6M-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(30), "GBP-6M-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(1), "GBP-1Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(2), "GBP-1Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(3), "GBP-1Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(4), "GBP-1Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(5), "GBP-1Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(7), "GBP-1Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(10), "GBP-1Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(15), "GBP-1Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(20), "GBP-1Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(30), "GBP-1Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(1), "GBP-2Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(2), "GBP-2Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(3), "GBP-2Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(4), "GBP-2Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(5), "GBP-2Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(7), "GBP-2Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(10), "GBP-2Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(15), "GBP-2Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(20), "GBP-2Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(30), "GBP-2Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(1), "GBP-3Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(2), "GBP-3Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(3), "GBP-3Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(4), "GBP-3Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(5), "GBP-3Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(7), "GBP-3Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(10), "GBP-3Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(15), "GBP-3Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(20), "GBP-3Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(30), "GBP-3Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(1), "GBP-4Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(2), "GBP-4Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(3), "GBP-4Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(4), "GBP-4Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(5), "GBP-4Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(7), "GBP-4Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(10), "GBP-4Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(15), "GBP-4Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(20), "GBP-4Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(30), "GBP-4Y-30Y=ICIO", "beta", "rho", "nu"),									
												
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(1), "GBP-5Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(2), "GBP-5Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(3), "GBP-5Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(4), "GBP-5Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(5), "GBP-5Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(7), "GBP-5Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(10), "GBP-5Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(15), "GBP-5Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(20), "GBP-5Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(30), "GBP-5Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(1), "GBP-7Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(2), "GBP-7Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(3), "GBP-7Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(4), "GBP-7Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(5), "GBP-7Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(7), "GBP-7Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(10), "GBP-7Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(15), "GBP-7Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(20), "GBP-7Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(30), "GBP-7Y-30Y=ICIO", "beta", "rho", "nu"),
										
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(1), "GBP-10Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(2), "GBP-10Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(3), "GBP-10Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(4), "GBP-10Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(5), "GBP-10Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(7), "GBP-10Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(10), "GBP-10Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(15), "GBP-10Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(20), "GBP-10Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(30), "GBP-10Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(1), "GBP-15Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(2), "GBP-15Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(3), "GBP-15Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(4), "GBP-15Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(5), "GBP-15Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(7), "GBP-15Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(10), "GBP-15Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(15), "GBP-15Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(20), "GBP-15Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(30), "GBP-15Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(1), "GBP-20Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(2), "GBP-20Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(3), "GBP-20Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(4), "GBP-20Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(5), "GBP-20Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(7), "GBP-20Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(10), "GBP-20Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(15), "GBP-20Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(20), "GBP-20Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(30), "GBP-20Y-30Y=ICIO", "beta", "rho", "nu")};
	}	
}
