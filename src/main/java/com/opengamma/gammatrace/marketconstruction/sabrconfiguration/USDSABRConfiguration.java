package com.opengamma.gammatrace.marketconstruction.sabrconfiguration;

import org.threeten.bp.Period;

import com.gammatrace.commonutils.Utils;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.quote.SABRNodeQuote;
import com.opengamma.gammatrace.marketconstruction.quote.SwaptionQuoteInstrumentType;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;

public class USDSABRConfiguration extends SABRConfiguration {
	public USDSABRConfiguration() {
		calendar = new MondayToFridayCalendar("NYC");
		underlyingGenerator = GeneratorSwapFixedIborTemplate.getInstance().getGenerator(Generators.USD6MLIBOR3M, calendar);
		quoteInstrumentType = SwaptionQuoteInstrumentType.STRADDLE_PHYSICAL;
		sabrNodeQuotes = new SABRNodeQuote[] {	new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(1), "USD-1M-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(2), "USD-1M-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(3), "USD-1M-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(4), "USD-1M-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(5), "USD-1M-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(7), "USD-1M-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(10), "USD-1M-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(15), "USD-1M-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(20), "USD-1M-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(1), Period.ofYears(30), "USD-1M-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(1), "USD-3M-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(2), "USD-3M-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(3), "USD-3M-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(4), "USD-3M-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(5), "USD-3M-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(7), "USD-3M-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(10), "USD-3M-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(15), "USD-3M-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(20), "USD-3M-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(3), Period.ofYears(30), "USD-3M-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(1), "USD-6M-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(2), "USD-6M-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(3), "USD-6M-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(4), "USD-6M-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(5), "USD-6M-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(7), "USD-6M-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(10), "USD-6M-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(15), "USD-6M-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(20), "USD-6M-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofMonths(6), Period.ofYears(30), "USD-6M-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(1), "USD-1Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(2), "USD-1Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(3), "USD-1Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(4), "USD-1Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(5), "USD-1Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(7), "USD-1Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(10), "USD-1Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(15), "USD-1Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(20), "USD-1Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(1), Period.ofYears(30), "USD-1Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(1), "USD-2Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(2), "USD-2Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(3), "USD-2Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(4), "USD-2Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(5), "USD-2Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(7), "USD-2Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(10), "USD-2Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(15), "USD-2Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(20), "USD-2Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(2), Period.ofYears(30), "USD-2Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(1), "USD-3Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(2), "USD-3Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(3), "USD-3Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(4), "USD-3Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(5), "USD-3Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(7), "USD-3Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(10), "USD-3Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(15), "USD-3Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(20), "USD-3Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(3), Period.ofYears(30), "USD-3Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(1), "USD-4Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(2), "USD-4Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(3), "USD-4Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(4), "USD-4Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(5), "USD-4Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(7), "USD-4Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(10), "USD-4Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(15), "USD-4Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(20), "USD-4Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(4), Period.ofYears(30), "USD-4Y-30Y=ICIO", "beta", "rho", "nu"),									
												
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(1), "USD-5Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(2), "USD-5Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(3), "USD-5Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(4), "USD-5Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(5), "USD-5Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(7), "USD-5Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(10), "USD-5Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(15), "USD-5Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(20), "USD-5Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(5), Period.ofYears(30), "USD-5Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(1), "USD-7Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(2), "USD-7Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(3), "USD-7Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(4), "USD-7Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(5), "USD-7Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(7), "USD-7Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(10), "USD-7Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(15), "USD-7Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(20), "USD-7Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(7), Period.ofYears(30), "USD-7Y-30Y=ICIO", "beta", "rho", "nu"),
										
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(1), "USD-10Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(2), "USD-10Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(3), "USD-10Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(4), "USD-10Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(5), "USD-10Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(7), "USD-10Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(10), "USD-10Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(15), "USD-10Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(20), "USD-10Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(10), Period.ofYears(30), "USD-10Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(1), "USD-15Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(2), "USD-15Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(3), "USD-15Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(4), "USD-15Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(5), "USD-15Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(7), "USD-15Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(10), "USD-15Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(15), "USD-15Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(20), "USD-15Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(15), Period.ofYears(30), "USD-15Y-30Y=ICIO", "beta", "rho", "nu"),
												
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(1), "USD-20Y-1Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(2), "USD-20Y-2Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(3), "USD-20Y-3Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(4), "USD-20Y-4Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(5), "USD-20Y-5Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(7), "USD-20Y-7Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(10), "USD-20Y-10Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(15), "USD-20Y-15Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(20), "USD-20Y-20Y=ICIO", "beta", "rho", "nu"),
												new SABRNodeQuote(Period.ofYears(20), Period.ofYears(30), "USD-20Y-30Y=ICIO", "beta", "rho", "nu")};
		

	}	
}
