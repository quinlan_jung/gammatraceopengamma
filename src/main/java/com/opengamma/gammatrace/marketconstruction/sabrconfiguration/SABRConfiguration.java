package com.opengamma.gammatrace.marketconstruction.sabrconfiguration;

import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.gammatrace.marketconstruction.quote.SABRNodeQuote;
import com.opengamma.gammatrace.marketconstruction.quote.SwaptionQuoteInstrumentType;

public abstract class SABRConfiguration {
	protected SABRNodeQuote[] sabrNodeQuotes; 
	protected GeneratorSwapFixedIbor underlyingGenerator;
	protected Calendar calendar;
	protected SwaptionQuoteInstrumentType quoteInstrumentType; 
	
	public SABRNodeQuote[] getVolQuotes() {
		return sabrNodeQuotes;
	}
	
	public GeneratorSwapFixedIbor getUnderlyingGenerator() {
		return underlyingGenerator;
	}
	
	public Calendar getCalendar() {
		return calendar;
	}
	public SwaptionQuoteInstrumentType getQuoteInstrumentType() {
		return quoteInstrumentType;
	}
	
}
