package com.opengamma.gammatrace.marketconstruction.dictionary;

import com.opengamma.gammatrace.exceptions.UnsupportedTradeException;
import com.opengamma.gammatrace.instrument.Instrument;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
@Deprecated
public class DictionaryManager {
	//ParameterProviderInterface
	public static CurveMaker makeDictionary(Instrument instrument) throws UnsupportedTradeException {
		InstrumentDescription description = instrument.getInstrumentDescription();
		switch (description) {
		case FIXED_IBOR_SWAP: case FIXED_ON_SWAP: case IBOR_IBOR_SWAP: case IBOR_ON_SWAP:
			return new CurveMaker(instrument.getSecondNotional().getCurrency(), instrument.getExecutionTimestamp());
		case XCCY_IBOR_IBOR_SWAP: case XCCY_FIXED_IBOR_SWAP: case XCCY_FIXED_FIXED_SWAP:
			return new CurveMaker(instrument.getSecondNotional().getCurrency(), instrument.getFirstNotional().getCurrency(), instrument.getExecutionTimestamp());

		//case FIXED_IBOR_SWAPTION: case FIXED_IBOR_STRADDLE_SWAPTION:
			//return new SABRSwaptionCurveMaker(instrument.getSecondNotional().getCurrency(), instrument.getExecutionTimestamp());

		case FX_OPTION:
			return null;
			//break;
		default:
			return null;
		}
		
	}
}
