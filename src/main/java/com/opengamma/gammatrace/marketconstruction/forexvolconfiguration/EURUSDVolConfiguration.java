package com.opengamma.gammatrace.marketconstruction.forexvolconfiguration;

import java.util.LinkedHashMap;

import org.threeten.bp.Period;

import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.quote.ForexVolQuoteInstrumentType;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class EURUSDVolConfiguration implements ForexVolConfiguration {
/**
 * 
 */
	private static final Calendar JOINT_CALENDAR = new MondayToFridayCalendar("Joint");
	private static final int SETTLEMENT_LAG = 2;
	
	private static final Period[] EXPIRY_PERIOD = new Period[] {Period.ofMonths(0), Period.ofMonths(3), Period.ofMonths(6), Period.ofYears(1), Period.ofYears(2), 
		Period.ofYears(3), Period.ofYears(5), Period.ofYears(10)};
	private static final double[] DELTA = {0.10, 0.25};
	
	private static final String[] ATM_TICKERS = {"a" , "b", "c", "d", "e", "f", "g", "h"};
	private static final String[][] RR_TICKERS = {{"a1", "a2"} , {"b1", "b2"}, {"c1", "c2"}, {"d1", "d2"}, {"e1", "e2"}, {"f1", "f2"}, {"g1", "g2"}, {"h1", "h2"}};
	private static final String[][] FLY_TICKERS = {{"a3", "a4"} , {"b3", "b4"}, {"c3", "c4"}, {"d3", "d4"}, {"e3", "e4"}, {"f3", "f4"}, {"g3", "g4"}, {"h3", "h4"}};
	
	private static final String[] VOL_NAMES = {"ATM", "RiskReversal", "Fly"};
	private static final ForexVolQuoteInstrumentType[] QUOTE_TYPE = {ForexVolQuoteInstrumentType.ATM, ForexVolQuoteInstrumentType.RISK_REVERSAL, ForexVolQuoteInstrumentType.FLY};
	
	public Period[] getInstrumentTenors() {
		return EXPIRY_PERIOD;
	}
	
	public LinkedHashMap<ForexVolQuoteInstrumentType, String[][]> getInstrumentTickers(){
		LinkedHashMap<ForexVolQuoteInstrumentType, String[][]> map = new LinkedHashMap<>();
		map.put(QUOTE_TYPE[0], new String[][] {ATM_TICKERS});
		map.put(QUOTE_TYPE[1], RR_TICKERS);
		map.put(QUOTE_TYPE[2], FLY_TICKERS);
		return map;
	}
	public Calendar getCalendar() {
		return JOINT_CALENDAR;
	}
	
	public int getSettlementLag() {
		return SETTLEMENT_LAG;
	}
	
	public double[] getDeltaPoints() {
		return DELTA;
	}
	public String[] getNames() {	
    	return VOL_NAMES;
    }
	public ForexVolQuoteInstrumentType[] getQuoteInstrumentType() {	
    	return QUOTE_TYPE;
    }	
}
