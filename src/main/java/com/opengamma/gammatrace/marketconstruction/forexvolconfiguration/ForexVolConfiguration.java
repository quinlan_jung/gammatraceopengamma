package com.opengamma.gammatrace.marketconstruction.forexvolconfiguration;

import java.util.LinkedHashMap;

import org.threeten.bp.Period;

import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.gammatrace.marketconstruction.quote.ForexVolQuoteInstrumentType;

public interface ForexVolConfiguration {
	public Calendar getCalendar();
	
	public int getSettlementLag();
	
	public Period[]  getInstrumentTenors();
	
	public LinkedHashMap<ForexVolQuoteInstrumentType, String[][]> getInstrumentTickers();
	
	public double[] getDeltaPoints();
	
	public String[] getNames();
    
	public ForexVolQuoteInstrumentType[] getQuoteInstrumentType();	
}
