package com.opengamma.gammatrace.marketconstruction.sabr;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.threeten.bp.ZonedDateTime;

import com.gammatrace.sungard.api.ServiceApi;
import com.gammatrace.sungard.api.ServiceApiImpl;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.model.volatility.smile.function.SABRFormulaData;
import com.opengamma.gammatrace.marketconstruction.quote.SABRNodeQuote;
import com.opengamma.gammatrace.marketconstruction.quote.SwaptionQuoteInstrumentType;
import com.opengamma.gammatrace.marketconstruction.sabrconfiguration.EURSABRConfiguration;
import com.opengamma.gammatrace.marketconstruction.sabrconfiguration.GBPSABRConfiguration;
import com.opengamma.gammatrace.marketconstruction.sabrconfiguration.JPYSABRConfiguration;
import com.opengamma.gammatrace.marketconstruction.sabrconfiguration.SABRConfiguration;
import com.opengamma.gammatrace.marketconstruction.sabrconfiguration.USDSABRConfiguration;
import com.opengamma.util.money.Currency;

public class SABRMarketDataProvider {

	private SABRConfiguration configuration;
	private ServiceApi serviceApi = ServiceApiImpl.getInstance();
	SABRNodeQuote[] sabrNodeQuotes; 
	private Map<String, Double> requestedPremiumValues = new HashMap<>();
	private Map<String, Double> requestedParameterValues = new HashMap<>();
	
	public SABRMarketDataProvider(Currency Ccy, ZonedDateTime curveTime) {
		configuration = setConfiguration(Ccy);
		sabrNodeQuotes = configuration.getVolQuotes();

		requestedPremiumValues.putAll(serviceApi.getQuotes(Arrays.asList(SABRNodeQuote.getStraddleATMPremiumTickersFromArray(sabrNodeQuotes)), curveTime.toEpochSecond()));
		requestedParameterValues.put("beta", 0.4);	// TODO: set up with real values, stored in our database
		requestedParameterValues.put("rho", 0.0);
		requestedParameterValues.put("nu", 0.1);
	}
	
	public Map<String, Double> getRequestedPremiumValues() { 
		return requestedPremiumValues;
	}
	
	public SABRNodeQuote[] getSABRNodeQuotes() {
		return configuration.getVolQuotes();
	}
	
	public SABRFormulaData[] getSABRFormulaDataWithoutAlpha() {
		SABRFormulaData[] sabrFormulaData = new SABRFormulaData[sabrNodeQuotes.length];
		for (int i = 0; i < sabrNodeQuotes.length; i++) {
			sabrFormulaData[i] = new SABRFormulaData(0.1, requestedParameterValues.get(sabrNodeQuotes[i].getBetaTicker()), requestedParameterValues.get(sabrNodeQuotes[i].getRhoTicker()), requestedParameterValues.get(sabrNodeQuotes[i].getNuTicker())); 
		}
		return sabrFormulaData;
	}
	
	public GeneratorSwapFixedIbor getUnderlyingGenerator() {
		return configuration.getUnderlyingGenerator();
	}
	
	public SwaptionQuoteInstrumentType getQuoteInstrumentType() {
		return configuration.getQuoteInstrumentType();
	}
	
	private SABRConfiguration setConfiguration(Currency Ccy) {
		if (Ccy.equals(Currency.USD)) {
			return new USDSABRConfiguration();
		} else if (Ccy.equals(Currency.EUR)) {
			return new EURSABRConfiguration();
		} else if (Ccy.equals(Currency.GBP)) {
			return new GBPSABRConfiguration();
		} else if (Ccy.equals(Currency.JPY)) {
			return new JPYSABRConfiguration();
		}
		System.out.println("Don't have this SABR configuration");
		return null; 
	}
}
