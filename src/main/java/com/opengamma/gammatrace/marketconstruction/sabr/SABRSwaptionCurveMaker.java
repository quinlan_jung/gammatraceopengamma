package com.opengamma.gammatrace.marketconstruction.sabr;

import java.util.Map;

import org.apache.log4j.Logger;
import org.threeten.bp.Period;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.logging.CustomLogger;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeIR;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swaption.SwaptionCashFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swaption.SwaptionPhysicalFixedIborDefinition;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Coupon;
import com.opengamma.analytics.financial.interestrate.swap.derivative.SwapFixedCoupon;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionCashFixedIbor;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.analytics.financial.model.option.definition.SABRInterestRateParameters;
import com.opengamma.analytics.financial.model.option.pricing.analytic.formula.EuropeanVanillaOption;
import com.opengamma.analytics.financial.model.volatility.smile.fitting.SABRATMVolatilityCalculator;
import com.opengamma.analytics.financial.model.volatility.smile.function.SABRFormulaData;
import com.opengamma.analytics.financial.model.volatility.smile.function.SABRHaganVolatilityFunction;
import com.opengamma.analytics.financial.model.volatility.smile.function.VolatilityFunctionProvider;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteDiscountingCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.SABRSwaptionProviderDiscount;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.analytics.math.interpolation.CombinedInterpolatorExtrapolatorFactory;
import com.opengamma.analytics.math.interpolation.GridInterpolator2D;
import com.opengamma.analytics.math.interpolation.Interpolator1D;
import com.opengamma.analytics.math.interpolation.Interpolator1DFactory;
import com.opengamma.analytics.math.surface.InterpolatedDoublesSurface;
import com.opengamma.analytics.util.time.TimeCalculator;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.exceptions.UnsupportedTradeException;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.marketconstruction.quote.SABRNodeQuote;
import com.opengamma.gammatrace.marketconstruction.quote.SwaptionQuoteInstrumentType;
import com.opengamma.gammatrace.rates.calculator.BlackImpliedVolatilityCalculator;
import com.opengamma.gammatrace.rates.calculator.DeltaDisplayBundle;
import com.opengamma.gammatrace.rates.instrument.FixingBuilder;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;
// this class is built to only handle cash or physically settled atm swaption straddles.
public class SABRSwaptionCurveMaker {
	static Logger logger = CustomLogger.getLogger(SABRSwaptionCurveMaker.class);
	private final static VolatilityFunctionProvider<SABRFormulaData> SABR = new SABRHaganVolatilityFunction();
	private final static ParSpreadMarketQuoteDiscountingCalculator PSMQC = ParSpreadMarketQuoteDiscountingCalculator.getInstance();
	private Calendar calendar = new MondayToFridayCalendar("NYC");
	private BusinessDayConvention busDays = BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following");
	
	private ZonedDateTime curveTime;
	private Currency currency;
	private SABRMarketDataProvider volProvider;
	private MulticurveProviderDiscount multicurves;
	private CurveBuildingBlockBundle curveBuildingBlock;
	private DeltaDisplayBundle deltaBundle;
	private GeneratorSwapFixedIbor underlyingGenerator;
	private SwaptionQuoteInstrumentType quoteInstrumentType;

	public SABRSwaptionCurveMaker(Currency ccy, ZonedDateTime curveTime) throws UnsupportedTradeException {
		this.curveTime = curveTime;
		this.currency = ccy;
		CurveMaker cm = new CurveMaker(ccy, curveTime);
		Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> curvePair = cm.makeCurve(); 
		multicurves = curvePair.getFirst();
		curveBuildingBlock = curvePair.getSecond();
		deltaBundle = cm.getDeltaDisplayBundle();
		volProvider = new SABRMarketDataProvider(ccy, curveTime);
		underlyingGenerator = volProvider.getUnderlyingGenerator();
		quoteInstrumentType = volProvider.getQuoteInstrumentType();
	}
	
	public SABRSwaptionProviderDiscount makeSABRSwaptionCurve() {
		// get required info
		SABRNodeQuote[] sabrNodeQuotes = volProvider.getSABRNodeQuotes();
		SABRFormulaData[] sabrFormulaData = volProvider.getSABRFormulaDataWithoutAlpha();
		Map<String, Double> premiumValues = volProvider.getRequestedPremiumValues();
		// set up dates
		ZonedDateTime settlementDate = ScheduleCalculator.getAdjustedDate(curveTime, underlyingGenerator.getSpotLag(), calendar);
		double[] expiryPeriodConverted = convertPeriodToDouble(SABRNodeQuote.getExpiriesFromArray(sabrNodeQuotes), settlementDate);
		double[] underlyingTenorConverted = convertPeriodToDouble(SABRNodeQuote.getUnderlyingTenorFromArray(sabrNodeQuotes), settlementDate);	// SABRSwaptionProviderDiscount seems to be built from expiry vs. tenor, so a 1y2y swaption would have x = 1.0 and y = 2.0 NOT the times from now i.e. x = 1.0 y = 3.0
		// calibrate alpha from current premia
		sabrFormulaData = calibrateSABRAlpha(sabrFormulaData, sabrNodeQuotes, premiumValues);
		SABRInterestRateParameters sabrParameters = makeSABRParameters(expiryPeriodConverted, underlyingTenorConverted, sabrFormulaData);
		return new SABRSwaptionProviderDiscount(multicurves, sabrParameters, underlyingGenerator);
	}
	
	private SABRFormulaData[] calibrateSABRAlpha(SABRFormulaData[] sabrFormulaData, SABRNodeQuote[] quotes, Map<String, Double> premiumValues) {
		ZonedDateTime tradeDate = curveTime.toLocalDate().atStartOfDay(ZoneOffset.UTC);
		for (int i = 0; i < quotes.length; i++) {
			// calculate the forward swap rate
			GeneratorAttributeIR tenor = new GeneratorAttributeIR(quotes[i].getExpiry(), quotes[i].getUnderlyingTenor());		// the spotLag between expiry and underlying effective date is put in the generation part below
			SwapFixedIborDefinition tempDef = underlyingGenerator.generateInstrument(tradeDate, 0, 1, tenor);					// make the definition with zero rate
			SwapFixedCoupon<Coupon> swap = tempDef.toDerivative(tradeDate, FixingBuilder.makeFixingTimeSeries(tempDef, tradeDate));
			double forwardRate = swap.accept(PSMQC, multicurves);																// calculates the SPREAD, but ok bc derivative build with 0 rate.
			// build the underlying swap definitions for payer and receiver
			SwapFixedIborDefinition swapDefinitionPayer = generateSwapInstrument(tradeDate, forwardRate, 1, tenor, underlyingGenerator, true);					// payer swap with rate = forward
			SwapFixedIborDefinition swapDefinitionReceiver = generateSwapInstrument(tradeDate, forwardRate, 1, tenor, underlyingGenerator, false);				// receiver swap with rate = forward
			
			// build the swaption straddle and calculate the implied vol from the straddle premium
			ZonedDateTime expiryDate = ScheduleCalculator.getAdjustedDate(tradeDate, quotes[i].getExpiry(), busDays, calendar);
			EuropeanVanillaOption swaptionPayer; 
			double DF = multicurves.getDiscountFactor(currency, convertPeriodToDouble(quotes[i].getExpiry(), tradeDate));
			double impliedVol;
			if (quoteInstrumentType.equals(SwaptionQuoteInstrumentType.STRADDLE_PHYSICAL)) {
				SwaptionPhysicalFixedIborDefinition swaptionDefinitionPayer = SwaptionPhysicalFixedIborDefinition.from(expiryDate, swapDefinitionPayer, true);
				SwaptionPhysicalFixedIborDefinition swaptionDefinitionReceiver = SwaptionPhysicalFixedIborDefinition.from(expiryDate, swapDefinitionReceiver, true);
				SwaptionPhysicalFixedIbor[] swaptionStraddle = new SwaptionPhysicalFixedIbor[] {swaptionDefinitionPayer.toDerivative(tradeDate), swaptionDefinitionReceiver.toDerivative(tradeDate)};
				impliedVol = BlackImpliedVolatilityCalculator.getImpliedVolatility(swaptionStraddle, premiumValues.get(quotes[i].getStraddleATMPremiumTicker())/10000 * DF, multicurves);		// this methods takes the SPOT premium.
				swaptionPayer = swaptionStraddle[0];
			} else {
				SwaptionCashFixedIborDefinition swaptionDefinitionPayer = SwaptionCashFixedIborDefinition.from(expiryDate, swapDefinitionPayer, true);
				SwaptionCashFixedIborDefinition swaptionDefinitionReceiver = SwaptionCashFixedIborDefinition.from(expiryDate, swapDefinitionReceiver, true);
				SwaptionCashFixedIbor[] swaptionStraddle = new SwaptionCashFixedIbor[] {swaptionDefinitionPayer.toDerivative(tradeDate), swaptionDefinitionReceiver.toDerivative(tradeDate)};
				impliedVol = BlackImpliedVolatilityCalculator.getImpliedVolatility(swaptionStraddle, premiumValues.get(quotes[i].getStraddleATMPremiumTicker())/10000 * DF, multicurves);		// this methods takes the SPOT premium.
				swaptionPayer = swaptionStraddle[0];
			}
			// calibrate alpha to the ATM implied vol
			SABRATMVolatilityCalculator alphaCalculator = new SABRATMVolatilityCalculator(SABR);
			double alpha = alphaCalculator.calculate(sabrFormulaData[i], swaptionPayer, forwardRate, impliedVol);
			sabrFormulaData[i] = sabrFormulaData[i].withAlpha(alpha);
			logger.info(quotes[i].getStraddleATMPremiumTicker()+"  "+premiumValues.get(quotes[i].getStraddleATMPremiumTicker())+"  "+impliedVol+"  "+forwardRate);
		}
		return sabrFormulaData;
	}
	
	private SABRInterestRateParameters makeSABRParameters(double[] expiryPeriodConverted, double[] underlyingTenorConverted, SABRFormulaData[] sabrFormulaData) {
		// interpolator
		final Interpolator1D linearFlat = CombinedInterpolatorExtrapolatorFactory.getInterpolator(Interpolator1DFactory.LINEAR, Interpolator1DFactory.FLAT_EXTRAPOLATOR, Interpolator1DFactory.FLAT_EXTRAPOLATOR);
		final GridInterpolator2D interpolator2D = new GridInterpolator2D(linearFlat, linearFlat);
		// transform the SABRFormulaData into arrays of each parameter
		int numNodes = expiryPeriodConverted.length;
		double[] alphaValues = new double[numNodes];
		double[] betaValues = new double[numNodes];
		double[] rhoValues = new double[numNodes];
		double[] nuValues = new double[numNodes];
		for (int i = 0; i < numNodes; i++) {
			alphaValues[i] = sabrFormulaData[i].getAlpha();
			betaValues[i] = sabrFormulaData[i].getBeta();
			rhoValues[i] = sabrFormulaData[i].getRho();
			nuValues[i] = sabrFormulaData[i].getNu();
		}
		// create interpolated surfaces
		InterpolatedDoublesSurface alphaSurface = InterpolatedDoublesSurface.from(expiryPeriodConverted, underlyingTenorConverted, alphaValues, interpolator2D);
		InterpolatedDoublesSurface betaSurface = InterpolatedDoublesSurface.from(expiryPeriodConverted, underlyingTenorConverted, betaValues, interpolator2D);
		InterpolatedDoublesSurface rhoSurface = InterpolatedDoublesSurface.from(expiryPeriodConverted, underlyingTenorConverted, rhoValues, interpolator2D);
		InterpolatedDoublesSurface nuSurface = InterpolatedDoublesSurface.from(expiryPeriodConverted, underlyingTenorConverted, nuValues, interpolator2D);
		
		return new SABRInterestRateParameters(alphaSurface, betaSurface, rhoSurface, nuSurface, underlyingGenerator.getFixedLegDayCount(), SABR);
	}
	
	private double convertPeriodToDouble(Period period, ZonedDateTime settlementDate) {
		return TimeCalculator.getTimeBetween(settlementDate, ScheduleCalculator.getAdjustedDate(settlementDate, period, busDays, calendar));
	}

	private double[] convertPeriodToDouble(Period[] period, ZonedDateTime settlementDate) {
		double[] interval = new double[period.length];
		for (int i = 0; i < period.length; i++) {
			interval[i] = convertPeriodToDouble(period[i], settlementDate); //TimeCalculator.getTimeBetween(settlementDate, ScheduleCalculator.getAdjustedDate(settlementDate, period[i], busDays, calendar));
		}
		return interval;
	}
	
	public static SwapFixedIborDefinition generateSwapInstrument(ZonedDateTime date, double rate, double notional, GeneratorAttributeIR attribute, GeneratorSwapFixedIbor generator, boolean isFixedPayer) {
		final ZonedDateTime spot = ScheduleCalculator.getAdjustedDate(date, generator.getSpotLag(), generator.getCalendar());
		final ZonedDateTime startDate = ScheduleCalculator.getAdjustedDate(spot, attribute.getStartPeriod(), generator.getIborIndex(), generator.getCalendar());
		return SwapFixedIborDefinition.from(startDate, attribute.getEndPeriod(), generator, notional, rate, isFixedPayer);
	}
	
	public CurveBuildingBlockBundle getCurveBuildingBlockBundle() {
		return curveBuildingBlock;
	}
	
	public DeltaDisplayBundle getDeltaDisplayBundle() {
		return deltaBundle;
	}
	
}
