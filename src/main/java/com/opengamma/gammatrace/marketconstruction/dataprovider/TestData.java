package com.opengamma.gammatrace.marketconstruction.dataprovider;

import java.util.LinkedHashMap;

import org.threeten.bp.Period;
@Deprecated
public class TestData {
	
	
	public final static String [] DSC_POINTS = {"0D","3M","6M","1Y","2Y","3Y","4Y","5Y","10Y","15Y","30Y"};
	public final static String [] FWD3_POINTS_COB = {"0M","1Y","2Y","3Y","4Y","5Y","7Y","10Y","15Y","30Y"};
	public final static String [] FWD3M1M_POINTS_COB = {"0D","6M","1Y","2Y","3Y","5Y","7Y","10Y","15Y","30Y"};
	public final static String [] FWD3M6M_POINTS_COB = {"0D","1Y","2Y","3Y","5Y","7Y","10Y","15Y","30Y"};
	
	// USD
	public final static Double[] DSC_USD_MARKET_QUOTES_COB = new Double[] {0.008, -0.0015, -0.0015, -0.0015,-0.0015, -0.0015, -0.0015, -0.0016,-0.0015, -0.0015, -0.0015 };
	public static final Double[] FWD3_USD_MARKET_QUOTES_COB = new Double[] {0.00235, 0.0036, 0.0082, 0.0130, 0.0165, 0.0193, 0.0230, 0.0263, 0.0280, 0.0317 };
	public static final Double[] FWD3M1M_USD_MARKET_QUOTES_COB = new Double[] {0.0017, -0.0010, -0.0010, -0.0010, -0.00100, -0.00100, -0.00100, -0.00100, -0.00100, -0.00100 };
	public static final Double[] FWD3M6M_USD_MARKET_QUOTES_COB = new Double[] {0.0036, 0.0020, 0.0020, 0.00200, 0.00200, 0.00200, 0.00200, 0.00200, 0.00200 };
	
	//EUR 
	public static final Double[] DSC_EUR_MARKET_QUOTES_COB = new Double[] {0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}; 
	//public static final Double[] DSC_EUR_MARKET_QUOTES_COB = new Double[] {0.0, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002};
	
	public static final Double[] FWD3_EUR_MARKET_QUOTES_COB = new Double[] {0.00235, 0.0036, 0.0082, 0.0130, 0.0165, 0.0193, 0.0230, 0.0263, 0.0280, 0.0317 };
	public static final Double[] FWD3M1M_EUR_MARKET_QUOTES_COB = new Double[] {0.0017, -0.0010, -0.0010, -0.0010, -0.00100, -0.00100, -0.00100, -0.00100, -0.00100, -0.00100 };
	public static final Double[] FWD3M6M_EUR_MARKET_QUOTES_COB = new Double[] {0.0036, 0.0020, 0.0020, 0.00200, 0.00200, 0.00200, 0.00200, 0.00200, 0.00200 };

	// USD Beta
	public final static Double[] DSC_USD_MARKET_QUOTES_LONG = new Double[] {0.01, 0.01, 0.01,0.01, 0.01, 0.01,0.01, 0.01, 0.01,0.01, 0.01, 0.01, 0.01};
	public static final Double[] FWD3_USD_MARKET_QUOTES_LONG = new Double[] {0.00235, 0.0036, 0.0082, 0.0130, 0.0165, 0.0193, 0.0230, 0.0263, 0.0280, 0.0317, 0.0317, 0.0317};
	public static final Double[] FWD3M1M_USD_MARKET_QUOTES_LONG = new Double[] {0.0017, -0.0010, -0.0010, -0.0010, -0.00100, -0.00100, -0.00100, -0.00100, -0.00100  };
	public static final Double[] FWD3M6M_USD_MARKET_QUOTES_LONG = new Double[] {0.0020, 0.0020, 0.00200, 0.00200, 0.00200, 0.00200, 0.00200, 0.00200 };
	
	
	public static LinkedHashMap<String, Double[]> getTestData(LinkedHashMap<String, String[]> tickers) {
		LinkedHashMap<String, Double[]> testData = new LinkedHashMap<>();
		testData.put("USD DSC", DSC_USD_MARKET_QUOTES_COB);
		testData.put("USD fwd 3m", FWD3_USD_MARKET_QUOTES_COB);
		testData.put("USD fwd 1m", FWD3M1M_USD_MARKET_QUOTES_COB);
		testData.put("USD fwd 6m", FWD3M6M_USD_MARKET_QUOTES_COB);
		testData.put("EUR DSC", DSC_EUR_MARKET_QUOTES_COB);
		testData.put("EUR fwd 3m", FWD3_EUR_MARKET_QUOTES_COB);
		testData.put("EUR fwd 1m", FWD3M1M_EUR_MARKET_QUOTES_COB);
		testData.put("EUR fwd 6m", FWD3M6M_EUR_MARKET_QUOTES_COB);
		testData.put("JPY DSC", DSC_EUR_MARKET_QUOTES_COB);
		testData.put("JPY fwd 3m", FWD3_EUR_MARKET_QUOTES_COB);
		testData.put("JPY fwd 1m", FWD3M1M_EUR_MARKET_QUOTES_COB);
		testData.put("JPY fwd 6m", FWD3M6M_EUR_MARKET_QUOTES_COB);
		testData.put("GBP DSC", DSC_EUR_MARKET_QUOTES_COB);
		testData.put("GBP fwd 3m", FWD3_EUR_MARKET_QUOTES_COB);
		testData.put("GBP fwd 6m", FWD3M6M_EUR_MARKET_QUOTES_COB);
		return testData;
	}
	
	public static LinkedHashMap<String, Double[]> getTestDataBeta(LinkedHashMap<String, String[]> tickers) {
		LinkedHashMap<String, Double[]> testData = new LinkedHashMap<>();
		testData.put("USD DSC", DSC_USD_MARKET_QUOTES_LONG);
		testData.put("USD fwd 3m", FWD3_USD_MARKET_QUOTES_LONG);
		testData.put("USD fwd 1m", FWD3M1M_USD_MARKET_QUOTES_LONG);
		testData.put("USD fwd 6m", FWD3M6M_USD_MARKET_QUOTES_LONG);
		return testData;
	}
	

}
