package com.opengamma.gammatrace.marketconstruction.dataprovider;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.instrument.index.GeneratorAttribute;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.util.money.Currency;

/**
 * DO NOT CHANGE RANDOM GENERATOR HERE WITHOUT COMMITTING TO PARSER PACKAGE
 * THIS IS JUST A TEMPORARY IMPORT BEFORE THE PACKAGE MERGE
 * @author teamawesome
 *
 */
public class RandomGenerator {
	private ZonedDateTime curveTime;
	
	public final static String [] DSC_POINTS = {"0D","6M","9M","1Y","2Y","3Y","4Y","5Y","6Y","7Y","8Y","9Y","10Y","12Y","15Y","20Y","25Y","30Y","40Y","50Y","80Y"};
	public final static String [] LIBOR_POINTS = {"0D","7D","14D","1M","2M","3M","6M","12M"};
	
	// Points that were originally hard coded in tests
	public final static String [] DSC_POINTS2 = {"0D","1M","2M","3M","6M","9M","1Y","2Y","3Y","4Y","5Y","10Y"};
	public static final Double[] DSC_USD_MARKET_QUOTES = new Double[] {0.0400, 0.0400, 0.0400, 0.0400, 0.0400, 0.0400, 0.0400, 0.0400, 0.0400, 0.0400, 0.0400, 0.0400 };
	public final static String [] FWD3_POINTS = {"0M","6M","1Y","2Y","3Y","7Y","10Y"};
	public static final Double[] FWD3_USD_MARKET_QUOTES = new Double[] {0.0500, 0.0500, 0.0500, 0.0500, 0.0500, 0.0501, 0.0500, 0.0500 };
	public final static String [] FWD3M6M_POINTS = {"6M","1Y","2Y","3Y","5Y","7Y","10Y"};
	public static final Double[] FWD3M6M_USD_MARKET_QUOTES = new Double[] {0.0020, 0.0020, 0.0020, 0.00200, 0.00190, 0.00200, 0.00200 };
	
	// Realistic points based on COB 29/09
	public final static String [] DSC_POINTS_COB = {"0D","3M","6M","1Y","2Y","3Y","4Y","5Y","10Y","15Y","30Y"};
	public static final Double[] DSC_USD_MARKET_QUOTES_COB = new Double[] {0.008, -0.0015, -0.0015, -0.0015,-0.0015, -0.0015, -0.0015, -0.0015,-0.0015, -0.0015, -0.0015 };
	public static final Double[] DSC_USD_MARKET_QUOTES_BETA = new Double[] {0.008, 0.0020, 0.0067, 0.0115, 0.0150, 0.0178, 0.0215, 0.0248, 0.0265, 0.0300, 0.0300 };
	public static final Double[] DSC_USD_MARKET_QUOTES_COB2 = new Double[] {0.009, -0.0014, -0.0014, -0.0014,-0.0014, -0.0014, -0.0014, -0.0014,-0.0014, -0.0014, -0.0014 };
	public final static String [] FWD3_POINTS_COB = {"0M","1Y","2Y","3Y","4Y","5Y","7Y","10Y","15Y","30Y"};
	public static final Double[] FWD3_USD_MARKET_QUOTES_COB = new Double[] {0.00235, 0.0036, 0.0082, 0.0130, 0.0165, 0.0193, 0.0230, 0.0263, 0.0280, 0.0317 };
	public static final Double[] FWD3_USD_MARKET_QUOTES_COB2 = new Double[] {0.00245, 0.0037, 0.0083, 0.0131, 0.0166, 0.0194, 0.0231, 0.0264, 0.0281, 0.0318 };
	public final static String [] FWD3M1M_POINTS_COB = {"0D","6M","1Y","2Y","3Y","5Y","7Y","10Y","15Y","30Y"};
	public static final Double[] FWD3M1M_USD_MARKET_QUOTES_COB = new Double[] {0.0017, -0.0010, -0.0010, -0.0010, -0.00100, -0.00100, -0.00100, -0.00100, -0.00100, -0.00100 };
	public final static String [] FWD3M6M_POINTS_COB = {"0D","1Y","2Y","3Y","5Y","7Y","10Y","15Y","30Y"};
	public static final Double[] FWD3M6M_USD_MARKET_QUOTES_COB = new Double[] {0.0036, 0.0020, 0.0020, 0.00200, 0.00200, 0.00200, 0.00200, 0.00200, 0.00200 };
	
	public static final Double[] BLAH = new Double[] {0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.000, 0.000, 0.000, 0.000, 0.000 };
	
	public final static double DSC_MAX = 0.05;
	public final static double DSC_MIN = 0.01;
	public final static double LIBOR_MAX = 0.005;
	public final static double LIBOR_MIN = 0.001;
	private final static Random rand = new Random(System.currentTimeMillis());
	
	// constructor
	public RandomGenerator(ZonedDateTime curveTime){
		this.curveTime = curveTime;
	}
	
	public List<Entry<String, Double>> makeSortedList(String [] s, Double [] d){
		List<Entry<String, Double>> l = new ArrayList<Entry<String, Double>> ();
		for (int i = 0; i < s.length; i++){
			Entry<String, Double> e = new AbstractMap.SimpleEntry<String, Double>(s[i], d[i]);
			l.add(e);
		}
		return l;
	}
	
	/**
	 * Returns a pseudo-random number between min and max, inclusive.
	 * The difference between min and max can be at most
	 * <code>Integer.MAX_VALUE - 1</code>.
	 *
	 * @param min Minimum value
	 * @param max Maximum value.  Must be greater than min.
	 * @return Double between min and max, inclusive.
	 * @see java.util.Random#nextDouble(double)
	 */
	public double randDouble(double min, double max) {
	    // nextDouble is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    return min + (Math.abs(max - min) * rand.nextDouble());
	}
	
	public Map <String, Double> getQuotes(String [] points, double min, double max) {
		Map <String, Double> quotes = new HashMap <String, Double> ();
		List <Double> randPoints = new ArrayList<Double>();
		for (int i = 0; i < points.length; i++) {
			randPoints.add(randDouble(min, max));
		}
		Collections.sort(randPoints);
		for (int i = 0; i < points.length; i++) {
			quotes.put(points[i], randPoints.get(i));
		}
		return quotes;
	}
	//TODO: this is all to be replaced....just to get things working 
	public double[] getFxVolQuotes(String[] tickers) {
		return new double[] {0.175, 0.175, 0.18, 0.18, 0.175, 0.16, 0.16, 0.15 };
	}
	public double[][] getFxVolQuotesRR() {
		return new double[][] { {-0.010, -0.0050 }, {-0.010, -0.0050 }, {-0.011, -0.0060 }, {-0.012, -0.0070 }, {-0.013, -0.0080 }, {-0.014, -0.0090 }, {-0.014, -0.0090 } , {-0.014, -0.0090 }};
		
	}
	public double[][] getFxVolQuotesFly() {
		return new double[][] { {0.0300, 0.0100 }, {0.0300, 0.0100 }, {0.0310, 0.0110 }, {0.0320, 0.0120 }, {0.0330, 0.0130 }, {0.0340, 0.0140 }, {0.0340, 0.0140 }, {0.0340, 0.0140 } };
	}
	
	
	//TODO: method to return values from tickers using API call to market database... right now just hard coded with some numbers
	public Double[][] getQuotes(String[][] tickers) {
		Double[][] quotes = new Double[tickers.length][];
		
		// TEMPORARY HACK...
		if (tickers.length == 4){
			quotes[0] = DSC_USD_MARKET_QUOTES_COB;
			quotes[1] = FWD3_USD_MARKET_QUOTES_COB;
			quotes[2] = FWD3M1M_USD_MARKET_QUOTES_COB;
			quotes[3] = FWD3M6M_USD_MARKET_QUOTES_COB;		
		} else {
			quotes[0] = DSC_USD_MARKET_QUOTES_COB;
			quotes[1] = FWD3_USD_MARKET_QUOTES_COB;
			quotes[2] = FWD3M6M_USD_MARKET_QUOTES_COB;
		}
		
		return quotes;
	}
	// this is all temporary junk.. until we get market data sorted out. 
	public LinkedHashMap<String, Double[]> getQuotes(LinkedHashMap<String, String[]> tickerMap) {
		LinkedHashMap<String, Double[]> map = new LinkedHashMap<>();
		String[] tickers = new String[tickerMap.size()];
		int i = 0;
		for (String name: tickerMap.keySet()) {
			tickers[i] = name;
			i++;
		}
		if (tickers.length == 4){
			if (tickerMap.get(tickers[0])[0] == "x") {
				map.put(tickers[0], BLAH);
			} else {
				map.put(tickers[0], DSC_USD_MARKET_QUOTES_COB);
			}
			
			map.put(tickers[1], FWD3_USD_MARKET_QUOTES_COB);
			map.put(tickers[2], FWD3M1M_USD_MARKET_QUOTES_COB);
			map.put(tickers[3], FWD3M6M_USD_MARKET_QUOTES_COB);
		} else {
			map.put(tickers[0], DSC_USD_MARKET_QUOTES_COB);
			map.put(tickers[1], FWD3_USD_MARKET_QUOTES_COB);
			map.put(tickers[2], FWD3M6M_USD_MARKET_QUOTES_COB);
		}
		return map;
	}
	
	public LinkedHashMap<String, Double[]> getQuotesNew(LinkedHashMap<String, String[]> tickerMap) {
		LinkedHashMap<String, Double[]> map = new LinkedHashMap<>();
		String[] curves = new String[tickerMap.size()];
		int i = 0;
		for (String name: tickerMap.keySet()) {
			curves[i] = name;
			i++;
		}
		if (curves.length == 4) {
			if (tickerMap.get(curves[0])[0] == "x") {
				map.put(curves[0], fillValues(tickerMap.get(curves[0]), BLAH));
			} else {
				map.put(curves[0], fillValues(tickerMap.get(curves[0]), DSC_USD_MARKET_QUOTES_BETA));
			}
			map.put(curves[1], fillValues(tickerMap.get(curves[1]),FWD3_USD_MARKET_QUOTES_COB));
			map.put(curves[2], fillValues(tickerMap.get(curves[2]),FWD3M1M_USD_MARKET_QUOTES_COB));
			map.put(curves[3], fillValues(tickerMap.get(curves[3]),FWD3M6M_USD_MARKET_QUOTES_COB));
		} else {
			map.put(curves[0], fillValues(tickerMap.get(curves[0]), DSC_USD_MARKET_QUOTES_BETA));
			map.put(curves[1], fillValues(tickerMap.get(curves[1]),FWD3_USD_MARKET_QUOTES_COB));
			map.put(curves[2], fillValues(tickerMap.get(curves[2]),FWD3M6M_USD_MARKET_QUOTES_COB));
		}
		return map;
	}
	
	private Double[] fillValues(String[] tickers, Double[] inputValues) {
		Double[] values = new Double[tickers.length];
		for (int element = 0; element < tickers.length; element++) {
			if (element < inputValues.length) {
				values[element] = inputValues[element];
			} else {
				values[element] = inputValues[inputValues.length-1];
			}
		}
		return values;
	}
	
	public <K,V extends Comparable<? super V>> List<Entry<K, V>> entriesSortedByValues(Map<K,V> map) {
        List<Entry<K,V>> sortedEntries = new ArrayList<Entry<K,V>>(map.entrySet());

        Collections.sort(sortedEntries, new Comparator<Entry<K,V>>() {
        	@Override
        	public int compare(Entry<K,V> e1, Entry<K,V> e2) {
        		return e1.getValue().compareTo(e2.getValue());
        	}
        });

        return sortedEntries;
	}
	
	public void printQuotes (Map<String, Double> quotes) {
		for (Entry<String, Double> entry : entriesSortedByValues(quotes)) {
			System.out.println(String.format("%s %6.5f", entry.getKey(), entry.getValue()));
		}
	}
	
	public void testRun () {
		System.out.println("Printing DSC points");
		Map <String, Double> DSC_QUOTES = getQuotes(DSC_POINTS, DSC_MIN, DSC_MAX);
		printQuotes(DSC_QUOTES);
		
		System.out.println("\n==========================\n");
		
		System.out.println("Printing LIBOR points");
		Map <String, Double> LIBOR_QUOTES = getQuotes(LIBOR_POINTS, LIBOR_MIN, LIBOR_MAX);
		printQuotes(LIBOR_QUOTES);
	}
	
	public static void main(String[] args) {
		//RandomGenerator rg = new RandomGenerator ();
		//rg.testRun();
	}

}
