package com.opengamma.gammatrace.marketconstruction.curve;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.threeten.bp.Period;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.curve.interestrate.generator.GeneratorCurveYieldInterpolated;
import com.opengamma.analytics.financial.curve.interestrate.generator.GeneratorYDCurve;
import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.instrument.InstrumentDefinition;
import com.opengamma.analytics.financial.instrument.cash.CashDefinition;
import com.opengamma.analytics.financial.instrument.fra.ForwardRateAgreementDefinition;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.analytics.financial.instrument.payment.CouponIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedONDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapIborIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapIborONDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapXCcyIborIborDefinition;
import com.opengamma.analytics.financial.interestrate.InstrumentDerivative;
import com.opengamma.analytics.financial.interestrate.InstrumentDerivativeVisitor;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.generic.LastTimeCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.curve.MultiCurveBundle;
import com.opengamma.analytics.financial.provider.curve.SingleCurveBundle;
import com.opengamma.analytics.financial.provider.curve.multicurve.MulticurveDiscountBuildingRepository;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderInterface;
import com.opengamma.analytics.financial.provider.sensitivity.multicurve.MulticurveSensitivity;
import com.opengamma.analytics.math.interpolation.CombinedInterpolatorExtrapolatorFactory;
import com.opengamma.analytics.math.interpolation.Interpolator1D;
import com.opengamma.analytics.math.interpolation.Interpolator1DFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.exceptions.UnsupportedTradeException;
import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.gammatrace.marketconstruction.quote.SwapQuote;
import com.opengamma.gammatrace.rates.calculator.DeltaDisplayBundle;
import com.opengamma.gammatrace.rates.instrument.FixingBuilder;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class CurveMaker {
	
	private final Currency[] ccys;
	private final ZonedDateTime curveTime;
	private final CurveMarketDataProvider CMDP;
	//numerical parameters
	private static final double TOLERANCE_ROOT = 1.0E-10;
	private static final int STEP_MAX = 100;
	private static final MulticurveDiscountBuildingRepository CURVE_BUILDING_REPOSITORY = new MulticurveDiscountBuildingRepository(TOLERANCE_ROOT, TOLERANCE_ROOT, STEP_MAX);
	//Interpolaters and calculators
    private static final ParSpreadMarketQuoteDiscountingCalculator PSMQC = ParSpreadMarketQuoteDiscountingCalculator.getInstance();
	private static final ParSpreadMarketQuoteCurveSensitivityDiscountingCalculator PSMQCSC = ParSpreadMarketQuoteCurveSensitivityDiscountingCalculator.getInstance();
    
	public CurveMaker(Currency ccy, ZonedDateTime curveTime) throws UnsupportedTradeException {
		ccys = new Currency[1];
		ccys[0] = ccy;
		this.curveTime = curveTime;
		CMDP = new SingleCurrencyCurveMarketDataProvider(ccy, curveTime);
	}
	
	// this is just for debugging / testing
	public CurveMaker(Currency ccy, ZonedDateTime curveTime, Map<String, Double> data) throws UnsupportedTradeException {
		ccys = new Currency[1];
		ccys[0] = ccy;
		this.curveTime = curveTime;
		CMDP = new SingleCurrencyCurveMarketDataProvider(ccy, curveTime, data);
	}
	
	public CurveMaker(Currency firstCcy, Currency secondCcy, ZonedDateTime curveTime) {
		ccys = new Currency[2];
		ccys[0] = firstCcy;
		ccys[1] = secondCcy;
		this.curveTime = curveTime;
		CMDP = new MultiCurrencyCurveMarketDataProvider(firstCcy, secondCcy, curveTime);
	}
	
	public Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> makeCurve() {
		//final Calendar calendar = setCalendar(ccys[0], ccys[1]);
		
		final int numUnits = CMDP.getNumUnits();
		
		final InstrumentDefinition<?>[][][] definitions = CMDP.getDefinitions();
		final String[][] curveNames = CMDP.getNames();
		final FXMatrix fx = CMDP.getFXMatrix();
		final GeneratorYDCurve[][] generators = new GeneratorYDCurve[numUnits][];
		
		final Interpolator1D interpolatorLinear = CombinedInterpolatorExtrapolatorFactory.getInterpolator(Interpolator1DFactory.LINEAR, Interpolator1DFactory.FLAT_EXTRAPOLATOR, Interpolator1DFactory.FLAT_EXTRAPOLATOR);
		final LastTimeCalculator maturityCalculator = LastTimeCalculator.getInstance();
		final GeneratorYDCurve curveInterpolator = new GeneratorCurveYieldInterpolated(maturityCalculator, interpolatorLinear);
		
		for (int i = 0; i < numUnits; i++) {
			generators[i] = new GeneratorYDCurve[curveNames[i].length];
			for (int j = 0; j < curveNames[i].length; j++) {
				generators[i][j] = curveInterpolator;
			}
		}
		
		final MulticurveProviderDiscount knownData = new MulticurveProviderDiscount(fx);
		
		final LinkedHashMap<String, Currency> discountMap = CMDP.getDiscountMap();
		final LinkedHashMap<String, IndexON[]> indexONMap = CMDP.getIndexONMap();
		final LinkedHashMap<String, IborIndex[]> iborIndexMap = CMDP.getIborIndexMap();
		
		return makeCurvesFromDefinitions(definitions, generators, curveNames, knownData, PSMQC, PSMQCSC, curveTime, discountMap, indexONMap, iborIndexMap);
	}
	@Deprecated
	public Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> makeCurve(LinkedHashMap<String, Double> bumpMap) {
		//final Calendar calendar = setCalendar(ccys[0], ccys[1]);
		
		final int numUnits = CMDP.getNumUnits();
		
		final InstrumentDefinition<?>[][][] definitions = CMDP.getDefinitions();
		final String[][] curveNames = CMDP.getNames();
		final FXMatrix fx = CMDP.getFXMatrix();
		final GeneratorYDCurve[][] generators = new GeneratorYDCurve[numUnits][];
		
		final Interpolator1D interpolatorLinear = CombinedInterpolatorExtrapolatorFactory.getInterpolator(Interpolator1DFactory.LINEAR, Interpolator1DFactory.FLAT_EXTRAPOLATOR, Interpolator1DFactory.FLAT_EXTRAPOLATOR);
		final LastTimeCalculator maturityCalculator = LastTimeCalculator.getInstance();
		final GeneratorYDCurve curveInterpolator = new GeneratorCurveYieldInterpolated(maturityCalculator, interpolatorLinear);
		
		for (int i = 0; i < numUnits; i++) {
			generators[i] = new GeneratorYDCurve[curveNames[i].length];
			for (int j = 0; j < curveNames[i].length; j++) {
				generators[i][j] = curveInterpolator;
			}
		}
		
		final MulticurveProviderDiscount knownData = new MulticurveProviderDiscount(fx);
		
		final LinkedHashMap<String, Currency> discountMap = CMDP.getDiscountMap();
		final LinkedHashMap<String, IndexON[]> indexONMap = CMDP.getIndexONMap();
		final LinkedHashMap<String, IborIndex[]> iborIndexMap = CMDP.getIborIndexMap();
		
		return makeCurvesFromDefinitions(definitions, generators, curveNames, knownData, PSMQC, PSMQCSC, curveTime, discountMap, indexONMap, iborIndexMap);
	}
	
	
	@SuppressWarnings("unchecked")
	private static Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> makeCurvesFromDefinitions(final InstrumentDefinition<?>[][][] definitions, final GeneratorYDCurve[][] curveGenerators,
                                                                                                        final String[][] curveNames, final MulticurveProviderDiscount knownData, final InstrumentDerivativeVisitor<MulticurveProviderInterface, Double> calculator,
                                                                                                        final InstrumentDerivativeVisitor<MulticurveProviderInterface, MulticurveSensitivity> sensitivityCalculator, final ZonedDateTime curveTime,
                                                                                                        final LinkedHashMap<String, Currency> discMap, final LinkedHashMap<String, IndexON[]> onMap, final LinkedHashMap<String, IborIndex[]> iborMap) {
		
		final int nUnits = definitions.length;
	    final MultiCurveBundle<GeneratorYDCurve>[] curveBundles = new MultiCurveBundle[nUnits];
	    
	    for (int i = 0; i < nUnits; i++) {
	    	final int nCurves = definitions[i].length;
	    	final SingleCurveBundle<GeneratorYDCurve>[] singleCurves = new SingleCurveBundle[nCurves];
	    	for (int j = 0; j < nCurves; j++) {
	    		final int nInstruments = definitions[i][j].length;
	    		final InstrumentDerivative[] derivatives = new InstrumentDerivative[nInstruments];
	    		final double[] rates = new double[nInstruments];
                for (int k = 0; k < nInstruments; k++) {
                	derivatives[k] = convert(definitions[i][j][k], curveTime);
                    rates[k] = initialGuess(definitions[i][j][k]);
                }
	    		final GeneratorYDCurve generator = curveGenerators[i][j].finalGenerator(derivatives);
	    		final double[] initialGuess = generator.initialGuess(rates);
	    		singleCurves[j] = new SingleCurveBundle<>(curveNames[i][j], derivatives, initialGuess, generator);
	    	}
	    	curveBundles[i] = new MultiCurveBundle<>(singleCurves);
	    }
	    return CURVE_BUILDING_REPOSITORY.makeCurvesFromDerivatives(curveBundles, knownData, discMap, iborMap, onMap, calculator, sensitivityCalculator);
	}
	
	private static InstrumentDerivative convert(final InstrumentDefinition<?> instrument, final ZonedDateTime curveTime) {
		InstrumentDerivative ird;
	    if (instrument instanceof SwapFixedONDefinition) {
	    	ird = ((SwapFixedONDefinition) instrument).toDerivative(curveTime, FixingBuilder.makeFixingTimeSeries((SwapFixedONDefinition) instrument, curveTime));
	    } else if (instrument instanceof SwapFixedIborDefinition) {
	    	ird = ((SwapFixedIborDefinition) instrument).toDerivative(curveTime, FixingBuilder.makeFixingTimeSeries((SwapFixedIborDefinition) instrument, curveTime));
	    } else if (instrument instanceof SwapIborIborDefinition) {
	    	ird = ((SwapIborIborDefinition) instrument).toDerivative(curveTime, FixingBuilder.makeFixingTimeSeries((SwapIborIborDefinition) instrument, curveTime));
	    } else if (instrument instanceof SwapIborONDefinition) {
	    	ird = ((SwapIborONDefinition) instrument).toDerivative(curveTime, FixingBuilder.makeFixingTimeSeries((SwapIborONDefinition) instrument, curveTime));
	    } else if (instrument instanceof SwapXCcyIborIborDefinition ) {
	    	ird = ((SwapXCcyIborIborDefinition) instrument).toDerivative(curveTime, FixingBuilder.makeFixingTimeSeries((SwapXCcyIborIborDefinition) instrument, curveTime));
	    } else {
	    	ird = instrument.toDerivative(curveTime);
	    }
	    return ird;
	}
	
	private static double initialGuess(final InstrumentDefinition<?> instrument) {
	    if (instrument instanceof SwapFixedONDefinition) {
	    	return ((SwapFixedONDefinition) instrument).getFixedLeg().getNthPayment(0).getRate();
	    }
	    if (instrument instanceof SwapFixedIborDefinition) {
	    	return ((SwapFixedIborDefinition) instrument).getFixedLeg().getNthPayment(0).getRate();
	    }
	    if (instrument instanceof ForwardRateAgreementDefinition) {
	    	return ((ForwardRateAgreementDefinition) instrument).getRate();
	    }
	    if (instrument instanceof SwapIborIborDefinition) {
	    	return 0.002;
	    }
	    if (instrument instanceof CashDefinition) {
	    	return ((CashDefinition) instrument).getRate();
	    }
        return 0.01;
    }

	public ZonedDateTime getCurveTime() {
		return curveTime;
	}
	
	public DeltaDisplayBundle getDeltaDisplayBundle() {
		List<String> nameList = new ArrayList<>();
		String[][] curveNamesByUnits = CMDP.getNames();
		for (String[] eachUnitNames : curveNamesByUnits) {
			nameList.addAll(Arrays.asList(eachUnitNames));
		}
		String[] curveNames = nameList.toArray(new String[nameList.size()]);
		
		LinkedHashMap<String, SwapQuote[]> quoteMap = CMDP.getSwapQuotes();
		LinkedHashMap<String, Period[]> instrumentTenors = new LinkedHashMap<>(); 
		LinkedHashMap<String, QuoteInstrumentType[]> quoteInstrumentTypes = new LinkedHashMap<>();
		
		for (Map.Entry<String, SwapQuote[]> entry : quoteMap.entrySet()) {
			Period[] tenors = new Period[entry.getValue().length];
			QuoteInstrumentType[] descriptions = new QuoteInstrumentType[entry.getValue().length];
			int counter = 0;
			for (SwapQuote quote : entry.getValue()) {
				tenors[counter] = quote.getPeriod();
				descriptions[counter] = quote.getDescription();
				counter++;
			}
			instrumentTenors.put(entry.getKey(), tenors);
			quoteInstrumentTypes.put(entry.getKey(), descriptions);
		}
		return new DeltaDisplayBundle(curveNames, CMDP.getCurveRelations(), instrumentTenors, quoteInstrumentTypes);
	}

	private Calendar setCalendar(Currency firstCcy, Currency secondCcy) {
		Calendar calendar;
		if (firstCcy == Currency.USD) {
			calendar = new MondayToFridayCalendar("NYC");
		} else {
			calendar = new MondayToFridayCalendar("NYC");
		}
		return calendar;
	}
	
}
