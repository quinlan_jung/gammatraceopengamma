package com.opengamma.gammatrace.marketconstruction.curve;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.threeten.bp.Period;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.sungard.api.ServiceApi;
import com.gammatrace.sungard.api.ServiceApiImpl;
import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.instrument.InstrumentDefinition;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttribute;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeFX;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeIR;
import com.opengamma.analytics.financial.instrument.index.GeneratorForexSwap;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.gammatrace.marketconstruction.curveconfiguration.CurveConfiguration;
import com.opengamma.gammatrace.marketconstruction.curveconfiguration.EURMultiCcyConfiguration;
import com.opengamma.gammatrace.marketconstruction.curveconfiguration.GBPMultiCcyConfiguration;
import com.opengamma.gammatrace.marketconstruction.curveconfiguration.JPYMultiCcyConfiguration;
import com.opengamma.gammatrace.marketconstruction.curveconfiguration.USDConfiguration;
import com.opengamma.gammatrace.marketconstruction.quote.SwapQuote;
import com.opengamma.gammatrace.utils.CurveMakerUtils;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class MultiCurrencyCurveMarketDataProvider implements CurveMarketDataProvider {
	private ServiceApi serviceApi = ServiceApiImpl.getInstance();
	private ZonedDateTime curveTime;
	private static final double NOTIONAL = 1.0;
	
	private int numUnits;
	private String[][] curveNamesByUnits;
	private CurveConfiguration[] configuration;
	private FXMatrix fx;
	private LinkedHashMap<String, SwapQuote[]> quoteMap = new LinkedHashMap<>();
	private Map<String, Double> requestedValues;
	
	
	private LinkedHashMap<String, Currency> discountMap = new LinkedHashMap<>();
	private LinkedHashMap<String, IndexON[]> indexONMap = new LinkedHashMap<>();
	private LinkedHashMap<String, IborIndex[]> iborIndexMap = new LinkedHashMap<>();
	
	
	
	public MultiCurrencyCurveMarketDataProvider(Currency firstCcy, Currency secondCcy, ZonedDateTime executionTimestamp) {
		this.curveTime = executionTimestamp;
		numUnits = (firstCcy != Currency.USD && secondCcy != Currency.USD)? 3 : 2;
		configuration = new CurveConfiguration[numUnits];
		// if non-USD trade, need USD as the reference currency (in case data for the cross doesn't exist.... normally the case)
		if (numUnits == 3) {
			configuration[0] = new USDConfiguration();
			configuration[1] = setConfiguration(firstCcy);
			configuration[2] = setConfiguration(secondCcy);
		} else {
			configuration[0] = new USDConfiguration();					// need USD as first ccy
			configuration[1] = (firstCcy == Currency.USD)? setConfiguration(secondCcy) : setConfiguration(firstCcy);
		}
		// create an array of arrays holding curve names for each unit
		curveNamesByUnits = new String[numUnits][];								// this equates units to individual configurations i.e. currencies
		for (int i = 0; i < numUnits; i++) {
			curveNamesByUnits[i] = configuration[i].getNames();
		}
		// fill in the maps by looping over each configuration
		for (CurveConfiguration eachConfig : configuration) {
			quoteMap.putAll(eachConfig.getSwapQuotes());	
			discountMap.putAll(eachConfig.getDiscountMap());
			indexONMap.putAll(eachConfig.getIndexONMap());
			iborIndexMap.putAll(eachConfig.getIborIndexMap());
		}
		// get the data from Sungard, update the quotes
		Map<String, Double> requestedValues = CurveMakerUtils.requestAllQuotes(getAllFxTickers(), quoteMap, serviceApi, curveTime.toEpochSecond());
		CurveMakerUtils.updateQuoteMap(requestedValues, quoteMap);
		CurveMakerUtils.printResults(quoteMap);
		//System.out.println(requestedValues.toString());

		// build the fx matrix
		if (numUnits == 3) {
			fx = new FXMatrix(Currency.USD);
			fx.addCurrency(configuration[1].getFxOrder().getFirst(), configuration[1].getFxOrder().getSecond(), requestedValues.get(configuration[1].getFxTicker()));
			fx.addCurrency(configuration[2].getFxOrder().getFirst(), configuration[2].getFxOrder().getSecond(), requestedValues.get(configuration[2].getFxTicker()));
		} else {
			fx = new FXMatrix(configuration[1].getFxOrder().getFirst(), configuration[1].getFxOrder().getSecond(), requestedValues.get(configuration[1].getFxTicker()));
		}
		
	}
	
	private List<String> getAllFxTickers(){
		List<String> tickers = new ArrayList<>();
		for (int i = 1; i < configuration.length; i++) { // Dont include USD configuration at index 0
			tickers.add(configuration[i].getFxTicker());
		}
		return tickers;
	}
	
	public InstrumentDefinition<?>[][][] getDefinitions() {
		InstrumentDefinition<?>[][][] definitions = new InstrumentDefinition<?>[numUnits][][];
		int unitLoop = 0; 
		for (String[] eachUnit : curveNamesByUnits) { // for each unit
			definitions[unitLoop] = new InstrumentDefinition<?>[eachUnit.length][];
			int curveLoop = 0;
			for (String eachCurve : eachUnit) {
				definitions[unitLoop][curveLoop] = makeDefinitions(quoteMap.get(eachCurve));
				curveLoop++;
			}
			unitLoop++;
		}
		return definitions;
	}

	private InstrumentDefinition<?>[] makeDefinitions(final SwapQuote[] quotes) {
		final InstrumentDefinition<?>[] definitions = new InstrumentDefinition<?>[quotes.length];
		for (int loopmv = 0; loopmv < quotes.length; loopmv++) {
			definitions[loopmv] = quotes[loopmv].getGenerator().generateInstrument(curveTime.toLocalDate().atStartOfDay(ZoneOffset.UTC), quotes[loopmv].getValue(), NOTIONAL, makeTenorAttribute(quotes[loopmv]));
		}
		return definitions;
	}
	
	private LinkedHashMap<String, GeneratorAttribute[]> makeTenorAttributes(final LinkedHashMap<String, Period[]> tenor, LinkedHashMap<String, GeneratorInstrument<?>[]> generatorsMap){
		LinkedHashMap<String, GeneratorAttribute[]> map = new LinkedHashMap<>();
		for(Map.Entry<String, Period[]> eachCurve: tenor.entrySet()){
			Period[] periods = eachCurve.getValue();
			GeneratorInstrument<?>[] generators = generatorsMap.get(eachCurve.getKey());	// get the associated generators   
			GeneratorAttribute[] tenorAttribute = new GeneratorAttribute[periods.length];
			for (int loopElements = 0; loopElements < eachCurve.getValue().length; loopElements++){
				if (generators[loopElements] instanceof GeneratorSwapXCcyIborIbor || generators[loopElements] instanceof GeneratorForexSwap) {
					tenorAttribute[loopElements] = new GeneratorAttributeFX(periods[loopElements], fx);
				} else {
					tenorAttribute[loopElements] = new GeneratorAttributeIR(periods[loopElements]);
				}
			}
			map.put(eachCurve.getKey(), tenorAttribute);
		}
		return map;
	}
	
	private GeneratorAttribute makeTenorAttribute (SwapQuote swapQuote) {
		if (swapQuote.getGenerator() instanceof GeneratorSwapXCcyIborIbor || swapQuote.getGenerator() instanceof GeneratorForexSwap) {
			return new GeneratorAttributeFX(swapQuote.getPeriod(), fx);
		} else {
			return new GeneratorAttributeIR(swapQuote.getPeriod());
		}
	}
	
	public LinkedHashMap<String, SwapQuote[]> getSwapQuotes() {
		return quoteMap;
	}
	
	public FXMatrix getFXMatrix() {
		return fx;
	}
	
	public int getNumUnits() {
		return numUnits;
	}
	
	public String[][] getNames() {
		return curveNamesByUnits;
	}
	
	public LinkedHashMap<String, Currency> getDiscountMap() {
		return discountMap;
	}
	
	public LinkedHashMap<String, IndexON[]> getIndexONMap() {
		return indexONMap;
	}
	
	public LinkedHashMap<String, IborIndex[]> getIborIndexMap() {
		return iborIndexMap;
	}
	
	public HashMap<String, Pair<Currency, String>> getCurveRelations() {
		HashMap<String, Pair<Currency, String>> curveRelations = new HashMap<>();
		for (CurveConfiguration eachConfig : configuration) {
			curveRelations.putAll(eachConfig.getCurveRelations());
		}
		return curveRelations;
	}
	
	private static CurveConfiguration setConfiguration(final Currency ccy) {
		if(ccy == Currency.USD ){
			return new USDConfiguration();	// basis defined against USD, so no MultiCcy configuration
		} else if (ccy == Currency.EUR) {
			return new EURMultiCcyConfiguration();
		} else if (ccy == Currency.GBP) {
			return new GBPMultiCcyConfiguration();
		} else if (ccy == Currency.JPY) {
			return new JPYMultiCcyConfiguration();
		} else if (ccy == Currency.AUD) {
			return null;
		} else {
			return null;
		}
	}
	
}
