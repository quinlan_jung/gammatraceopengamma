package com.opengamma.gammatrace.marketconstruction.curve;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.threeten.bp.Period;

import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.instrument.InstrumentDefinition;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.gammatrace.marketconstruction.quote.SwapQuote;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public interface CurveMarketDataProvider {
	
	public InstrumentDefinition<?>[][][] getDefinitions();
	
	public FXMatrix getFXMatrix();
	
	public int getNumUnits();
	
	public String[][] getNames();
	
	public LinkedHashMap<String, SwapQuote[]> getSwapQuotes();
	
	public LinkedHashMap<String, Currency> getDiscountMap();
	
	public LinkedHashMap<String, IndexON[]> getIndexONMap();
	
	public LinkedHashMap<String, IborIndex[]> getIborIndexMap();
	
	public HashMap<String, Pair<Currency, String>> getCurveRelations();
}
