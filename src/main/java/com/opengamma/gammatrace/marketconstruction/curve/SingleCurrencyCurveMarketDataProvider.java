package com.opengamma.gammatrace.marketconstruction.curve;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.threeten.bp.Period;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.sungard.api.ServiceApi;
import com.gammatrace.sungard.api.ServiceApiImpl;
import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.instrument.InstrumentDefinition;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttribute;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeIR;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.gammatrace.exceptions.UnsupportedTradeException;
import com.opengamma.gammatrace.marketconstruction.curveconfiguration.CurveConfiguration;
import com.opengamma.gammatrace.marketconstruction.curveconfiguration.EURConfiguration;
import com.opengamma.gammatrace.marketconstruction.curveconfiguration.GBPConfiguration;
import com.opengamma.gammatrace.marketconstruction.curveconfiguration.JPYConfiguration;
import com.opengamma.gammatrace.marketconstruction.curveconfiguration.USDConfiguration;
import com.opengamma.gammatrace.marketconstruction.quote.SwapQuote;
import com.opengamma.gammatrace.utils.CurveMakerUtils;
import com.opengamma.util.money.Currency;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;

public class SingleCurrencyCurveMarketDataProvider implements CurveMarketDataProvider {
	private ZonedDateTime curveTime;
	private static final double NOTIONAL = 1.0;
	private String[] curveNames;
	private CurveConfiguration configuration; 
	private FXMatrix fx;
	
	private LinkedHashMap<String, SwapQuote[]> quoteMap = new LinkedHashMap<>();
	
	private ServiceApi serviceApi = ServiceApiImpl.getInstance();

	
	public SingleCurrencyCurveMarketDataProvider(Currency ccy, ZonedDateTime curveTime) throws UnsupportedTradeException {
		configuration = setConfiguration(ccy); 
		this.curveTime = curveTime;
		curveNames = configuration.getNames();
		quoteMap = configuration.getSwapQuotes();
		Map<String, Double> requestedValues = CurveMakerUtils.requestAllQuotes(quoteMap, serviceApi, curveTime.toEpochSecond());
		CurveMakerUtils.updateQuoteMap(requestedValues, quoteMap);
		CurveMakerUtils.printResults(quoteMap);
		fx = new FXMatrix(ccy);
	}
	
	// this is just for debugging
	public SingleCurrencyCurveMarketDataProvider(Currency ccy, ZonedDateTime curveTime, Map<String, Double> data) throws UnsupportedTradeException {
		configuration = setConfiguration(ccy); 
		this.curveTime = curveTime;
		curveNames = configuration.getNames();
		quoteMap = configuration.getSwapQuotes();
		Map<String, Double> requestedValues = data;
		CurveMakerUtils.updateQuoteMap(requestedValues, quoteMap);
		CurveMakerUtils.printResults(quoteMap);
		fx = new FXMatrix(ccy);
	}

	public InstrumentDefinition<?>[][][] getDefinitions() {
		InstrumentDefinition<?>[][][] definitions = new InstrumentDefinition<?>[1][][];
		definitions[0] = new InstrumentDefinition<?>[curveNames.length][];
		int curveLoop = 0;
		for (String eachCurve : curveNames) {
			definitions[0][curveLoop] = makeDefinitions(quoteMap.get(eachCurve));
			curveLoop++;
		}
		return definitions;
	}
	
	private InstrumentDefinition<?>[] makeDefinitions(final SwapQuote[] quotes) {
		final InstrumentDefinition<?>[] definitions = new InstrumentDefinition<?>[quotes.length];
		for (int loopmv = 0; loopmv < quotes.length; loopmv++) {
			definitions[loopmv] = quotes[loopmv].getGenerator().generateInstrument(curveTime.toLocalDate().atStartOfDay(ZoneOffset.UTC), quotes[loopmv].getValue(), NOTIONAL, makeTenorAttribute(quotes[loopmv].getPeriod()));
		}
		return definitions;
	}
	
	private GeneratorAttribute makeTenorAttribute (Period tenor) {
		return new GeneratorAttributeIR(tenor);
	}

	public LinkedHashMap<String, SwapQuote[]> getSwapQuotes() {
		return quoteMap;
	}
	
	
	public FXMatrix getFXMatrix() {
		return fx;
	}
	
	public int getNumUnits() {
		return 1;
	}
	
	public String[][] getNames() {
		String[][] curveNamesByUnits = new String[1][];
		curveNamesByUnits[0] = configuration.getNames();
		return curveNamesByUnits;
	}
	
	public LinkedHashMap<String, Currency> getDiscountMap() {
		return configuration.getDiscountMap();
	}
	
	public LinkedHashMap<String, IndexON[]> getIndexONMap() {
		return configuration.getIndexONMap();		
	}
	
	public LinkedHashMap<String, IborIndex[]> getIborIndexMap() {
		return configuration.getIborIndexMap();
	}
	
	public HashMap<String, Pair<Currency, String>> getCurveRelations() {
		return configuration.getCurveRelations();
	}

	private static CurveConfiguration setConfiguration(final Currency ccy) throws UnsupportedTradeException {
		if(ccy == Currency.USD){
			return new USDConfiguration();				
		} else if (ccy == Currency.EUR) {
			return new EURConfiguration();
		} else if (ccy == Currency.GBP){
			return new GBPConfiguration();
		} else if (ccy == Currency.JPY) {
			return new JPYConfiguration();
		} else {
			throw new UnsupportedTradeException("Currency "+ccy+" not supported");
		}
	}
}
