package com.opengamma.gammatrace.ogextension;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponFixedDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityDefinitionBuilder;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.payment.PaymentDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapDefinition;
import com.opengamma.analytics.financial.interestrate.annuity.derivative.Annuity;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Payment;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.ArgumentChecker;
import com.opengamma.util.money.Currency;

/**
 * Class describing a Ibor+Spread for Ibor+Spread payments swap. The two legs can be in different currencies.
 */
public class SwapXCcyFixedIborDefinition extends SwapDefinition {

  /**
   * Constructor of the ibor-ibor swap from its two legs.
   * @param firstLeg The first Ibor leg.
   * @param secondLeg The second Ibor leg.
   */
  public SwapXCcyFixedIborDefinition(final AnnuityDefinition<PaymentDefinition> fixedLeg, final AnnuityDefinition<PaymentDefinition> secondLeg) {
    super(fixedLeg, secondLeg);
  }

  public static SwapXCcyFixedIborDefinition from(final ZonedDateTime settlementDate, final Period tenor, final GeneratorSwapXCcyFixedIbor generator, final double fixedNotional, final double iborNotional,
      final double fixedRate, final boolean isPayer, final Calendar calendar1, final Calendar calendar2) {
    ArgumentChecker.notNull(settlementDate, "settlement date");
    ArgumentChecker.notNull(tenor, "Tenor");
    ArgumentChecker.notNull(generator, "Swap generator");
    // TODO: create a mechanism for the simultaneous payments on both legs, i.e. joint calendar
    final ZonedDateTime maturityDate = ScheduleCalculator.getAdjustedDate(settlementDate, tenor, generator.getIborIndex(), calendar1);
    return from(settlementDate, maturityDate, generator, fixedNotional, iborNotional, fixedRate, 0.0, isPayer);
  }

  /**
   * Builder from the settlement date and a generator. The legs have different notionals.
   * The notionals are paid on the settlement date and final payment date of each leg.
   * @param settlementDate The settlement date.
   * @param maturityDate The swap maturity date.
   * @param generator The Ibor/Ibor swap generator.
   * @param notional1 The first leg notional.
   * @param notional2 The second leg notional.
   * @param spread1 The spread to be applied to the first leg.
   * @param spread2 The spread to be applied to the second leg.
   * @param isPayer The payer flag for the first leg.
   * @return The swap.
   */
  public static SwapXCcyFixedIborDefinition from(final ZonedDateTime settlementDate, final ZonedDateTime maturityDate, final GeneratorSwapXCcyFixedIbor generator, final double fixedLegNotional,
      final double iborLegNotional, final double fixedRate, final double spread, final boolean isFixedPayer) {
    ArgumentChecker.notNull(settlementDate, "settlement date");
    ArgumentChecker.notNull(maturityDate, "Maturity date");
    ArgumentChecker.notNull(generator, "Swap generator");
    // TODO: create a mechanism for the simultaneous payments on both legs, i.e. joint calendar
    final AnnuityDefinition<PaymentDefinition> firstLegNotional = AnnuityDefinitionBuilder.annuityCouponFixedWithNotional(generator.getFixedLegCurrency(), settlementDate, maturityDate, generator.getFixedLegPeriod(), 
    		generator.getFixedLegCalendar(), generator.getFixedLegDayCount(), generator.getBusinessDayConvention(), generator.isEndOfMonth(), fixedLegNotional, fixedRate, isFixedPayer); 
    final AnnuityDefinition<PaymentDefinition> secondLegNotional = AnnuityDefinitionBuilder.annuityIborSpreadWithNotionalFrom(settlementDate, maturityDate,
        iborLegNotional, generator.getIborIndex(), spread, !isFixedPayer, generator.getIborLegCalendar());
    return new SwapXCcyFixedIborDefinition(firstLegNotional, secondLegNotional);
  }
  /**
   * Builder from all the details.
   * @param settlementDate
   * @param maturityDate
   * @param fixedCurrency
   * @param fixedNotional
   * @param fixedPaymentPeriod
   * @param fixedDayCount
   * @param fixedRate
   * @param iborNotional
   * @param iborIndex
   * @param iborSpread
   * @param calendar
   * @param businessDayConvention
   * @param isEOM
   * @param isFixedPayer
   * @return The swap.
   */
  public static SwapXCcyFixedIborDefinition from(final ZonedDateTime settlementDate, final ZonedDateTime maturityDate, final Currency fixedCurrency, final double fixedNotional, final Period fixedPaymentPeriod, 
			final DayCount fixedDayCount, final double fixedRate, final double iborNotional, final IborIndex iborIndex, final double iborSpread, final Calendar calendar, 
			final BusinessDayConvention businessDayConvention, final boolean isEOM, final boolean isFixedPayer) {
		
		final AnnuityDefinition<PaymentDefinition> fixedLegWithNotional = AnnuityDefinitionBuilder.annuityCouponFixedWithNotional(fixedCurrency, settlementDate, maturityDate, fixedPaymentPeriod, calendar, 
				fixedDayCount, businessDayConvention, isEOM, fixedNotional, fixedRate, isFixedPayer);
		final AnnuityDefinition<PaymentDefinition> iborLegWithNotional = AnnuityDefinitionBuilder.annuityIborSpreadWithNotionalFrom(settlementDate, maturityDate, iborNotional, iborIndex, iborSpread, 
				!isFixedPayer, calendar);
		
		return new SwapXCcyFixedIborDefinition(fixedLegWithNotional, iborLegWithNotional);
  }
  
  /**
   * The fixed leg of the swap.
   * @return Fixed leg.
   */
  @SuppressWarnings("unchecked")
  public AnnuityDefinition<PaymentDefinition> getFixedLeg() {
    return (AnnuityDefinition<PaymentDefinition>) getFirstLeg();
  }
  /**
   * The ibor leg of the swap.
   * @return Ibor leg.
   */
  @SuppressWarnings("unchecked")
  public AnnuityDefinition<PaymentDefinition> getIborLeg() {
	return (AnnuityDefinition<PaymentDefinition>) getSecondLeg();
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public Swap<Payment, Payment> toDerivative(final ZonedDateTime date) {
    final Annuity<Payment> fixedLeg = (Annuity<Payment>) getFirstLeg().toDerivative(date);
    final Annuity<Payment> iborLeg = (Annuity<Payment>) getSecondLeg().toDerivative(date);
    return new Swap<>(fixedLeg, iborLeg);
  }
  @SuppressWarnings("unchecked")
  @Override
  public Swap<Payment, Payment> toDerivative(final ZonedDateTime date, final ZonedDateTimeDoubleTimeSeries[] indexDataTS) {
    ArgumentChecker.notNull(indexDataTS, "index data time series array");
    final Annuity<Payment> fixedLeg = (Annuity<Payment>) getFirstLeg().toDerivative(date);
    final Annuity<Payment> iborLeg = (Annuity<Payment>) getSecondLeg().toDerivative(date, indexDataTS[0]);
    return new Swap<>(fixedLeg, iborLeg);
  }
}
