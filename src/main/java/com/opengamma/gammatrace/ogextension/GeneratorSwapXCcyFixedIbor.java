package com.opengamma.gammatrace.ogextension;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeFX;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.util.ArgumentChecker;
import com.opengamma.util.money.Currency;

/**
 * Class with the description of swap characteristics.
 */
public class GeneratorSwapXCcyFixedIbor extends GeneratorInstrument<GeneratorAttributeFX> {

  /**
   * The currency of the first leg. 
   */
  private final Currency _fixedLegCurrency;
  /**
   * The payment period of the fixed leg. 
   */
  private final Period _fixedLegPeriod;
  /**
   * The day count convention of the fixed leg.
   */
  private final DayCount _fixedLegDayCount;
  /**
   * The Ibor index of the second leg.
   */
  private final IborIndex _iborIndex;
  /**
   * The business day convention associated to the swap.
   */
  private final BusinessDayConvention _businessDayConvention;
  /**
   * The flag indicating if the end-of-month rule is used for the swap.
   */
  private final boolean _endOfMonth;
  /**
   * The index spot lag in days between trade and settlement date (usually 2 or 0).
   */
  private final int _spotLag;
  /**
   * The holiday calendar for the first leg.
   */
  private final Calendar _fixedLegCalendar;
  /**
   * The holiday calendar for the second leg.
   */
  private final Calendar _iborLegCalendar;

  // REVIEW: Do we need stubShort and stubFirst flags?
  // TODO: Add a merged calendar? [PLAT-1747]

  /**
   * Constructor from the details. The business day conventions, end-of-month and spot lag are from the first Ibor index.
   * @param name The generator name. Not null.
   * @param iborIndex1 The Ibor index of the first leg.
   * @param iborIndex2 The Ibor index of the second leg.
   * @param calendar1 The holiday calendar for the first leg.
   * @param calendar2 The holiday calendar for the second leg.
   */
  public GeneratorSwapXCcyFixedIbor(final String name, final Currency fixedLegCurrency, final Period fixedLegPeriod, final DayCount fixedLegDayCount,final IborIndex iborIndex, final Calendar fixedLegCalendar, final Calendar iborLegCalendar) {
    super(name);
    ArgumentChecker.notNull(fixedLegCurrency, "fixed leg currency");
    ArgumentChecker.notNull(fixedLegPeriod, "fixed leg period");
    ArgumentChecker.notNull(fixedLegDayCount, "fixed leg day count");
    ArgumentChecker.notNull(iborIndex, "ibor index");
    ArgumentChecker.notNull(fixedLegCalendar, "calendar1");
    ArgumentChecker.notNull(iborLegCalendar, "calendar2");
    _fixedLegCurrency = fixedLegCurrency;
    _fixedLegPeriod = fixedLegPeriod;
    _fixedLegDayCount = fixedLegDayCount;
    _iborIndex = iborIndex;
    _businessDayConvention = iborIndex.getBusinessDayConvention();
    _endOfMonth = iborIndex.isEndOfMonth();
    _spotLag = iborIndex.getSpotLag();
    _fixedLegCalendar = fixedLegCalendar;
    _iborLegCalendar = iborLegCalendar;
  }

  /**
   * Gets the Currency of the fixed leg.
   * @return The currency.
   */
  public Currency getFixedLegCurrency() {
	  return _fixedLegCurrency;
  }
  /**
   * Gets the payment period of the fixed leg.
   * @return The period.
   */
  public Period getFixedLegPeriod() {
	  return _fixedLegPeriod;
  }
  /**
   * Gets the daycount of the fixed leg.
   * @return The daycount.
   */
  public DayCount getFixedLegDayCount() {
	  return _fixedLegDayCount;
  }
  
  /**
   * Gets the Ibor index of the second leg.
   * @return The index.
   */
  public IborIndex getIborIndex() {
    return _iborIndex;
  }

  /**
   * Gets the swap generator business day convention.
   * @return The convention.
   */
  public BusinessDayConvention getBusinessDayConvention() {
    return _businessDayConvention;
  }

  /**
   * Gets the swap generator spot lag.
   * @return The lag (in days).
   */
  public int getSpotLag() {
    return _spotLag;
  }

  /**
   * Gets the swap generator end-of-month rule.
   * @return The EOM.
   */
  public Boolean isEndOfMonth() {
    return _endOfMonth;
  }

  /**
   * Gets the holiday calendar for the first leg.
   * @return The holiday calendar
   */
  public Calendar getFixedLegCalendar() {
    return _fixedLegCalendar;
  }

  /**
   * Gets the holiday calendar for the second leg.
   * @return The holiday calendar
   */
  public Calendar getIborLegCalendar() {
    return _iborLegCalendar;
  }

  /**
   * Generate the cross-currency swap from the spread and the FX exchange rate.
   * @param date The reference date (the effective date of the swap will be the spot lag of the generator after the reference date).
   * @param spread The spread above the index (is applied to the first leg).
   * @param notional The notional of the first leg. The second leg notional is that number multiplied by the FX rate (1 Ccy1 = x Ccy2).
   * @param attribute The FX instrument attributes. The start period is the date between the spot date and the effective period.
   *   The end period is the period between the effective date and the maturity.
   * @return The cross-currency swap.
   */
  @Override
  public SwapXCcyFixedIborDefinition generateInstrument(final ZonedDateTime date, final double spread, final double notional, final GeneratorAttributeFX attribute) {
    ArgumentChecker.notNull(date, "Reference date");
    ArgumentChecker.notNull(attribute, "Attributes");
    final ZonedDateTime spot = ScheduleCalculator.getAdjustedDate(date, _spotLag, _iborLegCalendar);
    final ZonedDateTime startDate = ScheduleCalculator.getAdjustedDate(spot, attribute.getStartPeriod(), _iborIndex, _iborLegCalendar);
    final double fx = attribute.getFXMatrix().getFxRate(_fixedLegCurrency, _iborIndex.getCurrency());
    return SwapXCcyFixedIborDefinition.from(startDate, attribute.getEndPeriod(), this, notional, fx * notional, spread, true, _fixedLegCalendar, _iborLegCalendar);
  }

  @Override
  public String toString() {
    return getName();
  }
  
  
  // removed hashCode and equals
}

