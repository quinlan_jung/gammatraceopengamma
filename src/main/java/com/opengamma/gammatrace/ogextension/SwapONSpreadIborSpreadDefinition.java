package com.opengamma.gammatrace.ogextension;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponIborSpreadDefinition;
import com.opengamma.gammatrace.ogextension.AnnuityCouponONSpreadDefinition;			// this is our corrected version.
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborON;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.analytics.financial.instrument.swap.SwapDefinition;
import com.opengamma.analytics.financial.interestrate.annuity.derivative.Annuity;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Coupon;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.ArgumentChecker;

/**
 * Class describing an Ibor for overnight swap. Both legs are in the same currency.
 * The payment dates on the ibor leg a slightly different from the FixedIbor swap due to the lag in payment at the end of each coupon.
 */
public class SwapONSpreadIborSpreadDefinition extends SwapDefinition {

  /**
   * Constructor of the fixed-OIS swap from its two legs.
   * @param iborLeg The Ibor leg.
   * @param oisLeg The OIS leg.
   */
  public SwapONSpreadIborSpreadDefinition(final AnnuityCouponONSpreadDefinition oisLeg, final AnnuityCouponIborSpreadDefinition iborLeg) {
    super(oisLeg, iborLeg);
    ArgumentChecker.isTrue(oisLeg.getCurrency() == iborLeg.getCurrency(), "Legs should have the same currency");
  }

  public static SwapONSpreadIborSpreadDefinition from(final ZonedDateTime settlementDate, final ZonedDateTime endFixingPeriodDate, final double notional, final double oisSpread, final double iborSpread, 
		  final GeneratorSwapIborON generator, final boolean isIborPayer) {
	  return from(settlementDate, endFixingPeriodDate, notional,  generator.getIndexIbor().getTenor(), generator.getIndexON(), oisSpread,  
			  generator.getIndexIbor().getTenor(), generator.getIndexIbor(), iborSpread, generator.getIborCalendar(), generator.getBusinessDayConvention(), generator.isEndOfMonth(), isIborPayer);
  }
  
  public static SwapONSpreadIborSpreadDefinition from(final ZonedDateTime settlementDate, final ZonedDateTime maturityDate, final double notional, final Period oisPaymentPeriod, final IndexON indexON, final double oisSpread,
		  final Period iborPaymentPeriod, final IborIndex iborIndex, final double iborSpread, final Calendar calendar, final BusinessDayConvention businessDayConvention, final boolean isEOM, final boolean isIborPayer) {
	    
	  	AnnuityCouponONSpreadDefinition oisLeg = AnnuityCouponONSpreadDefinition.from(settlementDate, maturityDate, notional, !isIborPayer, indexON, iborIndex.getSpotLag(), calendar, businessDayConvention, oisPaymentPeriod,
	    		isEOM, oisSpread);
		AnnuityCouponIborSpreadDefinition iborLeg = AnnuityCouponIborSpreadDefinition.from(settlementDate, maturityDate, iborPaymentPeriod, notional, iborIndex, isIborPayer, businessDayConvention, isEOM, 
				iborIndex.getDayCount(), iborSpread, calendar);
	    
	    return new SwapONSpreadIborSpreadDefinition(oisLeg, iborLeg);
  }
  
  /**
   * The ON leg of the swap.
   * @return The leg.
   */
  public AnnuityCouponONSpreadDefinition getOISLeg() {
    return (AnnuityCouponONSpreadDefinition) getFirstLeg();
  }
  
  /**
   * The Ibor leg of the swap.
   * @return The leg.
   */
  public AnnuityCouponIborSpreadDefinition getIborLeg() {
    return (AnnuityCouponIborSpreadDefinition) getSecondLeg();
  }
  @SuppressWarnings("unchecked")
  @Override
  public Swap<Coupon, Coupon> toDerivative(final ZonedDateTime date) {
	  final Annuity<? extends Coupon> oisLeg = (Annuity<? extends Coupon>) this.getOISLeg().toDerivative(date);
	  final Annuity<? extends Coupon> iborLeg = this.getIborLeg().toDerivative(date);
    return new Swap<>((Annuity<Coupon>) oisLeg, (Annuity<Coupon>) iborLeg);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Swap<Coupon, Coupon> toDerivative(final ZonedDateTime date, final ZonedDateTimeDoubleTimeSeries[] indexDataTS) {
    ArgumentChecker.notNull(indexDataTS, "index data time series array");
    ArgumentChecker.isTrue(indexDataTS.length > 1, "index data time series must contain at least two elements");
    final Annuity<? extends Coupon> oisLeg = this.getOISLeg().toDerivative(date, indexDataTS[0]);
    final Annuity<? extends Coupon> iborLeg = this.getIborLeg().toDerivative(date, indexDataTS[1]);
    return new Swap<>((Annuity<Coupon>) oisLeg, (Annuity<Coupon>) iborLeg);
  }
}

