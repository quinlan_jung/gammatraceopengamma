package com.opengamma.gammatrace.prod;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.Period;

import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.util.tuple.Pair;

public class DeltaResults {
	List<String> periods = new ArrayList<String>();
	List<CurveDelta> curveDeltas = new ArrayList<CurveDelta>();

	
	public DeltaResults(List<String> periods, List<CurveDelta> curveDeltas) {
		this.periods = periods;
		this.curveDeltas = curveDeltas;
	}
	
	public DeltaResults(List<Period> periods) {
		for (Period p : periods){
			this.periods.add(p.toString());
		}
	}
	
	public DeltaResults(Pair<Map<String, List<Pair<QuoteInstrumentType, Double>>>, List<Period>> rawDelta){
		Map<String, List<Pair<QuoteInstrumentType, Double>>> vector = rawDelta.getFirst();
		for (Entry<String, List<Pair<QuoteInstrumentType, Double>>> entry : vector.entrySet()){
			String curveName = entry.getKey();
			List<String> quoteInstrumentTypes = new ArrayList<>();
			List<Double> quotes = new ArrayList<Double>();
			for (Pair<QuoteInstrumentType, Double> pair : entry.getValue()){
				if (pair == null){
					quoteInstrumentTypes.add(null);
					quotes.add(null);
				}
				else{
					quoteInstrumentTypes.add(pair.getFirst().toString());
					quotes.add(pair.getSecond());
				}
			}
			curveDeltas.add(new CurveDelta(curveName, quoteInstrumentTypes, quotes));
		}
		
		for (Period p : rawDelta.getSecond()){
			periods.add(p.toString());
		}
	}
	
	public DeltaResults(){}
	
	public String toJsonString() throws JsonGenerationException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(this);
	}
	
	public List<String> getPeriods() {
		return periods;
	}
	public void setPeriods(List<String> periods) {
		this.periods = periods;
	}
	public List<CurveDelta> getCurveDeltas() {
		return curveDeltas;
	}

	public void addCurveDelta(CurveDelta curveDelta) {
		curveDeltas.add(curveDelta);
	}

	public void setCurveDeltas(List<CurveDelta> curveDeltas) {
		this.curveDeltas = curveDeltas;
	}
	
	public static DeltaResults reverseDirection(DeltaResults original) {
		if (original == null) return null;
		List<CurveDelta> reversed = new ArrayList<>();
		for (CurveDelta element : original.getCurveDeltas()) {
			reversed.add(CurveDelta.reverseDirection(element));
		}
		return new DeltaResults(original.getPeriods(), reversed);
	}
}
