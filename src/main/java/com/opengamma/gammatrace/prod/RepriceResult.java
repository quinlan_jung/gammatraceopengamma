package com.opengamma.gammatrace.prod;

// TODO: primary delta
public class RepriceResult {
	private Double tradeImpliedFairQuote = null;
	private Double endDateRate = null;
	private Double effectiveDateRate = null;
	private Double marketImpliedFairQuote = null;
	private DeltaResults spreadDelta = null;
	private DeltaResults fixedDelta = null;
	private DeltaResults xccyDelta = null;
	private FlatDelta flatDelta = null;
	private FlatDelta primaryDelta = null;
	private VegaMatrix vegaMatrix = null;
	private Boolean isQuotePayer = null;
	
	public RepriceResult() {
	}
	
	public Double getTradeImpliedFairQuote(){
		return tradeImpliedFairQuote;
	}
	
	public Double getEndDateRate(){
		return endDateRate;
	}
	
	public Double getEffectiveDateRate(){
		return effectiveDateRate;
	}
	
	public Double getMarketImpliedFairQuote() {
		return marketImpliedFairQuote;
	}
	
	public DeltaResults getSpreadDelta(){
		return spreadDelta;
	}
	
	public DeltaResults getFixedDelta(){
		return fixedDelta;
	}

	public DeltaResults getXccyDelta() {
		return xccyDelta;
	}
	
	public FlatDelta getFlatDelta() {
		return flatDelta;
	}
	public FlatDelta getPrimaryDelta() {
		return primaryDelta;
	}

	public VegaMatrix getVegaMatrix() {
		return vegaMatrix;
	}
	
	public Boolean getIsQuotePayer() {
		return isQuotePayer;
	}
	
	public void setPrimaryDelta(FlatDelta primaryDelta) {
		this.primaryDelta = primaryDelta;
	}

	public void setVegaMatrix(VegaMatrix vegaMatrix) {
		this.vegaMatrix = vegaMatrix;
	}

	public void setXccyDelta(DeltaResults xccyDelta) {
		this.xccyDelta = xccyDelta;
	}

	public void setTradeImpliedFairQuote(Double tradeImpliedFairQuote) {
		this.tradeImpliedFairQuote = tradeImpliedFairQuote;
	}

	public void setEndDateRate(Double endDateRate) {
		this.endDateRate = endDateRate;
	}

	public void setEffectiveDateRate(Double effectiveDateRate) {
		this.effectiveDateRate = effectiveDateRate;
	}
	
	public void setMarketImpliedFairQuote(Double marketImpliedFairQuote) { 
		this.marketImpliedFairQuote = marketImpliedFairQuote;
	}

	public void setSpreadDelta(DeltaResults spreadDelta) {
		this.spreadDelta = spreadDelta;
	}

	public void setFixedDelta(DeltaResults fixedDelta) {
		this.fixedDelta = fixedDelta;
	}
	
	public void setFlatDelta(FlatDelta flatDelta) {
		this.flatDelta = flatDelta;
	}
	
	public void setIsQuotePayer(Boolean isQuotePayer) {
		this.isQuotePayer = isQuotePayer;
	}
	
	public static RepriceResult reverseDirection(RepriceResult original) {
		if (original == null) return null;
		RepriceResult reversed = new RepriceResult();
		// fields that don't change
		reversed.setTradeImpliedFairQuote(original.getTradeImpliedFairQuote());
		reversed.setEndDateRate(original.getEndDateRate());
		reversed.setEffectiveDateRate(original.getEffectiveDateRate());
		reversed.setMarketImpliedFairQuote(original.getMarketImpliedFairQuote());
		//reversed.setVolatility(original.getVolatility());
		// risk fields
		reversed.setFlatDelta(FlatDelta.reverseDirection(original.getFlatDelta()));
		reversed.setFixedDelta(DeltaResults.reverseDirection(original.getFixedDelta()));
		reversed.setSpreadDelta(DeltaResults.reverseDirection(original.getSpreadDelta()));
		reversed.setXccyDelta(DeltaResults.reverseDirection(original.getXccyDelta()));
		reversed.setPrimaryDelta(FlatDelta.reverseDirection(original.getPrimaryDelta()));
		// direction flag
		reversed.setIsQuotePayer(original.getIsQuotePayer() == null ? null : !original.getIsQuotePayer());
		
		return reversed;
	}
}
