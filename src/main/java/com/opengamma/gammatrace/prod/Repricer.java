package com.opengamma.gammatrace.prod;

import com.gammatrace.datamodel.Trade;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionCashFixedIbor;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.SABRSwaptionProviderDiscount;
import com.opengamma.gammatrace.exceptions.UnsupportedOperation;
import com.opengamma.gammatrace.exceptions.UnsupportedTradeException;
import com.opengamma.gammatrace.instrument.Instrument;
import com.opengamma.gammatrace.instrument.InstrumentBuildManager;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.instrument.SwaptionInstrument;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.marketconstruction.dictionary.DictionaryManager;
import com.opengamma.gammatrace.marketconstruction.sabr.SABRSwaptionCurveMaker;
import com.opengamma.gammatrace.rates.calculator.BlackImpliedVolatilityCalculator;
import com.opengamma.gammatrace.rates.calculator.DeltaRiskCalculatorOld;
import com.opengamma.gammatrace.rates.calculator.DeltaRiskCalculator;
import com.opengamma.gammatrace.rates.calculator.MarketStandardFairFixedRateCalculator;
import com.opengamma.gammatrace.rates.calculator.SABRMarketImpliedBlackVolatilityCalculator;
import com.opengamma.gammatrace.rates.calculator.SABRSwaptionVegaCalculator;
import com.opengamma.util.tuple.Pair;

public class Repricer {
	public static RepriceResult reprice(Trade trade) throws UnsupportedOperation, UnsupportedTradeException{
		RepriceResult repriceResult = new RepriceResult();
		InstrumentBuildManager build = new InstrumentBuildManager(trade);
		Instrument instrument = build.makeInstrument();
		InstrumentDescription description = instrument.getInstrumentDescription();
		switch(description){
		case FIXED_IBOR_SWAP: case FIXED_ON_SWAP: case IBOR_IBOR_SWAP: case IBOR_ON_SWAP: {
			//Build the curve 
			CurveMaker SCCM = new CurveMaker(instrument.getSecondNotional().getCurrency(), instrument.getExecutionTimestamp());
			Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> pair = SCCM.makeCurve();
			// calculate the price metrics
			calculateFairRates(repriceResult, instrument, pair.getFirst());
			// calculate delta risk
			Swap<?, ?> derivative = ((SwapInstrument) instrument).getDerivative();
			DeltaRiskCalculator DRC = new DeltaRiskCalculator(derivative, pair.getFirst(), pair.getSecond(), SCCM.getDeltaDisplayBundle());
			repriceResult.setSpreadDelta(DRC.extractSpreadDelta());
			repriceResult.setFixedDelta(DRC.extractFixedDelta());
			repriceResult.setFlatDelta(DRC.getFlatDelta());
			// set direction
			repriceResult.setIsQuotePayer(true);
			break;
		}
		case XCCY_IBOR_IBOR_SWAP: case XCCY_FIXED_IBOR_SWAP: case XCCY_FIXED_FIXED_SWAP: {
			//Build the curve 
			CurveMaker SCCM = new CurveMaker(instrument.getSecondNotional().getCurrency(), instrument.getFirstNotional().getCurrency(), instrument.getExecutionTimestamp());
			Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> pair = SCCM.makeCurve();
			// calculate the price metrics
			calculateFairRates(repriceResult, instrument, pair.getFirst());
			// calculate delta risk
			Swap<?, ?> derivative = ((SwapInstrument) instrument).getDerivative();
			DeltaRiskCalculator DRC = new DeltaRiskCalculator(derivative, pair.getFirst(), pair.getSecond(), SCCM.getDeltaDisplayBundle());
			repriceResult.setSpreadDelta(DRC.extractSpreadDelta());
			repriceResult.setFixedDelta(DRC.extractFixedDelta());
			repriceResult.setXccyDelta(DRC.extractXCcySpreadDelta());
			repriceResult.setFlatDelta(DRC.getFlatDelta());
			// set direction
			repriceResult.setIsQuotePayer(true);
			break;
		}
		case FIXED_IBOR_SWAPTION: case FIXED_IBOR_STRADDLE_SWAPTION: {
			SwaptionInstrument swaptionInstrument = (SwaptionInstrument) instrument;
			SwaptionPhysicalFixedIbor[] derivative = (SwaptionPhysicalFixedIbor[]) swaptionInstrument.getDerivative();
			SABRSwaptionCurveMaker curveMaker = new SABRSwaptionCurveMaker(instrument.getSecondNotional().getCurrency(), instrument.getExecutionTimestamp());
			SABRSwaptionProviderDiscount curve = curveMaker.makeSABRSwaptionCurve();
			// price metrics 
			Double volatility = BlackImpliedVolatilityCalculator.getImpliedVolatility(derivative, swaptionInstrument.getSpotOptionPremium(), curve.getMulticurveProvider());
			repriceResult.setTradeImpliedFairQuote(volatility);
			repriceResult.setMarketImpliedFairQuote(SABRMarketImpliedBlackVolatilityCalculator.getSABRMarketImpliedBlackVolatility(swaptionInstrument, curve));
			//delta
			DeltaRiskCalculator DRC = new DeltaRiskCalculator(derivative, curve, curveMaker.getCurveBuildingBlockBundle(), curveMaker.getDeltaDisplayBundle());
			repriceResult.setSpreadDelta(DRC.extractSpreadDelta());
			repriceResult.setFixedDelta(DRC.extractFixedDelta());
			repriceResult.setFlatDelta(DRC.getFlatDelta());
			//vega
			SABRSwaptionVegaCalculator vegaCalculator = new SABRSwaptionVegaCalculator(swaptionInstrument, curve);
			repriceResult.setVegaMatrix(vegaCalculator.getVegaMatrix());
			// set direction
			repriceResult.setIsQuotePayer(true);
			break;
		}
		case FIXED_IBOR_CASH_SWAPTION: case FIXED_IBOR_STRADDLE_CASH_SWAPTION: {
			SwaptionInstrument swaptionInstrument = (SwaptionInstrument) instrument;
			SwaptionCashFixedIbor[] derivative = (SwaptionCashFixedIbor[]) swaptionInstrument.getDerivative();
			SABRSwaptionCurveMaker curveMaker = new SABRSwaptionCurveMaker(instrument.getSecondNotional().getCurrency(), instrument.getExecutionTimestamp());
			SABRSwaptionProviderDiscount curve = curveMaker.makeSABRSwaptionCurve();
			// price metrics 
			Double volatility = BlackImpliedVolatilityCalculator.getImpliedVolatility(derivative, swaptionInstrument.getSpotOptionPremium(), curve.getMulticurveProvider());
			repriceResult.setTradeImpliedFairQuote(volatility);
			repriceResult.setMarketImpliedFairQuote(SABRMarketImpliedBlackVolatilityCalculator.getSABRMarketImpliedBlackVolatility(swaptionInstrument, curve));
			//delta
			DeltaRiskCalculator DRC = new DeltaRiskCalculator(derivative, curve, curveMaker.getCurveBuildingBlockBundle(), curveMaker.getDeltaDisplayBundle());
			repriceResult.setSpreadDelta(DRC.extractSpreadDelta());
			repriceResult.setFixedDelta(DRC.extractFixedDelta());
			repriceResult.setFlatDelta(DRC.getFlatDelta());
			//vega
			SABRSwaptionVegaCalculator vegaCalculator = new SABRSwaptionVegaCalculator(swaptionInstrument, curve);
			repriceResult.setVegaMatrix(vegaCalculator.getVegaMatrix());
			// set direction
			repriceResult.setIsQuotePayer(true);
			break;
		}
		case FX_OPTION:
			break;
		default:
			throw new UnsupportedOperation(String.format("No reprice support for %s instrument", description.toString()));
		}
		return repriceResult;
	}
	
	private static void calculateFairRates(RepriceResult repriceResult, Instrument instrument, MulticurveProviderDiscount curve) throws UnsupportedOperation {
		// calculate the price metrics
		MarketStandardFairFixedRateCalculator MSFFRC = new MarketStandardFairFixedRateCalculator((SwapInstrument) instrument, curve);
		repriceResult.setTradeImpliedFairQuote(MSFFRC.getFairFixedRate());
		repriceResult.setMarketImpliedFairQuote(MSFFRC.getMarketImpliedFixedRate());
		Pair<Double, Double> priceMetricTermStructure = MSFFRC.getTwoPartFairFixedRate();
		if (priceMetricTermStructure != null) {
			repriceResult.setEndDateRate(priceMetricTermStructure.getFirst());
			repriceResult.setEffectiveDateRate(priceMetricTermStructure.getSecond());
		}
	}
}
