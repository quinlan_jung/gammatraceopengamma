package com.opengamma.gammatrace.prod;

import java.util.ArrayList;
import java.util.List;

import com.opengamma.analytics.math.matrix.DoubleMatrix1D;
import com.opengamma.util.ArgumentChecker;

public class CurveDelta {
	String name;
	List<String> quoteInstrumentTypes = new ArrayList<String>();		// should this be saved as List<QuoteInstrumentType> ?
	List<Double> deltaValues = new ArrayList<Double>();
	
	public CurveDelta(String name, List<String> quoteInstrumentTypes, List<Double> deltaValues){
		ArgumentChecker.isTrue(quoteInstrumentTypes.size() == deltaValues.size(), "Must have the same number of QuoteInstrumentTypes and values");
		this.name = name;
		this.quoteInstrumentTypes = quoteInstrumentTypes;
		this.deltaValues = deltaValues;
	}

	public String getName() {
		return name;
	}

	public List<String> getQuoteInstrumentTypes() {
		return quoteInstrumentTypes;
	}

	public List<Double> getDeltaValues() {
		return deltaValues;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setQuoteInstrumentTypes(List<String> quoteInstrumentTypes) {
		this.quoteInstrumentTypes = quoteInstrumentTypes;
	}

	public void setDeltaValues(List<Double> deltaValues) {
		this.deltaValues = deltaValues;
	}
	
	public static CurveDelta reverseDirection(final CurveDelta original) {
		List<Double> reversed = new ArrayList<>();
		for (Double element : original.getDeltaValues()) {
			if (element == null) {
				reversed.add(null);
			} else {
				reversed.add(-element);
			}
		}
		return new CurveDelta(original.getName(), original.getQuoteInstrumentTypes(), reversed);
	}
}
