package com.opengamma.gammatrace.prod;

import java.io.IOException;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.opengamma.gammatrace.rates.calculator.SABRSwaptionVegaStructure;

public class VegaMatrix {
	SABRSwaptionVegaStructure alpha = null;
	SABRSwaptionVegaStructure beta = null;
	SABRSwaptionVegaStructure rho = null;
	SABRSwaptionVegaStructure nu = null;
	
	public VegaMatrix(){}
	
	public VegaMatrix(SABRSwaptionVegaStructure alpha, SABRSwaptionVegaStructure beta, SABRSwaptionVegaStructure rho, SABRSwaptionVegaStructure nu) {
		this.alpha = alpha;
		this.beta = beta;
		this.rho = rho;
		this.nu = nu;
	}
	
	public String toJsonString() throws JsonGenerationException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(this);
	}
	
	public SABRSwaptionVegaStructure getAlpha() {
		return alpha;
	}
	public void setAlpha(SABRSwaptionVegaStructure alpha) {
		this.alpha = alpha;
	}
	public SABRSwaptionVegaStructure getBeta() {
		return beta;
	}
	public void setBeta(SABRSwaptionVegaStructure beta) {
		this.beta = beta;
	}
	public SABRSwaptionVegaStructure getRho() {
		return rho;
	}
	public void setRho(SABRSwaptionVegaStructure rho) {
		this.rho = rho;
	}
	public SABRSwaptionVegaStructure getNu() {
		return nu;
	}
	public void setNu(SABRSwaptionVegaStructure nu) {
		this.nu = nu;
	}
	
	public static VegaMatrix reverseDirection(VegaMatrix original) {
		return new VegaMatrix(SABRSwaptionVegaStructure.reverseDirection(original.getAlpha()), SABRSwaptionVegaStructure.reverseDirection(original.getBeta()), 
				SABRSwaptionVegaStructure.reverseDirection(original.getRho()), SABRSwaptionVegaStructure.reverseDirection(original.getNu()));
	}
}
