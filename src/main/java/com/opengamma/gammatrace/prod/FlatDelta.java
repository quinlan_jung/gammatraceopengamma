package com.opengamma.gammatrace.prod;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.opengamma.analytics.util.amount.ReferenceAmount;
import com.opengamma.util.money.Currency;
import com.opengamma.util.tuple.Pair;

public class FlatDelta {
	private Map<String, Double> map; //Map<Currency*CurveName, Double>
	
	public FlatDelta(ReferenceAmount<Pair<String, Currency>> flatDelta){
		this.map = convertMap(flatDelta.getMap());
	}
	
	public FlatDelta(Map<String, Double> map) {
		this.map = map;
	}
	
	private Map<String,Double> convertMap(Map<Pair<String, Currency>, Double> refMap){
		Map<String, Double> map = new HashMap<String, Double>();
		for (Entry<Pair<String, Currency>, Double> entry : refMap.entrySet()){
			String curveName = entry.getKey().getFirst();
			//String currency = entry.getKey().getSecond().toString();
			//String hashKey = String.format("%s*%s", currency, curveName);
			map.put(curveName, entry.getValue());
		}
		return map;
	}
	
	public String toJsonString() throws JsonGenerationException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(map);
	}

	public Map<String, Double> getMap() {
		return map;
	}
	
	public static FlatDelta reverseDirection(FlatDelta original) {
		if (original == null) return null;
		Map<String, Double> reversed = new HashMap<>();
		for (Map.Entry<String, Double> entry : original.getMap().entrySet()) {
			reversed.put(entry.getKey(), - entry.getValue());
		}
		return new FlatDelta(reversed);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((map == null) ? 0 : map.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlatDelta other = (FlatDelta) obj;
		if (map == null) {
			if (other.map != null)
				return false;
		} else if (!map.equals(other.map))
			return false;
		return true;
	}
	
	
}
