import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;
import java.lang.Math;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponIborDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.annuity.AnnuityCouponONDefinition;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedONMaster;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.payment.CouponIborDefinition;
import com.opengamma.analytics.financial.instrument.payment.CouponONDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedONDefinition;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Coupon;
import com.opengamma.analytics.financial.interestrate.payments.derivative.CouponIborSpread;
import com.opengamma.analytics.financial.interestrate.swap.derivative.SwapFixedCoupon;
import com.opengamma.analytics.financial.provider.calculator.discounting.PV01CurveParametersCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParRateDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueMarketQuoteSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.generic.MarketQuoteSensitivityBlockCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderInterface;
import com.opengamma.analytics.financial.provider.sensitivity.multicurve.MultipleCurrencyParameterSensitivity;
import com.opengamma.analytics.financial.provider.sensitivity.parameter.ParameterSensitivityParameterCalculator;
import com.opengamma.analytics.util.amount.ReferenceAmount;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.CalendarFactory;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.gammatrace.exceptions.UnsupportedTradeException;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.rates.calculator.DeltaRiskCalculatorOld;
import com.opengamma.gammatrace.rates.calculator.FairFixedRateCalculator;
import com.opengamma.gammatrace.rates.calculator.ImpliedCurveFlatSpreadCalculator;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;
import com.opengamma.gammatrace.rates.indextemplate.ONTemplateByName;
import com.opengamma.gammatrace.rates.instrument.FixingBuilder;
import com.opengamma.timeseries.precise.zdt.ImmutableZonedDateTimeDoubleTimeSeries;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;

public class NewTest {
	
	private static final PresentValueDiscountingCalculator PVC = PresentValueDiscountingCalculator.getInstance();
	private static final ParSpreadMarketQuoteDiscountingCalculator PSMQC = ParSpreadMarketQuoteDiscountingCalculator.getInstance();
	
	private static final GeneratorSwapFixedIborTemplate SWAP_MASTER = GeneratorSwapFixedIborTemplate.getInstance();
	private static final GeneratorSwapFixedONTemplate ON_MASTER = GeneratorSwapFixedONTemplate.getInstance();
	private static final GeneratorSwapIborIborTemplate BASIS_MASTER = GeneratorSwapIborIborTemplate.getInstance();
	
	private static final IborTemplateByName iborTemplate = IborTemplateByName.getInstance();
	private static final IborIndex IBOR_RATE = iborTemplate.getIndex(Indicies.USDLIBOR3M);
	
	
	// junk ... to be replaced with data from the database
	private static final ZonedDateTime settlementDate = DateUtils.getUTCDate(2015, 2, 1, 13 , 0);
	private static final ZonedDateTime maturityDate = DateUtils.getUTCDate(2019, 2, 1, 13, 0);
	private static final ZonedDateTime timestamp = DateUtils.getUTCDate(2015, 2, 3, 13, 0);
	private static final Calendar CALENDAR = new MondayToFridayCalendar("NYC");
			//new MondayToFridayCalendar("NYC");
			//CalendarFactory.INSTANCE.getCalendarByCountry("USD");
	private static final Currency CCY = Currency.USD;
	private static final double notional = 1000000;
	private static final double rateFixed = 0.0180;
	private static final Period fixedLegPeriod = Period.ofMonths(12);
	private static final DayCount fixedLegDayCount = DayCountFactory.INSTANCE.getDayCount("30/360");
	private static final BusinessDayConvention businessDaysConvention = IBOR_RATE.getBusinessDayConvention();
	private static final Boolean fixedLegEOM = true; // check this!!!
	private static final Period floatLegPeriod = IBOR_RATE.getTenor();
	private static final DayCount floatLegDayCount = DayCountFactory.INSTANCE.getDayCount("ACT/360");
	private static final Boolean floatLegEOM = IBOR_RATE.isEndOfMonth();
	
	private static final SwapFixedIborSpreadDefinition SWAP_FIXED_IBOR_DEFINITION_FULL = SwapFixedIborSpreadDefinition.from(settlementDate, maturityDate, fixedLegPeriod, fixedLegDayCount,
                                                                                                 businessDaysConvention, fixedLegEOM, notional, rateFixed, floatLegPeriod, floatLegDayCount, businessDaysConvention, floatLegEOM, notional,
                                                                                                 IBOR_RATE, 0.0000,false, CALENDAR);
	
	private static final ZonedDateTimeDoubleTimeSeries[] TS_IBOR = FixingBuilder.makeFixingTimeSeries(SWAP_FIXED_IBOR_DEFINITION_FULL, timestamp);
	private static final SwapFixedCoupon<Coupon> fullSwap = SWAP_FIXED_IBOR_DEFINITION_FULL.toDerivative(timestamp, TS_IBOR);
	
	private static final FXMatrix fx = new FXMatrix(Currency.EUR, Currency.GBP, 1.40);
	private static final MultipleCurrencyAmount amount = MultipleCurrencyAmount.of(Currency.GBP, 0.0);
	private static final MultipleCurrencyAmount nullAmount = null; 
	
	private static final GeneratorSwapFixedON GENERATOR_OIS_USD = GeneratorSwapFixedONTemplate.getInstance().getGenerator(Generators.USD1YFEDFUND, CALENDAR);
	private static final SwapFixedONDefinition SWAP_FIXED_ON_DEFINITION_FULL = SwapFixedONDefinition.from(settlementDate, maturityDate, notional, notional,
			GENERATOR_OIS_USD, rateFixed, true);
	
	private static final ZonedDateTimeDoubleTimeSeries[] TS_ON_2 = FixingBuilder.makeFixingTimeSeries(SWAP_FIXED_ON_DEFINITION_FULL, timestamp);
	
	private static final SwapFixedCoupon<Coupon> oisSwap = SWAP_FIXED_ON_DEFINITION_FULL.toDerivative(timestamp, TS_ON_2);
	
	private static final ParRateDiscountingCalculator PRDC = ParRateDiscountingCalculator.getInstance();
	
	public static void main (String[] args){
		LinkedHashMap<String, Double> bumpMap = new LinkedHashMap<>();
		bumpMap.put("USD DSC", 0.0);
		bumpMap.put("USD fwd 3m", 0.0);
		bumpMap.put("USD fwd 6m", 0.0);
		bumpMap.put("blah", 0.0);
		
		
		CurveMaker SCCM = null;
		try {
			SCCM = new CurveMaker(CCY, timestamp);
		} catch (UnsupportedTradeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long startTime = System.currentTimeMillis();
		Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> pair = SCCM.makeCurve(bumpMap);
		long finishTime = System.currentTimeMillis();
		MulticurveProviderDiscount curve = pair.getFirst();
		
		
		final MultipleCurrencyAmount fullPv = fullSwap.accept(PVC, curve);
		final double fullParSpread = fullSwap.accept(PSMQC, curve);
		double fair = fullSwap.accept(PRDC, curve);
		System.out.println("PV = "+fullPv+" Fair rate = "+fullParSpread+" "+fair+" "+(finishTime-startTime));
		//System.out.println(TS_IBOR[0].toString());
		/*
		System.out.println("============");
		PV01CurveParametersCalculator<MulticurveProviderInterface> PV01CPC = new PV01CurveParametersCalculator<>(PresentValueCurveSensitivityDiscountingCalculator.getInstance());
		ReferenceAmount<Pair<String, Currency>> pv01Computed = fullSwap.accept(PV01CPC, curve);
		Entry<Pair<String, Currency>, Double> maxRisk = null;
		Iterator<Entry<Pair<String, Currency>, Double>> itr = pv01Computed.getMap().entrySet().iterator();
		while(itr.hasNext()){
			Entry<Pair<String, Currency>, Double> pv01 = itr.next();
			if (maxRisk == null){
				maxRisk = pv01;
			} else if (Math.abs(pv01.getValue()) > Math.abs(maxRisk.getValue())) {
				maxRisk = pv01;
			} 
			System.out.println(pv01);
		}	
		System.out.println(maxRisk);
		
		//System.out.println(fx.convert(amount, Currency.USD));
		
		//ParameterSensitivityMulticurveDiscountInterpolatedFDCalculator 	
		*/
		System.out.println(((CouponIborSpread) fullSwap.getSecondLeg().getNthPayment(0)).getSpread());
		final PresentValueMarketQuoteSensitivityDiscountingCalculator PVMQSC = PresentValueMarketQuoteSensitivityDiscountingCalculator.getInstance();
		double fixedpv01 = fullSwap.getFirstLeg().accept(PVMQSC, curve);
		double floatpv01 = fullSwap.getSecondLeg().accept(PVMQSC, curve);
		System.out.println(fixedpv01+"  "+floatpv01);
		
		System.out.println(fullSwap.getFirstLeg().isPayer()+"  "+fullSwap.getSecondLeg().isPayer());
		
		/*
		startTime = System.currentTimeMillis();
		double IFS = ImpliedCurveFlatSpreadCalculator.calculate(fullSwap, amount, SCCM);
		finishTime = System.currentTimeMillis();
		System.out.println(IFS+" "+(finishTime-startTime));
		*/
		
		/*
		startTime = System.currentTimeMillis();
		double FFRC = FairFixedRateCalculator.calculate(fullSwap, amount, curve, timestamp);
		finishTime = System.currentTimeMillis();
		System.out.println(FFRC+" "+(finishTime-startTime));
		
		System.out.println("============");
		
		ReferenceAmount<Pair<String, Currency>> pv01Expected = new ReferenceAmount<>();
		*/
		 
		//=================
		
		final PresentValueCurveSensitivityDiscountingCalculator PVCSC = PresentValueCurveSensitivityDiscountingCalculator.getInstance();
		final ParameterSensitivityParameterCalculator<MulticurveProviderInterface> PSC = new ParameterSensitivityParameterCalculator<>(PVCSC);
		final MarketQuoteSensitivityBlockCalculator<MulticurveProviderInterface> MQSC = new MarketQuoteSensitivityBlockCalculator<>(PSC);
		/*
		DecimalFormat dfFormat = new DecimalFormat("0");
		MultipleCurrencyParameterSensitivity delta = new MultipleCurrencyParameterSensitivity();
		delta = MQSC.fromInstrument(fullSwap, curve, pair.getSecond());
		for (final Pair<String, Currency> nameCcy : delta.getAllNamesCurrency()) {
			System.out.println(nameCcy.toString());
	      	final double[] array = delta.getSensitivity(nameCcy).getData();
	      	Period[] nodes = SCCM.getInstrumentTenors().get(nameCcy.getFirst());
	      	for (int i = 0; i < array.length; i++) {
	      		System.out.println(nodes[i]+"    "+dfFormat.format(array[i] * 1e-4));
	      	}
		}*/
		/*
		Pair<Map<String, List<Double>>, List<Period>> delta = new DeltaRiskCalculator(fullSwap, pair, SCCM, Currency.USD).extractSpreadDelta();
		for (Map.Entry<String, List<Double>> e : delta.getFirst().entrySet()) {
			int printCounter = 0;
			System.out.println(e.getKey());
			for (Double d : e.getValue()) {
				String displayValue = (d == null)? "-" : d.toString();
				System.out.println(delta.getSecond().get(printCounter)+"     "+displayValue);
				printCounter++;
			}
		}*/
				/*
		Iterator<Entry<Pair<String, Currency>, Double>> itr1 = pv01Expected.getMap().entrySet().iterator();
		while(itr1.hasNext()){
			Entry<Pair<String, Currency>, Double> pv01 = itr1.next();
			System.out.println(pv01);
		}
		System.out.println("============");
		*/
		
		
	}
}
