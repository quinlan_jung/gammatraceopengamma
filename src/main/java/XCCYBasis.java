import static org.testng.AssertJUnit.assertEquals;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.curve.interestrate.generator.GeneratorCurveYieldInterpolated;
import com.opengamma.analytics.financial.curve.interestrate.generator.GeneratorYDCurve;
import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.instrument.InstrumentDefinition;
import com.opengamma.analytics.financial.instrument.cash.CashDefinition;
import com.opengamma.analytics.financial.instrument.fra.ForwardRateAgreementDefinition;
import com.opengamma.analytics.financial.instrument.future.InterestRateFutureTransactionDefinition;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttribute;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeFX;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeIR;
import com.opengamma.analytics.financial.instrument.index.GeneratorDepositIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorDepositON;
import com.opengamma.analytics.financial.instrument.index.GeneratorFRA;
import com.opengamma.analytics.financial.instrument.index.GeneratorForexSwap;
import com.opengamma.analytics.financial.instrument.index.GeneratorInstrument;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIborMaster;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedONMaster;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexIborMaster;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.analytics.financial.instrument.swap.SwapDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedONDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapIborIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapXCcyIborIborDefinition;
import com.opengamma.analytics.financial.interestrate.InstrumentDerivative;
import com.opengamma.analytics.financial.interestrate.InstrumentDerivativeVisitor;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Coupon;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Payment;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.interestrate.swap.derivative.SwapFixedCoupon;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.generic.LastTimeCalculator;
import com.opengamma.analytics.financial.provider.calculator.generic.MarketQuoteSensitivityBlockCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.curve.MultiCurveBundle;
import com.opengamma.analytics.financial.provider.curve.SingleCurveBundle;
import com.opengamma.analytics.financial.provider.curve.multicurve.MulticurveDiscountBuildingRepository;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderInterface;
import com.opengamma.analytics.financial.provider.sensitivity.multicurve.MulticurveSensitivity;
import com.opengamma.analytics.financial.provider.sensitivity.multicurve.MultipleCurrencyParameterSensitivity;
import com.opengamma.analytics.financial.provider.sensitivity.parameter.ParameterSensitivityParameterCalculator;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.analytics.math.interpolation.CombinedInterpolatorExtrapolatorFactory;
import com.opengamma.analytics.math.interpolation.Interpolator1D;
import com.opengamma.analytics.math.interpolation.Interpolator1DFactory;
import com.opengamma.analytics.util.time.TimeCalculator;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.timeseries.precise.zdt.ImmutableZonedDateTimeDoubleTimeSeries;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;

public class XCCYBasis {
	private static final Interpolator1D INTERPOLATOR_LINEAR = CombinedInterpolatorExtrapolatorFactory.getInterpolator(Interpolator1DFactory.LINEAR, Interpolator1DFactory.FLAT_EXTRAPOLATOR,
                                                                                                                      Interpolator1DFactory.FLAT_EXTRAPOLATOR);
    
    private static final LastTimeCalculator MATURITY_CALCULATOR = LastTimeCalculator.getInstance();
    private static final double TOLERANCE_ROOT = 1.0E-10;
    private static final int STEP_MAX = 100;
    
    private static final Calendar TARGET = new MondayToFridayCalendar("TARGET");
    private static final Calendar NYC = new MondayToFridayCalendar("NYC");
    
    private static final Currency EUR = Currency.EUR;
    private static final Currency USD = Currency.USD;
    
    private static final double FX_EURUSD = 1.40;
    private static final FXMatrix FX_MATRIX = new FXMatrix(USD);
    static {
        FX_MATRIX.addCurrency(EUR, USD, FX_EURUSD);
    }
    
    private static final double NOTIONAL = 1.0;
    
    private static final GeneratorSwapFixedON GENERATOR_OIS_EUR = GeneratorSwapFixedONMaster.getInstance().getGenerator("EUR1YEONIA", TARGET);
    private static final GeneratorSwapFixedON GENERATOR_OIS_USD = GeneratorSwapFixedONMaster.getInstance().getGenerator("USD1YFEDFUND", TARGET);
    private static final IndexON INDEX_ON_EUR = GENERATOR_OIS_EUR.getIndex();
    private static final IndexON INDEX_ON_USD = GENERATOR_OIS_USD.getIndex();
    private static final GeneratorDepositON GENERATOR_DEPOSIT_ON_EUR = new GeneratorDepositON("EUR Deposit ON", EUR, TARGET, INDEX_ON_EUR.getDayCount());
    private static final GeneratorDepositON GENERATOR_DEPOSIT_ON_USD = new GeneratorDepositON("USD Deposit ON", USD, TARGET, INDEX_ON_USD.getDayCount());
    private static final GeneratorSwapFixedIborMaster GENERATOR_SWAP_MASTER = GeneratorSwapFixedIborMaster.getInstance();
    private static final GeneratorSwapFixedIbor EUR1YEURIBOR3M = GENERATOR_SWAP_MASTER.getGenerator("EUR1YEURIBOR3M", TARGET);
    private static final GeneratorSwapFixedIbor USD6MLIBOR3M = GENERATOR_SWAP_MASTER.getGenerator("USD6MLIBOR3M", TARGET);
    
    private static final IborIndex EURIBOR3M = EUR1YEURIBOR3M.getIborIndex();
    private static final IborIndex USDLIBOR3M = USD6MLIBOR3M.getIborIndex();
    
    private static final IborIndex EUROLIBOR3M = new IborIndex(EUR, Period.ofMonths(3), 2, EURIBOR3M.getDayCount(), EURIBOR3M.getBusinessDayConvention(), true, "EUROLIBOR3M");
    private static final GeneratorFRA GENERATOR_USD_FRA_3M = new GeneratorFRA("GENERATOR USD FRA 3M", USDLIBOR3M, NYC);
    private static final GeneratorDepositIbor GENERATOR_EURIBOR3M = new GeneratorDepositIbor("GENERATOR_EURIBOR3M", EURIBOR3M, TARGET);
    private static final GeneratorDepositIbor GENERATOR_USDLIBOR3M = new GeneratorDepositIbor("GENERATOR_USDLIBOR3M", USDLIBOR3M, NYC);
    
    private static final GeneratorSwapXCcyIborIbor EURIBOR3MUSDLIBOR3M = new GeneratorSwapXCcyIborIbor("EURIBOR3MUSDLIBOR3M", EURIBOR3M, USDLIBOR3M, TARGET, NYC); // Spread on EUR leg
    private static final GeneratorForexSwap GENERATOR_FX_EURUSD = new GeneratorForexSwap("EURUSD", EUR, USD, TARGET, EURIBOR3M.getSpotLag(), EURIBOR3M.getBusinessDayConvention(), true);
    
    private static final ZonedDateTime NOW = DateUtils.getUTCDate(2011, 9, 28);
    
    private static final ZonedDateTimeDoubleTimeSeries TS_EMPTY = ImmutableZonedDateTimeDoubleTimeSeries.ofEmptyUTC();
    private static final ZonedDateTimeDoubleTimeSeries TS_ON_USD_WITH_TODAY = ImmutableZonedDateTimeDoubleTimeSeries.ofUTC(new ZonedDateTime[] {DateUtils.getUTCDate(2011, 9, 27),
        DateUtils.getUTCDate(2011, 9, 28) }, new double[] {0.07, 0.08 });
    private static final ZonedDateTimeDoubleTimeSeries TS_ON_USD_WITHOUT_TODAY = ImmutableZonedDateTimeDoubleTimeSeries.ofUTC(new ZonedDateTime[] {DateUtils.getUTCDate(2011, 9, 27),
        DateUtils.getUTCDate(2011, 9, 28) }, new double[] {0.07, 0.08 });
    private static final ZonedDateTimeDoubleTimeSeries[] TS_FIXED_OIS_USD_WITH_TODAY = new ZonedDateTimeDoubleTimeSeries[] {TS_EMPTY, TS_ON_USD_WITH_TODAY };
    private static final ZonedDateTimeDoubleTimeSeries[] TS_FIXED_OIS_USD_WITHOUT_TODAY = new ZonedDateTimeDoubleTimeSeries[] {TS_EMPTY, TS_ON_USD_WITHOUT_TODAY };
    
    private static final ZonedDateTimeDoubleTimeSeries TS_IBOR_USD3M_WITH_TODAY = ImmutableZonedDateTimeDoubleTimeSeries.ofUTC(new ZonedDateTime[] {DateUtils.getUTCDate(2011, 9, 27),
        DateUtils.getUTCDate(2011, 9, 28) }, new double[] {0.0035, 0.0036 });
    private static final ZonedDateTimeDoubleTimeSeries TS_IBOR_USD3M_WITHOUT_TODAY = ImmutableZonedDateTimeDoubleTimeSeries.ofUTC(new ZonedDateTime[] {DateUtils.getUTCDate(2011, 9, 27) },
                                                                                                                                  new double[] {0.0035 });
    
    private static final ZonedDateTimeDoubleTimeSeries TS_IBOR_EUR3M_WITH_TODAY = ImmutableZonedDateTimeDoubleTimeSeries.ofUTC(new ZonedDateTime[] {DateUtils.getUTCDate(2011, 9, 27),
        DateUtils.getUTCDate(2011, 9, 28) }, new double[] {0.0060, 0.0061 });
    private static final ZonedDateTimeDoubleTimeSeries TS_IBOR_EUR3M_WITHOUT_TODAY = ImmutableZonedDateTimeDoubleTimeSeries.ofUTC(new ZonedDateTime[] {DateUtils.getUTCDate(2011, 9, 27) },
                                                                                                                                  new double[] {0.0060 });
    
    private static final ZonedDateTimeDoubleTimeSeries[] TS_FIXED_IBOR_EUR3M_WITH_TODAY = new ZonedDateTimeDoubleTimeSeries[] {TS_IBOR_EUR3M_WITH_TODAY };
    private static final ZonedDateTimeDoubleTimeSeries[] TS_FIXED_IBOR_EUR3M_WITHOUT_TODAY = new ZonedDateTimeDoubleTimeSeries[] {TS_IBOR_EUR3M_WITHOUT_TODAY };
    private static final ZonedDateTimeDoubleTimeSeries[] TS_FIXED_IBOR_EURUSD3M_WITH_TODAY = new ZonedDateTimeDoubleTimeSeries[] {TS_IBOR_EUR3M_WITH_TODAY, TS_IBOR_USD3M_WITH_TODAY };
    private static final ZonedDateTimeDoubleTimeSeries[] TS_FIXED_IBOR_EURUSD3M_WITHOUT_TODAY = new ZonedDateTimeDoubleTimeSeries[] {TS_IBOR_EUR3M_WITHOUT_TODAY, TS_IBOR_USD3M_WITHOUT_TODAY };
    
    private static final String CURVE_NAME_DSC_EUR = "EUR Dsc";
    private static final String CURVE_NAME_FWD3_EUR = "EUR Fwd 3M";
    private static final String CURVE_NAME_DSC_USD = "USD Dsc";
    private static final String CURVE_NAME_FWD3_USD = "USD Fwd 3M";
    
    /** Market values for the dsc USD curve */
    private static final double[] DSC_USD_MARKET_QUOTES = new double[] {0.0010, 0.0010, 0.0010, 0.0010, 0.0010, 0.0010, 0.0010, 0.0010, 0.0015, 0.0020, 0.0035, 0.0050, 0.0130 };
    /** Generators for the dsc USD curve */
    private static final GeneratorInstrument<? extends GeneratorAttribute>[] DSC_USD_GENERATORS = new GeneratorInstrument<?>[] {GENERATOR_DEPOSIT_ON_USD, GENERATOR_DEPOSIT_ON_USD, GENERATOR_OIS_USD,
    GENERATOR_OIS_USD, GENERATOR_OIS_USD, GENERATOR_OIS_USD, GENERATOR_OIS_USD, GENERATOR_OIS_USD, GENERATOR_OIS_USD, GENERATOR_OIS_USD, GENERATOR_OIS_USD, GENERATOR_OIS_USD, GENERATOR_OIS_USD };
    /** Tenors for the dsc USD curve */
    private static final Period[] DSC_USD_TENOR = new Period[] {Period.ofDays(0), Period.ofDays(1), Period.ofMonths(1), Period.ofMonths(2),
    Period.ofMonths(3), Period.ofMonths(6), Period.ofMonths(9),
    Period.ofYears(1), Period.ofYears(2), Period.ofYears(3), Period.ofYears(4), Period.ofYears(5), Period.ofYears(10) };
    private static final GeneratorAttributeIR[] DSC_USD_ATTR = new GeneratorAttributeIR[DSC_USD_TENOR.length];
    static {
        for (int loopins = 0; loopins < 2; loopins++) {
            DSC_USD_ATTR[loopins] = new GeneratorAttributeIR(DSC_USD_TENOR[loopins], Period.ZERO);
        }
        for (int loopins = 2; loopins < DSC_USD_TENOR.length; loopins++) {
            DSC_USD_ATTR[loopins] = new GeneratorAttributeIR(DSC_USD_TENOR[loopins]);
        }
    }
    
    /** Market values for the Fwd 3M USD curve */
    private static final double[] FWD3_USD_MARKET_QUOTES = new double[] {0.0045, 0.0045, 0.0045, 0.0045, 0.0060, 0.0070, 0.0080, 0.0160 };
    /** Generators for the Fwd 3M USD curve */
    private static final GeneratorInstrument<? extends GeneratorAttribute>[] FWD3_USD_GENERATORS = new GeneratorInstrument<?>[] {GENERATOR_USDLIBOR3M, GENERATOR_USD_FRA_3M, USD6MLIBOR3M, USD6MLIBOR3M,
    USD6MLIBOR3M, USD6MLIBOR3M, USD6MLIBOR3M, USD6MLIBOR3M };
    /** Tenors for the Fwd 3M USD curve */
    private static final Period[] FWD3_USD_TENOR = new Period[] {Period.ofMonths(0), Period.ofMonths(6), Period.ofYears(1), Period.ofYears(2),
    Period.ofYears(3), Period.ofYears(4), Period.ofYears(5),
    Period.ofYears(10) };
    private static final GeneratorAttributeIR[] FWD3_USD_ATTR = new GeneratorAttributeIR[FWD3_USD_TENOR.length];
    static {
        for (int loopins = 0; loopins < FWD3_USD_TENOR.length; loopins++) {
            FWD3_USD_ATTR[loopins] = new GeneratorAttributeIR(FWD3_USD_TENOR[loopins]);
        }
    }
    
    /** Market values for the dsc EUR curve */
    private static final double[] DSC_EUR_MARKET_QUOTES = new double[] {0.0020, 0.0020, 0.0020, 0.0020, 0.0020, 0.0020, 0.0020, 0.0020, 0.1020, 0.0020, 0.0020, 0.1020, 0.0020 };
    /** Generators for the dsc EUR curve */
    private static final GeneratorInstrument<? extends GeneratorAttribute>[] DSC_EUR_GENERATORS = new GeneratorInstrument<?>[] {GENERATOR_DEPOSIT_ON_EUR, GENERATOR_DEPOSIT_ON_EUR, GENERATOR_FX_EURUSD,
    GENERATOR_FX_EURUSD, GENERATOR_FX_EURUSD, GENERATOR_FX_EURUSD, GENERATOR_FX_EURUSD, GENERATOR_FX_EURUSD, EURIBOR3MUSDLIBOR3M, EURIBOR3MUSDLIBOR3M, EURIBOR3MUSDLIBOR3M, EURIBOR3MUSDLIBOR3M,
    EURIBOR3MUSDLIBOR3M };
    /** Tenors for the dsc EUR curve */
    private static final Period[] DSC_EUR_TENOR = new Period[] {Period.ofDays(0), Period.ofDays(1), Period.ofMonths(1), Period.ofMonths(2),
    Period.ofMonths(3), Period.ofMonths(6), Period.ofMonths(9),
    Period.ofYears(1), Period.ofYears(2), Period.ofYears(3), Period.ofYears(4), Period.ofYears(5), Period.ofYears(10) };
    private static final GeneratorAttribute[] DSC_EUR_ATTR = new GeneratorAttribute[DSC_EUR_TENOR.length];
    static {
        for (int loopins = 0; loopins < 2; loopins++) {
            DSC_EUR_ATTR[loopins] = new GeneratorAttributeIR(DSC_EUR_TENOR[loopins], Period.ZERO);
        }
        for (int loopins = 2; loopins < DSC_EUR_TENOR.length; loopins++) {
            DSC_EUR_ATTR[loopins] = new GeneratorAttributeFX(DSC_EUR_TENOR[loopins], FX_MATRIX);
        }
    }
    
    /** Market values for the Fwd 3M EUR curve */
    private static final double[] FWD3_EUR_MARKET_QUOTES = new double[] {0.0045, 0.0045, 0.0045, 0.0045, 0.0050, 0.0060, 0.0085, 0.0160 };
    /** Generators for the Fwd 3M USD curve */
    private static final GeneratorInstrument<? extends GeneratorAttribute>[] FWD3_EUR_GENERATORS = new GeneratorInstrument<?>[] {GENERATOR_EURIBOR3M, EUR1YEURIBOR3M, EUR1YEURIBOR3M, EUR1YEURIBOR3M,
    EUR1YEURIBOR3M, EUR1YEURIBOR3M, EUR1YEURIBOR3M, EUR1YEURIBOR3M };
    /** Tenors for the Fwd 3M USD curve */
    private static final Period[] FWD3_EUR_TENOR = new Period[] {Period.ofMonths(0), Period.ofMonths(6), Period.ofYears(1), Period.ofYears(2),
    Period.ofYears(3), Period.ofYears(4), Period.ofYears(5),
    Period.ofYears(10) };
    private static final GeneratorAttributeIR[] FWD3_EUR_ATTR = new GeneratorAttributeIR[FWD3_EUR_TENOR.length];
    static {
        for (int loopins = 0; loopins < FWD3_EUR_TENOR.length; loopins++) {
            FWD3_EUR_ATTR[loopins] = new GeneratorAttributeIR(FWD3_EUR_TENOR[loopins]);
        }
    }
    
    /** Standard USD discounting curve instrument definitions */
    private static final InstrumentDefinition<?>[] DEFINITIONS_DSC_USD;
    /** Standard USD Forward 3M curve instrument definitions */
    private static final InstrumentDefinition<?>[] DEFINITIONS_FWD3_USD;
    /** Standard EUR discounting curve instrument definitions */
    private static final InstrumentDefinition<?>[] DEFINITIONS_DSC_EUR;
    /** Standard EUR Forward 3M curve instrument definitions */
    private static final InstrumentDefinition<?>[] DEFINITIONS_FWD3_EUR;
    
    /** Units of curves */
    private static final int[] NB_UNITS = new int[] {3};
    private static final int NB_BLOCKS = NB_UNITS.length;
    private static final InstrumentDefinition<?>[][][][] DEFINITIONS_UNITS = new InstrumentDefinition<?>[NB_BLOCKS][][][];
    private static final GeneratorYDCurve[][][] GENERATORS_UNITS = new GeneratorYDCurve[NB_BLOCKS][][];
    private static final String[][][] NAMES_UNITS = new String[NB_BLOCKS][][];
    
    private static final MulticurveProviderDiscount MULTICURVE_KNOWN_DATA = new MulticurveProviderDiscount(FX_MATRIX);
    
    private static final LinkedHashMap<String, Currency> DSC_MAP = new LinkedHashMap<>();
    private static final LinkedHashMap<String, IndexON[]> FWD_ON_MAP = new LinkedHashMap<>();
    private static final LinkedHashMap<String, IborIndex[]> FWD_IBOR_MAP = new LinkedHashMap<>();
    
    static {
        DEFINITIONS_DSC_USD = getDefinitions(DSC_USD_MARKET_QUOTES, DSC_USD_GENERATORS, DSC_USD_ATTR);
        DEFINITIONS_FWD3_USD = getDefinitions(FWD3_USD_MARKET_QUOTES, FWD3_USD_GENERATORS, FWD3_USD_ATTR);
        DEFINITIONS_DSC_EUR = getDefinitions(DSC_EUR_MARKET_QUOTES, DSC_EUR_GENERATORS, DSC_EUR_ATTR);
        DEFINITIONS_FWD3_EUR = getDefinitions(FWD3_EUR_MARKET_QUOTES, FWD3_EUR_GENERATORS, FWD3_EUR_ATTR);
        
        for (int loopblock = 0; loopblock < NB_BLOCKS; loopblock++) {
            DEFINITIONS_UNITS[loopblock] = new InstrumentDefinition<?>[NB_UNITS[loopblock]][][];
            GENERATORS_UNITS[loopblock] = new GeneratorYDCurve[NB_UNITS[loopblock]][];
            NAMES_UNITS[loopblock] = new String[NB_UNITS[loopblock]][];
        }
        
        DEFINITIONS_UNITS[0][0] = new InstrumentDefinition<?>[][] {DEFINITIONS_DSC_USD };
        DEFINITIONS_UNITS[0][1] = new InstrumentDefinition<?>[][] {DEFINITIONS_FWD3_USD };
        DEFINITIONS_UNITS[0][2] = new InstrumentDefinition<?>[][] {DEFINITIONS_DSC_EUR, DEFINITIONS_FWD3_EUR };
        
        final GeneratorYDCurve genIntLin = new GeneratorCurveYieldInterpolated(MATURITY_CALCULATOR, INTERPOLATOR_LINEAR);
        
        GENERATORS_UNITS[0][0] = new GeneratorYDCurve[] {genIntLin };
        GENERATORS_UNITS[0][1] = new GeneratorYDCurve[] {genIntLin };
        GENERATORS_UNITS[0][2] = new GeneratorYDCurve[] {genIntLin, genIntLin };
        
        NAMES_UNITS[0][0] = new String[] {CURVE_NAME_DSC_USD };
        NAMES_UNITS[0][1] = new String[] {CURVE_NAME_FWD3_USD };
        NAMES_UNITS[0][2] = new String[] {CURVE_NAME_DSC_EUR, CURVE_NAME_FWD3_EUR };
        
        // Note: the sensitivity is computed in the order of the curve names. The names order should be in line with the units definition order.
        DSC_MAP.put(CURVE_NAME_DSC_USD, USD);
        DSC_MAP.put(CURVE_NAME_DSC_EUR, EUR);
        
        FWD_ON_MAP.put(CURVE_NAME_DSC_USD, new IndexON[] {INDEX_ON_USD });
        
        FWD_IBOR_MAP.put(CURVE_NAME_FWD3_USD, new IborIndex[] {USDLIBOR3M });
        FWD_IBOR_MAP.put(CURVE_NAME_FWD3_EUR, new IborIndex[] {EURIBOR3M, EUROLIBOR3M });
    }
    
    @SuppressWarnings("unchecked")
    public static InstrumentDefinition<?>[] getDefinitions(final double[] marketQuotes, @SuppressWarnings("rawtypes") final GeneratorInstrument[] generators, final GeneratorAttribute[] attribute) {
        final InstrumentDefinition<?>[] definitions = new InstrumentDefinition<?>[marketQuotes.length];
        for (int loopmv = 0; loopmv < marketQuotes.length; loopmv++) {
            definitions[loopmv] = generators[loopmv].generateInstrument(NOW, marketQuotes[loopmv], NOTIONAL, attribute[loopmv]);
        }
        return definitions;
    }
    
    private static List<Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle>> CURVES_PAR_SPREAD_MQ_WITHOUT_TODAY_BLOCK = new ArrayList<>();
    
    // Calculators
    private static final PresentValueDiscountingCalculator PVDC = PresentValueDiscountingCalculator.getInstance();
    private static final PresentValueCurveSensitivityDiscountingCalculator PVCSDC = PresentValueCurveSensitivityDiscountingCalculator.getInstance();
    private static final ParSpreadMarketQuoteDiscountingCalculator PSMQDC = ParSpreadMarketQuoteDiscountingCalculator.getInstance();
    private static final ParSpreadMarketQuoteCurveSensitivityDiscountingCalculator PSMQCSDC = ParSpreadMarketQuoteCurveSensitivityDiscountingCalculator.getInstance();
    
    private static final MulticurveDiscountBuildingRepository CURVE_BUILDING_REPOSITORY = new MulticurveDiscountBuildingRepository(TOLERANCE_ROOT, TOLERANCE_ROOT, STEP_MAX);
    
    private static final double TOLERANCE_CAL = 1.0E-9;
    /*
     public void curveConstructionTest(final InstrumentDefinition<?>[][][] definitions, final MulticurveProviderDiscount curves, final boolean withToday, final int block) {
     final int nbBlocks = definitions.length;
     for (int loopblock = 0; loopblock < nbBlocks; loopblock++) {
     final InstrumentDerivative[][] instruments = convert(definitions[loopblock], withToday);
     final double[][] pv = new double[instruments.length][];
     for (int loopcurve = 0; loopcurve < instruments.length; loopcurve++) {
     pv[loopcurve] = new double[instruments[loopcurve].length];
     for (int loopins = 0; loopins < instruments[loopcurve].length; loopins++) {
     pv[loopcurve][loopins] = curves.getFxRates().convert(instruments[loopcurve][loopins].accept(PVDC, curves), EUR).getAmount();
     assertEquals("Curve construction: block " + block + ", unit " + loopblock + " - instrument " + loopins, 0, pv[loopcurve][loopins], TOLERANCE_CAL);
     }
     }
     }
     }*/
    
    @SuppressWarnings("unchecked")
    private static Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> makeCurvesFromDefinitions(final InstrumentDefinition<?>[][][] definitions, final GeneratorYDCurve[][] curveGenerators,
                                                                                                        final String[][] curveNames, final MulticurveProviderDiscount knownData, final InstrumentDerivativeVisitor<MulticurveProviderInterface, Double> calculator,
                                                                                                        final InstrumentDerivativeVisitor<MulticurveProviderInterface, MulticurveSensitivity> sensitivityCalculator, final boolean withToday) {
        final int nUnits = definitions.length;
        final MultiCurveBundle<GeneratorYDCurve>[] curveBundles = new MultiCurveBundle[nUnits];
        for (int i = 0; i < nUnits; i++) {
            final int nCurves = definitions[i].length;
            final SingleCurveBundle<GeneratorYDCurve>[] singleCurves = new SingleCurveBundle[nCurves];
            for (int j = 0; j < nCurves; j++) {
		        final int nInstruments = definitions[i][j].length;
		        final InstrumentDerivative[] derivatives = new InstrumentDerivative[nInstruments];
		        final double[] rates = new double[nInstruments];
		        for(int k = 0; k < nInstruments; k++) {
                    derivatives[k] = convert(definitions[i][j][k], withToday);
                    rates[k] = initialGuess(definitions[i][j][k]);
		        }
		        final GeneratorYDCurve generator = curveGenerators[i][j].finalGenerator(derivatives);
		        final double[] initialGuess = generator.initialGuess(rates);
		        singleCurves[j] = new SingleCurveBundle<>(curveNames[i][j], derivatives, initialGuess, generator);
            }
            curveBundles[i] = new MultiCurveBundle<>(singleCurves);
        }
        return CURVE_BUILDING_REPOSITORY.makeCurvesFromDerivatives(curveBundles, knownData, DSC_MAP, FWD_IBOR_MAP, FWD_ON_MAP, calculator, sensitivityCalculator);
    }
    
    private static InstrumentDerivative[][] convert(final InstrumentDefinition<?>[][] definitions, final boolean withToday) {
        final InstrumentDerivative[][] instruments = new InstrumentDerivative[definitions.length][];
        for (int loopcurve = 0; loopcurve < definitions.length; loopcurve++) {
            instruments[loopcurve] = new InstrumentDerivative[definitions[loopcurve].length];
            int loopins = 0;
            for (final InstrumentDefinition<?> instrument : definitions[loopcurve]) {
		        InstrumentDerivative ird;
		        if (instrument instanceof SwapFixedONDefinition) {
                    ird = ((SwapFixedONDefinition) instrument).toDerivative(NOW, getTSSwapFixedON(withToday));
		        } else {
                    if (instrument instanceof SwapFixedIborDefinition) {
                        ird = ((SwapFixedIborDefinition) instrument).toDerivative(NOW, getTSSwapFixedIbor(withToday));
                    } else {
                        if (instrument instanceof SwapXCcyIborIborDefinition) {
                            ird = ((SwapXCcyIborIborDefinition) instrument).toDerivative(NOW, getTSSwapXCcyIborIbor(withToday));
                        } else {
                            ird = instrument.toDerivative(NOW);
                        }
                    }
		        }
		        instruments[loopcurve][loopins++] = ird;
            }
        }
        return instruments;
    }
    
    private static InstrumentDerivative convert(final InstrumentDefinition<?> instrument, final boolean withToday) {
        InstrumentDerivative ird;
        if (instrument instanceof SwapFixedONDefinition) {
            ird = ((SwapFixedONDefinition) instrument).toDerivative(NOW, getTSSwapFixedON(withToday));
        } else {
            if (instrument instanceof SwapFixedIborDefinition) {
		        ird = ((SwapFixedIborDefinition) instrument).toDerivative(NOW, getTSSwapFixedIbor(withToday));
            } else {
		        if (instrument instanceof SwapXCcyIborIborDefinition) {
                    ird = ((SwapXCcyIborIborDefinition) instrument).toDerivative(NOW, getTSSwapXCcyIborIbor(withToday));
		        } else {
                    ird = instrument.toDerivative(NOW);
		        }
            }
        }
        return ird;
    }
    
    private static ZonedDateTimeDoubleTimeSeries[] getTSSwapFixedON(final Boolean withToday) {
        return withToday ? TS_FIXED_OIS_USD_WITH_TODAY : TS_FIXED_OIS_USD_WITHOUT_TODAY;
    }
    
    private static ZonedDateTimeDoubleTimeSeries[] getTSSwapFixedIbor(final Boolean withToday) { // TODO: different fixing by currency and for 3 and 6 m
        return withToday ? TS_FIXED_IBOR_EUR3M_WITH_TODAY : TS_FIXED_IBOR_EUR3M_WITHOUT_TODAY;
    }
    
    private static ZonedDateTimeDoubleTimeSeries[] getTSSwapXCcyIborIbor(final Boolean withToday) { // TODO: different currencies
        return withToday ? TS_FIXED_IBOR_EURUSD3M_WITH_TODAY : TS_FIXED_IBOR_EURUSD3M_WITHOUT_TODAY;
    }
    
    private static double initialGuess(final InstrumentDefinition<?> instrument) {
        if (instrument instanceof SwapFixedONDefinition) {
            return ((SwapFixedONDefinition) instrument).getFixedLeg().getNthPayment(0).getRate();
        }
        if (instrument instanceof SwapFixedIborDefinition) {
            return ((SwapFixedIborDefinition) instrument).getFixedLeg().getNthPayment(0).getRate();
        }
        if (instrument instanceof ForwardRateAgreementDefinition) {
            return ((ForwardRateAgreementDefinition) instrument).getRate();
        }
        if (instrument instanceof CashDefinition) {
            return ((CashDefinition) instrument).getRate();
        }
        if (instrument instanceof InterestRateFutureTransactionDefinition) {
            return 1 - ((InterestRateFutureTransactionDefinition) instrument).getTransactionPrice();
        }
        return 0.01;
    }
    
    public static void main(String[] args) {
        /******************
		 * Test to print out discount factors and forward 3m Libor rates
		 * Note this is not generating dates properly due to bumps!
		 ******************/
		DecimalFormat dfFormat = new DecimalFormat("0.00000");
		// initialise startDate to t+2, and declare endDate
		ZonedDateTime startDate = ScheduleCalculator.getAdjustedDate(NOW, USDLIBOR3M.getSpotLag(), NYC);
		ZonedDateTime endDate;
		// getForwardRate takes time periods as double, so need to declare variables that will be time period now and date
		double startTime;
		double endTime;
        
		final int numDates = 40;
		int loopblock = 0;
		
		Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> pair = makeCurvesFromDefinitions(DEFINITIONS_UNITS[loopblock], GENERATORS_UNITS[loopblock], NAMES_UNITS[loopblock], MULTICURVE_KNOWN_DATA, PSMQDC, PSMQCSDC, false);
		MulticurveProviderDiscount curve = pair.getFirst();
		
		for (int i = 0; i < numDates; i++) {
			startTime = TimeCalculator.getTimeBetween(NOW, startDate);
			// increment endDate by 3 months (corresponding to tenor of ibor index)... this is problematic due to bumps!
			endDate = ScheduleCalculator.getAdjustedDate(startDate, USDLIBOR3M, NYC);
			endTime = TimeCalculator.getTimeBetween(NOW, endDate);
			
			final double accrualFactor = USDLIBOR3M.getDayCount().getDayCountFraction(startDate, endDate);
			final double discountUSD = curve.getDiscountFactor(USD, startTime);
			final double discountEUR = curve.getDiscountFactor(EUR, startTime);
			double fwd3MUSD = curve.getForwardRate(USDLIBOR3M, startTime, endTime, accrualFactor);
			double fwd3MEUR = curve.getForwardRate(EURIBOR3M, startTime, endTime, accrualFactor);
			
			System.out.println(startDate+" "+endDate+" "+"DF USD: "+dfFormat.format(discountUSD)+" "+"DF EUR: "+dfFormat.format(discountEUR)+" 3mL: "+dfFormat.format(fwd3MUSD)+" 3mE: "+dfFormat.format(fwd3MEUR));
			// increment startDate of next period to end of this period
			startDate = endDate;
		}
		System.out.println("====================================================");
        
		/******************
		 * XCCY Basis Swap pricing
		 *
		 ******************/
		// create a XCCY Ibor-Ibor swap definition
		final Period SWAP_TENOR = Period.ofYears(5);
		final ZonedDateTime SETTLEMENT_DATE = ScheduleCalculator.getAdjustedDate(NOW, EURIBOR3MUSDLIBOR3M.getSpotLag(), NYC);
		double NOTIONAL_EUR = 100000000; //100m
		double NOTIONAL_USD = NOTIONAL_EUR * FX_EURUSD;
		final double SPREAD = 0.002; //NOTE: spread goes on first leg by default using the from method
		// true means is payer of the first leg
		SwapXCcyIborIborDefinition SWAP_XCCY_BASIS_DEFINITION = SwapXCcyIborIborDefinition.from(SETTLEMENT_DATE, SWAP_TENOR, EURIBOR3MUSDLIBOR3M, NOTIONAL_EUR, NOTIONAL_USD, SPREAD, true, TARGET, NYC);
		
		//creating today's swap instrument from a swap definition
		InstrumentDerivative swap = SWAP_XCCY_BASIS_DEFINITION.toDerivative(NOW);
        
		MultipleCurrencyAmount pv = swap.accept(PVDC, curve);
		double parSpread = swap.accept(PSMQDC, curve);
		System.out.println("PV = "+pv+" Fair rate = "+parSpread);
		
		SWAP_XCCY_BASIS_DEFINITION = SwapXCcyIborIborDefinition.from(SETTLEMENT_DATE, SWAP_TENOR, EURIBOR3MUSDLIBOR3M, NOTIONAL_EUR, NOTIONAL_USD, SPREAD + parSpread, true, TARGET, NYC);
		swap = SWAP_XCCY_BASIS_DEFINITION.toDerivative(NOW);
        
		pv = swap.accept(PVDC, curve);
		parSpread = swap.accept(PSMQDC, curve);
		System.out.println("PV = "+pv+" Fair rate = "+parSpread);
		System.out.println("====================================================");
		
	    final CurveBuildingBlockBundle block = pair.getSecond();
		final ParameterSensitivityParameterCalculator<MulticurveProviderInterface> PSC = new ParameterSensitivityParameterCalculator<>(PVCSDC);
	    final MarketQuoteSensitivityBlockCalculator<MulticurveProviderInterface> MQSC = new MarketQuoteSensitivityBlockCalculator<>(PSC);
	    final MultipleCurrencyParameterSensitivity mqs = MQSC.fromInstrument(swap, curve, block);
		
	    System.out.println(mqs.toString());
		
    }
}
