package com.opengamma.gammatrace.marketconstruction.sabr;

import org.junit.Test;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.model.option.parameters.BlackFlatSwaptionParameters;
import com.opengamma.analytics.financial.provider.calculator.blackswaption.PresentValueBlackSwaptionCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.testdatasets.BlackDataSets;
import com.opengamma.gammatrace.testdatasets.MulticurveProviderDiscountDataSets;
import com.opengamma.util.time.DateUtils;

public class SABRSwaptionCurveMakerTest {
	private static final Calendar CALENDAR = new MondayToFridayCalendar("CALENDAR");
	private static final GeneratorSwapFixedIborTemplate FIXED_IBOR_MASTER = GeneratorSwapFixedIborTemplate.getInstance();
	private static final GeneratorSwapFixedIbor FIXED_IBOR_SWAP = FIXED_IBOR_MASTER.getGenerator(Generators.USD6MLIBOR3M, CALENDAR);
	private static final ZonedDateTime CURVE_TIME = DateUtils.getUTCDate(2015, 2, 12, 0, 0);
	
	private static final PresentValueDiscountingCalculator PVDC = PresentValueDiscountingCalculator.getInstance(); 
	private static final PresentValueBlackSwaptionCalculator PVBSC = PresentValueBlackSwaptionCalculator.getInstance();
	private static final double TOLERANCE = 0.00001;
	
	private static final MulticurveProviderDiscount MULTICURVES = MulticurveProviderDiscountDataSets.createMulticurveUsd();
	private static final BlackFlatSwaptionParameters BLACK_PARAMETERS = BlackDataSets.createBlackSwaptionUSD3();
	
	@Test
	public void shouldRepriceSwaptionStraddles() {
		
	}
}
