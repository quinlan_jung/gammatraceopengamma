package com.opengamma.gammatrace.testdatasets;

import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.index.IndexIborMaster;
import com.opengamma.analytics.financial.instrument.index.IndexON;
import com.opengamma.analytics.financial.instrument.index.IndexONMaster;
import com.opengamma.analytics.financial.model.interestrate.curve.YieldAndDiscountCurve;
import com.opengamma.analytics.financial.model.interestrate.curve.YieldCurve;
import com.opengamma.analytics.financial.provider.description.inflation.InflationIssuerProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.math.curve.InterpolatedDoublesCurve;
import com.opengamma.analytics.math.interpolation.CombinedInterpolatorExtrapolatorFactory;
import com.opengamma.analytics.math.interpolation.Interpolator1D;
import com.opengamma.analytics.math.interpolation.Interpolator1DFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;
import com.opengamma.util.money.Currency;

/**
 * Sets of market data used in tests.
 */
public class MulticurveProviderDiscountDataSets {

  private static final Interpolator1D LINEAR_FLAT = CombinedInterpolatorExtrapolatorFactory.getInterpolator(Interpolator1DFactory.LINEAR, Interpolator1DFactory.FLAT_EXTRAPOLATOR,
      Interpolator1DFactory.FLAT_EXTRAPOLATOR);

  private static final Calendar CALENDAR_USD = new MondayToFridayCalendar("USD");
  private static final Calendar CALENDAR_EUR = new MondayToFridayCalendar("EUR");

  private static final double[] USD_DSC_TIME = new double[] {0.0, 0.5, 1.0, 2.0, 5.0, 10.0 };
  private static final double[] USD_DSC_RATE = new double[] {0.0100, 0.0120, 0.0120, 0.0140, 0.0140, 0.0140 };
  private static final String USD_DSC_NAME = "USD Dsc";
  private static final YieldAndDiscountCurve USD_DSC = new YieldCurve(USD_DSC_NAME, new InterpolatedDoublesCurve(USD_DSC_TIME, USD_DSC_RATE, LINEAR_FLAT, true, USD_DSC_NAME));
  private static final double[] USD_FWD3_TIME = new double[] {0.0, 0.5, 1.0, 2.0, 5.0, 10.0 };
  private static final double[] USD_FWD3_RATE = new double[] {0.0150, 0.0125, 0.0150, 0.0175, 0.0150, 0.0150 };
  private static final String USD_FWD3_NAME = "USD LIBOR 3M";
  private static final YieldAndDiscountCurve USD_FWD3 = new YieldCurve(USD_FWD3_NAME, new InterpolatedDoublesCurve(USD_FWD3_TIME, USD_FWD3_RATE, LINEAR_FLAT, true, USD_FWD3_NAME));
  private static final double[] USD_FWD6_TIME = new double[] {0.0, 0.5, 1.0, 2.0, 5.0, 10.0 };
  private static final double[] USD_FWD6_RATE = new double[] {0.0175, 0.0150, 0.0170, 0.0190, 0.0165, 0.0165 };
  private static final String USD_FWD6_NAME = "USD LIBOR 6M";
  private static final YieldAndDiscountCurve USD_FWD6 = new YieldCurve(USD_FWD6_NAME, new InterpolatedDoublesCurve(USD_FWD6_TIME, USD_FWD6_RATE, LINEAR_FLAT, true, USD_FWD6_NAME));

  private static final double[] EUR_DSC_TIME = new double[] {0.0, 0.5, 1.0, 2.0, 5.0, 10.0 };
  private static final double[] EUR_DSC_RATE = new double[] {0.0150, 0.0125, 0.0150, 0.0175, 0.0150, 0.0150 };
  private static final String EUR_DSC_NAME = "EUR Dsc";
  private static final YieldAndDiscountCurve EUR_DSC = new YieldCurve(EUR_DSC_NAME, new InterpolatedDoublesCurve(EUR_DSC_TIME, EUR_DSC_RATE, LINEAR_FLAT, true, EUR_DSC_NAME));
  private static final double[] EUR_FWD3_TIME = new double[] {0.0, 0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 10.0 };
  private static final double[] EUR_FWD3_RATE = new double[] {0.0150, 0.0125, 0.0150, 0.0175, 0.0175, 0.0190, 0.0200, 0.0210 };
  private static final String EUR_FWD3_NAME = "EUR EURIBOR 3M";
  private static final YieldAndDiscountCurve EUR_FWD3 = new YieldCurve(EUR_FWD3_NAME, new InterpolatedDoublesCurve(EUR_FWD3_TIME, EUR_FWD3_RATE, LINEAR_FLAT, true, EUR_FWD3_NAME));
  private static final double[] EUR_FWD6_TIME = new double[] {0.0, 0.5, 1.0, 2.0, 5.0, 10.0 };
  private static final double[] EUR_FWD6_RATE = new double[] {0.0150, 0.0125, 0.0150, 0.0175, 0.0150, 0.0150 };
  private static final String EUR_FWD6_NAME = "EUR EURIBOR 6M";
  private static final YieldAndDiscountCurve EUR_FWD6 = new YieldCurve(EUR_FWD6_NAME, new InterpolatedDoublesCurve(EUR_FWD6_TIME, EUR_FWD6_RATE, LINEAR_FLAT, true, EUR_FWD6_NAME));

  private static final double[] GBP_DSC_TIME = new double[] {0.0, 0.5, 1.0, 2.0, 5.0, 10.0 };
  private static final double[] GBP_DSC_RATE = new double[] {0.0150, 0.0125, 0.0150, 0.0175, 0.0150, 0.0150 };
  private static final String GBP_DSC_NAME = "GBP Dsc";
  private static final YieldAndDiscountCurve GBP_DSC = new YieldCurve(GBP_DSC_NAME, new InterpolatedDoublesCurve(EUR_DSC_TIME, EUR_DSC_RATE, LINEAR_FLAT, true, EUR_DSC_NAME));
  private static final double[] GBP_FWD3_TIME = new double[] {0.0, 0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 10.0 };
  private static final double[] GBP_FWD3_RATE = new double[] {0.0150, 0.0125, 0.0150, 0.0175, 0.0175, 0.0190, 0.0200, 0.0210 };
  private static final String GBP_FWD3_NAME = "GBP LIBOR 3M";
  private static final YieldAndDiscountCurve GBP_FWD3 = new YieldCurve(GBP_FWD3_NAME, new InterpolatedDoublesCurve(EUR_FWD3_TIME, EUR_FWD3_RATE, LINEAR_FLAT, true, EUR_FWD3_NAME));
  private static final double[] GBP_FWD6_TIME = new double[] {0.0, 0.5, 1.0, 2.0, 5.0, 10.0 };
  private static final double[] GBP_FWD6_RATE = new double[] {0.0150, 0.0125, 0.0150, 0.0175, 0.0150, 0.0150 };
  private static final String GBP_FWD6_NAME = "GBP LIBOR 6M";
  private static final YieldAndDiscountCurve GBP_FWD6 = new YieldCurve(GBP_FWD6_NAME, new InterpolatedDoublesCurve(EUR_FWD6_TIME, EUR_FWD6_RATE, LINEAR_FLAT, true, EUR_FWD6_NAME));
  
  private static final IborTemplateByName MASTER_IBOR_INDEX = IborTemplateByName.getInstance();
  private static final IborIndex USDLIBOR3M = MASTER_IBOR_INDEX.getIndex(Indicies.USDLIBOR3M);
  private static final IborIndex USDLIBOR6M = MASTER_IBOR_INDEX.getIndex(Indicies.USDLIBOR6M);
  private static final IborIndex EURIBOR3M = MASTER_IBOR_INDEX.getIndex(Indicies.EURIBOR3M);
  private static final IborIndex EURIBOR6M = MASTER_IBOR_INDEX.getIndex(Indicies.EURIBOR6M);
  private static final IborIndex GBPLIBOR3M = MASTER_IBOR_INDEX.getIndex(Indicies.GBPLIBOR3M);
  private static final IborIndex GBPLIBOR6M = MASTER_IBOR_INDEX.getIndex(Indicies.GBPLIBOR6M);
  
  private static final IndexON SONIA = IndexONMaster.getInstance().getIndex("SONIA");
  private static final IndexON EONIA = IndexONMaster.getInstance().getIndex("EONIA");
  private static final IndexON FEDFUND = IndexONMaster.getInstance().getIndex("FED FUND");
  
  private static final FXMatrix EURUSD = new FXMatrix(Currency.EUR, Currency.USD, 1.2);
  private static final FXMatrix GBPUSD = new FXMatrix(Currency.GBP, Currency.USD, 1.5);

  private static final MulticurveProviderDiscount MULTICURVES_EUR_USD = new MulticurveProviderDiscount();
  static {
    MULTICURVES_EUR_USD.setCurve(Currency.USD, USD_DSC);
    MULTICURVES_EUR_USD.setCurve(FEDFUND, USD_DSC);
    MULTICURVES_EUR_USD.setCurve(Currency.EUR, EUR_DSC);
    MULTICURVES_EUR_USD.setCurve(USDLIBOR3M, USD_FWD3);
    MULTICURVES_EUR_USD.setCurve(USDLIBOR6M, USD_FWD6);
    MULTICURVES_EUR_USD.setCurve(EONIA, EUR_DSC);
    MULTICURVES_EUR_USD.setCurve(EURIBOR3M, EUR_FWD3);
    MULTICURVES_EUR_USD.setCurve(EURIBOR6M, EUR_FWD6);
    MULTICURVES_EUR_USD.setForexMatrix(EURUSD);
  }
  
  private static final MulticurveProviderDiscount MULTICURVES_GBP_USD = new MulticurveProviderDiscount();
  static {
    MULTICURVES_GBP_USD.setCurve(Currency.USD, USD_DSC);
    MULTICURVES_GBP_USD.setCurve(FEDFUND, USD_DSC);
    MULTICURVES_GBP_USD.setCurve(Currency.GBP, GBP_DSC);
    MULTICURVES_GBP_USD.setCurve(USDLIBOR3M, USD_FWD3);
    MULTICURVES_GBP_USD.setCurve(USDLIBOR6M, USD_FWD6);
    MULTICURVES_GBP_USD.setCurve(SONIA, GBP_DSC);
    MULTICURVES_GBP_USD.setCurve(GBPLIBOR3M, GBP_FWD3);
    MULTICURVES_GBP_USD.setCurve(GBPLIBOR6M, GBP_FWD6);
    MULTICURVES_GBP_USD.setForexMatrix(GBPUSD);
  }
  
  private static final MulticurveProviderDiscount MULTICURVES_USD = new MulticurveProviderDiscount();
  static {
	    MULTICURVES_USD.setCurve(Currency.USD, USD_DSC);
	    MULTICURVES_USD.setCurve(FEDFUND, USD_DSC);
	    MULTICURVES_USD.setCurve(USDLIBOR3M, USD_FWD3);
	    MULTICURVES_USD.setCurve(USDLIBOR6M, USD_FWD6);
  }
  
  private static final MulticurveProviderDiscount MULTICURVES_EUR = new MulticurveProviderDiscount();
  static {
	    MULTICURVES_EUR.setCurve(Currency.EUR, EUR_DSC);
	    MULTICURVES_EUR.setCurve(EONIA, EUR_DSC);
	    MULTICURVES_EUR.setCurve(EURIBOR3M, EUR_FWD3);
	    MULTICURVES_EUR.setCurve(EURIBOR6M, EUR_FWD6);
  }
  

   /**
   * Returns a multi-curves provider with two currencies (EUR, USD), four Ibor indexes (Euribor3M, Euribor6M, UsdLibor3M, UsdLibor6M).
   * @return The provider.
   */
  public static MulticurveProviderDiscount createMulticurveEurUsd() {
    return MULTICURVES_EUR_USD;
  }

  public static MulticurveProviderDiscount createMulticurveGbpUsd() {
	    return MULTICURVES_GBP_USD;
  }
  
  public static MulticurveProviderDiscount createMulticurveUsd() {
	    return MULTICURVES_USD;
  }
  public static MulticurveProviderDiscount createMulticurveEur() {
	    return MULTICURVES_EUR;
  }
  
  public static IborIndex[] getIndexesIborMulticurveEurUsd() {
    return new IborIndex[] {EURIBOR3M, EURIBOR6M, USDLIBOR3M, USDLIBOR6M };
  }

  public static IndexON[] getIndexesON() {
    return new IndexON[] {FEDFUND, EONIA };
  }

  public static Calendar getEURCalendar() {
    return CALENDAR_EUR;
  }

  public static Calendar getUSDCalendar() {
    return CALENDAR_USD;
  }
}
 


