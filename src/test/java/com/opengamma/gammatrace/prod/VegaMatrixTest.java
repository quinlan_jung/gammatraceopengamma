package com.opengamma.gammatrace.prod;

import java.util.HashMap;

import org.junit.Test;

import com.opengamma.analytics.util.amount.SurfaceValue;
import com.opengamma.gammatrace.rates.calculator.SABRSwaptionVegaStructure;
import com.opengamma.util.tuple.DoublesPair;

public class VegaMatrixTest {
	private static Double[] expiries = new Double[] {1.0 , 2.0};
	private static Double[] tenors = new Double[] {1.0 , 2.0};
	private static HashMap<DoublesPair, Double> map = new HashMap<>(); 
	static {
		map.put(DoublesPair.of(1.0, 1.0), 50.0);
		map.put(DoublesPair.of(1.0, 2.0), 60.0);
		map.put(DoublesPair.of(2.0, 1.0), 70.0);
		map.put(DoublesPair.of(2.0, 2.0), 80.0);
	}
	
	private static HashMap<DoublesPair, Double> expectedMap = new HashMap<>(); 
	static {
		expectedMap.put(DoublesPair.of(1.0, 1.0), -50.0);
		expectedMap.put(DoublesPair.of(1.0, 2.0), -60.0);
		expectedMap.put(DoublesPair.of(2.0, 1.0), -70.0);
		expectedMap.put(DoublesPair.of(2.0, 2.0), -80.0);
	}
	private static SurfaceValue expectedSurfaceValue = SurfaceValue.from(expectedMap);

	@Test
	public void shouldReverseDirection() {
		SurfaceValue surfaceValue = SurfaceValue.from(map);
		SABRSwaptionVegaStructure structure = new SABRSwaptionVegaStructure(expiries, tenors, surfaceValue);
		VegaMatrix vega = new VegaMatrix(structure, structure, structure, structure);
		
		System.out.println(vega.getAlpha().getVegaPoints().getMap().toString());
		VegaMatrix reversed = VegaMatrix.reverseDirection(vega);
		System.out.println(reversed.getAlpha().getVegaPoints().getMap().toString());
	}
	
}
