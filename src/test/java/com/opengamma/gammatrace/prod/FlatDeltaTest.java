package com.opengamma.gammatrace.prod;

import java.util.HashMap;

import org.junit.Test;
import org.testng.Assert;

public class FlatDeltaTest {
	private static final HashMap<String, Double> map = new HashMap<>();
	static {
		map.put("curve1", 1000.0);
		map.put("curve2", -5000.0);
	}

	private static final HashMap<String, Double> expectedMap = new HashMap<>();
	static {
		expectedMap.put("curve1", -1000.0);
		expectedMap.put("curve2", 5000.0);
	}
	private static final FlatDelta expected = new FlatDelta(expectedMap); 
	
	@Test
	public void shouldReverseDirection() {
		FlatDelta flatDelta = new FlatDelta(map);
		FlatDelta calculated = FlatDelta.reverseDirection(flatDelta);
		System.out.println(flatDelta.getMap().toString());
		System.out.println(calculated.getMap().toString());
		
		Assert.assertEquals(calculated, expected);
		
	}
}


