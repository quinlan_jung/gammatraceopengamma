package com.opengamma.gammatrace.rates.instrument;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.OpenGammaRuntimeException;
import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeFX;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborSpreadDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedONDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapXCcyIborIborDefinition;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.ogextension.SwapONSpreadIborSpreadDefinition;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapXCcyIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.timeseries.precise.zdt.ZonedDateTimeDoubleTimeSeries;
import com.opengamma.util.money.Currency;
import com.opengamma.util.time.DateUtils;

public class FixingBuilderTest {
	private static final Calendar CALENDAR = new MondayToFridayCalendar("CALENDAR");
	private static final GeneratorSwapFixedIborTemplate FIXED_IBOR_MASTER = GeneratorSwapFixedIborTemplate.getInstance();
	private static final GeneratorSwapIborIborTemplate IBOR_IBOR_MASTER = GeneratorSwapIborIborTemplate.getInstance();
	private static final GeneratorSwapFixedONTemplate FIXED_ON_MASTER = GeneratorSwapFixedONTemplate.getInstance();
	private static final GeneratorSwapXCcyIborIborTemplate XCCY_IBOR_IBOR_MASTER = GeneratorSwapXCcyIborIborTemplate.getInstance();
	private static final GeneratorSwapIborONTemplate IBOR_ON_MASTER = GeneratorSwapIborONTemplate.getInstance();
	
	private static final GeneratorSwapFixedIbor FIXED_IBOR_SWAP = FIXED_IBOR_MASTER.getGenerator(Generators.USD6MLIBOR3M, CALENDAR);
	private static final GeneratorSwapFixedON FIXED_ON_SWAP = FIXED_ON_MASTER.getGenerator(Generators.USD1YFEDFUND, CALENDAR);
	private static final GeneratorSwapXCcyIborIbor XCCY_IBOR_IBOR = XCCY_IBOR_IBOR_MASTER.getGenerator(Generators.GBP3MUSD3M, CALENDAR, CALENDAR);
	private static final GeneratorSwapIborON IBOR_ON_SWAP = IBOR_ON_MASTER.getGenerator(Generators.USD3MON, CALENDAR);
	
	private static final ZonedDateTime EXECUTION_TIMESTAMP = DateUtils.getUTCDate(2015, 2, 12);
	private static final ZonedDateTime EXECUTION_TIMESTAMP_2 = DateUtils.getUTCDate(2015, 2, 12, 12, 30);
	
	/**
	 * Test of the ibor coupon fixing date method
	 */
	@Test
	public void shouldContainThisDate() {
		ZonedDateTime expectedFixingDate = DateUtils.getUTCDate(2015, 2, 9, 0, 0);
		SwapFixedIborDefinition swapDefinition = SwapFixedIborDefinition.from(DateUtils.getUTCDate(2015,2,11), Period.ofYears(5), FIXED_IBOR_SWAP, 100,  0.03, true);
		ZonedDateTimeDoubleTimeSeries[] fixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, EXECUTION_TIMESTAMP);
		Swap<?, ?> derivative = swapDefinition.toDerivative(EXECUTION_TIMESTAMP, fixing);
		assertTrue("",fixing[0].containsTime(expectedFixingDate));
	}
	/**
	 * Test of the ibor coupon fixing date method when given non start of day times 
	 */
	@Test
	public void shouldContainThisDateTime() {
		ZonedDateTime expectedFixingDate = DateUtils.getUTCDate(2015, 2, 9, 0, 0);
		SwapFixedIborDefinition swapDefinition = SwapFixedIborDefinition.from(DateUtils.getUTCDate(2015, 2, 11, 2, 20), Period.ofYears(5), FIXED_IBOR_SWAP, 100,  0.03, true);
		ZonedDateTimeDoubleTimeSeries[] fixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, EXECUTION_TIMESTAMP_2);
		Swap<?, ?> derivative = swapDefinition.toDerivative(EXECUTION_TIMESTAMP_2, fixing);
		assertTrue("",fixing[0].containsTime(expectedFixingDate));
	}
	/**
	 * 
	 */
	@Test (expected = OpenGammaRuntimeException.class)
	public void shouldFailBecauseOfIborSpreadBugInOG() {			// bug in the opengamma code, in CouponIborSpread, we have toHour(0), but not toStartOfDay() or similar.
		ZonedDateTime expectedFixingDate = DateUtils.getUTCDate(2015, 2, 9, 0, 0);
		SwapFixedIborSpreadDefinition swapDefinition = SwapFixedIborSpreadDefinition.from(DateUtils.getUTCDate(2015, 2, 11, 2, 20), Period.ofYears(5), FIXED_IBOR_SWAP, 100.0,  0.03, 0.0, true, CALENDAR);
		ZonedDateTimeDoubleTimeSeries[] fixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, EXECUTION_TIMESTAMP_2);
		Swap<?, ?> derivative = swapDefinition.toDerivative(EXECUTION_TIMESTAMP_2, fixing);
		//assertTrue("",fixing[0].containsTime(expectedFixingDate));
	}
	/**
	 * Test of the ON fixing date method.
	 */
	@Test
	public void shouldContainTheseDatesON() {
		ZonedDateTime[] expectedFixingDate = {DateUtils.getUTCDate(2015, 2, 10, 0, 0), DateUtils.getUTCDate(2015, 2, 11, 0, 0)};
		SwapFixedONDefinition swapDefinition = SwapFixedONDefinition.from(DateUtils.getUTCDate(2015, 2, 10), Period.ofYears(5), 100, FIXED_ON_SWAP, 0.03, true);
		ZonedDateTimeDoubleTimeSeries[] fixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, EXECUTION_TIMESTAMP);
		Swap<?, ?> derivative = swapDefinition.toDerivative(EXECUTION_TIMESTAMP, fixing);
		assertTrue("",fixing[0].containsTime(expectedFixingDate[0]));
		assertTrue("",fixing[0].containsTime(expectedFixingDate[1]));
	}
	
	/**
	 * Test of the ONSpread fixing date method.
	 */
	@Test
	public void shouldContainTheseDatesONSpread() {
		ZonedDateTime[] expectedFixingDate = {DateUtils.getUTCDate(2015, 2, 10, 0, 0), DateUtils.getUTCDate(2015, 2, 11, 0, 0)};
		SwapONSpreadIborSpreadDefinition swapDefinition = SwapONSpreadIborSpreadDefinition.from(DateUtils.getUTCDate(2015, 2, 10), DateUtils.getUTCDate(2020, 2, 10), 100.0, 0.0, 0.0, IBOR_ON_SWAP, true);
		ZonedDateTimeDoubleTimeSeries[] fixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, EXECUTION_TIMESTAMP);
		Swap<?, ?> derivative = swapDefinition.toDerivative(EXECUTION_TIMESTAMP, fixing);
		assertTrue("",fixing[0].containsTime(expectedFixingDate[0]));
		assertTrue("",fixing[0].containsTime(expectedFixingDate[1]));
	}
	
	/**
	 * Test of the ibor fixing date method in xccy swap. 
	 */
	@Test
	public void xCcyFixingShouldContainThisDate() {
		ZonedDateTime expectedFixingDate = DateUtils.getUTCDate(2015, 2, 9, 0, 0);
		SwapXCcyIborIborDefinition swapDefinition = SwapXCcyIborIborDefinition.from(DateUtils.getUTCDate(2015, 2, 11, 0, 0), DateUtils.getUTCDate(2020, 2, 11, 0, 0), XCCY_IBOR_IBOR, 100, 150, 0, 0, true);
		ZonedDateTimeDoubleTimeSeries[] fixing = FixingBuilder.makeFixingTimeSeries(swapDefinition, EXECUTION_TIMESTAMP);
		
		Swap<?, ?> derivative = swapDefinition.toDerivative(EXECUTION_TIMESTAMP, fixing);
		assertTrue("",fixing[1].containsTime(expectedFixingDate)); // USD leg
		
		ZonedDateTime expectedFixingDate2 = DateUtils.getUTCDate(2015, 2, 11, 0, 0);
		SwapXCcyIborIborDefinition swapDefinition2 = XCCY_IBOR_IBOR.generateInstrument(DateUtils.getUTCDate(2015, 2, 11, 0, 0), 0.0, 100.0, new GeneratorAttributeFX(Period.ofYears(5), new FXMatrix(Currency.GBP, Currency.USD, 1.0)));
		ZonedDateTimeDoubleTimeSeries[] fixing2 = FixingBuilder.makeFixingTimeSeries(swapDefinition2, EXECUTION_TIMESTAMP);
		assertTrue("",fixing2[1].containsTime(expectedFixingDate2)); // USD leg
	}
}
