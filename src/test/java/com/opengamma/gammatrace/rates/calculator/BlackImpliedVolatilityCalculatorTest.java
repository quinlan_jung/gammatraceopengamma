package com.opengamma.gammatrace.rates.calculator;

import static org.junit.Assert.*;

import org.junit.Test;
import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swaption.SwaptionCashFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swaption.SwaptionPhysicalFixedIborDefinition;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionCashFixedIbor;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.analytics.financial.interestrate.swaption.provider.SwaptionPhysicalFixedIborBlackMethod;
import com.opengamma.analytics.financial.model.option.parameters.BlackFlatSwaptionParameters;
import com.opengamma.analytics.financial.provider.calculator.blackswaption.PresentValueBlackSwaptionCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.description.interestrate.BlackSwaptionFlatProvider;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.util.time.TimeCalculator;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.testdatasets.BlackDataSets;
import com.opengamma.gammatrace.testdatasets.MulticurveProviderDiscountDataSets;
import com.opengamma.util.money.Currency;
import com.opengamma.util.time.DateUtils;

public class BlackImpliedVolatilityCalculatorTest {
	private static final Calendar CALENDAR = new MondayToFridayCalendar("CALENDAR");
	private static final GeneratorSwapFixedIborTemplate FIXED_IBOR_MASTER = GeneratorSwapFixedIborTemplate.getInstance();
	private static final GeneratorSwapFixedIbor FIXED_IBOR_SWAP = FIXED_IBOR_MASTER.getGenerator(Generators.USD6MLIBOR3M, CALENDAR);
	private static final ZonedDateTime EXECUTION_TIMESTAMP = DateUtils.getUTCDate(2015, 2, 12);
	private static final ZonedDateTime EXPIRY = DateUtils.getUTCDate(2016, 2, 12);
	private static final ZonedDateTime EFFECTIVE = DateUtils.getUTCDate(2016, 2, 14);
	private static final ZonedDateTime MATURITY = DateUtils.getUTCDate(2026, 2, 14);
	private static final PresentValueDiscountingCalculator PVDC = PresentValueDiscountingCalculator.getInstance(); 
	private static final PresentValueBlackSwaptionCalculator PVBSC = PresentValueBlackSwaptionCalculator.getInstance();
	private static final double TOLERANCE = 0.00001;
	
	@Test
	public void singlePhysicalSwaption() {
		SwapFixedIborDefinition swapDefinition = SwapFixedIborDefinition.from(EFFECTIVE, Period.ofYears(10), FIXED_IBOR_SWAP, 100,  0.015, true);		// payer swap
		SwaptionPhysicalFixedIborDefinition swaptionDefinitionPayer = SwaptionPhysicalFixedIborDefinition.from(EXPIRY, swapDefinition, true);		// long payer swaption
		SwaptionPhysicalFixedIbor swaptionPayer = swaptionDefinitionPayer.toDerivative(EXECUTION_TIMESTAMP); 
		
		MulticurveProviderDiscount multicurves = MulticurveProviderDiscountDataSets.createMulticurveUsd();
		BlackFlatSwaptionParameters blackParameters = BlackDataSets.createBlackSwaptionUSD3();
		BlackSwaptionFlatProvider blackMulticurves = new BlackSwaptionFlatProvider(multicurves, blackParameters);
		
		double pv = swaptionPayer.accept(PVBSC, blackMulticurves).getAmount(Currency.USD);
		double expectedVol = blackParameters.getVolatility(TimeCalculator.getTimeBetween(EXECUTION_TIMESTAMP, EXPIRY), TimeCalculator.getTimeBetween(EXPIRY, MATURITY));
		double calculatedVol = BlackImpliedVolatilityCalculator.getImpliedVolatility(new SwaptionPhysicalFixedIbor[] {swaptionPayer}, pv, multicurves);		
		//System.out.println(pv+"  "+expectedVol+"  "+calculatedVol);
		assertEquals("", calculatedVol, expectedVol, TOLERANCE);
	}
	
	@Test
	public void physicalStraddle() {
		SwapFixedIborDefinition swapDefinitionPayer = SwapFixedIborDefinition.from(EFFECTIVE, Period.ofYears(10), FIXED_IBOR_SWAP, 100,  0.015, true);		// payer swap
		SwapFixedIborDefinition swapDefinitionReceiver = SwapFixedIborDefinition.from(EFFECTIVE, Period.ofYears(10), FIXED_IBOR_SWAP, 100,  0.015, true);		// payer swap
		SwaptionPhysicalFixedIborDefinition swaptionDefinitionPayer = SwaptionPhysicalFixedIborDefinition.from(EXPIRY, swapDefinitionPayer, true);		// long receiver swaption
		SwaptionPhysicalFixedIborDefinition swaptionDefinitionReceiver = SwaptionPhysicalFixedIborDefinition.from(EXPIRY, swapDefinitionReceiver, true);		// long receiver swaption
		SwaptionPhysicalFixedIbor swaptionPayer = swaptionDefinitionPayer.toDerivative(EXECUTION_TIMESTAMP);
		SwaptionPhysicalFixedIbor swaptionReceiver = swaptionDefinitionReceiver.toDerivative(EXECUTION_TIMESTAMP);
		
		MulticurveProviderDiscount multicurves = MulticurveProviderDiscountDataSets.createMulticurveUsd();
		BlackFlatSwaptionParameters blackParameters = BlackDataSets.createBlackSwaptionUSD3();
		BlackSwaptionFlatProvider blackMulticurves = new BlackSwaptionFlatProvider(multicurves, blackParameters);
		
		double pv1 = swaptionPayer.accept(PVBSC, blackMulticurves).getAmount(Currency.USD);
		double pv2 = swaptionReceiver.accept(PVBSC, blackMulticurves).getAmount(Currency.USD);
		double expectedVol = blackParameters.getVolatility(TimeCalculator.getTimeBetween(EXECUTION_TIMESTAMP, EXPIRY), TimeCalculator.getTimeBetween(EXPIRY, MATURITY));
		double calculatedVol = BlackImpliedVolatilityCalculator.getImpliedVolatility(new SwaptionPhysicalFixedIbor[] {swaptionPayer, swaptionReceiver}, pv1+pv2, multicurves);
		
		assertEquals("", calculatedVol, expectedVol, TOLERANCE);
	}
	
	@Test
	public void singleCashSwaption() {
		SwapFixedIborDefinition swapDefinition = SwapFixedIborDefinition.from(EFFECTIVE, Period.ofYears(10), FIXED_IBOR_SWAP, 100,  0.015, true);		// payer swap
		SwaptionCashFixedIborDefinition swaptionDefinitionPayer = SwaptionCashFixedIborDefinition.from(EXPIRY, swapDefinition, true);		// long payer swaption
		SwaptionCashFixedIbor swaptionPayer = swaptionDefinitionPayer.toDerivative(EXECUTION_TIMESTAMP); 
		
		MulticurveProviderDiscount multicurves = MulticurveProviderDiscountDataSets.createMulticurveUsd();
		BlackFlatSwaptionParameters blackParameters = BlackDataSets.createBlackSwaptionUSD3();
		BlackSwaptionFlatProvider blackMulticurves = new BlackSwaptionFlatProvider(multicurves, blackParameters);
		
		double pv = swaptionPayer.accept(PVBSC, blackMulticurves).getAmount(Currency.USD);
		double expectedVol = blackParameters.getVolatility(TimeCalculator.getTimeBetween(EXECUTION_TIMESTAMP, EXPIRY), TimeCalculator.getTimeBetween(EXPIRY, MATURITY));
		double calculatedVol = BlackImpliedVolatilityCalculator.getImpliedVolatility(new SwaptionCashFixedIbor[] {swaptionPayer}, pv, multicurves);
		
		assertEquals("", calculatedVol, expectedVol, TOLERANCE);
	}
	
}
