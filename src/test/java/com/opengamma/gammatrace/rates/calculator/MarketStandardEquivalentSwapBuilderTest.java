package com.opengamma.gammatrace.rates.calculator;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.testng.Assert;
import org.threeten.bp.Period;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapXCcyIborIborDefinition;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.gammatrace.exceptions.UnsupportedOperation;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapXCcyIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.rates.indextemplate.Indicies;
import com.opengamma.gammatrace.testdatasets.MulticurveProviderDiscountDataSets;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;

public class MarketStandardEquivalentSwapBuilderTest {
	private static final Calendar CALENDAR = new MondayToFridayCalendar("CALENDAR");
	
	private static final GeneratorSwapFixedIborTemplate FIXED_IBOR_MASTER = GeneratorSwapFixedIborTemplate.getInstance();
	private static final GeneratorSwapXCcyIborIborTemplate XCCY_IBOR_IBOR_MASTER = GeneratorSwapXCcyIborIborTemplate.getInstance();
	
	private static final IborTemplateByName IBOR_MASTER = IborTemplateByName.getInstance();
	private static final ZonedDateTime EXECUTION_TIMESTAMP = DateUtils.getUTCDate(2015, 2, 12, 0, 0);
	private static final ZonedDateTime TRADE_DATE = EXECUTION_TIMESTAMP.toLocalDate().atStartOfDay(ZoneOffset.UTC);
	private static final ZonedDateTime STANDARD_SETTLEMENT =  DateUtils.getUTCDate(2015, 2, 16, 0, 0);
	private static final ZonedDateTime EFFECTIVE = DateUtils.getUTCDate(2016, 2, 14);
	private static final ZonedDateTime MATURITY = DateUtils.getUTCDate(2026, 2, 14);
	private static final MulticurveProviderDiscount MULTICURVES = MulticurveProviderDiscountDataSets.createMulticurveUsd();
	private static final MulticurveProviderDiscount MULTICURVES_GBPUSD = MulticurveProviderDiscountDataSets.createMulticurveGbpUsd();
	private static final MulticurveProviderDiscount MULTICURVES_EURUSD = MulticurveProviderDiscountDataSets.createMulticurveEurUsd();
	
	private static final DayCount ACT_ACT = DayCountFactory.of("ACT/ACT");
	private static final DayCount ACT_360 = DayCountFactory.of("ACT/ACT");
	private static final BusinessDayConvention MOD_FOLL = BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following");
	
	/**
	 * A spot starting swap with 1 month payments with Act/Act vs. 3m Libor paid every 6m with Act/Act should map to USD6MLIBOR3M standard swap.
	 */
	@Test
	public void shouldBuildUSD6MLIBOR3MSwap() {
		try {
			IborIndex USDLIBOR3M = IBOR_MASTER.getIndex(Indicies.USDLIBOR3M);
			SwapFixedIborDefinition strangeSwapDefinition = SwapFixedIborDefinition.from(EFFECTIVE, MATURITY, Period.ofMonths(1), ACT_ACT, MOD_FOLL, true, 100, 0.03, 
					Period.ofMonths(6), ACT_ACT, MOD_FOLL, true, 100, USDLIBOR3M, true, CALENDAR);
			Swap<?, ?> strangeSwap = strangeSwapDefinition.toDerivative(EXECUTION_TIMESTAMP);
			SwapInstrument swapInstrument = new SwapInstrument(strangeSwap, TRADE_DATE, EFFECTIVE, MATURITY, CurrencyAmount.of(Currency.USD, 100), CurrencyAmount.of(Currency.USD, 100), 
				TRADE_DATE, MultipleCurrencyAmount.of(Currency.USD, 0), InstrumentDescription.FIXED_IBOR_SWAP, Pair.of("FIXED", "USDLIBOR3M"));
			
			GeneratorSwapFixedIbor expectedGenerator = FIXED_IBOR_MASTER.getGenerator(Generators.USD6MLIBOR3M, CALENDAR);
			SwapFixedIborDefinition expectedSwapDefinitionForward = SwapFixedIborDefinition.from(EFFECTIVE, MATURITY, expectedGenerator, 100, 0.0, true);
			Swap<?, ?> expectedSwapForward = expectedSwapDefinitionForward.toDerivative(TRADE_DATE);
			SwapFixedIborDefinition expectedSwapDefinitionShort = SwapFixedIborDefinition.from(STANDARD_SETTLEMENT, EFFECTIVE, expectedGenerator, 100, 0.0, false);
			Swap<?, ?> expectedSwapShort = expectedSwapDefinitionShort.toDerivative(TRADE_DATE);
			SwapFixedIborDefinition expectedSwapDefinitionLong = SwapFixedIborDefinition.from(STANDARD_SETTLEMENT, MATURITY, expectedGenerator, 100, 0.0, true);
			Swap<?, ?> expectedSwapLong = expectedSwapDefinitionLong.toDerivative(TRADE_DATE);
			
			List<Swap<?, ?>> list = new MarketStandardEquivalentSwapBuilder(swapInstrument, MULTICURVES).makeMarketStandardEquivalent();	
			assertEquals(expectedSwapForward, list.get(0));
			assertEquals(expectedSwapLong, list.get(1));
			assertEquals(expectedSwapShort, list.get(2));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	/**
	 * Test for XCcy swap. Correct dates, switched notionals, correct fx rate.
	 */
	@Test
	public void shouldBuildGBP3MUSD3MSwap() {
		try {
			IborIndex GBPLIBOR3M = IBOR_MASTER.getIndex(Indicies.GBPLIBOR3M);
			IborIndex USDLIBOR3M = IBOR_MASTER.getIndex(Indicies.USDLIBOR3M); 
			GeneratorSwapXCcyIborIbor generator = new GeneratorSwapXCcyIborIbor("strangeGenerator", USDLIBOR3M, GBPLIBOR3M, MOD_FOLL, true, 2, CALENDAR, CALENDAR);
			SwapXCcyIborIborDefinition swapDefinition = SwapXCcyIborIborDefinition.from(EFFECTIVE, MATURITY,generator, 140, 100, 0.0, 0.001, true);
			Swap<?, ?> swap = swapDefinition.toDerivative(TRADE_DATE);
			SwapInstrument swapInstrument = new SwapInstrument(swap, TRADE_DATE, EFFECTIVE, MATURITY, CurrencyAmount.of(Currency.USD, 140), CurrencyAmount.of(Currency.GBP, 100), 
					TRADE_DATE, MultipleCurrencyAmount.of(Currency.USD, 0), InstrumentDescription.XCCY_IBOR_IBOR_SWAP, Pair.of("USDLIBOR3M", "GBPLIBOR3M"));
			
			GeneratorSwapXCcyIborIbor expectedGenerator = XCCY_IBOR_IBOR_MASTER.getGenerator(Generators.GBP3MUSD3M, CALENDAR, CALENDAR);
			SwapXCcyIborIborDefinition expectedSwapDefinitionForward = SwapXCcyIborIborDefinition.from(EFFECTIVE, MATURITY, expectedGenerator, 100, 150, 0.0, 0.0, true);
			Swap<?, ?> expectedSwapForward = expectedSwapDefinitionForward.toDerivative(TRADE_DATE);
			SwapXCcyIborIborDefinition expectedSwapDefinitionShort = SwapXCcyIborIborDefinition.from(STANDARD_SETTLEMENT, EFFECTIVE, expectedGenerator, 100, 150, 0.0, 0.0, false);
			Swap<?, ?> expectedSwapShort = expectedSwapDefinitionShort.toDerivative(TRADE_DATE);
			SwapXCcyIborIborDefinition expectedSwapDefinitionLong = SwapXCcyIborIborDefinition.from(STANDARD_SETTLEMENT, MATURITY, expectedGenerator, 100, 150, 0.0, 0.0, true);
			Swap<?, ?> expectedSwapLong = expectedSwapDefinitionLong.toDerivative(TRADE_DATE);
			
			
			List<Swap<?, ?>> list = new MarketStandardEquivalentSwapBuilder(swapInstrument, MULTICURVES_GBPUSD).makeMarketStandardEquivalent();
			assertEquals(expectedSwapForward, list.get(0));
			assertEquals(expectedSwapLong, list.get(1));
			assertEquals(expectedSwapShort, list.get(2));
		} catch (Exception e){
			e.printStackTrace();
			fail();
		}
	}
}
