package com.opengamma.gammatrace.rates.calculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.index.IborIndex;
import com.opengamma.analytics.financial.instrument.swap.SwapXCcyIborIborDefinition;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueMarketQuoteSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.financial.convention.businessday.BusinessDayConvention;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.financial.convention.daycount.DayCount;
import com.opengamma.financial.convention.daycount.DayCountFactory;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapXCcyIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.rates.indextemplate.IborTemplateByName;
import com.opengamma.gammatrace.testdatasets.MulticurveProviderDiscountDataSets;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;

public class MarketStandardFairFixedRateCalculatorTest {
	private static final Calendar CALENDAR = new MondayToFridayCalendar("CALENDAR");
	
	private static final GeneratorSwapFixedIborTemplate FIXED_IBOR_MASTER = GeneratorSwapFixedIborTemplate.getInstance();
	private static final GeneratorSwapXCcyIborIborTemplate XCCY_IBOR_IBOR_MASTER = GeneratorSwapXCcyIborIborTemplate.getInstance();
	
	private static final IborTemplateByName IBOR_MASTER = IborTemplateByName.getInstance();
	private static final ZonedDateTime EXECUTION_TIMESTAMP = DateUtils.getUTCDate(2015, 2, 12, 0, 0);
	private static final ZonedDateTime TRADE_DATE = EXECUTION_TIMESTAMP.toLocalDate().atStartOfDay(ZoneOffset.UTC);
	private static final ZonedDateTime STANDARD_SETTLEMENT =  DateUtils.getUTCDate(2015, 2, 16, 0, 0);
	private static final ZonedDateTime EFFECTIVE = DateUtils.getUTCDate(2016, 2, 14);
	private static final ZonedDateTime MATURITY = DateUtils.getUTCDate(2026, 2, 14);
	private static final MulticurveProviderDiscount MULTICURVES = MulticurveProviderDiscountDataSets.createMulticurveUsd();
	private static final MulticurveProviderDiscount MULTICURVES_GBPUSD = MulticurveProviderDiscountDataSets.createMulticurveGbpUsd();
	private static final MulticurveProviderDiscount MULTICURVES_EURUSD = MulticurveProviderDiscountDataSets.createMulticurveEurUsd();
	
	private static final DayCount ACT_ACT = DayCountFactory.of("ACT/ACT");
	private static final DayCount ACT_360 = DayCountFactory.of("ACT/ACT");
	private static final BusinessDayConvention MOD_FOLL = BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following");
	
	private static final PresentValueDiscountingCalculator PVDC = PresentValueDiscountingCalculator.getInstance();
	private static final PresentValueMarketQuoteSensitivityDiscountingCalculator PVMQSC = PresentValueMarketQuoteSensitivityDiscountingCalculator.getInstance();
	private static final ParSpreadMarketQuoteDiscountingCalculator PSMQC = ParSpreadMarketQuoteDiscountingCalculator.getInstance();
	
	
	/**
	 * Test to show that the pv effect of FX in XCcy basis swap is small (for trades with initial and final exchange). 
	 */
	@Test
	public void testA() {
		try {
			GeneratorSwapXCcyIborIbor expectedGenerator = XCCY_IBOR_IBOR_MASTER.getGenerator(Generators.GBP3MUSD3M, CALENDAR, CALENDAR);
			SwapXCcyIborIborDefinition expectedSwapDefinitionForward = SwapXCcyIborIborDefinition.from(EFFECTIVE, MATURITY, expectedGenerator, 100, 99 * MULTICURVES_GBPUSD.getFxRate(Currency.GBP, Currency.USD), 
					-0.005765695039856123, 0.0, true);
			Swap<?, ?> expectedSwapForward = expectedSwapDefinitionForward.toDerivative(TRADE_DATE);
			double rate = expectedSwapForward.accept(PSMQC, MULTICURVES_GBPUSD);
			double pv = -MULTICURVES_GBPUSD.getFxRates().convert(expectedSwapForward.accept(PVDC, MULTICURVES_GBPUSD), expectedSwapForward.getFirstLeg().getCurrency()).getAmount();
			Assert.assertTrue(pv < 0.01 && pv > -0.01);		// illustration that the PV effect from a 1% shift in FX of trade is small
		} catch (Exception e){
			e.printStackTrace();
			fail();
		}
	}
	
	
}
