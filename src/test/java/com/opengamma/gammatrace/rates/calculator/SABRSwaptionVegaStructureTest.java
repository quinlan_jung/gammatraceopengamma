package com.opengamma.gammatrace.rates.calculator;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.opengamma.analytics.util.amount.SurfaceValue;
import com.opengamma.util.tuple.DoublesPair;

public class SABRSwaptionVegaStructureTest {

	private static Double[] expiries = new Double[] {1.0 , 2.0};
	private static Double[] tenors = new Double[] {1.0 , 2.0};
	private static HashMap<DoublesPair, Double> map = new HashMap<>(); 
	static {
		map.put(DoublesPair.of(1.0, 1.0), 50.0);
		map.put(DoublesPair.of(1.0, 2.0), 60.0);
		map.put(DoublesPair.of(2.0, 1.0), 70.0);
		map.put(DoublesPair.of(2.0, 2.0), 80.0);
	}
	
	private static HashMap<DoublesPair, Double> expectedMap = new HashMap<>(); 
	static {
		expectedMap.put(DoublesPair.of(1.0, 1.0), -50.0);
		expectedMap.put(DoublesPair.of(1.0, 2.0), -60.0);
		expectedMap.put(DoublesPair.of(2.0, 1.0), -70.0);
		expectedMap.put(DoublesPair.of(2.0, 2.0), -80.0);
	}
	private static SurfaceValue expectedSurfaceValue = SurfaceValue.from(expectedMap);
	
	@Test
	public void shouldReverseSigns() {
		SurfaceValue surfaceValue = SurfaceValue.from(map);
		SABRSwaptionVegaStructure structure = new SABRSwaptionVegaStructure(expiries, tenors, surfaceValue);
		SABRSwaptionVegaStructure reversed = SABRSwaptionVegaStructure.reverseDirection(structure);
		System.out.println(surfaceValue.getMap().toString());
		System.out.println(reversed.getVegaPoints().getMap().toString());
		Assert.assertEquals(expectedSurfaceValue, reversed.getVegaPoints());
	}
	
}
