package com.opengamma.gammatrace.rates.calculator;

import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.testng.Assert;
import org.threeten.bp.Period;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.provider.calculator.discounting.PV01CurveParametersCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParRateDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.generic.MarketQuoteSensitivityBlockCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderInterface;
import com.opengamma.analytics.financial.provider.sensitivity.parameter.ParameterSensitivityParameterCalculator;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.exceptions.UnsupportedTradeException;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.testdatasets.ConfigurationDataSet;
import com.opengamma.gammatrace.testdatasets.MulticurveProviderDiscountDataSets;
import com.opengamma.util.money.Currency;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;

public class DeltaRiskCalculatorTest {
	private static final Calendar CALENDAR = new MondayToFridayCalendar("CALENDAR");
	
	private static final GeneratorSwapFixedIborTemplate FIXED_IBOR_MASTER = GeneratorSwapFixedIborTemplate.getInstance();
	private static final GeneratorSwapFixedIbor FIXED_IBOR_SWAP = FIXED_IBOR_MASTER.getGenerator(Generators.USD6MLIBOR3M, CALENDAR);
	private static final ZonedDateTime EXECUTION_TIMESTAMP = DateUtils.getUTCDate(2015, 2, 12, 0, 0);
	private static final ZonedDateTime TRADE_DATE = EXECUTION_TIMESTAMP.toLocalDate().atStartOfDay(ZoneOffset.UTC);
	private static final ZonedDateTime EFFECTIVE = DateUtils.getUTCDate(2016, 2, 14);
	private static final ZonedDateTime MATURITY = DateUtils.getUTCDate(2026, 2, 14);
	
	private static final MulticurveProviderDiscount MULTICURVES = MulticurveProviderDiscountDataSets.createMulticurveUsd();
	private static final Map<String, Double> DATA = ConfigurationDataSet.getUSDData();
	
	private static final PresentValueCurveSensitivityDiscountingCalculator PVCSC = PresentValueCurveSensitivityDiscountingCalculator.getInstance();
	private static final PresentValueDiscountingCalculator PVDC = PresentValueDiscountingCalculator.getInstance();
	private static final PV01CurveParametersCalculator<MulticurveProviderInterface> PV01CPC = new PV01CurveParametersCalculator<>(PVCSC);
	private static final ParameterSensitivityParameterCalculator<MulticurveProviderInterface> PSC = new ParameterSensitivityParameterCalculator<>(PVCSC);
	private static final MarketQuoteSensitivityBlockCalculator<MulticurveProviderInterface> MQSC = new MarketQuoteSensitivityBlockCalculator<>(PSC);
	
	@Test 
	@Ignore
	public void shouldEqualPV01Calculator() throws Exception {
		SwapFixedIborDefinition swapDefinition = SwapFixedIborDefinition.from(EFFECTIVE, Period.ofYears(10), FIXED_IBOR_SWAP, 100,  0.015, true);		// payer swap
		Swap<?,?> swap = swapDefinition.toDerivative(TRADE_DATE);
		CurveMaker cm = new CurveMaker(Currency.USD, EXECUTION_TIMESTAMP, DATA);
		Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> pair = cm.makeCurve();
		DeltaRiskCalculator DRC = new DeltaRiskCalculator(swap, pair.getFirst(), pair.getSecond(), cm.getDeltaDisplayBundle());
		
		Assert.assertEquals(DRC.getFlatDelta(), swap.accept(PV01CPC, pair.getFirst()));
		
		System.out.println(DRC.getFlatDelta().getMap());
		System.out.println(swap.accept(PV01CPC, pair.getFirst()).getMap());
			
	}
	@Test
	public void shouldEqualMQSCCalculator() throws UnsupportedTradeException {
		SwapFixedIborDefinition swapDefinition = SwapFixedIborDefinition.from(EFFECTIVE, Period.ofYears(10), FIXED_IBOR_SWAP, 100,  0.015, true);		// payer swap
		Swap<?,?> swap = swapDefinition.toDerivative(TRADE_DATE);
		CurveMaker cm = new CurveMaker(Currency.USD, EXECUTION_TIMESTAMP, DATA);
		Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> pair = cm.makeCurve();
		DeltaRiskCalculator DRC = new DeltaRiskCalculator(swap, pair.getFirst(), pair.getSecond(), cm.getDeltaDisplayBundle());
		
		System.out.println(DRC.extractFixedDelta().getCurveDeltas().get(0).getDeltaValues().toString());
		System.out.println(MQSC.fromInstrument(swap, pair.getFirst(), pair.getSecond()));
	}
}
