package com.opengamma.gammatrace.rates.calculator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedIbor;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swaption.SwaptionCashFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swaption.SwaptionPhysicalFixedIborDefinition;
import com.opengamma.analytics.financial.interestrate.payments.derivative.Coupon;
import com.opengamma.analytics.financial.interestrate.swap.derivative.SwapFixedCoupon;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionCashFixedIbor;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.analytics.financial.model.option.definition.SABRInterestRateParameters;
import com.opengamma.analytics.financial.provider.calculator.blackswaption.PresentValueBlackSwaptionCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParRateDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.sabrswaption.PresentValueSABRSwaptionCalculator;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.SABRSwaptionProviderDiscount;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwaptionInstrument;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.testdatasets.MulticurveProviderDiscountDataSets;
import com.opengamma.gammatrace.testdatasets.SABRDataSets;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.CurrencyAmount;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;
// TODO: this isn't a good test, bc we're testing against the trade implied vol calculator, which is tested against the getVolatility method i.e. circular. Should really calibrate a SABR vol from Black, and assert equal to the input
public class SABRMarketImpliedBlackVolatilityCalculatorTest {
	private static final Calendar CALENDAR = new MondayToFridayCalendar("CALENDAR");
	private static final GeneratorSwapFixedIborTemplate FIXED_IBOR_MASTER = GeneratorSwapFixedIborTemplate.getInstance();
	private static final GeneratorSwapFixedIbor FIXED_IBOR_SWAP = FIXED_IBOR_MASTER.getGenerator(Generators.USD6MLIBOR3M, CALENDAR);
	private static final ZonedDateTime EXECUTION_TIMESTAMP = DateUtils.getUTCDate(2015, 2, 12);
	private static final ZonedDateTime EXPIRY = DateUtils.getUTCDate(2016, 2, 12);
	private static final ZonedDateTime EFFECTIVE = DateUtils.getUTCDate(2016, 2, 14);
	private static final ZonedDateTime MATURITY = DateUtils.getUTCDate(2026, 2, 14);
	private static final PresentValueDiscountingCalculator PVDC = PresentValueDiscountingCalculator.getInstance(); 
	private static final PresentValueBlackSwaptionCalculator PVBSC = PresentValueBlackSwaptionCalculator.getInstance();
	private static final PresentValueSABRSwaptionCalculator PVSSC = PresentValueSABRSwaptionCalculator.getInstance();
	private static final ParRateDiscountingCalculator PRDC = ParRateDiscountingCalculator.getInstance();
	
	private static final double TOLERANCE = 0.00001;
	
	@Test
	public void singlePhysicalSwaption() {
		double fixedRate = 0.01507480311;
		double notional = 100;
		SwapFixedIborDefinition swapDefinition = SwapFixedIborDefinition.from(EFFECTIVE, Period.ofYears(10), FIXED_IBOR_SWAP, notional, fixedRate , true);		// payer swap
		SwaptionPhysicalFixedIborDefinition swaptionDefinitionPayer = SwaptionPhysicalFixedIborDefinition.from(EXPIRY, swapDefinition, true);		// long payer swaption
		SwapFixedCoupon<Coupon> swap = swapDefinition.toDerivative(EXECUTION_TIMESTAMP);
		SwaptionPhysicalFixedIbor swaptionPayer = swaptionDefinitionPayer.toDerivative(EXECUTION_TIMESTAMP); 
		
		MulticurveProviderDiscount multicurves = MulticurveProviderDiscountDataSets.createMulticurveUsd();
		SABRInterestRateParameters sabrParameters = SABRDataSets.createSABR1();
		SABRSwaptionProviderDiscount sabrMulticurves = new SABRSwaptionProviderDiscount(multicurves, sabrParameters, FIXED_IBOR_SWAP);
		
		Pair<String, String> assetPair = Pair.of("Fixed", "USDLIBOR3M");
		double pv = swaptionPayer.accept(PVSSC, sabrMulticurves).getAmount(Currency.USD);
		SwaptionInstrument instrument = new SwaptionInstrument(new SwaptionPhysicalFixedIbor[] {swaptionPayer}, EXECUTION_TIMESTAMP, pv, EFFECTIVE, EXPIRY, MATURITY, CurrencyAmount.of(Currency.USD, notional), 
				CurrencyAmount.of(Currency.USD, notional), EXECUTION_TIMESTAMP, MultipleCurrencyAmount.of(Currency.USD, 0.0), InstrumentDescription.FIXED_IBOR_SWAPTION, assetPair);
		
		double calculatedVol = SABRMarketImpliedBlackVolatilityCalculator.getSABRMarketImpliedBlackVolatility(instrument, sabrMulticurves);		
		double forward = swapDefinition.toDerivative(EXECUTION_TIMESTAMP).accept(PRDC, multicurves);
		double pvSwap = swap.accept(PVDC, multicurves).getAmount(Currency.USD);
		
		double expectedVol = BlackImpliedVolatilityCalculator.getImpliedVolatility(new SwaptionPhysicalFixedIbor[] {swaptionPayer}, pv, multicurves);
		//System.out.println(forward+" "+pvSwap+"  "+calculatedVol+" "+expectedVol);
		assertEquals("", calculatedVol, expectedVol, TOLERANCE);
	}
	
	@Test
	public void SingleCashSwaption() {
		double fixedRate = 0.015;
		double notional = 100;
		SwapFixedIborDefinition swapDefinition = SwapFixedIborDefinition.from(EFFECTIVE, Period.ofYears(10), FIXED_IBOR_SWAP, notional, fixedRate , true);		// payer swap
		SwaptionCashFixedIborDefinition swaptionDefinitionPayer = SwaptionCashFixedIborDefinition.from(EXPIRY, swapDefinition, true);		// long payer swaption
		SwapFixedCoupon<Coupon> swap = swapDefinition.toDerivative(EXECUTION_TIMESTAMP);
		SwaptionCashFixedIbor swaptionPayer = swaptionDefinitionPayer.toDerivative(EXECUTION_TIMESTAMP);
		
		MulticurveProviderDiscount multicurves = MulticurveProviderDiscountDataSets.createMulticurveUsd();
		SABRInterestRateParameters sabrParameters = SABRDataSets.createSABR1();
		SABRSwaptionProviderDiscount sabrMulticurves = new SABRSwaptionProviderDiscount(multicurves, sabrParameters, FIXED_IBOR_SWAP);
		
		Pair<String, String> assetPair = Pair.of("Fixed", "USDLIBOR3M");
		double pv = swaptionPayer.accept(PVSSC, sabrMulticurves).getAmount(Currency.USD);
		SwaptionInstrument instrument = new SwaptionInstrument(new SwaptionCashFixedIbor[] {swaptionPayer}, EXECUTION_TIMESTAMP, pv, EFFECTIVE, EXPIRY, MATURITY, CurrencyAmount.of(Currency.USD, notional), 
				CurrencyAmount.of(Currency.USD, notional), EXECUTION_TIMESTAMP, MultipleCurrencyAmount.of(Currency.USD, 0.0), InstrumentDescription.FIXED_IBOR_SWAPTION, assetPair);
		
		double calculatedVol = SABRMarketImpliedBlackVolatilityCalculator.getSABRMarketImpliedBlackVolatility(instrument, sabrMulticurves);		
		double forward = swapDefinition.toDerivative(EXECUTION_TIMESTAMP).accept(PRDC, multicurves);
		double pvSwap = swap.accept(PVDC, multicurves).getAmount(Currency.USD);
		
		double expectedVol = BlackImpliedVolatilityCalculator.getImpliedVolatility(new SwaptionCashFixedIbor[] {swaptionPayer}, pv, multicurves);
		System.out.println(forward+" "+pvSwap+"  "+calculatedVol+" "+expectedVol);
		assertEquals("", calculatedVol, expectedVol, TOLERANCE);
			
	}
	
	
}
