package com.opengamma.gammatrace.ogextension;

import org.junit.Test;
import org.testng.Assert;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.instrument.index.GeneratorSwapIborON;
import com.opengamma.gammatrace.ogextension.CouponONSpreadDefinition;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.util.time.TimeCalculator;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapIborONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.testdatasets.MulticurveProviderDiscountDataSets;
import com.opengamma.util.money.Currency;
import com.opengamma.util.time.DateUtils;

public class PresentValueMarketQuoteSensitivityDiscountingCalculatorExtendedTest {
	private static final Calendar CALENDAR = new MondayToFridayCalendar("CALENDAR");
	private static final GeneratorSwapIborONTemplate IBOR_ON_MASTER = GeneratorSwapIborONTemplate.getInstance();
	private static final GeneratorSwapIborON USD3MON = IBOR_ON_MASTER.getGenerator(Generators.USD3MON, CALENDAR);
	
	private static final ZonedDateTime EXECUTION_TIMESTAMP = DateUtils.getUTCDate(2015, 2, 12);
	private static final ZonedDateTime EFFECTIVE = DateUtils.getUTCDate(2016, 2, 14);
	private static final ZonedDateTime MATURITY = DateUtils.getUTCDate(2026, 2, 14);
	
	private static final MulticurveProviderDiscount MULTICURVES = MulticurveProviderDiscountDataSets.createMulticurveUsd();
	
	private static final PresentValueMarketQuoteSensitivityDiscountingCalculatorExtended PVMQDC = PresentValueMarketQuoteSensitivityDiscountingCalculatorExtended.getInstance(); 
	
	@Test
	public void presentValueBasisPointCouponONSpreadSwap() {
		SwapONSpreadIborSpreadDefinition swapDefinition = SwapONSpreadIborSpreadDefinition.from(EFFECTIVE, MATURITY, 100, 0.0, 0.0, USD3MON, false);
		Swap<?, ?> swap = swapDefinition.toDerivative(EXECUTION_TIMESTAMP);
		double pvbp = 0;
		for (CouponONSpreadDefinition coupon : swapDefinition.getOISLeg().getPayments()) {
			double paymentTime = TimeCalculator.getTimeBetween(EXECUTION_TIMESTAMP, coupon.getPaymentDate());
			pvbp += MULTICURVES.getDiscountFactor(Currency.USD, paymentTime) * coupon.getPaymentYearFraction() * coupon.getNotional();
		}
		Assert.assertEquals(swap.accept(PVMQDC, MULTICURVES), pvbp);
		//System.out.println(swap.accept(PVMQDC, MULTICURVES)+"   "+pvbp);
	}
}
